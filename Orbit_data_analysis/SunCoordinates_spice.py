
import os, csv, sys, time
import pandas as pd
from datetime import datetime, timedelta
path_ = os.path.abspath('Orbit_data_analysis/utilities/')
print(path_)
sys.path.append('{}'.format(path_))
import spiceypy as spice
import AstroConstants as AC




def suncoordinates(mission):
    """
    This functions uses NAIF SPICE data to compute the
    Sun's  Ra [deg] and Dec [deg], given an initial
    data in UTC and the propagation infos
    Times are obtained from the propagation file:
    *mission_name*/Output/S1_P1.csv
    
    INPUT:
        date = initial time [UTC]
        time_step = simulation time step [sec]
        path_out = output .csv file path
        mission = mission's name [str]
    
    """

    print('\nStaring to write SUN-POSITIONS file...')
    t = time.time()
    # do stuff
    date_format_str1 = '%Y-%m-%d %H:%M:%S'

    # SPICE options
    spice.furnsh(os.path.abspath('./SPICE_experiments/kernel/MetaK.txt'))
    targ = "SUN"
    ref = "J2000"
    obs = "EARTH"
    #Users current lat and lon
    lat = 0
    lon = 0

    data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TIMES_{}.csv".format(mission, mission))) # read the ephemeris file to use for analysis
    # Recovering data from the original single satellite file:
    elapsed_times = data['ElapsedTime'][:] # elapsed seconds [sec]
    gps_sec = data['GPSsec'][:] # gps seconds [sec]
    utc_times = data['UTCtime'][:] # utc times [sec]

    # Compute the Sun positions in ECI:
    with open(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/SUNPOS_{}.csv".format(mission,mission)), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['ElapsedTime', 'UTCtime', 'SUN_ra', 'SUN_dec', 'SUN_dist'])
        for kk in range(0, len(gps_sec)):
            ddd = datetime.strptime(utc_times[kk], date_format_str1)
            # 1. get et values:
            et = spice.str2et(str(ddd))
            # 2. run spkpos as a vectorized function:
            positions, _ = spice.spkpos(targ, et, ref, "NONE", obs)
            # 3. get right ascension, declination, and distance:
            dist, ra, dec = spice.recrad(positions) # [km , rad , rad]

            writer.writerow([elapsed_times[kk], utc_times[kk], ra/AC.D2R, dec/AC.D2R, dist])

    # Printing when completed:
    elapsed = time.time() - t
    print('SUN-POSITIONS file written!')
    print('[time taken: {} sec]'.format(round(elapsed, 4)))
    spice.kclear()
    return





def time_convs(date, mission):
    """
    Time translation file creation
    Times are obtained from the propagation file:
    *mission_name*/Output/S1_P1.csv

    INPUT:
        date = initial date [%d-%m-%Y %H:%M:%S]
        mission = mission name [str]
        
    """
    print('\nStaring to write TIMES file...')
    t = time.time()

    data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/Ephemeris/S1_P1.csv".format(mission))) # read the ephemeris file to use for analysis
    # Recovering data from the original single satellite file:
    elapsed_times = data['ElapsedTime'][:] # elapsed seconds [sec]
    gps_sec = data['GPSsec'][:] # gps seconds [sec]

    date_format_str1 = '%Y-%m-%d %H:%M:%S'
    d1 = datetime.strptime(date, date_format_str1)

    with open(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TIMES_{}.csv".format(mission,mission)), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['ElapsedTime', 'GPSsec', 'UTCtime'])
        for kk in range(0, len(elapsed_times)):
            date = d1 + timedelta(seconds=elapsed_times[kk])
            writer.writerow([elapsed_times[kk], gps_sec[kk], date])
            #writer.writerow([elapsed_times[kk], 0, date])

    elapsed = time.time() - t
    print('TIMES file written!')
    print('[time taken: {} sec]'.format(round(elapsed, 4)))
    spice.kclear()
    return



if __name__ == "__main__":

    mission = 'Test'
    date0 = '2020-05-01 12:00:00.0'
    time_convs(date0, mission)
    suncoordinates(mission)