# -----------------------
# EARTH ROTATION WRT ECI
# -----------------------
# This script inspects the Earth relative rotation in quaternions with respect to the ECI RF
# Reference:
# https://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 22/03/2022
#
#---------------------------------------------------

import astropy
from astropy.time import Time
from astropy import coordinates
from astropy.time import Time
from astropy.coordinates import solar_system_ephemeris, EarthLocation
import os, csv, math
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import utilities.CarKep as C2K
import utilities.AstroConstants as AC
import utilities.spaceConv_utilities as MF



# sim = 'Sim3'

# #--------------- PLEIADES -----------------
# if sim == 'Sim1':
#     sim_timeStep = 10 # [sec]
#     data= pd.read_csv("DATA/Sim1/S1-P1_ephemeris.csv")
# elif sim == 'Sim2':
#     sim_timeStep = 5 # [sec]
#     data= pd.read_csv("DATA/Sim2/S1-P1_ephemeris.csv")
# elif sim == 'Sim3':
#     sim_timeStep = 10 # [sec]
#     data= pd.read_csv("DATA/Sim3/S1-P1_ephemeris.csv")

# # Recovering data from the original single satellite file:
# elapsed_times = data['T'][:] # elapsed seconds [sec]

# orbit_period = 6179.918
# n_sats = 4 # numb satellites
# phasing = 1/n_sats 
# orbit_phasing = round(phasing * orbit_period) # [sec]

# secSat = [0, 0, 0, 0]
# for i in range(1, n_sats+1):
#     secSat[i-1] = int(round((orbit_phasing * i) / sim_timeStep)) # index when sats start

# # Computation of initial siderial time for Greenwich:
# t = Time('2012-12-02 12:00:00', scale='utc', location=('0d', '0d'))
# sT_ha=t.sidereal_time('mean', 'greenwich')  # Greenwich mean siderial time [hourangle]
# sT_ha = sT_ha.hour * 3600 #hour angle in [sec]

# start1 = 0
# end1 = len(elapsed_times)-secSat[3]

# # with open('DATA/Sim1/EQUAT_PL.csv', 'w', encoding='UTF8', newline='') as f:
# # with open('DATA/Sim2/EQUAT_PL.csv', 'w', encoding='UTF8', newline='') as f:
# with open('DATA/Sim3/EQUAT_PL.csv', 'w', encoding='UTF8', newline='') as f:

#     writer = csv.writer(f)
#     writer.writerow(['ElapsedTime', 'qw', 'qx', 'qy', 'qz'])
#     theta_g0 = AC.Earth_Rot * sT_ha # initial location in siderial time of 0-meridian
#     for kk in range(start1, end1):
#         theta = theta_g0 + (AC.Earth_Rot * elapsed_times[kk])
#         qw, qx, qy, qz = MF.Matrix2Quaternions(math.cos(theta), math.sin(theta), 0, -math.sin(theta), math.cos(theta), 0, 0, 0, 1)
#         writer.writerow([elapsed_times[kk], qw, qx, qy, qz])


# #--------------- ENVISAT -----------------


# if sim == 'Sim1':
#     sim_timeStep = 10 # [sec]
#     data= pd.read_csv("DATA/Sim1/ES1_P1.csv")
# elif sim == 'Sim2':
#     sim_timeStep = 5 # [sec]
#     data= pd.read_csv("DATA/Sim2/ES1_P1.csv")
# elif sim == 'Sim3':
#     sim_timeStep = 10 # [sec]
#     data= pd.read_csv("DATA/Sim3/ES1_P1.csv")


# # Recovering data from the original single satellite file:
# elapsed_times = data['T'][:] # elapsed seconds [sec]

# # Computation of initial siderial time for Greenwich:
# t = Time('2002-03-01 02:53:40', scale='utc', location=('0d', '0d'))
# sT_ha=t.sidereal_time('mean', 'greenwich')  # Greenwich mean siderial time [hourangle]
# sT_ha = sT_ha.hour * 3600 #hour angle in [sec]

# start1 = 0
# end1 = len(elapsed_times)

# # with open('DATA/Sim1/EQUAT_ENV.csv', 'w', encoding='UTF8', newline='') as f:
# # with open('DATA/Sim2/EQUAT_ENV.csv', 'w', encoding='UTF8', newline='') as f:
# with open('DATA/Sim3/EQUAT_ENV.csv', 'w', encoding='UTF8', newline='') as f:

#     writer = csv.writer(f)
#     writer.writerow(['ElapsedTime', 'qw', 'qx', 'qy', 'qz'])
#     theta_g0 = AC.Earth_Rot * sT_ha # initial location in siderial time of 0-meridian
#     for kk in range(start1, end1):
#         theta = theta_g0 + (AC.Earth_Rot * elapsed_times[kk])
#         qw, qx, qy, qz = MF.Matrix2Quaternions(math.cos(theta), math.sin(theta), 0, -math.sin(theta), math.cos(theta), 0, 0, 0, 1)
#         writer.writerow([elapsed_times[kk], qw, qx, qy, qz])



#--------------- CUSTOM -----------------

mission = 'SatRev'
sim_timeStep = 10 # [sec]
data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1_P1.csv".format(mission)))


# Recovering data from the original single satellite file:
elapsed_times = data['ElapsedTime'][:] # elapsed seconds [sec]

# Computation of initial siderial time for Greenwich:
t = Time('1994-10-15 00:00:00', scale='utc', location=('0d', '0d'))
sT_ha=t.sidereal_time('mean', 'greenwich')  # Greenwich mean siderial time [hourangle]
sT_ha = sT_ha.hour * 3600 #hour angle in [sec]

start1 = 0
end1 = len(elapsed_times)


with open(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/EQUAT_{}.csv".format(mission, mission)), 'w', encoding='UTF8', newline='') as f:

    writer = csv.writer(f)
    writer.writerow(['ElapsedTime', 'qw', 'qx', 'qy', 'qz'])
    theta_g0 = AC.Earth_Rot * sT_ha # initial location in siderial time of 0-meridian
    for kk in range(start1, end1):
        theta = theta_g0 + (AC.Earth_Rot * elapsed_times[kk])
        qw, qx, qy, qz = MF.Matrix2Quaternions(math.cos(theta), math.sin(theta), 0, -math.sin(theta), math.cos(theta), 0, 0, 0, 1)
        writer.writerow([elapsed_times[kk], qw, qx, qy, qz])




