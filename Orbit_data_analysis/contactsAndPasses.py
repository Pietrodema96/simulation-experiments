# GROUND STATIONS AND PASSES ANALYSIS
# This functions want to inspect the analysis of the contacts with GS
# and passes over TG

#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 22/03/2022
#
#---------------------------------------------------


from astropy.time import Time
from astropy import coordinates
from astropy.time import Time
from astropy.coordinates import solar_system_ephemeris, EarthLocation
import os, csv, math, configparser
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import utilities.CarKep as C2K
import utilities.AstroConstants as AC
import utilities.spaceConv_utilities as MF
import utilities.timeConv_utilities as TCU
import utilities.plotting_utilities_UmbraCont as PU


# ----------------- SIMULATIONS ROADMAP ----------------------
#   Sim1: duration=1 month, time-step=10s
#   Sim2: duration=40days, time-step=5s
#   Sim3: duration=3 month, time-step=10s



# -- Here the type of the mission considered:
# ENV
# PL

home_path = os.getcwd()


def GS_TG_plotMaster(mission, time_filterGS, periodicity):

    """
    Mastering the mission analysis plots relative to the Targets (TG) and Ground stations (GS)

    INPUT
        mission = mission name [str]
        time_filterGS = time filter for GS contacts duration [min]
        time_filterTG = time filter for TG contacts duration [s]
        sim_timeStep = simulation time step [s]
        GS_num = numer of ground stations [int]
    """

    # SIMULATION GENERALITIES:
    # --------------------------------

    # GROUND STATIONS:
    GS_sel = pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/GS_list.csv".format(mission)))

    if len(GS_sel['GS'][:]) == 1:
        data1= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_GS_contacts_bytime.csv".format(mission))) # reading the data csv file 
    else:
        data1= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_GS_contacts.csv".format(mission))) # reading the data csv file 

    config = configparser.ConfigParser(inline_comment_prefixes="#")
    config.read(os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input/input.cfg'.format(home_path, mission)))
    sim_initEpoch = config["Simulation Parameters"]["launch_date"] # [str]
    IDs_GS = GS_sel['GS'][:]

    # TARGETS:
    data2= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_TG_contacts_bytime.csv".format(mission))) # ENV
    GS_sel = pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TG_list.csv".format(mission)))
    
    # Selection of the number of TG considered....
    if len(GS_sel['TG'][0:50])>30:
        IDs_TG = GS_sel['TG'][0:30]
    else:
        IDs_TG = GS_sel['TG'][:]

    # periodicity = 34 # periodicity in days





    #---------------------------------------------------------------------------------------------------------
    #---------------------------------------------------------------------------------------------------------
    # ---------------------------------- GROUND ST. CONTACTS DISTRIBUTION ---------------------------------------
    #---------------------------------------------------------------------------------------------------------
    #---------------------------------------------------------------------------------------------------------

    date_format_str = '%Y-%m-%d %H:%M:%S'
    d1 = datetime.strptime(sim_initEpoch, date_format_str)
    # --------------------------------


    n_Gs = len(IDs_GS) # number of the GS


    GsTg = data1['GS'][:] # GS IDs [str] 
    durations = data1['Duration'][:] # durations [min]
    Epoch = data1['AOS_UTC'][:] # init Epoch [UTC, str]
    # --------------------------------

    Gs_secEl = np.zeros((len(durations), n_Gs))
    Gs_durations = np.zeros((len(durations), n_Gs))

    # Counting th number of contacts per each TG or GS
    counting_Gs = np.zeros(n_Gs)

    for jj in range(0, len(IDs_GS)): 
        for kk in range(0, len(durations)):
            if GsTg[kk] == IDs_GS[jj]:   
                if durations[kk] >= time_filterGS: # filtering contacts shorter than lower limit
                    Gs_secEl[kk, jj] = TCU.sec_btw_dates(d1, datetime.strptime(Epoch[kk], date_format_str)) # seconds from the initial epoch of the beginning of contact [sec] GS1
                    Gs_durations[kk, jj] = durations[kk]
                    counting_Gs[jj] += 1




    # Plot of contacts:
    PU.plot_contacts(Gs_secEl, Gs_durations, IDs_GS, 'all', 'GS-contacts', 'gen', counting_Gs, periodicity, mission)
    for jj in range(0, len(IDs_GS)):
        pr = jj+1
        PU.plot_contacts(Gs_secEl, Gs_durations, IDs_GS, jj+1, 'GS-contacts', str(pr), counting_Gs, periodicity, mission)


    print('\n --------------------------------------------------  ')
    print('\n Here the number of contacts per each station:')
    print('ID :  numb of passes/contacts')
    for jj in range(0, len(IDs_GS)):
        print('Station {} :  {}'.format(jj+1, counting_Gs[jj]))





    #---------------------------------------------------------------------------------------------------------
    #---------------------------------------------------------------------------------------------------------
    # ---------------------------------- TARGETS CONTACTS DISTRIBUTION ---------------------------------------
    #---------------------------------------------------------------------------------------------------------
    #---------------------------------------------------------------------------------------------------------

    n_Tg = len(IDs_TG) # number of the GS

    Tg = data2['TG'][:] # GS IDs [str] 
    durations = data2['Duration'][:] # durations [min]
    Epoch = data2['AOS_UTC'][:] # init Epoch [UTC, str]


    Tg_secEl = np.zeros((len(durations), n_Tg))
    Tg_durations = np.zeros((len(durations), n_Tg))

    # Counting th enumber of contacts per each TG or GS
    counting_Tg = np.zeros(n_Tg)


    for jj in range(0, len(IDs_TG)): 

        for kk in range(0, len(durations)):

            if Tg[kk] == IDs_TG[jj]:   
                Tg_secEl[kk, jj] = TCU.sec_btw_dates(d1, datetime.strptime(Epoch[kk], date_format_str)) # seconds from the initial epoch of the beginning of contact [sec] GS1
                Tg_durations[kk, jj] = durations[kk]
                counting_Tg[jj] += 1





    # Plot of passes:
    PU.plot_contacts(Tg_secEl, Tg_durations, IDs_TG, 'all', 'TG-passes', 'gen', counting_Tg, periodicity, mission)
    # for jj in range(0, len(IDs_TG)):
    #     pr = jj+1
    #     PU.plot_contacts(Tg_secEl, Tg_durations, IDs_TG, jj+1, 'TG-passes', str(pr), counting_Tg, periodicity, mission)


    print('\n --------------------------------------------------  ')
    print('\n Here the number of contacts per each target:')
    print('ID :  numb of passes/contacts')
    for jj in range(0, len(IDs_TG)):
        print('Target {} :  {}'.format(jj+1, counting_Tg[0]))

    return


if __name__ =="__main__":
    
    mission = 'SatRev'
    time_filterGS = 1 # 5 min of minimum communication with GS
    periodicity = 34 # of the orbit

    GS_TG_plotMaster(mission, time_filterGS, periodicity)
