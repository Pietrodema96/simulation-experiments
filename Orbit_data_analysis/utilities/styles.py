from cmath import pi
import os, csv
import math
from turtle import color, width
import matplotlib.pyplot as plt
import matplotlib 
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import utilities.AstroConstants as AC
import utilities.CarKep as C2K
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU
import utilities.plotting_utilities_Grid as PUG

Colors = ['royalblue', 'k', 'tomato', 'gold', 'magenta', 'green', 'c']