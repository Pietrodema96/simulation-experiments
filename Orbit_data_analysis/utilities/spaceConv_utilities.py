
import os, csv, math
import numpy as np
import pandas as pd
import utilities.AstroConstants as AC
import utilities.timeConv_utilities as TCU
import utilities.plotting_utilities_GT as PUGT


def yaw_pitch_roll_M(alpha, beta, gamma):
  """
  This function provides the matrix of rotations for the attitude
  given the yaw(alpha), pitch(beta) and roll(gamma) angles
  COUNTERCLOCKWISE ROTATIONS
  
  INPUT:
  alpha = yaw angle [deg]
  beta = pitch angle [deg]
  gamma = roll angle [deg]

  """
  alpha = alpha * AC.D2R
  beta = beta * AC.D2R
  gamma = gamma * AC.D2R
  
  # Rz = np.zeros((3, 3))
  # Ry = np.zeros((3, 3))
  # Rx = np.zeros((3, 3))

  # yaw matrix
  Rz = [[math.cos(alpha), -math.sin(alpha), 0],
        [math.sin(alpha), math.cos(alpha), 0],
        [0, 0, 1]]

  # pitch matrix
  Ry = [[math.cos(beta), 0, math.sin(beta)],
        [0, 1, 0],
        [-math.sin(beta), 0, math.sin(beta)]]

  # roll matrix
  Rx = [[1, 0, 0],
        [0, math.cos(gamma), -math.sin(gamma)],
        [0, math.sin(gamma), math.cos(gamma)]]



  # R = np.zeros((3,3))
  # R = []

  return Rz, Ry, Rx



def matrix_ECI2Perifocal(RAAN, AOP, INC):
  """ 
  This function computes the rotation matrix between ECI and perifocal
  orbital frame: ECI (I J K)  to Perifocal ( P Q W )

  INPUT:
  RAAN = Right ascention of the ascending node [deg]
  AOP = Argument of Perigee [deg]
  INC = Inclination [deg]

  """

  AOP = AOP * AC.D2R # [rad]
  AOP = AOP * AC.D2R # [rad]
  INC = INC * AC.D2R # [rad]

  M = np.zeros((3, 3))
  M[0, 0] = math.cos(RAAN)*math.cos(AOP) - math.sin(RAAN)*math.sin(AOP)*math.cos(INC)
  M[0, 1] = math.sin(RAAN)*math.cos(AOP) + math.cos(RAAN)*math.cos(INC)*math.cos(AOP)
  M[0, 2] = math.sin(INC)*math.sin(AOP)
  M[1, 0] = -math.cos(RAAN)*math.sin(AOP) -math.sin(RAAN)*math.cos(INC)*math.cos(AOP)
  M[1, 1] = -math.sin(RAAN)*math.sin(AOP) + math.cos(RAAN)*math.cos(INC)*math.cos(AOP)
  M[1, 2] = math.sin(INC)*math.cos(AOP)
  M[2, 0] = math.sin(RAAN)*math.sin(INC)
  M[2, 1] = -math.cos(RAAN)*math.sin(INC)
  M[2, 2] = math.cos(INC)

  return M



def Matrix2Quaternions(m00, m01, m02, m10, m11, m12, m20, m21, m22):

  """ 
  Matrix to quaternions conversion

  INPUT:
  Input matrix
    | m00 m01 m02 |
    | m10 m11 m12 |
    | m20 m21 m22 |

  """

  tr = m00 + m11 + m22 # 3x3 matrix trace

  if (tr > 0): 
    S = math.sqrt(tr+1.0) * 2
    qw = 0.25 * S
    qx = (m21 - m12) / S
    qy = (m02 - m20) / S
    qz = (m10 - m01) / S
  elif (m00 > m11) and (m00 > m22): 
    S = math.sqrt(1.0 + m00 - m11 - m22) * 2
    qw = (m21 - m12) / S
    qx = 0.25 * S
    qy = (m01 + m10) / S 
    qz = (m02 + m20) / S 
  elif (m11 > m22):
    S = math.sqrt(1.0 + m11 - m00 - m22) * 2
    qw = (m02 - m20) / S
    qx = (m01 + m10) / S 
    qy = 0.25 * S
    qz = (m12 + m21) / S 
  else: 
    S = math.sqrt(1.0 + m22 - m00 - m11) * 2
    qw = (m10 - m01) / S
    qx = (m02 + m20) / S
    qy = (m12 + m21) / S
    qz = 0.25 * S
  
  return qw, qx, qy, qz # returns quaternions


def eci2RAD(rr, vv):

  """  
  Commutation through the ECI and R.ASENTION and DECLINATION

  rr = ECI positions of propagation [time.inst x 3] [m]
  vv = ECI velocities of propagation [time.inst x 6] [m/sec]
  """

  alpha = np.empty(len(rr[:, 0])) # dim[time.inst]
  delta = np.empty(len(rr[:, 0])) # dim[time.inst]

  for i in range(0, len(rr[:,0])):
      R = np.linalg.norm(rr[i,:])
      delta[i] = math.asin(rr[i,2]/R)
      alpha[i] = math.acos( rr[i,0] / (R*math.cos(delta[i])) )

      if (rr[i,1]/ R)  <= 0:
          alpha[i] = 180 - (alpha[i]*180 /np.pi) # in deg
          alpha[i] = alpha[i] * np.pi /180 # in deg

  return alpha, delta


def rotmatrix(angle):
  """  
  Rotation matrix from an angle [rad]
  """
  C = math.cos(angle)
  S = math.sin(angle)
  rotmat = np.zeros((3, 3))
  rotmat[0, 0] = C 
  rotmat[0, 1] = S
  rotmat[0, 2] = 0
  rotmat[1, 0] = -1*S
  rotmat[1, 1] = C
  rotmat[1, 2] = 0
  rotmat[2, 0] = 0
  rotmat[2, 1] = 0
  rotmat[2, 2] = 1

  return rotmat

def rotmatrix2(angle):
  """  
  Rotation matrix from an angle [rad]
  """
  C = math.cos(angle)
  S = math.sin(angle)
  rotmat = np.zeros((3, 3))
  rotmat[0, 0] = C 
  rotmat[0, 1] = -S
  rotmat[0, 2] = 0
  rotmat[1, 0] = S
  rotmat[1, 1] = C
  rotmat[1, 2] = 0
  rotmat[2, 0] = 0
  rotmat[2, 1] = 0
  rotmat[2, 2] = 1

  return rotmat



def Geodetic(r):
  """  
  This function computes lat, lon [rad] from ECI positions
   given the position vector
   r = [ x y z] ECI [km]
  """

  eps = 1e-9
  R_equ = AC.R_E
  f  = 1/298.257223563
  epsRequ = eps*R_equ  # Convergence criterion
  e2      = f*(2-f)    # Square of eccentricity
  X = r[0]            # Cartesian coordinates
  Y = r[1]
  Z = r[2]
  rho2 = X*X + Y*Y      # Square of distance from z-axis
  # Check validity of input data
  if (np.linalg.norm(r)==0):
      print( ' invalid input in Geodetic constructor\n' )
      lon = 0
      lat = 0
      h   = -R_equ

  # Iteration
  dZ = e2*Z
  while(1):
      ZdZ    = Z + dZ
      Nh     = math.sqrt ( rho2 + ZdZ*ZdZ ) 
      SinPhi = ZdZ / Nh # Sine of geodetic latitude
      N      = R_equ / math.sqrt(1-e2*SinPhi*SinPhi)
      dZ_new = N*e2*SinPhi
      if (abs(dZ-dZ_new) < epsRequ):
        break
      dZ = dZ_new

  # Longitude, latitude, altitude
  lon = math.atan2(Y, X)
  lat = math.atan2(ZdZ, math.sqrt(rho2))
  h   = Nh - N
  return lon, lat, h



def haversine(lon1, lat1, lon2, lat2):
  """  
  This function allows to compute km distance between two locations
  on the Earth globe
   INPUT:
   1) LAT, LON: latitude and longitude [deg] of the first location
   2) LAT, LON: latitude and longitude [deg] of the second location
  """

  # Haversine formula 
  dlon = (lon2 - lon1) * AC.D2R 
  dlat = (lat2 - lat1) * AC.D2R 
  a = math.sin(dlat/2)**2 + math.cos(lat1*AC.D2R) * math.cos(lat2*AC.D2R) * math.sin(dlon/2)**2
  # c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a)) 
  c = 2 * math.asin(math.sqrt(a)) 

  r = AC.R_E # Radius of earth in kilometers. Use 3956 for miles. [km]

  dist = abs(c*r) # distance between locations [km]
  
  return dist


def haversine_INV1(lon1, lat, dist):
  """  
  This function allows to compute final longitude starting from the latitude and
  first longitude together with the distance along the parallel

   INPUT:
  lon1: starting longitute [deg]
  lat: latitude of the path [deg]
  dist: path distance on the parallel (lat) [km]
  """

  lon1 = abs(lon1)
  lat = abs(lat)

  c = dist/AC.R_E # [-]
  a = math.sin(c/2)**2
  lat = lat * AC.D2R
  lon_fin = lon1*AC.D2R + 2*(math.asin( math.sqrt(a) / math.cos(lat) )) # final lon [rad]
  lon_fin = lon_fin / AC.D2R # [deg]
  
  return abs(lon_fin)


def haversine_INV2(lon, lat1, dist):
  """  
  This function allows to compute final latitude starting from the latitude and
  first longitude together with the distance along the meridian

  INPUT:
  lon: starting longitute [deg]
  lat1: latitude of the path [deg]
  dist: path distance on the parallel (lat) [km]
  """

  lat1 = abs(lat1)

  c = dist/AC.R_E # [-]
  a = math.sin(c/2)**2
  lat1 = lat1 * AC.D2R
  lat_fin = lat1 + math.asin(math.sqrt(a))*2
  lat_fin = lat_fin / AC.D2R # [deg]

  return abs(lat_fin)


def GrTr_fun(step, rr, to, duration_plot_h):
  """    
  Grount Track computation for an orbit given the ephemeredes rr vv
  INPUTS:
    - step: simulation time step [sec]
    - rr: (n_stepsx3) positions matrix in ECI [km]
    - to: initial date [jdays]
    - duration_plot: time of the representation [h]

  --------------------------------------    
  OUTPUTS:
    - lon: longitude [deg] [-180, +180]
    - lat: latitude [deg] [-90, +90]
  ---------------------------------------
  """

  duration_plot_s = duration_plot_h*3600 # [sec]
  its = int(duration_plot_s/step)

  ref = np.zeros((its, 3))
  lamda = np.zeros(its)
  phi = np.zeros(its)
  height = np.zeros(its)


  for i in range(0, its):

      to = to + step/86400 # steps using mjdays
      U = rotmatrix(TCU.gmst(to-2400000.5)) # rotations accounting Earth mouvement
      ref[i, 0:3] = U.dot(rr[i, 0:3].T)
      lamda[i],phi[i],height[i] = Geodetic(ref[i, :]) # computing lat,lon from ECI
  
  #print(ref[0:10, :])

  lon = lamda * 180/math.pi # [deg]
  lat = phi * 180/math.pi # [deg]

  return lat, lon



# def GrTr_MultiOrbit_fun(step, rr1, rr2, rr3, rr4, to, duration):
#   """  Grount Track computations for a constellation of satellite
#   !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
#   This function provide the visualization with subplots

#   INPUTS:
#    - step: simulation time step [sec]
#    - rr1, rr2, rr3, rr4: (n_stepsx3) positions matrix in ECI
#    - to: initial date [jdays]
#    - duration: time of the representation [h]
#   --------------------------------------    
#   OUTPUTS:
#     FOR EACH SATELLITE
#    - lon: longitude [deg] [-180, +180]
#    - lat: latitude [deg] [-90, +90]
#   """

#   duration = duration*3600 # [sec]
#   its = int(duration/step)

#   ref1 = np.zeros((its, 3))
#   lamda1 = np.zeros(its)
#   phi1 = np.zeros(its)
#   height1 = np.zeros(its)
#   ref2 = np.zeros((its, 3))
#   lamda2 = np.zeros(its)
#   phi2 = np.zeros(its)
#   height2 = np.zeros(its)
#   ref3 = np.zeros((its, 3))
#   lamda3 = np.zeros(its)
#   phi3 = np.zeros(its)
#   height3 = np.zeros(its)
#   ref4 = np.zeros((its, 3))
#   lamda4 = np.zeros(its)
#   phi4 = np.zeros(its)
#   height4 = np.zeros(its)

#   lat = np.zeros((its, 4))
#   lon = np.zeros((its, 4))


#   for i in range(0, its):

#       to = to + step/86400 # steps using mjdays
#       U = rotmatrix(TCU.gmst(to-2400000.5)) # rotations accounting Earth mouvement

#       #1
#       ref1[i, 0:3] = U.dot(rr1[i, 0:3].T)
#       lamda1[i],phi1[i],height1[i] = Geodetic(ref1[i, :]) # computing lat,lon from ECI

#       #2
#       ref2[i, 0:3] = U.dot(rr2[i, 0:3].T)
#       lamda2[i],phi2[i],height2[i] = Geodetic(ref2[i, :]) # computing lat,lon from ECI

#       #3
#       ref3[i, 0:3] = U.dot(rr3[i, 0:3].T)
#       lamda3[i],phi3[i],height3[i] = Geodetic(ref3[i, :]) # computing lat,lon from ECI

#       #4
#       ref4[i, 0:3] = U.dot(rr4[i, 0:3].T)
#       lamda4[i],phi4[i],height4[i] = Geodetic(ref4[i, :]) # computing lat,lon from ECI

#   #1
#   lon[:,0] = lamda1 * 180/math.pi #[deg]
#   lat[:,0] = phi1 * 180/math.pi #[deg]
#   #2
#   lon[:,1] = lamda2 * 180/math.pi
#   lat[:,1] = phi2 * 180/math.pi
#   #3
#   lon[:,2] = lamda3 * 180/math.pi
#   lat[:,2] = phi3 * 180/math.pi
#   #4
#   lon[:,3] = lamda4 * 180/math.pi
#   lat[:,3] = phi4 * 180/math.pi

#   return lat, lon


# def GrTr_MultiOrbit_fun_Test(step, rr1, rr2, rr3, rr4, rr5, rr6, to, duration):

#   # Grount Track computations for a constellation of satellite
#   # !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
#   # This function provide the visualization with subplots
#   # INPUTS:
#   #  - step: simulation time step [sec]
#   #  - rr1, rr2, rr3, rr4: (n_stepsx3) positions matrix in ECI
#   #  - to: initial date [jdays]
#   #  - duration: time of the representation [h]
#   #--------------------------------------    
#   # OUTPUTS:
#   #   FOR EACH SATELLITE
#   #  - lon: longitude [deg] [-180, +180]
#   #  - lat: latitude [deg] [-90, +90]
#   #---------------------------------------

#   duration = duration*3600 # [sec]
#   its = int(duration/step)

#   ref1 = np.zeros((its, 3))
#   lamda1 = np.zeros(its)RRz
#   phi1 = np.zeros(its)
#   height1 = np.zeros(its)
#   ref2 = np.zeros((its, 3))
#   lamda2 = np.zeros(its)
#   phi2 = np.zeros(its)
#   height2 = np.zeros(its)
#   ref3 = np.zeros((its, 3))
#   lamda3 = np.zeros(its)
#   phi3 = np.zeros(its)
#   height3 = np.zeros(its)
#   ref4 = np.zeros((its, 3))
#   lamda4 = np.zeros(its)
#   phi4 = np.zeros(its)
#   height4 = np.zeros(its)
#   ref5 = np.zeros((its, 3))
#   lamda5 = np.zeros(its)
#   phi5 = np.zeros(its)
#   height5 = np.zeros(its)
#   ref6 = np.zeros((its, 3))
#   lamda6 = np.zeros(its)
#   phi6 = np.zeros(its)
#   height6 = np.zeros(its)

#   lat = np.zeros((its, 6))
#   lon = np.zeros((its, 6))


#   for i in range(0, its):

#       to = to + step/86400 # steps using mjdays
#       U = rotmatrix(TCU.gmst(to-2400000.5)) # rotations accounting Earth mouvement

#       #1
#       ref1[i, 0:3] = U.dot(rr1[i, 0:3].T)
#       lamda1[i],phi1[i],height1[i] = Geodetic(ref1[i, :]) # computing lat,lon from ECI

#       #2
#       ref2[i, 0:3] = U.dot(rr2[i, 0:3].T)
#       lamda2[i],phi2[i],height2[i] = Geodetic(ref2[i, :]) # computing lat,lon from ECI

#       #3
#       ref3[i, 0:3] = U.dot(rr3[i, 0:3].T)
#       lamda3[i],phi3[i],height3[i] = Geodetic(ref3[i, :]) # computing lat,lon from ECI

#       #4
#       ref4[i, 0:3] = U.dot(rr4[i, 0:3].T)
#       lamda4[i],phi4[i],height4[i] = Geodetic(ref4[i, :]) # computing lat,lon from ECI

#       #5
#       ref5[i, 0:3] = U.dot(rr5[i, 0:3].T)
#       lamda5[i],phi5[i],height5[i] = Geodetic(ref5[i, :]) # computing lat,lon from ECI

#       #6
#       ref6[i, 0:3] = U.dot(rr6[i, 0:3].T)
#       lamda6[i],phi6[i],height6[i] = Geodetic(ref6[i, :]) # computing lat,lon from ECI


#   #1
#   lon[:,0] = lamda1 * 180/math.pi #[deg]
#   lat[:,0] = phi1 * 180/math.pi #[deg]
#   #2
#   lon[:,1] = lamda2 * 180/math.pi
#   lat[:,1] = phi2 * 180/math.pi
#   #3
#   lon[:,2] = lamda3 * 180/math.pi
#   lat[:,2] = phi3 * 180/math.pi
#   #4
#   lon[:,3] = lamda4 * 180/math.pi
#   lat[:,3] = phi4 * 180/math.pi
#   #5
#   lon[:,4] = lamda5 * 180/math.pi
#   lat[:,4] = phi5 * 180/math.pi
#   #6
#   lon[:,5] = lamda6 * 180/math.pi
#   lat[:,5] = phi6 * 180/math.pi


#   return lat, lon


def GrTr_MultiOrbit_fun_R(step, RRx, RRy, RRz, to_1, \
        duration, sats):

  """  
  Grount Track computations for a constellation of satellite
  !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
  This function provide the visualization with subplots
  INPUTS:
   - step: simulation time step [sec]
   - RRx, RRy, RRz: x y z for all satellites, (instants x n_sats)
   - to: initial date [jdays]
   - duration: time of the representation [h]
  --------------------------------------    
  OUTPUTS:
    FOR EACH SATELLITE
   - lon: longitude [deg] [-180, +180]
   - lat: latitude [deg] [-90, +90]

  """

  duration = duration*3600 # [sec]
  its = int(duration/step)

  ref = np.zeros((its, 3))
  lamda = np.zeros((its, sats))
  phi = np.zeros((its, sats))
  height = np.zeros((its, sats))

  RR = np.zeros(3)
  lat = np.zeros((its, sats))
  lon = np.zeros((its, sats))

  # print(RRx[0:4,:])
  # print(ref[0:4,:])
  # print(ref[0:4,:])
  # print(ref[0:4,:])


  for j in range(0, sats): # running over satellites
    to = to_1
    for i in range(0, its): # running over times

        to = to + step/86400 # steps using mjdays
        U = rotmatrix(TCU.gmst(to-2400000.5)) # rotations accounting Earth mouvement

        # Sat lat lon
        RR[0] = RRx[i, j]
        RR[1] = RRy[i, j]
        RR[2] = RRz[i, j]
        ref[i, 0:3] = U.dot( RR[:].T )
        lamda[i, j],phi[i, j],height[i, j] = Geodetic(ref[i, :]) # computing lat,lon from ECI

    # Storing data
    lon[:, j] = lamda[:, j] * 180/math.pi # [deg]
    lat[:, j] = phi[:, j] * 180/math.pi # [deg]

    # print(lon[0:20, j])

  return lat, lon





def azimuth_elevation(lat_GS, lon_GS, lat_S, lon_S, rr):

  """ 
  Elevation wrt a GS for a sat

  INPUT:
    lat_GS, lon_GS = of the GS [deg] (single values)
    lat_S, lon_S = of the satellite [deg] (single values)
    rr = ECI positions of satellite [km] (1x3)
  """

  h = math.sqrt(rr[0]**2 + rr[1]**2 + rr[2]**2)-AC.R_E # altitude from the Earth surface [km]

  alpha, SP = PUGT.GS_lineSight(lat_GS, lon_GS, lat_S, lon_S, rr) # central angle # [deg]

  lat_GS = lat_GS*AC.D2R
  lon_GS = lon_GS*AC.D2R
  lat_S = lat_S*AC.D2R
  lon_S = lon_S*AC.D2R
  elevation = math.asin( (h*(h+2*AC.R_E)-SP**2)/(2*SP*AC.R_E) )/AC.D2R # elevation wrt the ground station # [rad]
  azimuth = math.atan( (math.sin(lon_GS-lon_S)*math.cos(lat_S))/(math.cos(lat_GS)*math.sin(lat_S) - \
    math.sin(lat_GS)*math.cos(lat_S)*math.cos(lon_S-lon_GS)) )
  
  # Fixing azimuth
  if azimuth*(lon_GS-lon_S) > 0:
    azimuth = math.atan(math.tan(azimuth)) /AC.D2R # [deg]
  else:
    azimuth = ( math.atan(math.tan(azimuth)) + np.pi ) / AC.D2R # [deg]

  
  return elevation, azimuth, alpha/AC.D2R


def filtering_az_el(lat_GS, lon_GS, lat_S, lon_S, rrx, rry, rrz, sats, instants, min_el):
  """
  Filtering and editing the results of azimuth computations
  for a GS along time, with a range of time analyszed
  
  INPUT:
    lat_GS, lon_GS = of the GS [deg] (single values)
    lat_S, lon_S = of the satellite [deg] (instants x n_sats)
    rr_x, rr_y, rr_z = ECI positions of satellite [km] (instants x n_sats) 
    sats = number of satellites
    instants = number of instants of simulation [-]
    min_el : min elevation to consider the pass

  """

  # Azimuths and Elevations per each satellite considered:
  azimuths_IN = np.zeros((instants, sats)) # (instants x n_sats)
  elevations_IN = np.zeros((instants, sats)) # (instants x n_sats)
  azimuths_OUT = np.zeros((instants, sats)) # (instants x n_sats)
  elevations_OUT = np.zeros((instants, sats)) # (instants x n_sats)
  azimuths_IN[:] = np.NaN # (instants x n_sats)
  elevations_IN[:] = np.NaN # (instants x n_sats)
  azimuths_OUT[:] = np.NaN # (instants x n_sats)
  elevations_OUT[:] = np.NaN # (instants x n_sats)
  max_El = np.zeros(sats)

  el_rmd = min_el

  count_passes = np.zeros(sats) # count of passes per sat
  st = 0
  for s in range(0, sats):
  
    for t in range(0, instants):

      rr = [rrx[t, s], rry[t, s], rrz[t, s]] # recovering ECI data per sat
      
      # [deg]
      EL, AZ, _ = azimuth_elevation(lat_GS, lon_GS, lat_S[t, s], lon_S[t, s], rr)
      if AZ < 0:
        AZ = 360 + AZ

      if EL < min_el:
        EL = np.NaN # marked as a NaN if elevation is too small ( NO CONTACT )
        AZ = np.NaN # marked as a NaN if elevation is too small ( NO CONTACT )
        el_rmd = min_el
        st = 0
      else:
      
        if EL > el_rmd: # ...in order to find beginning and end branches
          if st == 0:
            count_passes[s] += 1
            st = 1

          # ENTERING branch
          azimuths_IN[t, s] = AZ 
          elevations_IN[t, s] = EL
          # print(AZ)
          # print(EL)
        else:
          # EXITING branch
          if np.isnan(azimuths_OUT[t-1, s]): # just for the plot, joining the branches
            azimuths_OUT[t-1, s] = azimuths_IN[t-1, s]
            elevations_OUT[t-1, s] = elevations_IN[t-1, s]
          azimuths_OUT[t, s] = AZ
          elevations_OUT[t, s] = EL   
          # print(AZ)
          # print(EL)
      
        el_rmd = EL
  

  for s in range(0, sats): # computing the max elevation
    max_El[s] = max(np.nanmax(elevations_IN[:, s]), np.nanmax(elevations_OUT[:, s])) # [deg]
  
  return azimuths_IN, azimuths_OUT, elevations_IN, elevations_OUT, max_El, count_passes
