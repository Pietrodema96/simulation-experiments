
from cmath import nan, pi
import os, csv
import math
from turtle import color, width
import matplotlib.pyplot as plt
import matplotlib 
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import utilities.AstroConstants as AC
import utilities.CarKep as C2K
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU
import utilities.plotting_utilities_Grid as PUG
import utilities.styles as ST
m2km = 1000


def unlink_wrap(dat, lims=[-180, 180], thresh = 0.95):
    """
    Iterate over contiguous regions of `dat` (i.e. where it does not
    jump from near one limit to the other).

    This function returns an iterator object that yields slice
    objects, which index the contiguous portions of `dat`.

    This function implicitly assumes that all points in `dat` fall
    within `lims`.

    """
    jump = np.nonzero(np.abs(np.diff(dat)) > ((lims[1] - lims[0]) * thresh))[0]
    lasti = 0
    for ind in jump:
        yield slice(lasti, ind + 1)
        lasti = ind + 1
    yield slice(lasti, len(dat))


# def GrTr(step, rr, to, duration_plot_h, GS, TG, mission):
def GrTr(step, rr, to, duration_plot_h):
    """
    # Grount Track plot for an orbit given the ephemeredes rr vv
    # INPUTS:
    #  - step: simulation time step [sec]
    #  - rr: (n_stepsx3) positions matrix in ECI [km]
    #  - to: initial date [jdays]
    #  - duration_plot: time of the representation [h]
    #  - GS / TG: matrix of targets or ground-stations; example
    #    0. GS,lat,lon,alt,min_El
    #    1. ENV-FOS-DA,49.878,8.646,144.000,2.000
    #    2. ENV-PDCC-FR,41.809,12.676,320.000,2.000
    #    3. ENV-PDAS-FU,42.039,13.438,661.000,2.000
    #    4. ENV-MON-GU,5.099,-52.640,120.000,2.000
    #  - mission: str name of the mission

    #--------------------------------------    
    # OUTPUTS:
    #  - lon: longitude [deg] [-180, +180]
    #  - lat: latitude [deg] [-90, +90]
    #---------------------------------------
    """

    lat, lon = SCU.GrTr_fun(step, rr, to, duration_plot_h) # computing lat and lon [deg]

    duration_plot_s = duration_plot_h*3600 # [sec]
    its = int(duration_plot_s/step)

    fig, ax = plt.subplots(figsize=(15,7)) # Setting basemap
    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')

    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral',lake_color='aqua')
    # m.shadedrelief()
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 5), linewidth=1, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 10), linewidth=1, labels=[1,0,0,1], color='r')

    x, y = m(lon[0:its], lat[0:its])
    # m.scatter(x, y, marker='.',color='k', s=2) # plot of groundtrack
    for slc in unlink_wrap(x, [-180, 180]):
        m.plot(x[slc], y[slc], '-', color='k', linewidth=2, label='SAT')

    # --------------------------------------
    R, alpha = sat_coverage(45, rr[0, :])
    # print(R)
    fin_lon_FOV = lon[0] + alpha # final longitude FOV [deg]
    fin_lat_FOV = lat[0] + alpha # final longitude FOV [deg]


    x,y=m(lon[0], lat[0])
    # print(x)
    x2,y2 = m(fin_lon_FOV, lat[0])
    x22,y22 = m(lon[0], fin_lat_FOV)

    # print(x2, y2)
    # print(x22, y22)
    circle = plt.Circle((x, y), abs(x2-x), color='black',fill=False)

    ax.add_patch(circle)
    # ---------------------------

    plt.title("GroundTrack plot, propagation duration: %.2f [h]" %duration_plot_h)

    plt.show()



# def GrTr_MultiOrbit(step, rr1, rr2, rr3, rr4, to, duration_plot_h, GS, TG, mission):
#     # Grount Track plot for a constellation of satellite
#     # !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
#     # INPUTS:
#     #  - step: simulation time step [sec]
#     #  - rr1, rr2, rr3, rr4: (n_stepsx3) positions matrix in ECI
#     #  - to: initial date [jdays]
#     #  - duration_plot: time of the representation [h]
#     #--------------------------------------    
#     # OUTPUTS:
#     #   FOR EACH SATELLITE
#     #  - lon: longitude [deg] [-180, +180]
#     #  - lat: latitude [deg] [-90, +90]
#     #---------------------------------------

#     lat, lon = SCU.GrTr_MultiOrbit_fun(step, rr1, rr2, rr3, rr4, to, duration_plot_h)
#     duration = duration_plot_h*3600 # [sec]
#     its = int(duration/step)

#     plt.figure(figsize=(15,7))
#     m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
#                 llcrnrlon=-180,urcrnrlon=180,resolution='c')

#     m.drawmapboundary(fill_color='aqua')
#     m.fillcontinents(color='coral',lake_color='aqua')
#     m.drawcoastlines()
#     m.drawparallels(np.arange(-90, 90, 20), linewidth=1, labels=[1,0,0,1], color='r')
#     m.drawmeridians(np.arange(-180, 180, 20), linewidth=1, labels=[1,0,0,1], color='r')


#     x1, y1 = m(lon[0:its,0], lat[0:its,0]) #1
#     x2, y2 = m(lon[0:its,1], lat[0:its,1]) #2
#     x3, y3 = m(lon[0:its,2], lat[0:its,2]) #3
#     x4, y4 = m(lon[0:its,3], lat[0:its,3]) #4
#     m.scatter(x1, y1, marker='.',color='orangered', s=2, label='SAT 1')
#     # m.scatter(x1[0], y1[0], marker='D',color='orangered', s=25, label='SAT 1')

#     m.scatter(x2, y2, marker='.',color='k', s=2, label='SAT 2')
#     m.scatter(x3, y3, marker='.',color='blue', s=2, label='SAT 3')
#     m.scatter(x4, y4, marker='.',color='gold', s=2, label='SAT 4')

#     XGS, YGS = m(GS['lon'][:], GS['lat'][:])
#     XTG, YTG = m(TG['lon'][:], TG['lat'][:])

#     m.scatter(XGS, YGS, marker='D',color='b') # plot of GS
#     m.scatter(XTG, YTG, marker='D',color='g') # plot of TG

#     plt.suptitle('Mission: %s' %mission)
#     plt.title("GroundTrack plot, propagation duration: %.2f [h]" %duration_plot_h)
#     plt.legend(loc="upper left")


#     # plt.show()
#     return 






# def GrTr_MultiOrbit_Test(step, rr1, rr2, rr3, rr4, rr5, rr6, to, duration_plot_h,\
#      mission, lat_int, lon_int):
#     # Grount Track plot for a constellation of satellite
#     # !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
#     # INPUTS:
#     #  - step: simulation time step [sec]
#     #  - rr1, rr2, rr3, rr4: (n_stepsx3) positions matrix in ECI
#     #  - to: initial date [jdays]
#     #  - duration_plot: time of the representation [h]
#     #--------------------------------------    
#     # OUTPUTS:
#     #   FOR EACH SATELLITE
#     #  - lon: longitude [deg] [-180, +180]
#     #  - lat: latitude [deg] [-90, +90]
#     #---------------------------------------

#     lat, lon = SCU.GrTr_MultiOrbit_fun_Test(step, rr1, rr2, rr3, rr4, rr5, rr6, to, \
#         duration_plot_h)
#     duration = duration_plot_h*3600 # [sec]
#     its = int(duration/step)

#     plt.figure(figsize=(15,7))
#     # m = Basemap(projection='cyl',llcrnrlat=20,urcrnrlat=80,\
#     #             llcrnrlon=-5,urcrnrlon=30,resolution='c')
#     m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
#                 llcrnrlon=-180,urcrnrlon=180,resolution='c')

#     m.drawmapboundary(fill_color='w')
#     m.fillcontinents(color='w')
#     m.drawcoastlines()
#     m.drawparallels(np.arange(-90, 90, 30), linewidth=0.5, labels=[1,0,0,1], color='r')
#     m.drawmeridians(np.arange(-180, 180, 60), linewidth=0.5, labels=[1,0,0,1], color='r')


#     x1, y1 = m(lon[0:its,0], lat[0:its,0]) #1
#     x2, y2 = m(lon[0:its,1], lat[0:its,1]) #2
#     x3, y3 = m(lon[0:its,2], lat[0:its,2]) #3
#     x4, y4 = m(lon[0:its,3], lat[0:its,3]) #4
#     x5, y5 = m(lon[0:its,4], lat[0:its,4]) #5
#     x6, y6 = m(lon[0:its,5], lat[0:its,5]) #6
#     m.scatter(x1, y1, marker='.',color='orangered', s=20, label='S1_P1')
#     m.scatter(x2, y2, marker='.',color='k', s=20, label='S1_P2')
#     m.scatter(x3, y3, marker='.',color='blue', s=20, label='S1_P3')
#     m.scatter(x4, y4, marker='.',color='gold', s=20, label='S2_P1')
#     m.scatter(x5, y5, marker='.',color='green', s=20, label='S2_P2')
#     m.scatter(x6, y6, marker='.',color='m', s=20, label='S2_P3')

#     if lat_int[0] != np.NaN:
#         PUG.poly_basemap(lat_int, lon_int, m)

#     plt.suptitle('Mission: %s' %mission)
#     plt.title("GroundTrack plot, propagation duration: %.2f [h]" %duration_plot_h)
#     plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")


#     plt.show()



# def GrTr_MultiOrbit_R(step, RRx, RRy, RRz, to, duration_plot_h, \
#         mission, sats):
#     # Grount Track plot for a constellation of satellite
#     # !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
#     # INPUTS:
#     #  - step: simulation time step [sec]
#     #  - RRx, RRy, RRz: x y z for all satellites, (instants x n_sats)
#     #  - to: initial date [jdays]
#     #  - duration_plot: time of the representation [h]
#     #  - mission: name of the mission [str]
#     #  - sats: number of satellites
#     #--------------------------------------    
#     # OUTPUTS:
#     #   FOR EACH SATELLITE
#     #  - lon: longitude [deg] [-180, +180]
#     #  - lat: latitude [deg] [-90, +90]
#     #---------------------------------------

#     lat, lon = SCU.GrTr_MultiOrbit_fun_R(step, RRx, RRy, RRz, to, \
#         duration_plot_h, sats) # matrices: (n_inst x sats)

#     duration = duration_plot_h*3600 # [sec]
#     its = int(duration/step)

#     plt.figure(figsize=(15,7)) # Setting Basemap
#     m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
#                 llcrnrlon=-180,urcrnrlon=180,resolution='c')

#     m.drawmapboundary(fill_color='w')
#     m.fillcontinents(color='w')
#     m.drawcoastlines()
#     m.drawparallels(np.arange(-90, 90, 30), linewidth=0.5, labels=[1,0,0,1], color='r')
#     m.drawmeridians(np.arange(-180, 180, 60), linewidth=0.5, labels=[1,0,0,1], color='r')


#     for ii in range(0, sats): # running over sats
#         x, y = m(lon[0:its,ii], lat[0:its,ii]) # Sats
#         m.scatter(x, y, marker='.',color=ST.Colors[ii], s=20, label='SC {}'.format(ii+1))
#         # for kk in range(0, len(x)):
#         #     if abs(x[kk]) < 179:
#         #         m.plot(x[kk], y[kk], '--', color=ST.Colors[ii], linewidth=1, label='SC {}'.format(ii+1))
#         #     else:
#         #         continue

#     plt.suptitle('Mission: %s' %mission)
#     plt.title("GroundTrack plot, propagation duration: %.2f [h]" %duration_plot_h)
#     plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")
#     plt.show()





def GrTr_MultiOrbit_R(step, RRx, RRy, RRz, to, duration_plot_h, \
        mission, lat_int, lon_int, ROI_check, sats, FOV):
    """
    # Grount Track plot for a constellation of satellite
    # !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
    # INPUTS:
    #  - step: simulation time step [sec]
    #  - RRx, RRy, RRz: x y z for all satellites, (instants x n_sats)
    #  - to: initial date [jdays]
    #  - duration_plot: time of the representation [h]
    #  - mission: name of the mission [str]
    #  - lat_int / lon_int : lat lon bounadries for local areas
    #  - sats: number of satellites
    #--------------------------------------    
    # OUTPUTS:
    #   FOR EACH SATELLITE
    #  - lon: longitude [deg] [-180, +180]
    #  - lat: latitude [deg] [-90, +90]
    #---------------------------------------
    """
    lat, lon = SCU.GrTr_MultiOrbit_fun_R(step, RRx, RRy, RRz, to, \
        duration_plot_h, sats) # matrices: (n_inst x sats)

    # print(lon[0:20, 0])
    # print(lon[0:20, 1])
    # print(lon[0:20, 2])
    # print(lon[0:20, 3])

    duration = duration_plot_h*3600 # [sec]
    its = int(duration/step)

    fig = plt.figure(figsize=(15,8))
    gs = fig.add_gridspec(1, 2,
                width_ratios=[2,1]
                )

    # GLOBAL PLOT: ------------------------
    ax1 = plt.subplot(gs[0])
    # ax1 = plt.subplot()

    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')

    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 20), linewidth=0.4, labels=[1,0,0,1], color='grey')
    m.drawmeridians(np.arange(-180, 180, 30), linewidth=0.4, labels=[1,0,0,1], color='grey')

    pl = 0
    for ii in range(0, sats): # running over sats
        x, y = m(lon[0:its,ii], lat[0:its,ii]) # Sats
        pl += 1
        if pl > (sats/2):
            PL = int(pl-(sats/2))
            sc = 2
        else:
            PL = pl
            sc = 1
        for slc in unlink_wrap(x, [-180, 180]):
            m.plot(x[slc], y[slc], '-', color=ST.Colors[ii], linewidth=1.5)
        if sats ==1:
            m.scatter(360, 360, marker='.',color=ST.Colors[ii], s=15, label="S1_P1")
        else:
            m.scatter(360, 360, marker='.',color=ST.Colors[ii], s=15, label="S{}_P{}".format(sc, PL))


    if ROI_check == 1:
        PUG.poly_basemap_fill(lat_int, lon_int, m)

    ax1.set_title("GroundTrack plot global, propagation duration: %.2f [h]" %duration_plot_h)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=False, ncol=3)

    # LOCAL SUBPLOT: ------------------------
    ax2 = plt.subplot(gs[1])
    # ax2 = plt.subplot()

    if ROI_check == 0:
        m = Basemap(projection='cyl',llcrnrlat=30,urcrnrlat=60,\
            llcrnrlon=-10,urcrnrlon=+30,resolution='c')
    else:
        m = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
                    llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c')

    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 10), linewidth=0.4, labels=[1,0,0,1], color='grey')
    m.drawmeridians(np.arange(-180, 180, 20), linewidth=0.4, labels=[1,0,0,1], color='grey')

    pl = 0
    for ii in range(0, sats): # running over sats
        x, y = m(lon[0:its,ii], lat[0:its,ii]) # Sats
        # m.scatter(x, y, marker='.',color=ST.Colors[ii], s=20, label='SC {}'.format(ii+1))
        pl += 1
        if pl > (sats/2):
            PL = int(pl-(sats/2))
            sc = 2
        else:
            PL = pl
            sc = 1
        for slc in unlink_wrap(x, [-180, 180]):
            m.plot(x[slc], y[slc], '-', color=ST.Colors[ii], linewidth=1.2)
        if sats ==1:
            m.scatter(360, 360, marker='.',color=ST.Colors[ii], s=15, label="S1_P1")
        else:
            m.scatter(360, 360, marker='.',color=ST.Colors[ii], s=15, label="S{}_P{}".format(sc, PL))


    if ROI_check == 1:
        PUG.poly_basemap(lat_int, lon_int, m)


    if lat_int[0] < 0:
        ax2.set_title("GroundTrack - Europe")
    else:
        ax2.set_title("GroundTrack - local area")

    plt.suptitle('Mission: {} , propagation duration: {:.2f} [h]'.format(mission, duration_plot_h))
    ax2.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "GroundTrack.png"), bbox_inches='tight')





def GrTr_MultiOrbitSUB(step, rr1, rr2, rr3, rr4, to, duration_plot_h, GS, TG, mission):
    """
    # Grount Track plot for a constellation of satellite
    # !!! THIS FUNCTION WORKS JUST FOR 4 SATELLITES !!!
    # This function provide the visualization with subplots
    # INPUTS:
    #  - step: simulation time step [sec]
    #  - rr1, rr2, rr3, rr4: (n_stepsx3) positions matrix in ECI
    #  - to: initial date [jdays]
    #  - duration_plot: time of the representation [h]
    #--------------------------------------    
    # OUTPUTS:
    #   FOR EACH SATELLITE
    #  - lon: longitude [deg] [-180, +180]
    #  - lat: latitude [deg] [-90, +90]
    #---------------------------------------
    """

    lat, lon = SCU.GrTr_MultiOrbit_fun(step, rr1, rr2, rr3, rr4, to, duration_plot_h)
    duration = duration_plot_h*3600 # [sec]
    its = int(duration/step)

    fig = plt.figure(figsize=(15,7))
    # Cylindrical procjection:
    ax = fig.add_subplot(211)
    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')

    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral',lake_color='aqua')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 30), linewidth=0.4, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 60), linewidth=0.4, labels=[1,0,0,1], color='r')

    x1, y1 = m(lon[0:its,0], lat[0:its,0]) #1
    x2, y2 = m(lon[0:its,1], lat[0:its,1]) #2
    x3, y3 = m(lon[0:its,2], lat[0:its,2]) #3
    x4, y4 = m(lon[0:its,3], lat[0:its,3]) #4
    # m.scatter(x1, y1, marker='.',color='orangered', s=2, label='SAT 1')
    # m.scatter(x2, y2, marker='.',color='k', s=2, label='SAT 2')
    # m.scatter(x3, y3, marker='.',color='blue', s=2, label='SAT 3')
    # m.scatter(x4, y4, marker='.',color='gold', s=2, label='SAT 4')

    for slc in unlink_wrap(x1, [-180, 180]):
        m.plot(x1[slc], y1[slc], '-', color='orangered', s=2, label='SAT 1')
    for slc in unlink_wrap(x2, [-180, 180]):
        m.plot(x2[slc], y2[slc], '-', color='k', s=2, label='SAT 2')
    for slc in unlink_wrap(x3, [-180, 180]):
        m.plot(x3[slc], y3[slc], '-', color='blue', s=2, label='SAT 3')
    for slc in unlink_wrap(x4, [-180, 180]):
        m.plot(x4[slc], y4[slc], '-', color='gold', s=2, label='SAT 4')

    XGS, YGS = m(GS['lon'][:], GS['lat'][:])
    XTG, YTG = m(TG['lon'][:], TG['lat'][:])

    m.scatter(XGS, YGS, marker='D',color='b') # plot of GS
    m.scatter(XTG, YTG, marker='D',color='g') # plot of TG

    ax.set_title("GroundTrack plot (C), propagation duration: %.2f [h]" %duration_plot_h)

    # Robinson procjection:
    ax = fig.add_subplot(212)
    m = Basemap(projection='robin', lon_0 = 0, lat_0 = 0)

    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral',lake_color='aqua')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 20), linewidth=1, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 20), linewidth=1, labels=[1,0,0,1], color='r')

    x1, y1 = m(lon[0:its,0], lat[0:its,0]) #1
    x2, y2 = m(lon[0:its,1], lat[0:its,1]) #2
    x3, y3 = m(lon[0:its,2], lat[0:its,2]) #3
    x4, y4 = m(lon[0:its,3], lat[0:its,3]) #4

    m.scatter(x1, y1, marker='.',color='orangered', s=2, label='SAT 1')
    # m.scatter(x1[0], y1[0], marker='D',color='orangered', s=25, label='SAT 1')

    m.scatter(x2, y2, marker='.',color='k', s=2, label='SAT 2')
    m.scatter(x3, y3, marker='.',color='blue', s=2, label='SAT 3')
    m.scatter(x4, y4, marker='.',color='gold', s=2, label='SAT 4')

    XGS, YGS = m(GS['lon'][:], GS['lat'][:])
    XTG, YTG = m(TG['lon'][:], TG['lat'][:])

    m.scatter(XGS, YGS, marker='D',color='b') # plot of GS
    m.scatter(XTG, YTG, marker='D',color='g') # plot of TG

    ax.set_title("GroundTrack plot (R), propagation duration: %.2f [h]" %duration_plot_h)
    ax.legend(bbox_to_anchor=(1.04,1), loc="upper left")
    plt.suptitle('Mission-[{}]'.format(mission))
    plt.show()



def sat_coverage(f, rr):
    """
    Sat coverage computation:
    f = FOV of the satellite [deg]
    rr = position in ECI [km]
    """


    h = math.sqrt(rr[0]**2 + rr[1]**2 + rr[2]**2)-AC.R_E # altitude from the Earth surface [km]
    f = f * AC.D2R
    ni = AC.reduced_distance(h)
    # ni = (1 + (h / AC.R_E))
    eps = math.asin(ni * math.sin(f))
    # alpha = -f + math.asin(ni*math.sin(f))
    alpha = eps - math.asin(math.sin(eps)/ni) # internal angle

    R = AC.R_E * alpha # surface radius [km]
    alpha = alpha / AC.D2R # internal angle [deg]

    return abs(R), alpha



def GS_lineSight(lat_GS, lon_GS, lat_S, lon_S, rr):
    """
    Line of sight with Ground Station computations:

    INPUT:
        lat_GS, lon_GS = of the GS [deg]
        lat_S, lon_S = of the satellite [deg]
        rr = ECI positions of satellite [km]
    """

    OS = math.sqrt(rr[0]**2 + rr[1]**2 + rr[2]**2) # distance from the Earth center
    R = SCU.haversine(lon_GS, lat_GS, lon_S, lat_S,) # [km]
    alpha = R/AC.R_E # central angle [rad]

    SP = math.sqrt(OS**2 + (AC.R_E)**2 - 2*OS*(AC.R_E)*math.cos(alpha)) # [km]

    return alpha, SP



def land_sea(lonSat, latSat, GS, TG, mission, duration):
    """
    # Identification and plot of satellite passes over land and sea
    # CASE of single satellite
    #
    # lonSat = longitudes of satellite [deg]
    # latSat = latitude of satellite [deg]
    # GS/TG = data from csv for ground-stations and targets
    # mission = str name of the mission
    # duration = mission duration [h]
    """

    fig, (ax1, ax2) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [5, 2]}, figsize=(15,7))
    ax1 = fig.add_subplot(ax1)
    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')

    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral',lake_color='aqua')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 20), linewidth=1, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 20), linewidth=1, labels=[1,0,0,1], color='r')

    land=0
    sea=0

    for ii in range(0, len(lonSat)):
        x, y = m(lonSat[ii], latSat[ii]) 
        if m.is_land(x, y):
            land += 1
            if land == 1:
                m.scatter(x, y, marker='*',color='g', s=7, label='Land') # sea
            else:
                m.scatter(x, y, marker='*',color='g', s=7) # land
        else:
            sea += 1
            if sea == 1:
                m.scatter(x, y, marker='d',color='blue', s=7, label='Sea') # sea
            else:
                m.scatter(x, y, marker='d',color='blue', s=7) # sea


    XGS, YGS = m(GS['lon'][:], GS['lat'][:])
    XTG, YTG = m(TG['lon'][:], TG['lat'][:])

    # m.scatter(XGS, YGS, marker='D',color='r') # plot of GS
    # m.scatter(XTG, YTG, marker='D',color='gold') # plot of TG

    plt.suptitle('Mission: %s' %mission)
    ax1.set_title("GroundTrack plot, propagation duration: %.2f [h]" %duration)
    ax1.legend(loc="upper left")

    #------------------------------------
    ax2 = fig.add_subplot(ax2)
    labels = 'Land', 'Sea'
    tot = land+sea # total points
    sizes = [land*100/tot, sea*100/tot]
    ax2.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax2.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax2.set_title("Distribution of land and sea passes")

    # plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "landSea.png"), bbox_inches='tight')