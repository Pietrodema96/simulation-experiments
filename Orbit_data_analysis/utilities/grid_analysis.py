from mpl_toolkits.basemap import Basemap
import numpy as np
import math
import matplotlib.pyplot as plt
from numpy.random import uniform
from matplotlib.patches import Polygon
import utilities.spaceConv_utilities as SCU
import utilities.plotting_utilities_GT as PUGT
import utilities.AstroConstants as AC


# -----------------------
# Single satellite and constellation performances
# -----------------------
# This is a brief collection of the most useful grids developed along the AIKO projects
# These grids are useful for performance inspections related to Earth observation
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 20/03/2022
#
#---------------------------------------------------




def grid_implementation(grid_type, minLat, minLon, maxLat, maxLon, discrLat, discrLon):
    """
    TYPE OF THE GRID FOR THE ANALYSIS:
    ------------------------------------
    Types implemented:
    1. 'equal' = equally spaced grid depending from degs in lat and lon
    2. 'grad' = gradient increas spaced grid
    3. 'neareq' = near equal sizes of cases, with cosine inverse proportionality
    ------------------------------------
    The function implements the grid for performance analysis:
    
    OUTPUT:
    lat and lons of centroids on the map
    """

    if grid_type == 'equal':
        # Equally spaced grid algorithm

        # Latitudes of points:
        # lat = np.arange(minLat+discrLat, maxLat, discrLat) 
        lat = np.arange(minLat+discrLat, maxLat+discrLat, discrLat) - np.ones(len(np.arange(minLat+discrLat, maxLat+discrLat, discrLat)))*(discrLat)/2 # [deg]

        # Longitudes of points:
        # lon = np.arange(minLon+discrLon, maxLon, discrLon) 
        lon = np.arange(minLon+discrLon, maxLon+discrLon, discrLon) - np.ones(len(np.arange(minLon+discrLon, maxLon+discrLon, discrLon)))*(discrLon)/2 # [deg]


        return lat, lon


    if grid_type == 'grad':
        # Gradient spaced grid: multiplication gradient 
        # to get the number of points in each row

        # Gradient:
        grad_mult = 1.04

        # DRAW CENTRAL POINTS OF GRID:
        n_Lat_points = int((abs(minLat)+maxLat)/discrLat ) 
        n_Lon_points = int((abs(minLon)+maxLon)/discrLon ) 

        max_numb_lon = round(n_Lon_points * grad_mult**( (abs(minLat) + abs(maxLat)) / ( 2* discrLat ) )) # max number of points in latitude
        # print(max_numb_lon)
        lon = np.zeros((n_Lat_points, max_numb_lon)) # empty matrix for points

        lent = np.zeros(int(n_Lat_points/2))

        for kk in range(0, int(n_Lat_points/2)):

            # CENTRAL POINTS OF GRID:
            lent[kk] = len(np.arange(minLon + discrLon, maxLon + discrLon, discrLon))
            lon[kk, 0:int( lent[kk] )] = np.arange(minLon + discrLon, (maxLon + discrLon), discrLon) - np.ones(int(lent[kk]))*(discrLon)/2
            lon[n_Lat_points-kk-1, 0:int(lent[kk])] = np.arange(minLon + discrLon, (maxLon + discrLon), discrLon) - np.ones(int(lent[kk]))*(discrLon)/2
            # print(lon[kk, 0:lent])
            # print(lon[kk+int(n_Lat_points/2), 0:lent])

            n_Lon_points = round(n_Lon_points * grad_mult) # updated number of points per row
            discrLon = ( maxLon + abs(minLon) )/n_Lon_points # updated range of lon per rectangle in the row
            
        size = int(sum(lent))
        lat = np.arange(minLat + discrLat, maxLat + discrLat, discrLat) # vector of latitudes
        lat = np.arange(minLat+discrLat, maxLat+discrLat, discrLat) - np.ones(len(np.arange(minLat+discrLat, maxLat+discrLat, discrLat)))*(discrLat)/2
        # print(lat)
        return lon, lat, size # latitudes are just given by the indexes


    if grid_type == 'neareq':
        # Near equally spaced grid, with number of points proportional to the latitude
        # cosine


        lats = np.arange(minLat+discrLat, maxLat+discrLat, discrLat) # points' lat
        lons = np.arange(minLon+discrLat, maxLon+discrLat, discrLat) # points' lon

        LATS = lats - np.ones(len(lats))*(discrLat)/2 # effective lattitudes [lats x 0]
        LONS = np.zeros([len(LATS), round((abs(minLon)+abs(maxLon))/discrLat)]) # initialization of the matrix, with the max numb of lons ( @ the equator )
        # LONS = np.full((len(LATS), round((abs(minLon)+abs(maxLon))/discrLat)), False) # initialization of the matrix, with the max numb of lons ( @ the equator )

        cells=np.zeros(int(len(LATS)/2))
        for ii in range(0, int(len(LATS)/2)-1):

            var = (discrLat) / (math.cos( (LATS[len(LATS/2)-(ii+1)]) * AC.D2R))

            cell = 360/var
            while (360%cell) != 0:
                var = round(var) +1
                cell = round(360/var)
            cells[ii] = round(cell) # number of cells per row (half latitudes)
            discr = round((abs(minLon)+abs(maxLon))/int(cells[ii])) # discretization per each row


            # Writing the longitudes per row:
            ll = np.arange(minLon+discr, maxLon+discr, int(discr)) - np.ones(len(np.arange(minLon+discr, maxLon+discr, int(discr))))*(discr)/2
            find_zero = np.where(ll == 0.0)[0]
            ll[find_zero] = 1e-10 # avoid critical case where lon is zero
            LONS[ii, 0:int(cells[ii])] = ll
            LONS[(-1)-ii, 0:int(cells[ii])] = ll


        # Equatorial cases:
        cells[-1] = len(lons)
        discr = (abs(minLon)+abs(maxLon))/int(cells[-1])
        LONS[int(len(LATS)/2)-1, 0:int(cells[-1])] = np.arange(minLon+discr, maxLon+discr, int(discr)) - np.ones(len(np.arange(minLon+discr, maxLon+discr, int(discr))))*(discr)/2
        LONS[int(len(LATS)/2), 0:int(cells[-1])] = np.arange(minLon+discr, maxLon+discr, int(discr)) - np.ones(len(np.arange(minLon+discr, maxLon+discr, int(discr))))*(discr)/2

        # Total number of points:
        size = int(sum(cells)*2)

        return LONS, LATS, size




def grid_passes(grid_type, step, to, latGRID, lonGRID, rr, semi_angle, duration_plot_h, size, points):
    
    """    
    Computes performances of passes given the grid

    INPUT: 
    grid_type: type of the grid (equally spaced, gradient, ...) [str]
    step = time_step of simulation [s]
    to = initial time [mjds]
    latSAT/lonSAT: latitudes/longitudes of satellite by time [deg]
    latGRID/lonGRID: latitudes/longitudes of grid centroids [deg]
    rr: ECi sat positions [km]
    semi_angle: internal angle [deg]
    duration_plot_h = plot duration  [h]
    size = number of points in grid
    points = number of time points in the simulation

    """
    

    # Computing lat and lon for the sat:
    latSAT, lonSAT = SCU.GrTr_fun(step, rr, to, duration_plot_h)
    iterations = points
    # iterations = 1
    #print(lonSAT[0:20])


    if grid_type == 'equal':

        pass_lat = np.zeros((iterations, 15)) # storing at each time step (rows), the lat (colum) of points inside the FOV
        pass_lon = np.zeros((iterations, 15)) # storing at each time step (rows), the lon (colum) of points inside the FOV
        pass_lon[:] = np.NaN
        pass_lat[:] = np.NaN
        occ = 0 # occupation

        lon_fin = np.zeros(iterations) # longitude reached from the center of the FOV [deg]
        lat_fin = np.zeros(iterations) # latitude reached from the center of the FOV [deg]
        size = (len(latGRID))*(len(lonGRID)) # number of targets

        #mat_IDs = np.zeros((size, iterations)) # matrix used for passes
        mat_IDs = np.full((size, iterations), False) # matrix used for passes: [n_targets x iterations] BOOLEAN VERSION

        corr_mat = np.zeros(( len(latGRID), len(lonGRID) )) # matrix of correspondence between the ID and lat lon for targets
        progr = 0 # progress number for IDs

        # Creat IDs and correspondence matrix:
        for i_la in range(0, len(latGRID)): # running on all the latitudes of the grid

            for i_lo in range(0, len(lonGRID)): # running on all the longitudes of the grid
                progr += 1 # ID of the points
                corr_mat[i_la, i_lo] = int(progr) # correspondence matrix 
        

        # Min number of grid points:-----------------------
        rrr, alpha= PUGT.sat_coverage(semi_angle, rr[2, :])
        # print(rrr)
        # Min number of grid points:
        E_area = 510067420 # [km2]
        sat_cov = 2 * np.pi * AC.R_E**2 * ( 1-math.cos(alpha*AC.D2R)) # [km2]
        min_n_gridP = int( E_area/sat_cov ) # [-]
        print('-----------------')
        print('Min number of grid points: %f' %min_n_gridP)
        grid_points = len(latGRID)*len(lonGRID)
        if grid_points < min_n_gridP:
            print('!!! Not enough grid points !!!')
        print('Actual grid points %f' %grid_points)
        #---------------------------------------------------

        # Running over the sat locations:
        for i in range(0, iterations):

            # Plotting thge FOV circle radius and the internal Earth angle of coverage:
            R, alpha= PUGT.sat_coverage(semi_angle, rr[i, :])

            # Computing final lat and lon with Haversine results:
            lon_fin[i] = SCU.haversine_INV1(lonSAT[i], latSAT[i], R)
            lat_fin[i] = SCU.haversine_INV2(lonSAT[i], latSAT[i], R)

            # Computing difference of lat and lon with Haversine results:
            diffLA = abs(abs(latSAT[i])-abs(lat_fin[i]))
            diffLO = abs(abs(lonSAT[i])-abs(lon_fin[i]))

            for i_la in range(0, len(latGRID)): # running on all the latitudes of the grid

                if abs(abs(latGRID[i_la])-abs(latSAT[i])) <= diffLA: # FIRST FILTER: if delta_lat is smaller...
                    
                    # countLAT +=1
                    for i_lo in range(0, len(lonGRID)):

                        if abs(abs(lonGRID[i_lo])-abs(lonSAT[i])) <= diffLO: # SECOND FILTER: if delta_lon is smaller...
                            # Compute the effective distance in case it passes previous tests [km]
                            dist = SCU.haversine(lonSAT[i], latSAT[i], lonGRID[i_lo], latGRID[i_la]) # [km]

                            if dist < abs(R):
                                pass_lat[i, occ] = i_la # store latitude [index]
                                pass_lon[i, occ] = i_lo # store longitude [index]

                                # print(pass_lat[i, occ])
                                # print(pass_lon[i, occ])

                                progr = int(corr_mat[i_la, i_lo])
                                # mat_IDs[progr-1, i] = 1 # 1 if there is a pass at that time and that
                                mat_IDs[progr-1, i] = True # True if there is a pass at that time and that !!! BOOLEAN VERSION !!!
                                
                                occ += 1 # increasing row positions...

            occ = 0 # reinitialize the occupation position...
        



    if grid_type == 'grad' or grid_type == 'neareq':

        pass_lat = np.zeros((iterations, 15)) # storing at each time step (rows), the lat (colum) of points inside the FOV
        pass_lon = np.zeros((iterations, 15)) # storing at each time step (rows), the lon (colum) of points inside the FOV
        pass_lon[:] = np.NaN
        pass_lat[:] = np.NaN
        occ = 0 # occupation

        lon_fin = np.zeros(iterations) # longitude reached from the center of the FOV [deg]
        lat_fin = np.zeros(iterations) # latitude reached from the center of the FOV [deg]

        if grid_type == 'grad':
            mat_IDs = np.full((2*size, iterations), False) # matrix used for passes: [n_targets x iterations] BOOLEAN VERSION
        elif grid_type == 'neareq':
            mat_IDs = np.full((size, iterations), False) # matrix used for passes: [n_targets x iterations] BOOLEAN VERSION


        corr_mat = np.zeros((len(lonGRID[:, 0]), len(lonGRID[0, :]))) # matrix of correspondence between the ID and lat lon for targets
        progr = 0 # progress number for IDs
        #i_lon=0

        # Creat IDs and correspondence matrix:
        for i_la in range(0, len(lonGRID[:, 0])): # running on all the latitudes of the grid
            for i_lo in range(0, len(lonGRID[i_la, :])):

                if lonGRID[i_la, i_lo]:
                    progr += 1 # ID of the points
                    corr_mat[i_la, i_lo] = int(progr) # correspondence matrix 
                    #i_lon += 1
                else: 
                    continue # pass to the next iteration
        

        # Min number of grid points:-----------------------
        rrr, alpha= PUGT.sat_coverage(semi_angle, rr[2, :])
        # print(rrr)
        # Min number of grid points:
        E_area = 510067420 # [km2]
        sat_cov = 2 * np.pi * AC.R_E**2 * ( 1-math.cos(alpha*AC.D2R)) # [km2]
        min_n_gridP = int( E_area/sat_cov ) # [-]
        print('-----------------')
        print('Min number of grid points: %d' %int(min_n_gridP))
        grid_points = progr
        if grid_points < min_n_gridP:
            print('!!! Not enough grid points !!!')
        print('Actual grid points %d' %int(grid_points))
        #---------------------------------------------------

        # Running over the sat locations:
        for i in range(0, iterations):

            # Plotting thge FOV circle radius and the internal Earth angle of coverage:
            R, alpha= PUGT.sat_coverage(semi_angle, rr[i, :])
            
            # Computing final lat and lon with Haversine results:
            lon_fin[i] = SCU.haversine_INV1(lonSAT[i], latSAT[i], R)
            lat_fin[i] = SCU.haversine_INV2(lonSAT[i], latSAT[i], R)

            # Computing difference of lat and lon with Haversine results:
            diffLA = abs(abs(latSAT[i])-abs(lat_fin[i]))
            diffLO = abs(abs(lonSAT[i])-abs(lon_fin[i]))

            for i_la in range(0, len(lonGRID[:, 0])): # running on all the latitudes of the grid

                if abs(abs(latGRID[i_la])-abs(latSAT[i])) <= diffLA: # FIRST FILTER: if delta_lat is smaller...
                  
                    # countLAT +=1
                    for i_lo in range(0, len(lonGRID[i_la, :])):
                        if lonGRID[i_la, i_lo]:
                            
                            if abs(abs(lonGRID[i_la, i_lo])-abs(lonSAT[i])) <= diffLO: # SECOND FILTER: if delta_lon is smaller...
                                # Compute the effective distance in case it passes previous tests [km]
                                dist = SCU.haversine(lonSAT[i], latSAT[i], lonGRID[i_la, i_lo], latGRID[i_la]) # [km]

                                if dist < abs(R):
                                    pass_lat[i, occ] = i_la # store latitude [index]
                                    pass_lon[i, occ] = i_lo # store longitude [index]

                                    progr = int(corr_mat[i_la, i_lo])
                                    # mat_IDs[progr-1, i] = 1 # 1 if there is a pass at that time and that
                                    mat_IDs[progr-1, i] = True # True if there is a pass at that time and that !!! BOOLEAN VERSION !!!
                                    #print(mat_IDs[progr-1, i])
                                    occ += 1 # increasing row positions...
                        else:
                            continue

            occ = 0 # reinitialize the occupation position...


    return pass_lat, pass_lon, lon_fin, lat_fin, lonSAT, latSAT, mat_IDs, corr_mat


# def grid_passesConst(rr1, rr2, rr3, rr4, grid_type, step, to, latGRID, lonGRID, semi_angle, duration_plot_h, size):

#     pas_lat1, pas_lon1, lon_fin1, lat_fin1, lonSAT1, latSAT1, mat_IDs1, _, corr_mat, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr1, semi_angle, duration_plot_h, size) # SAT 1
#     pas_lat2, pas_lon2, lon_fin2, lat_fin2, lonSAT2, latSAT2, mat_IDs2, _, _, _, _, _= grid_passes(grid_type, step, to, latGRID, lonGRID, rr2, semi_angle, duration_plot_h, size) # SAT 2
#     pas_lat3, pas_lon3, lon_fin3, lat_fin3, lonSAT3, latSAT3, mat_IDs3, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr3, semi_angle, duration_plot_h, size) # SAT 3
#     pas_lat4, pas_lon4, lon_fin4, lat_fin4, lonSAT4, latSAT4, mat_IDs4, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr4, semi_angle, duration_plot_h, size) # SAT 4

#     mat_IDs = ((mat_IDs1 | mat_IDs2) | mat_IDs3) | mat_IDs4 # logical OR between matrices

#     mean_rev, Rev_Min, Rev_Max, Rev_Numb = revisit(mat_IDs, step)

#     return mean_rev, corr_mat, latSAT1, lonSAT1, latSAT2, lonSAT2, latSAT3, lonSAT3, latSAT4, lonSAT4, lon_fin1, lat_fin1 \
#             , lon_fin2, lat_fin2, lon_fin3, lat_fin3, lon_fin4, lat_fin4, pas_lat1, pas_lon1, pas_lat2, pas_lon2, pas_lat3, \
#                  pas_lon3, pas_lat4, pas_lon4, Rev_Min, Rev_Max, Rev_Numb



# def grid_passesConst_6(rr1, rr2, rr3, rr4, rr5, rr6, grid_type, step, to, latGRID, lonGRID, semi_angle, duration_plot_h, size):
#     # 6 satellites
#     pas_lat1, pas_lon1, lon_fin1, lat_fin1, lonSAT1, latSAT1, mat_IDs1, _, corr_mat, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr1, semi_angle, duration_plot_h, size) # SAT 1
#     pas_lat2, pas_lon2, lon_fin2, lat_fin2, lonSAT2, latSAT2, mat_IDs2, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr2, semi_angle, duration_plot_h, size) # SAT 2
#     pas_lat3, pas_lon3, lon_fin3, lat_fin3, lonSAT3, latSAT3, mat_IDs3, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr3, semi_angle, duration_plot_h, size) # SAT 3
#     pas_lat4, pas_lon4, lon_fin4, lat_fin4, lonSAT4, latSAT4, mat_IDs4, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr4, semi_angle, duration_plot_h, size) # SAT 4
#     pas_lat5, pas_lon5, lon_fin5, lat_fin5, lonSAT5, latSAT5, mat_IDs5, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr5, semi_angle, duration_plot_h, size) # SAT 5
#     pas_lat6, pas_lon6, lon_fin6, lat_fin6, lonSAT6, latSAT6, mat_IDs6, _, _, _, _, _ = grid_passes(grid_type, step, to, latGRID, lonGRID, rr6, semi_angle, duration_plot_h, size) # SAT 6

#     mat_IDs = (((((mat_IDs1 | mat_IDs2) | mat_IDs3) | mat_IDs4) | mat_IDs5) | mat_IDs6) # logical OR between matrices

#     mean_rev, Rev_Min, Rev_Max, Rev_Numb = revisit(mat_IDs, step, duration_plot_h)

#     return  mean_rev, corr_mat, latSAT1, lonSAT1, latSAT2, lonSAT2, latSAT3, lonSAT3, latSAT4, lonSAT4, latSAT5, lonSAT5, latSAT6, lonSAT6, lon_fin1, lat_fin1 \
#             , lon_fin2, lat_fin2, lon_fin3, lat_fin3, lon_fin4, lat_fin4, lon_fin5, lat_fin5, lon_fin6, lat_fin6, pas_lat1, pas_lon1, pas_lat2, pas_lon2, pas_lat3, \
#                  pas_lon3, pas_lat4, pas_lon4, pas_lat5, pas_lon5, pas_lat6, pas_lon6, Rev_Min, Rev_Max, Rev_Numb


def grid_passesConst_R(RRx, RRy, RRz, grid_type, step, to, latGRID, lonGRID, semi_angle, duration_plot_h, size, sats, points):
    """
    Computation of the passes and revisit time for all
    the constellation satellites merging results
    together for each satellite

    INPUT:
    RRx, RRy, RRz: x y z for all satellites, (instants x n_sats)
    grid_type = type of the grid empleyed just for the passes
    step = time step [sec]
    to = initial date in julian days
    latGRID = latitudes of the grid , matrix [deg]
    lonGRID = longitudes of the grid , matrix [deg]
    semi_angle = FOV of the satellite as semi angle [deg] 
    duration_plot_h = plot duration [h]
    size = number of points in the grid
    sats = number of satellites
    points = instants in the simulation
    """
    RR = np.zeros((len(RRx[:,0]), 3))
    latSAT = np.zeros((points, sats)) # latitude of constellation (iterations x sats)
    lonSAT = np.zeros((points, sats)) # longitude of constellation (iterations x sats)
    lat_fin = np.zeros((points, sats)) # latitude of constellation (iterations x sats)
    lon_fin = np.zeros((points, sats)) # longitude of constellation (iterations x sats)
    pas_lat = np.zeros((points*sats, 15)) # pass latitude of constellation (iterations x sats)
    pas_lon = np.zeros((points*sats, 15)) # pass longitude of constellation (n_points*n_sats x 15); 15 is the max number of
                                                        # contacts per time_instant


    mat_ID = {} # VARIABLE HOLDER

    for i in range(0, sats):
        RR[:, 0] = RRx[:, i]
        RR[:, 1] = RRy[:, i]
        RR[:, 2] = RRz[:, i]
        # Computation of passes per satellite
        pas_lat[points*(i):points*(i+1), :], pas_lon[points*(i):points*(i+1), :], lon_fin[:, i], lat_fin[:, i], lonSAT[:, i], \
            latSAT[:, i], mat_IDs_New, corr_mat= grid_passes(grid_type, step, to, latGRID, lonGRID, RR, semi_angle, duration_plot_h, size, points) # SAT 1
        
        mat_ID['mat_ID_S' + str(i+1)] = mat_IDs_New # !!! saves each sat passes matrix !!!
        
        if i == 0:
            mat_IDs1_old = mat_IDs_New
        else:
            # Merging matrixes
            mat_IDs1_old = (mat_IDs1_old | mat_IDs_New) # logical OR between matrices


    locals().update(mat_ID) # update the variable holder
    # print(mat_ID)

    # Computation of the revisit time with all sats contributions:
    mean_rev, Rev_Min, Rev_Max, Rev_Numb = revisit(mat_IDs1_old, step, duration_plot_h)


    return  mean_rev, corr_mat, latSAT, lonSAT, lon_fin, lat_fin, pas_lat, pas_lon, Rev_Min, Rev_Max, Rev_Numb, mat_ID, mat_IDs1_old





def revisit(mat_IDs, step, duration):

    """    
    Revisit time computation based on the grid employed

    INPUTS:
    mat_IDs = IDs matrix accounting for each time step (column) passes over each target (row)
    step = time step of the simulation [s]
    duration = simulation duration [h]
    """

    numb_days = int(round(duration/24)) # number of days in the simulation
    
    mean_rev = np.zeros(len(mat_IDs[:, 0]))
    mean_rev[:] = np.NaN
    
    # collects the min revisit time for each target
    Rev_Min = np.zeros(len(mat_IDs[:, 0])) 
    Rev_Min[:] = np.NaN

    # collects the max revisit time for each target
    Rev_Max = np.zeros(len(mat_IDs[:, 0])) 
    Rev_Max[:] = np.NaN

    # Av number of passes per day
    Rev_Numb = np.zeros(len(mat_IDs[:, 0])) 
    Rev_Numb[:] = np.NaN

    for j in range(0, len(mat_IDs[:, 0])): # over targets

        # vect_pass = np.nonzero(mat_IDs[j, :])[0]*step # durations [sec]
        vect_pass = np.where(mat_IDs[j, :])[0]*step # durations [sec] !!! BOOLEAN VERSION !!!

        if len(vect_pass)>1 :
            pas = vect_pass[0]
            count = 0 # for the mean computation
            rev = 0

            # collects the passes for each target (temporary)
            Rev_V = np.zeros(len(vect_pass)) 
            Rev_V[:] = np.NaN

            for i in range(0, len(vect_pass)):
                if (vect_pass[i]-pas) > step+step/3:
                    count += 1
                    rev += (vect_pass[i]-pas) # instant revisit time [s]
                    Rev_V[i] = (vect_pass[i]-pas) # collect the passes' revisit
                    pas = vect_pass[i]
                else:
                    pas = vect_pass[i] # in order to avoiun consecutive pass computation
            
            if rev > 0:
                mean_rev[j] = rev/( 3600 * count ) # mean revisit time [h]
                Rev_Min[j] = np.nanmin(Rev_V) /3600 # min revisit time [h]
                Rev_Max[j] = np.nanmax(Rev_V) /3600 # max revisit time [h]
                if numb_days < 1:
                    print('!!! Propagation shorter than 1 day !!!')
                    Rev_Numb[j] = 0
                else:
                    Rev_Numb[j] = count/numb_days

    return mean_rev, Rev_Min, Rev_Max, Rev_Numb





def latency_R(mat_IDs, step, duration, azimuths_IN, ID_sats, sats):

    """    
    Latency computation wrt a single GS

    INPUTS:
    mat_IDs = IDs matrix accounting for each time step (column) passes over each target (row)
    step = time step of the simulation [s]
    duration = simulation duration [h]
    azimuths_IN = entering branch of satellites' azimuths wrt the GS [deg] ; (instants x n_sats)
                  is NaN where there is no contact and value where contact 
    ID_sats = dictionary of all mat_IDs of each satellite
    sats = numb of satellites considered

    """

    numb_days = int(round(duration/24)) # number of days in the simulation
    # collects the mean latency
    Mean_latency = np.zeros(len(mat_IDs[:, 0])) 
    Mean_latency[:] = np.NaN
    # collects the min latency
    Min_latency = np.zeros(len(mat_IDs[:, 0])) 
    Min_latency[:] = np.NaN
    # collects the max latency
    Max_latency = np.zeros(len(mat_IDs[:, 0])) 
    Max_latency[:] = np.NaN
    # collects the median latency
    Median_latency = np.zeros(len(mat_IDs[:, 0])) 
    Median_latency[:] = np.NaN


    for j in range(0, len(mat_IDs[:, 0])): # over targets

        vect_pass = np.where(mat_IDs[j, :])[0]*step # durations [sec] !!! BOOLEAN VERSION !!!

        if len(vect_pass)>1 :
            pas = vect_pass[0]
            count = 0 # for the mean computation
            rev = 0

            # collects the passes for each target (temporary)
            Rev_V = np.zeros(len(vect_pass)) 
            Rev_V[:] = np.NaN
            lat_V = np.zeros(len(vect_pass)) 
            lat_V[:] = np.NaN

            for i in range(0, len(vect_pass)):
                if (vect_pass[i]-pas) > step+step/3:
                    count += 1
                    rev += (vect_pass[i]-pas) # instant revisit time [s]
                    Rev_V[i] = (vect_pass[i]-pas) # collect the passes' revisit
                    pas = vect_pass[i]

                    for ns in range(0, sats):
                        mat_ID = ID_sats['mat_ID_S' + str(ns+1)] # mat of the single sat inspection
                        idx = int(vect_pass[i]/step) # retrieve the index of passes
                        if mat_ID[j, idx]:
                            break
                    # print(int(vect_pass[i]/step))
                    # print(azimuths_IN[:, 0])
                    for con in range(int(vect_pass[i]/step), len(azimuths_IN[:, 0])):
                        if np.isnan(azimuths_IN[con, ns]):
                            continue
                        else:
                            lat_V[i] = (con - vect_pass[i]/step)*step + Rev_V[i] # sec
                            break

                else:
                    pas = vect_pass[i] # in order to avoiun consecutive pass computation
            
            if rev > 0:
                Mean_latency[j] = np.nansum(lat_V)/(3600*count) # [h]
                Min_latency[j] = np.nanmin(lat_V)/(3600*count) # [h]
                Max_latency[j] = np.nanmax(lat_V)/(3600*count) # [h]
                Median_latency[j] = np.nanmedian(lat_V)/(3600*count) # [h]


    return  Mean_latency, Min_latency, Max_latency, Median_latency








        

