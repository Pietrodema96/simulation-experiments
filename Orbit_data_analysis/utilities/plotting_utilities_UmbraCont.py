
from cmath import pi
import math, os
from turtle import color, width
import matplotlib.pyplot as plt
import matplotlib 
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import utilities.AstroConstants as AC
import utilities.CarKep as C2K
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU
import matplotlib.gridspec as gridspec
import utilities.plotting_utilities_GT as PUGT
import utilities.styles as ST

m2km = 1000



def plot_contacts(contactsM, durations, IDs, numb, title, progrs, countsE, periodicity, mission):

    """    
    Plot of contacts distributin in times
    plot of one or multiple GSs depending from the GS_IDs
    contactsM = matrix of contacts beginning elapsed-times [n x len(GS_IDs)] [sec]--only the ID written are considered
    durations =  durations for the relative GS contacts [min]
    IDs = IDs of the TG/GS considered
    numb = to select the number of data to plot (just one or partially the number of GS data)
    progrs = progress number for plot
    countsE = matrix[0 x nIDs] number of events for each station/target
    periodicity = days after which mission repeats
    mission = mission name
    """


    if title == 'TG-passes':
        ID_plot = 'TG'
        filter1 = 0.3 # [min]
        filter2 = 0.5 # [min]
        filter3 = 1.5 # [min]
        filter4 = 2. # [min]
        filter5 = 2.5 # [min]
        fil = [str(filter1), str(filter2),str(filter3), str(filter4), str(filter5)]


    if title == 'GS-contacts':
        ID_plot = 'GS'
        filter1 = 2 # [min]
        filter2 = 5 # [min]
        filter3 = 7 # [min]
        filter4 = 10 # [min]
        filter5 = 14 # [min]
        fil = ['2', '5', '7', '10', '14']

    # Periodicity computations:
    periodicity = periodicity*AC.sec_day # [sec]


    # fig, ax = plt.subplots(2, 2, figsize=(16,9))

    #-----------------------------
    if numb != 'all':

        fig = plt.figure(constrained_layout=True, figsize=(15,7))
        gs = fig.add_gridspec(1, 2,
                        width_ratios=[2,1]
                        )
        ax1 = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])
        ymin, ymax = plt.ylim()


        CON = contactsM[:, numb-1] # istant where occur the event [sec]
        DUR = durations[:, numb-1] # durations of contacts [min]
        ax1.bar(CON/(AC.sec_day), DUR, width=0.2)

        # Percentages of events for each range:
        count1 = len([jj for jj in DUR if jj >= filter1]) * 100 /countsE[numb-1]
        count2 = len([jj for jj in DUR if jj >= filter2]) * 100 /countsE[numb-1]
        count3 = len([jj for jj in DUR if jj >= filter3]) * 100 /countsE[numb-1]
        count4 = len([jj for jj in DUR if jj >= filter4]) * 100 /countsE[numb-1]
        count5 = len([jj for jj in DUR if jj >= filter5]) * 100 /countsE[numb-1]
        cc = [count1, count2, count3, count4, count5]

        MAX = max(DUR)
        MEAN = np.mean(DUR[np.nonzero(DUR)])

        # ax2.bar(fil, cc, width=0.6)
        # ax2.text(fil, cc, cc)


        width1 = 0.4 # for subplot 4
        r = np.arange(len(fil))

        for i in range(0, len(fil)):

            ax2.bar(r[i], cc[i], width=width1, color='b')
            ax2.text(r[i], cc[i], cc[i] )

        ax2.set_xticks(r, fil)

        ax1.axvline(x=periodicity/(AC.sec_day), color='k', linewidth=5) # highlight the periodicity
        ax1.annotate('PERIODICITY', xy=(periodicity/(AC.sec_day), ymax*2), xytext=(10, 25), textcoords='offset points',
             rotation=90, va='bottom', ha='center', annotation_clip=False)
        

        plt.legend(loc='lower left')
        plt.grid(True)
        plt.suptitle("Distribution of %s" %title)
        ax1.axhline(y=filter1, color='k', linestyle='-')
        ax1.axhline(y=filter2, color='k', linestyle='-')
        ax1.axhline(y=filter3, color='k', linestyle='-')
        ax1.axhline(y=filter4, color='k', linestyle='-')
        ax1.axhline(y=filter5, color='k', linestyle='-')
        ax1.set_ylabel('Duration [min]')
        ax1.set_xlabel('Elapsed time [days]')
        ax1.set_title('%s distribution' %IDs[numb-1])
        ax1.grid()

        ax2.set_ylabel('% (of the total of events)')
        ax2.set_xlabel('threshold [min]')
        ax2.set_title('Percentage of events longer than a threshold')
        ax2.grid()



    if numb =='all':

        fig = plt.figure(constrained_layout=True, figsize=(15,10))
        gs = fig.add_gridspec(2, 2,
                        width_ratios=[2,1],
                        height_ratios=[2,2]
                        )
        ax1 = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])
        ax3 = plt.subplot(gs[2])
        ax4 = plt.subplot(gs[3])
        ymin, ymax = plt.ylim()
        width1 = 0.25 # for subplot 4
        r = np.arange(len(IDs))

        for i in range(0, len(IDs)):
            CON = contactsM[:, i] # istant where occur the event [sec]
            DUR = durations[:, i] # durations of contacts [min]
            ax1.bar(CON/(AC.sec_day), DUR, width=0.2)
            ind_lo = np.where(CON < periodicity)[0]
            ax3.bar(CON[ind_lo]/(AC.sec_day), DUR[ind_lo], width=0.2) # just in the periodicity


        if len(IDs) > 10:
            for i in range(0, 11):
                CON = contactsM[:, i] # istant where occur the event [sec]
                DUR = durations[:, i] # durations of contacts [min]
                MIN = min(DUR) # minimum duration
                MAX = max(DUR) # max duration
                MEAN = np.mean(DUR[np.nonzero(DUR)]) # average duration
                ax1.bar(CON/(AC.sec_day), DUR, width=0.2)
                ax2.bar(IDs[i], countsE[i], width=0.1, color='blue')
                ax2.text(IDs[i], countsE[i], countsE[i])
                ax4.bar(r[i]-width1, MAX, width=width1, color='r')
                ax4.text(r[i]-width1, MAX, round(MAX, 3) )
                ax4.bar(r[i], MEAN, width=width1, color='b')
                ax4.text(r[i], MEAN, round(MEAN, 3) )

                ind_lo = np.where(CON < periodicity)[0]
                ax3.bar(CON[ind_lo]/(AC.sec_day), DUR[ind_lo], width=0.2) # just in the periodicity

            ax4.set_ylabel('duration [min]')
            ax4.set_xlabel('IDs')
            ax4.set_title('Statistical results (first 10)')
            ax4.grid()
            ax4.set_xticks(r-width1/2, IDs, rotation=45)
            
            
            ax2.set_ylabel('number of events')
            ax2.set_xlabel('IDs')
            ax2.set_title('Number of events for first 10 IDs')
            ax2.set_xticks(IDs[0:11], IDs[0:11], rotation=45)
            ax2.grid()

        else:

            for i in range(0, len(IDs)):
                CON = contactsM[:, i] # istant where occur the event [sec]
                DUR = durations[:, i] # durations of contacts [min]
                MIN = min(DUR) # minimum duration
                MAX = max(DUR) # max duration
                MEAN = np.mean(DUR[np.nonzero(DUR)]) # average duration
                ax2.bar(IDs[i], countsE[i], width=0.1, color='blue')
                ax2.text(IDs[i], countsE[i], countsE[i])
                ax4.bar(r[i]-width1, MAX, width=width1, color='r')
                ax4.text(r[i]-width1, MAX, round(MAX, 3) )
                ax4.bar(r[i], MEAN, width=width1, color='b')
                ax4.text(r[i], MEAN, round(MEAN, 3) )

            ax4.set_ylabel('duration [min]')
            ax4.set_xlabel('IDs')
            ax4.set_title('Statistical results')
            ax4.grid()
            ax4.set_xticks(r-width1/2, IDs, rotation=45)

            ax2.set_ylabel('number of events')
            ax2.set_xlabel('IDs')
            ax2.set_title('Number of events for each ID')
            ax2.set_xticks(IDs, IDs,rotation=45)
            ax2.grid()

        # Just for labelling
        ax4.bar(r[i]-width1, MAX, width=width1, color='r', label='max')
        ax4.bar(r[i], MEAN, width=width1, color='b', label='average')

        ax1.axvline(x=periodicity/(AC.sec_day), color='k', linewidth=5) # highlight the periodicity
        ax1.annotate('PERIODICITY', xy=(periodicity/(AC.sec_day), ymax*2), xytext=(10, 25), textcoords='offset points',
             rotation=90, va='bottom', ha='center', annotation_clip=False)

        plt.legend(loc='lower left')
        plt.grid(True)
        plt.suptitle("Distribution of %s" %title)
        ax1.axhline(y=filter1, color='k', linestyle='-')
        ax1.axhline(y=filter2, color='k', linestyle='-')
        ax1.axhline(y=filter3, color='k', linestyle='-')
        ax1.axhline(y=filter4, color='k', linestyle='-')
        ax1.axhline(y=filter5, color='k', linestyle='-')
        ax1.set_ylabel('Duration [min]')
        ax1.set_xlabel('Elapsed time [days]')
        ax1.set_title('%s distribution' %title)
        ax1.grid()

        ax3.set_ylabel('max duration [min]')
        ax3.set_xlabel('IDs')
        ax3.set_title('Distribution of events in the periodicity time')
        ax3.grid()


    # plt.suptitle("Minimum event duration: %f" %time_filter)
    # plt.text(0.5, 0.5, txt, fontsize = 5, color = 'g')
    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "all_contacts_{}_{}.png").format(ID_plot, progrs), bbox_inches='tight')



def plot_umbra(U_start, U_dur, P_dur_1segm, P_dur_2segm, mission):

    """    
    Plot of umbra distribution by time:

    INPUT:
    U_start = respectively U(Umbra) sec from the beginning
       of the simulation
    U_dur/P_dur = respectively U(Umbra) and P(Penumbra) sec from the beginning
       of the simulation
    time_step = simulation time step [sec]
    """


    fig = plt.figure( figsize=(15,10))

    w = 0.02
    delta_txt = 0.2
    week_times = int(round(len(U_start)/3)) # times the time_step in a week
    someDays_times = int(round(len(U_start)/11)) # times the time_step in 3 days


    max_U = max(U_dur) # maximu umbra duration [min]
    INDmax_U = np.argmax(U_dur) # indexmaximu umbra duration [min]

    print(INDmax_U)
    max_P1 = max(P_dur_1segm)
    max_P2 = max(P_dur_2segm) 
    max_P  = max([max_P1, max_P2])
    if max_P == max_P1:
        INDmax_P = np.argmax(P_dur_1segm)
    else:
        INDmax_P = np.argmax(P_dur_2segm) 

    min_U = min(U_dur) # maximu umbra duration [min]
    INDmin_U = np.argmin(U_dur) # maximu umbra duration [min]

    min_P1 = min(P_dur_1segm)
    min_P2 = min(P_dur_2segm)
    min_P = min([min_P1, min_P2])
    if min_P == min_P1:
        INDmin_P =  np.argmin(P_dur_1segm)
    else:
        INDmin_P =  np.argmin(P_dur_2segm)     

    ax1 = plt.subplot(212)
    ax1.plot(U_start/(AC.sec_day), U_dur)
    ax1.scatter(U_start[INDmax_U]/(AC.sec_day), max_U, color='r', label=('Max umbra: %fmin' %max_U))
    ax1.scatter(U_start[INDmin_U]/(AC.sec_day), min_U, max_U, color='g', label=('Min umbra: %fmin' %min_U))

    # ax1.annotate('Max umbra %.00fmin' %max_U, xy=(U_start[INDmax_U]/(AC.sec_day), max_U), xytext=(U_start[INDmax_U]/(AC.sec_day)+delta_txt, max_U+delta_txt),\
    #     arrowprops=dict(facecolor='black', shrink=0.05))
    # ax1.annotate('Min umbra %.00f min' %min_U, xy=(U_start[INDmin_U]/(AC.sec_day), min_U), xytext=(U_start[INDmin_U]/(AC.sec_day)-delta_txt, min_U+delta_txt),\
    #     arrowprops=dict(facecolor='black', shrink=0.05))


    ax2 = plt.subplot(221)
    ax2.plot(U_start[0: week_times]/(AC.sec_day), U_dur[0: week_times])
    ax2.scatter(U_start[0: week_times]/(AC.sec_day), U_dur[0: week_times], color='k')


    ax3 = plt.subplot(222)
    parax3 = ax3.twinx()
    ax3.bar(U_start[0: someDays_times]/(AC.sec_day), U_dur[0: someDays_times], width=w, color='b', label='umbra')
    parax3.bar(U_start[0: someDays_times]/(AC.sec_day)-w, P_dur_1segm[0: someDays_times], width=w, color='orangered', label='penumbra')
    parax3.bar(U_start[0: someDays_times]/(AC.sec_day)+w, P_dur_2segm[0: someDays_times], width=w, color='orangered')
    parax3.set_ylim([0, 20]) # sec




    plt.legend(loc='lower left')
    plt.grid(True)
    plt.suptitle("Distribution of umbra/penumbra - Mission: {}".format(mission))
    # ax1.axhline(y=31, color='k', linestyle='-')
    ax1.axhline(y=32, color='k', linestyle='--', linewidth=0.5)
    # ax1.axhline(y=33, color='k', linestyle='-')
    ax1.set_ylabel('Umbra Duration [min]')
    ax1.set_xlabel('Elapsed time [days]')
    ax1.set_title('Umbra distribution')
    ax1.legend(loc='lower center', shadow=True)

    ax2.set_ylabel('Umbra Duration [min]')
    ax2.set_xlabel('Elapsed time [days]')
    ax2.set_title('1-week umbra distribution')
    

    ax3.set_ylabel('Duration Umbra [min]')
    parax3.set_ylabel('Duration Penumbra [s]')
    ax3.set_xlabel('Elapsed time [days]')
    ax3.set_title('Umbras and Penumbras')
    ax1.legend(loc='lower center', shadow=True)

    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "all_eclipses.png"), bbox_inches='tight')



def polar_plot_GS(lat_GS, lon_GS, lat_S, lon_S, rrx, rry, rrz, sats, instants, min_el, GS_ID, mission, \
        date0, duration):
    """
    Polar rapresentation of passes over a GS in a range of time
    
    INPUT:
    lat/lon GS: GS latitude and longitude [deg]
    lat/lon SAT: latitude and longitude satellite [deg] (instants x sats)
    rrx/y/z : ECI positions of the satellite in time [km] (instants x sats)
    duration: simulation duration [h]
    instants : simulation times [-]
    min_el : minimum elevation for filtering [deg]
    GS_ID : name of the GS [str]
    mission : mission name [str]
    date0 : launch date [str]

    """

    final_date = TCU.add_sec_dates(date0, duration*3600)
    days = round(duration/24) # days of simulation

    # Computing polar local coordinates for the GS:
    # The GS is centered in the plot
    # azimuths / elevations : (instants x sats)
    azimuths_IN, azimuths_OUT, elevations_IN, elevations_OUT, max_El, count_pass = SCU.filtering_az_el(lat_GS, lon_GS, lat_S, lon_S, rrx, rry, rrz, sats, instants, min_el)
    azimuths_IN = azimuths_IN * AC.D2R
    azimuths_OUT = azimuths_OUT * AC.D2R

    fig = plt.figure(figsize=(14,10))
    gs = gridspec.GridSpec(nrows=2, ncols=2, height_ratios=[1, 1])
    ax1 = plt.subplot(gs[0,:], projection = 'polar')
    ax1.set_title("Polar view over the GS")

    for ss in range(0, sats):
        ax1.plot(-azimuths_IN[:, ss], abs(elevations_IN[:, ss]-90+min_el), 'b')
        ax1.plot(-azimuths_OUT[:, ss], abs(elevations_OUT[:, ss]-90+min_el), 'r')
    ax1.plot(-azimuths_IN[:, ss], abs(elevations_IN[:, ss]-90+min_el), 'b', label='in')
    ax1.plot(-azimuths_OUT[:, ss], abs(elevations_OUT[:, ss]-90+min_el), 'r', label='out')

    ax1.scatter(0, 0, s=170, marker='.', color='firebrick', zorder=10)
    ax1.set_theta_zero_location('N')
    ax1.set_theta_direction(-1)
    ax1.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
    ax1.grid(True, linewidth=0.5, color='k')
    ax1.set_yticks(np.arange(0, 91, 10))
    ax1.set_yticklabels(ax1.get_yticks()[::-1])
    ax1.set_facecolor("whitesmoke")
    ax1.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    ax2 = plt.subplot(gs[1,0])
    max_El = max(max_El) # max between all sats
    count_pass = sum(count_pass) # sum of all passes considering all sats

    # Description:
    str1 = 'Mission name  -{}-  \nn° satellites = {} \nlaunch date:    {} \n simulation duration:  {} days \npropagation time:    {} h'.format(mission, sats, date0, days, duration)
    str2 = 'Number of passes in the \npropagation time (all sats): {}'.format(int(count_pass)) 
    str3 = 'Minimum elevation for a contact: {:.2f} deg \nmax elevation: {:.2f} deg '.format(min_el, max_El)
    ax2.text(0.1, 0.6, str1, size=11, math_fontfamily='dejavuserif')
    ax2.text(0.1, 0.2, str2, size=11, math_fontfamily='dejavuserif')
    ax2.text(0.1, 0.4, str3, size=11, math_fontfamily='dejavuserif')
    
    ax2.axes.xaxis.set_ticklabels([])
    ax2.axes.yaxis.set_ticklabels([])
    ax2.axes.xaxis.set_ticks([])
    ax2.axes.yaxis.set_ticks([])
    ax2.set_facecolor("whitesmoke")
    ax2.set_title("-- Analysis --")


    # LOCAL SUBPLOT: ------------------------
    ax3 = plt.subplot(gs[1,1])
    # ax2 = plt.subplot()

    m = Basemap(projection='cyl',llcrnrlat=lat_GS-30,urcrnrlat=lat_GS+30,\
                llcrnrlon=lon_GS-30,urcrnrlon=lon_GS+30,resolution='c')

    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 20), linewidth=0.4, labels=[1,0,0,1], color='grey')
    m.drawmeridians(np.arange(-180, 180, 10), linewidth=0.4, labels=[1,0,0,1], color='grey')

    pl = 0
    for ii in range(0, sats): # running over sats
        x, y = m(lon_S[0:instants,ii], lat_S[0:instants,ii]) # Sats
        # m.scatter(x, y, marker='.',color=ST.Colors[ii], s=20, label='SC {}'.format(ii+1))
        pl += 1
        if pl > (sats/2):
            PL = int(pl-(sats/2))
            sc = 2
        else:
            PL = pl
            sc = 1
        for slc in PUGT.unlink_wrap(x, [-180, 180]):
            m.plot(x[slc], y[slc], '-', color=ST.Colors[ii], linewidth=1.2)
        if sats == 1:
            m.scatter(360, 360, marker='.',color=ST.Colors[ii], s=30, label="S1_P1")    
        else:
            m.scatter(360, 360, marker='.',color=ST.Colors[ii], s=30, label="S{}_P{}".format(sc, PL))
    x,y = m(lon_GS, lat_GS)
    m.scatter(x,y, marker='*',color='m', s=40, label="GS {}".format(GS_ID))
    circle1 = plt.Circle((x,y), radius=1.5, fill=False, facecolor=None,\
                         edgecolor="m", linewidth=1, zorder=5)
    ax3.add_artist(circle1)  # use this instead
    ax3.set_title('Ground track local')
    ax3.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    plt.suptitle("Passes over Ground station {}".format(GS_ID))

    # plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "polar_plot_{}.png").format(GS_ID), bbox_inches='tight')
    return azimuths_IN