from __future__ import division

from cmath import nan, pi
import math, os
import matplotlib.pyplot as plt
import numpy as np
import utilities.AstroConstants as AC
import utilities.CarKep as C2K
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU

import pylab
import numpy

from matplotlib.patches import Polygon
from mpl_toolkits.basemap import pyproj
from mpl_toolkits.basemap import Basemap
import utilities.styles as ST


m2km = 1000


def poly_basemap_fill(lats, lons, m):
    """
     Draw a custom poloygon on a basemap FILLED
    """
    x, y = m( lons, lats )
    xy = zip(x,y)
    poly = Polygon( list(xy), fill=True, facecolor='grey', edgecolor='saddlebrown', alpha=0.8 , linewidth=2, zorder=10)
    plt.gca().add_patch(poly)
    return poly


def poly_basemap(lats, lons, m):
    """ 
        Draw a custom poloygon on a basemap 
    """
    x, y = m( lons, lats )
    xy = zip(x,y)
    poly = Polygon( list(xy), fill=True, facecolor='coral', edgecolor='saddlebrown', alpha=0.2 , linewidth=2, zorder=10)
    plt.gca().add_patch(poly)
    return poly


def poly_basemap_AX(lats, lons, m, ax):
    """ 
        Draw a custom poloygon on a basemap for subplot
    """
    x, y = m( lons, lats )
    xy = zip(x,y)
    poly = Polygon( list(xy), fill=True, facecolor='coral', edgecolor='saddlebrown', alpha=0.3 , linewidth=2, zorder=3)
    ax.add_patch(poly)
    return poly

class Basemap(Basemap):
    def ellipse(self, x0, y0, a, b, n, ax=None, **kwargs):
        """
        Draws a polygon centered at ``x0, y0``. The polygon approximates an
        ellipse on the surface of the Earth with semi-major-axis ``a`` and 
        semi-minor axis ``b`` degrees longitude and latitude, made up of 
        ``n`` vertices.

        For a description of the properties of ellipsis, please refer to [1].

        The polygon is based upon code written do plot Tissot's indicatrix
        found on the matplotlib mailing list at [2].

        Extra keyword ``ax`` can be used to override the default axis instance.

        Other \**kwargs passed on to matplotlib.patches.Polygon

        RETURNS
            poly : a maptplotlib.patches.Polygon object.

        REFERENCES
            [1] : http://en.wikipedia.org/wiki/Ellipse
        """

        ax = kwargs.pop('ax', None) or self._check_ax()
        g = pyproj.Geod(a=self.rmajor, b=self.rminor)
        # Gets forward and back azimuths, plus distances between initial
        # points (x0, y0)
        azf, azb, dist = g.inv([x0, x0], [y0, y0], [x0+a, x0], [y0, y0+b])
        tsid = dist[0] * dist[1] # a * b

        # Initializes list of segments, calculates \del azimuth, and goes on 
        # for every vertex
        seg = [self(x0+a, y0)]
        AZ = numpy.linspace(azf[0], 360. + azf[0], n)
        for i, az in enumerate(AZ):
            # Skips segments along equator (Geod can't handle equatorial arcs).
            if numpy.allclose(0., y0) and (numpy.allclose(90., az) or
                numpy.allclose(270., az)):
                continue

            # In polar coordinates, with the origin at the center of the 
            # ellipse and with the angular coordinate ``az`` measured from the
            # major axis, the ellipse's equation  is [1]:
            #
            #                           a * b
            # r(az) = ------------------------------------------
            #         ((b * cos(az))**2 + (a * sin(az))**2)**0.5
            #
            # Azymuth angle in radial coordinates and corrected for reference
            # angle.
            azr = 2. * numpy.pi / 360. * (az + 90.)
            A = dist[0] * numpy.sin(azr)
            B = dist[1] * numpy.cos(azr)
            r = tsid / (B**2. + A**2.)**0.5
            lon, lat, azb = g.fwd(x0, y0, az, r)
            x, y = self(lon, lat)

            # Add segment if it is in the map projection region.
            if x < 1e20 and y < 1e20:
                seg.append((x, y))

        poly = Polygon(seg, **kwargs)
        ax.add_patch(poly)

        # Set axes limits to fit map region.
        self.set_axes_limits(ax=ax)

        return poly


def grid_passes(lat_ind, lon_ind, latGRID, lonGRID, lon_fin, lat_fin, lonSat, latSat, mission, duration, grid_type):
    """ 
        Visualization of passes over time with basemap
    """
    
    proj = 'cyl' 
    fig, ax1 = plt.subplots(figsize=(15,7)) # Setting basemap
    # -------------------------------------------------------------------
    if proj == 'cyl': # cylindrical projection
        m = Basemap(projection=proj,llcrnrlat=-90,urcrnrlat=90,\
                    llcrnrlon=-180,urcrnrlon=180,resolution='c')
    if proj == 'eck4': # Eckert projection
        m = Basemap(projection=proj,lon_0=0,resolution='c')
    if proj == 'ortho': # orthonormal projection
        m = Basemap(projection='ortho',lon_0=60,lat_0=-60,resolution='l')
    # --------------------------------------------------------------------

    m.drawcoastlines()
    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w') # orange and light-blue
    # m.bluemarble() # real plot high definition
    m.drawparallels(np.arange(-90, 90, 5), linewidth=0.7, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 10), linewidth=0.7, labels=[1,0,0,1], color='r')

    # DRAW THE GRID FOR THE ANALYSIS:
    # ------------------------------------
    # Types implemented:
    # 1. 'equal' = equally spaced grid depending from degs in lat and lon
    # 2. 'grad' = gradient increas spaced grid
    # 3. 'neareq' = near-equally spaced grid
    # ------------------------------------

    if grid_type == 'equal': # Case of equally spaced grid
        # Plot of base points:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID)):
                m.scatter(lonGRID[lons], latGRID[lats], marker='o', color='b', s=10, latlon=True)
        # Plot of passes-points:
        for its in range(0, len(lat_ind[:, 0])):
            for match in range(0, len(lon_ind[0, :])):
                if lat_ind[its, match] > 0:
                    # print(lat_ind[its, match])
                    eff_lat = latGRID[int(lat_ind[its, match])]
                    eff_lon = lonGRID[int(lon_ind[its, match])]
                    m.scatter(eff_lon, eff_lat, marker='o', color='r', s=10, latlon=True)


    elif grid_type == 'grad' or grid_type == 'neareq': # Case of gradient grid
        # Plot of base points:
        # print(len(latGRID))
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID[lats, :])):
                if lonGRID[lats, lons]:
                    m.scatter(lonGRID[lats, lons], latGRID[lats], marker='o', color='b', s=10, latlon=True)
                else:
                    continue
        # Plot of passes-points:
        for its in range(0, len(lat_ind[:, 0])):
            for match in range(0, len(lon_ind[0, :])):
                if lat_ind[its, match] > 0:
                    
                    # print(lat_ind[its, match])
                    eff_lat = latGRID[int(lat_ind[its, match])]
                    eff_lon = lonGRID[int(lat_ind[its, match]), int(lon_ind[its, match])]
                    m.scatter(eff_lon, eff_lat, marker='o', color='r', s=10, latlon=True)



    for jj in range(0, len(lon_fin)):
        x,y = m(lonSat[jj], latSat[jj], inverse=True)
        x2,y2 = m(lon_fin[jj], lat_fin[jj], inverse=True)

        # print(abs(x2)-abs(x))
        # print(abs(y2)-abs(y))
        # circle = plt.Circle((x, y), abs(x2-x), color='black',fill='b', alpha=0.3)
        # circle = plt.Circle((x, y), abs(x2-x), color='black', fill=False)
        if abs(x) + abs(abs(x2)-abs(x)) > 180:
            continue
        else:
            poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=1, edgecolor='black', \
                linewidth=1)
            ax1.add_patch(poly)
    
    m.scatter(eff_lon, eff_lat, marker='o', color='k', s=10, latlon=True, label='Sat')
    m.scatter(eff_lon, eff_lat, marker='o', color='r', s=10, latlon=True)

    plt.title("Grid equally spaced - propagation time: %f [h]" %duration)
    plt.suptitle('Mission: %s' %mission)
    plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")
    
    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "passes.png"), bbox_inches='tight')
    



# def grid_passesConst(latGRID, lonGRID, latSat1, lonSat1, \
#         latSat2, lonSat2, latSat3, lonSat3, latSat4, lonSat4, lon_fin1, lat_fin1 \
#             , lon_fin2, lat_fin2, lon_fin3, lat_fin3, lon_fin4, lat_fin4, mission, duration, grid_type):
#     # Visualization of passes over time with basemap
#     # Constellation case
    
#     proj = 'cyl' 
#     fig, ax1 = plt.subplots(figsize=(15,7)) # Setting basemap
#     # -------------------------------------------------------------------
#     if proj == 'cyl': # cylindrical projection
#         m = Basemap(projection=proj,llcrnrlat=-90,urcrnrlat=90,\
#                     llcrnrlon=-184,urcrnrlon=184,resolution='c')
#     if proj == 'eck4': # Eckert projection
#         m = Basemap(projection=proj,lon_0=0,resolution='c')
#     if proj == 'ortho': # orthonormal projection
#         m = Basemap(projection='ortho',lon_0=60,lat_0=-60,resolution='l')
#     # --------------------------------------------------------------------

#     m.drawcoastlines()
#     m.drawmapboundary(fill_color='w')
#     m.fillcontinents(color='w') # orange and light-blue
#     # m.bluemarble() # real plot high definition
#     m.drawparallels(np.arange(-90, 90, 30), linewidth=0.4, labels=[1,0,0,1], color='r')
#     m.drawmeridians(np.arange(-180, 180, 60), linewidth=0.4, labels=[1,0,0,1], color='r')

#     # DRAW THE GRID FOR THE ANALYSIS:
#     # ------------------------------------
#     # Types implemented:
#     # 1. 'equal' = equally spaced grid depending from degs in lat and lon
#     # 2. 'grad' = gradient increas spaced grid
#     # 3. 'neareq' = near-equally spaced grid
#     # ------------------------------------

#     if grid_type == 'equal':
#         # Plotting all base grid points:
#         for lats in range(0, len(latGRID)):
#             for lons in range(0, len(lonGRID)):
#                 m.scatter(lonGRID[lons], latGRID[lats], marker='o', color='grey', s=10, latlon=True)

#     elif grid_type == 'grad' or grid_type == 'neareq':
#         # Plot of base points:
#         # print(len(latGRID))
#         for lats in range(0, len(latGRID)):
#             for lons in range(0, len(lonGRID[lats, :])):
#                 if lonGRID[lats, lons]:
#                     m.scatter(lonGRID[lats, lons], latGRID[lats], marker='o', color='grey', s=10, latlon=True)
#                 else:
#                     continue

#     # Footprint SAT1
#     for jj in range(0, len(lon_fin1)):
#         x,y = m(lonSat1[jj], latSat1[jj], inverse=True)
#         x2,y2 = m(lon_fin1[jj], lat_fin1[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=1, edgecolor='k', \
#                 linewidth=1)
#             ax1.add_patch(poly)

#     m.scatter(x, y, marker='o', color='k', s=10, latlon=True, label='Sat-1')


#     # Footprint SAT2
#     for jj in range(0, len(lon_fin2)):
#         x,y = m(lonSat2[jj], latSat2[jj], inverse=True)
#         x2,y2 = m(lon_fin2[jj], lat_fin2[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 177:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=1, edgecolor='b', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='b', s=10, latlon=True, label='Sat-2')



#     # Footprint SAT3
#     for jj in range(0, len(lon_fin3)):
#         x,y = m(lonSat3[jj], latSat3[jj], inverse=True)
#         x2,y2 = m(lon_fin3[jj], lat_fin3[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=1, edgecolor='g', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='g', s=10, latlon=True, label='Sat-3')


#     # Footprint SAT4
#     for jj in range(0, len(lon_fin4)):
#         x,y = m(lonSat4[jj], latSat4[jj], inverse=True)
#         x2,y2 = m(lon_fin4[jj], lat_fin4[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=1, edgecolor='r', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='r', s=10, latlon=True, label='Sat-4')

#     plt.title("Grid equally spaced - propagation time: %f [h]" %duration)
#     plt.suptitle('Mission: %s - Passes' %mission)
#     plt.legend(loc="upper left")

#     plt.show()






# def grid_passesConst_Test(latGRID, lonGRID, latSAT1, lonSAT1, \
#         latSAT2, lonSAT2, latSAT3, lonSAT3, latSAT4, lonSAT4, latSAT5, lonSAT5, latSAT6, lonSAT6, lon_fin1, lat_fin1 \
#             , lon_fin2, lat_fin2, lon_fin3, lat_fin3, lon_fin4, lat_fin4, lon_fin5, lat_fin5, lon_fin6, lat_fin6, mission, duration, \
#                 grid_type, lat_int, lon_int):
#     # Visualization of passes over time with basemap
#     # Constellation case
    
#     proj = 'cyl' 
#     fig, ax1 = plt.subplots(figsize=(15,7)) # Setting basemap
#     # -------------------------------------------------------------------
#     if proj == 'cyl': # cylindrical projection
#         # m = Basemap(projection=proj,llcrnrlat=20,urcrnrlat=80,\
#         #             llcrnrlon=-5,urcrnrlon=30,resolution='c')
#         m = Basemap(projection=proj,llcrnrlat=-90,urcrnrlat=90,\
#                     llcrnrlon=-180,urcrnrlon=180,resolution='c')
#     # --------------------------------------------------------------------

#     m.drawcoastlines()
#     m.drawmapboundary(fill_color='w')
#     m.fillcontinents(color='w') # orange and light-blue
#     # m.bluemarble() # real plot high definition
#     m.drawparallels(np.arange(-90, 90, 30), linewidth=0.4, labels=[1,0,0,1], color='r')
#     m.drawmeridians(np.arange(-180, 180, 60), linewidth=0.4, labels=[1,0,0,1], color='r')

#     # DRAW THE GRID FOR THE ANALYSIS:
#     # ------------------------------------
#     # Types implemented:
#     # 1. 'equal' = equally spaced grid depending from degs in lat and lon
#     # 2. 'grad' = gradient increas spaced grid
#     # 3. 'neareq' = near-equally spaced grid
#     # ------------------------------------

#     if grid_type == 'equal':
#         # Plotting all base grid points:
#         for lats in range(0, len(latGRID)):
#             for lons in range(0, len(lonGRID)):
#                 m.scatter(lonGRID[lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)

#     elif grid_type == 'grad' or grid_type == 'neareq':
#         # Plot of base points:
#         # print(len(latGRID))
#         for lats in range(0, len(latGRID)):
#             for lons in range(0, len(lonGRID[lats, :])):
#                 if lonGRID[lats, lons]:
#                     m.scatter(lonGRID[lats, lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)
#                 else:
#                     continue

#     # Footprint SAT1
#     for jj in range(0, len(lon_fin1)):
#         x,y = m(lonSAT1[jj], latSAT1[jj], inverse=True)
#         x2,y2 = m(lon_fin1[jj], lat_fin1[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor='k', \
#                 linewidth=1)
#             ax1.add_patch(poly)

#     m.scatter(x, y, marker='o', color='k', s=1, latlon=True, label='S1_P1')


#     # Footprint SAT2
#     for jj in range(0, len(lon_fin2)):
#         x,y = m(lonSAT2[jj], latSAT2[jj], inverse=True)
#         x2,y2 = m(lon_fin2[jj], lat_fin2[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 177:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor='b', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='b', s=1, latlon=True, label='S1_P2')



#     # Footprint SAT3
#     for jj in range(0, len(lon_fin3)):
#         x,y = m(lonSAT3[jj], latSAT3[jj], inverse=True)
#         x2,y2 = m(lon_fin3[jj], lat_fin3[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor='g', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='g', s=1, latlon=True, label='S1_P3')


#     # Footprint SAT4
#     for jj in range(0, len(lon_fin4)):
#         x,y = m(lonSAT4[jj], latSAT4[jj], inverse=True)
#         x2,y2 = m(lon_fin4[jj], lat_fin4[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor='r', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='r', s=1, latlon=True, label='S2_P1')

#     # Footprint SAT5
#     for jj in range(0, len(lon_fin5)):
#         x,y = m(lonSAT5[jj], latSAT5[jj], inverse=True)
#         x2,y2 = m(lon_fin5[jj], lat_fin5[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 180:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor='c', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='c', s=1, latlon=True, label='S2_P2')


#     # Footprint SAT6
#     for jj in range(0, len(lon_fin6)):
#         x,y = m(lonSAT6[jj], latSAT6[jj], inverse=True)
#         x2,y2 = m(lon_fin6[jj], lat_fin6[jj], inverse=True)

#         if abs(x) + abs(abs(x2)-abs(x)) > 175:
#             continue
#         else:
#             poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor='grey', \
#                 linewidth=1)
#             ax1.add_patch(poly)
#     m.scatter(x, y, marker='o', color='grey', s=1, latlon=True, label='S2_P3')

#     if lat_int[0] != np.NaN:
#         poly_basemap(lat_int, lon_int, m)


#     plt.title("Grid NEAR-EQ SPACED - propagation time: %0.0f [h]" %duration)
#     plt.suptitle('Mission: %s - Passes' %mission)
#     plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

#     plt.show()





# def grid_passesConst_R(latGRID, lonGRID, latSAT, lonSAT, \
#         lon_fin, lat_fin, mission, duration, grid_type, sats):
#     # Visualization of passes over time with basemap
#     # Constellation case
    
#     proj = 'cyl' 
#     fig, ax1 = plt.subplots(figsize=(15,7)) # Setting basemap
#     # -------------------------------------------------------------------
#     if proj == 'cyl': # cylindrical projection
#         m = Basemap(projection=proj,llcrnrlat=-90,urcrnrlat=90,\
#                     llcrnrlon=-180,urcrnrlon=180,resolution='c')
#     # --------------------------------------------------------------------

#     m.drawcoastlines()
#     m.drawmapboundary(fill_color='w')
#     m.fillcontinents(color='w') # orange and light-blue
#     # m.bluemarble() # real plot high definition
#     m.drawparallels(np.arange(-90, 90, 30), linewidth=0.4, labels=[1,0,0,1], color='r')
#     m.drawmeridians(np.arange(-180, 180, 60), linewidth=0.4, labels=[1,0,0,1], color='r')

#     # DRAW THE GRID FOR THE ANALYSIS:
#     # ------------------------------------
#     # Types implemented:
#     # 1. 'equal' = equally spaced grid depending from degs in lat and lon
#     # 2. 'grad' = gradient increas spaced grid
#     # 3. 'neareq' = near-equally spaced grid
#     # ------------------------------------

#     if grid_type == 'equal':
#         # Plotting all base grid points:
#         for lats in range(0, len(latGRID)):
#             for lons in range(0, len(lonGRID)):
#                 m.scatter(lonGRID[lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)

#     elif grid_type == 'grad' or grid_type == 'neareq':
#         # Plot of base points:
#         # print(len(latGRID))
#         for lats in range(0, len(latGRID)):
#             for lons in range(0, len(lonGRID[lats, :])):
#                 if lonGRID[lats, lons]:
#                     m.scatter(lonGRID[lats, lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)
#                 else:
#                     continue

#     # Footprint SATs
#     for ii in range(0, sats): # running over sats
#         for jj in range(0, len(lon_fin[:, ii])): # running over times
#             x,y = m(lonSAT[jj, ii], latSAT[jj, ii], inverse=True)
#             x2,y2 = m(lon_fin[jj, ii], lat_fin[jj, ii], inverse=True)

#             if abs(x) + abs(abs(x2)-abs(x)) > 180:
#                 continue
#             else:
#                 poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor=ST.Colors[ii], \
#                     linewidth=1)
#                 ax1.add_patch(poly)

#         m.scatter(x, y, marker='o', color=ST.Colors[ii], s=1, latlon=True, label='SC {}'.format(ii))

#     plt.title("Grid NEAR-EQ SPACED - propagation time: %0.0f [h]" %duration)
#     plt.suptitle('Mission: %s - Passes' %mission)
#     plt.legend(bbox_to_anchor=(1.04,1), loc="upper left")

#     plt.show()






def grid_passesConst_R(latGRID, lonGRID, latSAT, lonSAT, \
        lon_fin, lat_fin, mission, duration, grid_type, lat_int, lon_int, ROI_check, sats):
    """ 
        Visualization of passes over time with basemap
        Constellation case
    """
    

    # print(lonSAT[0:20, 0])
    # print(lonSAT[0:20, 1])
    # print(lonSAT[0:20, 2])
    # print(lonSAT[0:20, 3])


    proj = 'cyl' 
    fig = plt.figure(figsize=(15,7))
    plt.suptitle('Mission: %s - Passes over local area' %mission)
    gs = fig.add_gridspec(1, 2,
                width_ratios=[2,1]
                )

    # GLOBAL PLOT: ------------------------
    ax1 = plt.subplot(gs[0])
    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')

    m.drawcoastlines()
    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w') # orange and light-blue
    # m.bluemarble() # real plot high definition
    m.drawparallels(np.arange(-90, 90, 20), linewidth=0.4, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 30), linewidth=0.4, labels=[1,0,0,1], color='r')

    # DRAW THE GRID FOR THE ANALYSIS:
    # ------------------------------------
    # Types implemented:
    # 1. 'equal' = equally spaced grid depending from degs in lat and lon
    # 2. 'grad' = gradient increas spaced grid
    # 3. 'neareq' = near-equally spaced grid
    # ------------------------------------

    if grid_type == 'equal':
        # Plotting all base grid points:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID)):
                m.scatter(lonGRID[lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)

    elif grid_type == 'grad' or grid_type == 'neareq':
        # Plot of base points:
        # print(len(latGRID))
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID[lats, :])):
                if lonGRID[lats, lons]:
                    m.scatter(lonGRID[lats, lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)
                else:
                    continue

    # Footprint SATs
    for ii in range(0, sats): # running over sats
        for jj in range(0, len(lon_fin[:, ii])): # running over times
            x,y = m(lonSAT[jj, ii], latSAT[jj, ii], inverse=True)
            x2,y2 = m(lon_fin[jj, ii], lat_fin[jj, ii], inverse=True)

            if abs(x) + abs(abs(x2)-abs(x)) > 180:
                continue
            else:
                poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor=ST.Colors[ii], \
                    linewidth=1)
                ax1.add_patch(poly)

        m.scatter(x, y, marker='o', color=ST.Colors[ii], s=1, latlon=True, label='SC {}'.format(ii))


    if ROI_check == 1:
        poly_basemap_fill(lat_int, lon_int, m)
    
    ax1.set_title("Global plot - Propagation time: %0.2f [h]" %duration)
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=False, ncol=3)

    # LOCAL PLOT: ------------------------
    ax2 = plt.subplot(gs[1])
    if ROI_check == 0: # EUROPE PLOT
        m = Basemap(projection='cyl',llcrnrlat=-20,urcrnrlat=80,\
            llcrnrlon=+20,urcrnrlon=+70,resolution='c')
    else: # LOCAL PLOT CUSTOM
        m = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
                    llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c')

    m.drawcoastlines()
    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w') # orange and light-blue
    # m.bluemarble() # real plot high definition
    m.drawparallels(np.arange(-90, 90, 10), linewidth=0.4, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(-180, 180, 20), linewidth=0.4, labels=[1,0,0,1], color='r')

    # DRAW THE GRID FOR THE ANALYSIS:
    # ------------------------------------
    # Types implemented:
    # 1. 'equal' = equally spaced grid depending from degs in lat and lon
    # 2. 'grad' = gradient increas spaced grid
    # 3. 'neareq' = near-equally spaced grid
    # ------------------------------------

    if grid_type == 'equal':
        # Plotting all base grid points:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID)):
                m.scatter(lonGRID[lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)

    elif grid_type == 'grad' or grid_type == 'neareq':
        # Plot of base points:
        # print(len(latGRID))
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID[lats, :])):
                if lonGRID[lats, lons]:
                    m.scatter(lonGRID[lats, lons], latGRID[lats], marker='o', color='dimgrey', s=15, latlon=True)
                else:
                    continue

    # Footprint SATs
    for ii in range(0, sats): # running over sats
        for jj in range(0, len(lon_fin[:, ii])): # running over times
            x,y = m(lonSAT[jj, ii], latSAT[jj, ii], inverse=True)
            x2,y2 = m(lon_fin[jj, ii], lat_fin[jj, ii], inverse=True)

            if abs(x) + abs(abs(x2)-abs(x)) > 180:
                continue
            else:
                poly = m.ellipse(x, y, abs(abs(x2)-abs(x)), abs(abs(y2)-abs(y)), 100, fill=False, facecolor=None, zorder=10, alpha=0.1, edgecolor=ST.Colors[ii], \
                    linewidth=1)
                ax2.add_patch(poly)

        m.scatter(x, y, marker='o', color=ST.Colors[ii], s=1, latlon=True, label='SC {}'.format(ii))


    if ROI_check == 1:
        poly_basemap(lat_int, lon_int, m)
    
    if ROI_check == 0:
        ax2.set_title("Europe Plot -- propagation time: %0.2f [h]" %duration)

    else:
        ax2.set_title("Local Plot -- propagation time: %0.2f [h]" %duration)

    ax2.legend(bbox_to_anchor=(1.04,1), loc="upper left")
    plt.suptitle('Coverage grid-analysis: MISSION-[{}] GRID-[{}]'.format(mission, grid_type))
    
    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "passesConst_R.png"), bbox_inches='tight')




def grid_revisit_mean(rev_mean, corr_mat, lonGrid, latGrid, mission, duration, grid_type, minLon, \
        maxLon, FOV):
    """
    Function for the visualization of mean revisit time
    INPUTS:
    rev_mean = mean revisit time [min] , matrix [IDs x contacts]
    corr_mat = correspondence matrix (between lat lon indexes and IDs of target) , matrix [lat x lon]
    lat/lonGrid = lats and lons of the grid points [deg]
    """

    proj = 'cyl' 

    fig, ax = plt.subplots(figsize=(15,7)) # Setting basemap
    # -------------------------------------------------------------------
    if proj == 'cyl': # cylindrical projection
        m = Basemap(projection=proj,llcrnrlat=-90,urcrnrlat=90,\
                    llcrnrlon=-180,urcrnrlon=180,resolution='c')
    if proj == 'eck4': # Eckert projection
        m = Basemap(projection=proj,lon_0=0,resolution='c')
    if proj == 'ortho': # orthonormal projection
        m = Basemap(projection='ortho',lon_0=60,lat_0=-60,resolution='l')
    # --------------------------------------------------------------------

    m.drawcoastlines()
    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w') # orange and light-blue

    if grid_type == 'equal':
        
        lonPlot = np.linspace(minLon, maxLon, len(latGrid))
        grad_values = np.ones((len(latGrid), len(lonPlot)))

        for ii in range(0, len(corr_mat[:, 0])):
            endRow_TG = (ii+1) * len(lonGrid) # computing final value of the rows for revisit time matrix
            data = rev_mean[((ii)*len(lonGrid)): endRow_TG] # estracting the rows from revisit time matrix
            mean_lat_rev = np.nansum(data) / (len(data) - np.count_nonzero(np.isnan(data)))
            grad_values[ii, :] = grad_values[ii, :] * mean_lat_rev

        # Plot the mesh plot for the revisit time
        x,y = m(lonPlot, latGrid)

    if grid_type == 'grad' or grid_type == 'neareq':
        
        lonPlot = np.linspace(minLon, maxLon, len(latGrid))
        grad_values = np.ones((len(latGrid), len(lonPlot)))
        endRow_TG = 0
        startRow_TG = 0
        for ii in range(0, len(corr_mat[:, 0])):
            not_z_ind = len( np.nonzero(lonGrid[ii, :])[0] ) # there was a -1
            endRow_TG += len(lonGrid[ii, 0:not_z_ind])
            data = rev_mean[startRow_TG:endRow_TG]
            # print(data)
            mean_lat_rev = np.nansum(data) / (len(data) - np.count_nonzero(np.isnan(data)))
            # print(mean_lat_rev)
            grad_values[ii, :] = grad_values[ii, :] * mean_lat_rev
            # print(grad_values[ii, :])
            startRow_TG = endRow_TG

        # Plot the mesh plot for the revisit time
        x,y = m(lonPlot, latGrid)

    m.shadedrelief(scale=0.5)
    m.pcolormesh(x, y, grad_values, latlon=True, cmap='jet')
            
    plt.title("Propagation time: %0.2f [h] (apprx.{} days)".format(round(duration/24)) %duration)
    plt.suptitle('MA-Revisit time grid-analysis: MISSION-[{}] GRID-[{}] FOV-[{} deg]'.format(mission, grid_type, FOV))
    plt.colorbar(label='Mean-Revisit time [h]')

    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "mean_revisit.png"), bbox_inches='tight')


def grid_revisit(mean_rev, corr_mat, grid_type, latGRID, lonGRID, mission, Rev_Min, Rev_Max, Rev_Numb, lat_int, lon_int, ROI_check, FOV, size, duration_track):
    """
    Visualization of single points revisit statistics
    """

    if ROI_check == 0: # Maps GLOBAL CASE
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(15,7)) # Setting basemap
        m1 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax1)
        m2 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax2)
        m3 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax3)
        m4 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax4)
        # --------------------------------------------------------------------
    else: # LOCAL CASE
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(12,5)) # Setting basemap
        # ----------------------------- Maps LOCAL CASE
        m1 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax1)        
        m2 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax2)
        m3 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax3)
        m4 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax4,)
        # --------------------------------------------------------------------

    m1.drawcoastlines()
    m1.drawmapboundary(fill_color='w')
    m1.fillcontinents(color='w') # orange and light-blue

    m2.drawcoastlines()
    m2.drawmapboundary(fill_color='w')
    m2.fillcontinents(color='w') # orange and light-blue

    m3.drawcoastlines()
    m3.drawmapboundary(fill_color='w')
    m3.fillcontinents(color='w') # orange and light-blue

    m4.drawcoastlines()
    m4.drawmapboundary(fill_color='w')
    m4.fillcontinents(color='w') # orange and light-blue

    if grid_type == 'equal':
        LON = np.zeros((len(latGRID), len(lonGRID)))
        LAT = np.zeros((len(latGRID), len(lonGRID)))
        REV = np.zeros((len(latGRID), len(lonGRID)))
        MIN = np.zeros((len(latGRID), len(lonGRID)))
        MAX = np.zeros((len(latGRID), len(lonGRID)))
        NUMB = np.zeros((len(latGRID), len(lonGRID)))
        LON[:] = np.NaN
        LAT[:] = np.NaN
        REV[:] = np.NaN
        MIN[:] = np.NaN
        MAX[:] = np.NaN
        NUMB[:] = np.NaN
        # Plotting rev:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID)):
                ind = int(corr_mat[lats, lons]) # ID of the target

                LON[lats, lons] = lonGRID[lons]
                LAT[lats, lons] = latGRID[lats]
                REV[lats, lons] = mean_rev[ind-1]
                MIN[lats, lons] = Rev_Min[ind-1]
                MAX[lats, lons] = Rev_Max[ind-1]
                NUMB[lats, lons] = Rev_Numb[ind-1]


        R1 = m1.scatter(LON, LAT, marker='o', c=REV, s=15, latlon=True, cmap='jet')
        R2 = m2.scatter(LON, LAT, marker='o', c=MIN, s=15, latlon=True, cmap='jet')
        R3 = m3.scatter(LON, LAT, marker='o', c=MAX, s=15, latlon=True, cmap='jet')
        R4 = m4.scatter(LON, LAT, marker='o', c=NUMB, s=15, latlon=True, cmap='jet')


    elif grid_type == 'grad' or grid_type == 'neareq':
        LON = np.zeros((len(latGRID), len(lonGRID[0, :])))
        LAT = np.zeros((len(latGRID), len(lonGRID[0, :])))
        REV = np.zeros((len(latGRID), len(lonGRID[0, :])))
        MIN = np.zeros((len(latGRID), len(lonGRID[0, :])))
        MAX = np.zeros((len(latGRID), len(lonGRID[0, :])))
        NUMB = np.zeros((len(latGRID), len(lonGRID[0, :])))
        LON[:] = np.NaN
        LAT[:] = np.NaN
        REV[:] = np.NaN
        MIN[:] = np.NaN
        MAX[:] = np.NaN
        NUMB[:] = np.NaN
        # Plot of rev:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID[lats, :])):
                if lonGRID[lats, lons]:
                    ind = int(corr_mat[lats, lons]) # ID of the target
                    LON[lats, lons] = lonGRID[lats, lons]
                    LAT[lats, lons] = latGRID[lats]
                    REV[lats, lons] = mean_rev[ind-1]
                    MIN[lats, lons] = Rev_Min[ind-1]
                    MAX[lats, lons] = Rev_Max[ind-1]
                    NUMB[lats, lons] = Rev_Numb[ind-1]

        R1 = m1.scatter(LON, LAT, marker='o', c=REV, s=15, latlon=True, cmap='jet')
        R2 = m2.scatter(LON, LAT, marker='o', c=MIN, s=15, latlon=True, cmap='jet')
        R3 = m3.scatter(LON, LAT, marker='o', c=MAX, s=15, latlon=True, cmap='jet')
        R4 = m4.scatter(LON, LAT, marker='o', c=NUMB, s=15, latlon=True, cmap='jet')

    poly_basemap_AX(lat_int, lon_int, m1, ax1)
    poly_basemap_AX(lat_int, lon_int, m2, ax2)
    poly_basemap_AX(lat_int, lon_int, m3, ax3)
    poly_basemap_AX(lat_int, lon_int, m4, ax4)

    clb1 = plt.colorbar(R1, ax=ax1)
    clb1.ax.set_title('[h]')
    ax1.set_title("Mean-Revisit time")
    ax1.legend(loc="upper left")

    clb2 = plt.colorbar(R2, ax=ax2)
    clb2.ax.set_title('[h]')
    ax2.set_title("Min-Revisit time ")
    ax2.legend(loc="upper left")

    clb3 = plt.colorbar(R3, ax=ax3)
    clb3.ax.set_title('[h]')
    ax3.set_title("Max-Revisit time ")
    ax3.legend(loc="upper left")

    plt.colorbar(R4, ax=ax4, label='passes/day ')
    ax4.set_title("Average n° of passes /day")
    ax4.legend(loc="upper left")

    if ROI_check == 0: # Maps LOCAL CASE
        plt.suptitle('Revisit time grid-analysis GLOBAL : MISSION-[{}] GRID-[{}] FOV-[{} deg] POINTS-[{}] DURATION-[{} d]'.format(mission, grid_type, FOV, size, round(duration_track/24)))
    else: # GLOBAL CASE
        plt.suptitle('Revisit time grid-analysis LOCAL: MISSION-[{}] GRID-[{}] FOV-[{} deg] POINTS-[{}] DURATION-[{} d]'.format(mission, grid_type, FOV, size, round(duration_track/24)))
    
    # plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "revisit_mean.png"), bbox_inches='tight')




def grid_latency(Mean_latency, corr_mat, grid_type, latGRID, lonGRID, mission, Min_latency, Max_latency, Med_latency, lat_int, lon_int, ROI_check, GS_ID, size, duration_track):
    """
    Visualization of single points revisit statistics
    """

    if ROI_check == 0: # Maps GLOBAL CASE
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(15,7)) # Setting basemap
        m1 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax1)
        m2 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax2)
        m3 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax3)
        m4 = Basemap(projection='robin',lon_0=0,resolution='c', ax=ax4)
        # --------------------------------------------------------------------
    else: # LOCAL CASE
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(12,5)) # Setting basemap
        # ----------------------------- Maps LOCAL CASE
        m1 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax1)        
        m2 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax2)
        m3 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax3)
        m4 = Basemap(projection='cyl',llcrnrlat=min(lat_int)-10,urcrnrlat=max(lat_int)+10,\
            llcrnrlon=min(lon_int)-10,urcrnrlon=max(lon_int)+10,resolution='c', ax=ax4,)
        # --------------------------------------------------------------------

    m1.drawcoastlines()
    m1.drawmapboundary(fill_color='w')
    m1.fillcontinents(color='w') # orange and light-blue

    m2.drawcoastlines()
    m2.drawmapboundary(fill_color='w')
    m2.fillcontinents(color='w') # orange and light-blue

    m3.drawcoastlines()
    m3.drawmapboundary(fill_color='w')
    m3.fillcontinents(color='w') # orange and light-blue

    m4.drawcoastlines()
    m4.drawmapboundary(fill_color='w')
    m4.fillcontinents(color='w') # orange and light-blue

    if grid_type == 'equal':
        LON = np.zeros((len(latGRID), len(lonGRID)))
        LAT = np.zeros((len(latGRID), len(lonGRID)))
        MEAN = np.zeros((len(latGRID), len(lonGRID)))
        MIN = np.zeros((len(latGRID), len(lonGRID)))
        MAX = np.zeros((len(latGRID), len(lonGRID)))
        MED = np.zeros((len(latGRID), len(lonGRID)))
        LON[:] = np.NaN
        LAT[:] = np.NaN
        MEAN[:] = np.NaN
        MIN[:] = np.NaN
        MAX[:] = np.NaN
        MED[:] = np.NaN
        # Plotting rev:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID)):
                ind = int(corr_mat[lats, lons]) # ID of the target

                LON[lats, lons] = lonGRID[lons]
                LAT[lats, lons] = latGRID[lats]
                MEAN[lats, lons] = Mean_latency[ind-1]
                MIN[lats, lons] = Min_latency[ind-1]
                MAX[lats, lons] = Max_latency[ind-1]
                MED[lats, lons] = Med_latency[ind-1]


        R1 = m1.scatter(LON, LAT, marker='o', c=MEAN, s=15, latlon=True, cmap='jet')
        R2 = m2.scatter(LON, LAT, marker='o', c=MIN, s=15, latlon=True, cmap='jet')
        R3 = m3.scatter(LON, LAT, marker='o', c=MAX, s=15, latlon=True, cmap='jet')
        R4 = m4.scatter(LON, LAT, marker='o', c=MED, s=15, latlon=True, cmap='jet')


    elif grid_type == 'grad' or grid_type == 'neareq':
        LON = np.zeros((len(latGRID), len(lonGRID[0, :])))
        LAT = np.zeros((len(latGRID), len(lonGRID[0, :])))
        MEAN = np.zeros((len(latGRID), len(lonGRID[0, :])))
        MIN = np.zeros((len(latGRID), len(lonGRID[0, :])))
        MAX = np.zeros((len(latGRID), len(lonGRID[0, :])))
        MED = np.zeros((len(latGRID), len(lonGRID[0, :])))
        LON[:] = np.NaN
        LAT[:] = np.NaN
        MEAN[:] = np.NaN
        MIN[:] = np.NaN
        MAX[:] = np.NaN
        MED[:] = np.NaN
        # Plot of rev:
        for lats in range(0, len(latGRID)):
            for lons in range(0, len(lonGRID[lats, :])):
                if lonGRID[lats, lons]:
                    ind = int(corr_mat[lats, lons]) # ID of the target
                    LON[lats, lons] = lonGRID[lats, lons]
                    LAT[lats, lons] = latGRID[lats]
                    MEAN[lats, lons] = Mean_latency[ind-1]
                    MIN[lats, lons] = Min_latency[ind-1]
                    MAX[lats, lons] = Max_latency[ind-1]
                    MED[lats, lons] = Med_latency[ind-1]

        R1 = m1.scatter(LON, LAT, marker='o', c=MEAN, s=15, latlon=True, cmap='jet')
        R2 = m2.scatter(LON, LAT, marker='o', c=MIN, s=15, latlon=True, cmap='jet')
        R3 = m3.scatter(LON, LAT, marker='o', c=MAX, s=15, latlon=True, cmap='jet')
        R4 = m4.scatter(LON, LAT, marker='o', c=MED, s=15, latlon=True, cmap='jet')

    poly_basemap_AX(lat_int, lon_int, m1, ax1)
    poly_basemap_AX(lat_int, lon_int, m2, ax2)
    poly_basemap_AX(lat_int, lon_int, m3, ax3)
    poly_basemap_AX(lat_int, lon_int, m4, ax4)

    clb1 = plt.colorbar(R1, ax=ax1)
    clb1.ax.set_title('[h]')
    ax1.set_title("Mean Latency")
    ax1.legend(loc="upper left")

    clb2 = plt.colorbar(R2, ax=ax2)
    clb2.ax.set_title('[h]')
    ax2.set_title("Min Latency ")
    ax2.legend(loc="upper left")

    clb3 = plt.colorbar(R3, ax=ax3)
    clb3.ax.set_title('[h]')
    ax3.set_title("Max Latency ")
    ax3.legend(loc="upper left")

    plt.colorbar(R4, ax=ax4, label='h')
    ax4.set_title("Median of Latency")
    ax4.legend(loc="upper left")

    if lat_int[0] < 0: # Maps LOCAL CASE
        plt.suptitle('Latency grid-analysis GLOBAL : MISSION-[{}] GS-[{}] POINTS-[{}] DURATION-[{} d]'.format(mission, GS_ID, size, round(duration_track/24)))
    else: # GLOBAL CASE
        plt.suptitle('Latency grid-analysis LOCAL: MISSION-[{}] GS-[{}] POINTS-[{}] DURATION-[{} d]'.format(mission, GS_ID, size, round(duration_track/24)))
    
    #plt.show()
    path = os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/MA_plots/'.format(mission))
    fig.savefig(os.path.join(path, "latency.png"), bbox_inches='tight')




