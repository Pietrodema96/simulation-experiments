from astropy.time import Time
import numpy as np
import math as m
import matplotlib.pyplot as plt
import utilities.AstroConstants as AC
import utilities.CarKep as CK

def osc2mean(rr, vv, it):
    """
    Conversion from osculating from mean elements using Kaula theory
    INPUTS:
        rr = position in ECI         [3x1] [km]
        vv = velocity in ECI         [3x1] [km/s]
        ch = choice for the format   [1x1] [-]
        ch=1  Keplerian
        ch=2  Ustinov
        it=0 (no iteration)
        it=1 (correction with 1-iteration)

    OUTPUTS:
        or in Keplerian (K) or in Ustinov way (U)
        a_mean = mean SMA            [1x1] [Km]
        e_mean = mean ECC            [1x1] [-]
        i_mean = mean INC            [1x1] [rad]
        OM_mean = mean RAAN          [1x1] [rad]
        om_mean = mean AOP           [1x1] [rad]
        
    %--------------------------------------------------------------------------
    % DESCRIPTION:
    % Convertion from osculating to mean elements using Ustinov elements h,l
    % and lambda.
    %
    % 1) COMPUTATION OF KEPLER AND USTINOV PARAMETERS
    % 2) COMPUTE DISTURBANCES (delta_i)
    % 3) COMPUTE THE MEAN ORBIT ELEMENTs
    % 4) COMPUTE ALTERNATIVE SMA
    % 5) OBTAIN BACK MEAN ELEMENTS
    %
    %--------------------------------------------------------------------------
    """

    # Recovering Kepler and Ustinov elements:
    R_e = AC.R_E # km - equatorial
    mu = AC.MU_E # km3/s2

    # !!!! Keplerian vector !!!!:
    elem = CK.CtoK(mu, rr, vv) # km, rad                           [CURTIS VERSION]
    # elem = CK.keplerian_elems_from_cart(mu, rr, vv) # km, rad    [OREKIT VERSION]
    
    a_osc = elem[0]
    e_osc = elem[1]
    i_osc = elem[2]
    OM_osc = elem[3]
    om_osc = elem[4]
    theta_osc = elem[5]

    # Ustinov elements:
    h_osc = e_osc*m.sin(om_osc)
    l_osc = e_osc*m.cos(om_osc)

    # pr_scal = np.dot(rr, vv)
    # NNorm = np.linalg.norm(rr)
    # E_osc = m.atan2(pr_scal/(m.sqrt(mu*a_osc)), (1-NNorm/a_osc))
    # # E_osc=2*atan(tan(theta_osc/2) * sqrt((1-e_osc) * (1+e_osc)));
    # M_osc = E_osc - e_osc*m.sin(E_osc)
    M_osc = CK.meanAnomaly(AC.MU_E, rr, vv, a_osc, e_osc) # [rad]

    lamb = om_osc + M_osc
    u_osc = om_osc + theta_osc

    # Computation of disturbances:

    # data needed for the deltas' computtaion:
    lambda0 = lamb
    a0 = a_osc
    i0 = i_osc
    h0 = h_osc
    l0 = l_osc
    OM0 = OM_osc
    u0 = u_osc


    J2 = AC.J2_EARTH
    G_due = -J2*(R_e**2)/a0**2
    beta0 = m.sin(i0)
    lambda_star = 1-3*G_due*(3-4*beta0)/2
    # epsilon0=sqrt(1-beta0^2)*sign(i0-pi/2);
    epsilon0 = m.cos(i0)


    # delta_i: modified
    delta_i=-3*G_due*beta0*epsilon0/(4*lambda_star) * (-l0*m.cos(lambda0) + h0*m.sin(lambda0) + m.cos(2*lambda0) + (7*l0*m.cos(3*lambda0)/3 + h0*m.sin(3*lambda0)))

    # delta_OM: modified
    delta_OM=3*G_due*epsilon0/(4*lambda_star) * (7*l0*m.sin(lambda0) + 5*h0*m.cos(lambda0) - m.sin(2*lambda0) + (0-7*l0*m.sin(3*lambda0)/3 + h0*m.cos(3*lambda0)))

    # delta_a:
    delta_a=-3*G_due*a0/(2*lambda_star) * ((2-7*beta0**2 /2)*l0*m.cos(lambda0) + (2-5*beta0**2 /2)*h0*m.sin(lambda0) + beta0**2 *m.cos(2*lambda0) + 7*beta0**2 *(l0*m.cos(3*lambda0) + h0*m.sin(3*lambda0))/2) + 3*a0*G_due**2 *beta0**2 /4 * (7*(2-3*beta0**2)*m.cos(2*lambda0) + beta0**2 *m.cos(4*lambda0))

    # delta_h:
    delta_h=-3*G_due/(2*lambda_star) * ((1-7*beta0**2 /4)*m.sin(lambda0) + (1-3*beta0**2)*l0*m.sin(2*lambda0) + (2*beta0**2-3/2)*h0*m.cos(2*lambda0) + 17*beta0**2 *(l0*m.sin(4*lambda0) - h0*m.cos(4*lambda0))/8 + 7*beta0**2 *m.sin(3*lambda0)/12 )

    # delta_l:
    delta_l=-3*G_due/(2*lambda_star) * ((1-5*beta0**2 /4)*m.cos(lambda0) + (3-5*beta0**2)/2 *l0*m.cos(2*lambda0) + (2 - 3*beta0**2 /2)*h0*m.sin(2*lambda0) + 17*beta0**2 *(l0*m.cos(4*lambda0) + h0*m.sin(4*lambda0))/8 + 7*beta0**2 *m.cos(3*lambda0)/12 )

    # % delta_OM:
    # delta_OM=3*G_due*sqrt(1-beta0^2)/(2*lambda_star) * (7*l0*sin(lambda0)/2 - 5*h0*cos(lambda0)/2 - sin(2*lambda0)/2 - 7*l0*sin(3*lambda0)/6 + 7*h0*cos(3*lambda0)/6 );

    # delta_lambda:
    delta_lambda=-3*G_due/(2*lambda_star) * ( (10-119*beta0**2 /8)*l0*m.sin(lambda0) + (85*beta0**2 /8 - 9)*h0*m.cos(lambda0) + (2*beta0**2 -1/2)*m.sin(2*lambda0) + (-7/6+119*beta0**2 /24)*( l0*m.sin(3*lambda0)-h0*m.cos(3*lambda0) ) - (3-21*beta0**2 /4)*l0*m.sin(lambda0) + (3-15*beta0**2 /4)*h0*m.cos(lambda0) - 3*beta0**2 *m.sin(2*lambda0)/4 - 21*beta0**2 *( l0*m.sin(3*lambda0)-h0*m.cos(3*lambda0) )/12 )



    # ComputaTion of mean elements:

    # a_mean=a0-delta_a;
    i_mean = i0-delta_i
    OM_mean = OM0-delta_OM
    h_mean = h0-delta_h
    l_mean = l0-delta_l
    lambda_mean = lambda0-delta_lambda
    # om_mean=atan(h_mean/l_mean)
    # e_mean=h_mean/sin(om_mean)
    e_mean = np.sqrt(l_mean**2 + h_mean**2)
    om_mean = m.atan2(h_mean,l_mean)

    # check for sign errors
    if om_mean<0:
        om_mean=2*np.pi+om_mean


    # Improuvement of a_mean:
    ni=np.sqrt(1-e_mean**2)
    gamma_due=-J2*(R_e/a0)**2 /2
    a_mean=a0 + gamma_due*a0*( (3*m.cos(i0)**2 -1)*((a0/np.linalg.norm(rr))**3-1/ni**3) + 3*(1-m.cos(i0)**2)*(a0/np.linalg.norm(rr))**3 *m.cos(2*u0))


    # Considering 1-iteration:
    if it == 1:
        h0 = h_mean
        l0 = l_mean
        G_due = -J2*(R_e**2)/a_mean**2
        beta0 = m.sin(i_mean)
        lambda_star = 1-3*G_due*(3-4*beta0)/2
        # epsilon0=sqrt(1-beta0^2)*sign(i0-pi/2)
        epsilon0 = m.cos(i_mean)

        # delta_i: modified
        delta_i=-3*G_due*beta0*epsilon0/(4*lambda_star) * (-l0*m.cos(lambda0) + h0*m.sin(lambda0) + m.cos(2*lambda0) + \
            (7*l0*m.cos(3*lambda0)/3 + h0*m.sin(3*lambda0)))

        # delta_OM: modified
        delta_OM=3*G_due*epsilon0/(4*lambda_star) * (7*l0*m.sin(lambda0) + 5*h0*m.cos(lambda0) - m.sin(2*lambda0) + \
            (0-7*l0*m.sin(3*lambda0)/3 + h0*m.cos(3*lambda0)))


        # delta_a:
        delta_a=-3*G_due*a0/(2*lambda_star) * ((2-7*beta0**2 /2)*l0*m.cos(lambda0) + (2-5*beta0**2 /2)*h0*m.sin(lambda0) + \
            beta0**2 *m.cos(2*lambda0) + 7*beta0**2 *(l0*m.cos(3*lambda0) + h0*m.sin(3*lambda0))/2) + \
                3*a_mean*G_due**2 *beta0**2 /4 * (7*(2-3*beta0**2)*m.cos(2*lambda0) + beta0**2 *m.cos(4*lambda0))

        # delta_h:
        delta_h=-3*G_due/(2*lambda_star) * ((1-7*beta0**2 /4)*m.sin(lambda0) + (1-3*beta0**2)*l0*m.sin(2*lambda0) + \
            (2*beta0**2-3/2)*h0*m.cos(2*lambda0) + 17*beta0**2 *(l0*m.sin(4*lambda0) - \
                h0*m.cos(4*lambda0))/8 + 7*beta0**2 *m.sin(3*lambda0)/12 )

        # delta_l:
        delta_l=-3*G_due/(2*lambda_star) * ((1-5*beta0**2 /4)*m.cos(lambda0) + (3-5*beta0**2)/2 *l0*m.cos(2*lambda0) + \
            (2 - 3*beta0**2 /2)*h0*m.sin(2*lambda0) + 17*beta0**2 *(l0*m.cos(4*lambda0) + h0*m.sin(4*lambda0))/8 + 7*beta0**2 *m.cos(3*lambda0)/12 )
        
        # delta_OM:
        # delta_OM=3*G_due*sqrt(1-beta0^2)/(2*lambda_star) * (7*l0*sin(lambda0)/2 - 5*h0*cos(lambda0)/2 - sin(2*lambda0)/2 - 7*l0*sin(3*lambda0)/6 + 7*h0*cos(3*lambda0)/6 );

        # delta_lambda:
        delta_lambda=-3*G_due/(2*lambda_star) * ( (10-119*beta0**2 /8)*l0*m.sin(lambda0) + (85*beta0**2 /8 - 9)*h0*m.cos(lambda0) + \
            (2*beta0**2 -1/2)*m.sin(2*lambda0) + (-7/6+119*beta0*2 /24)*( l0*m.sin(3*lambda0)-h0*m.cos(3*lambda0) ) - \
                (3-21*beta0**2 /4)*l0*m.sin(lambda0) + (3-15*beta0**2 /4)*h0*m.cos(lambda0) - 3*beta0**2 *m.sin(2*lambda0)/4 - 21*beta0**2 *( l0*m.sin(3*lambda0)-h0*m.cos(3*lambda0) )/12 )

        i_mean = i0-delta_i
        OM_mean = OM0-delta_OM
        h_mean = h_osc-delta_h
        l_mean = l_osc-delta_l
        lambda_mean = lambda0-delta_lambda
        # om_mean = m.atan(h_mean/l_mean)
        # e_mean = h_mean/sin(om_mean)
        e_mean = np.sqrt(l_mean**2 + h_mean**2)
        om_mean = m.atan2(h_mean,l_mean)

        # check for sign errors
        if om_mean<0:
            om_mean=2*np.pi+om_mean

        ni = np.sqrt(1-e_mean**2)
        gamma_due = -J2*(R_e/a_mean)**2 /2
        a_mean = a0 + gamma_due*a_mean*( (3*m.cos(i_mean)**2 -1)*((a_mean/np.linalg.norm(rr))**3-1/ni**3) + 3*(1-m.cos(i_mean)**2)*(a_mean/np.linalg.norm(rr))**3 *m.cos(2*u0))
        

    # Recovering mean elements set:

    # Ustinov mean set
    mean_elements_ustinov = [a_mean, h_mean, l_mean, i_mean, OM_mean, lambda_mean] # [km rad]
    # conversion to Keplerian mean set
    M_mean = lambda_mean-om_mean # Mean anomaly
    mean_elements=[a_mean, e_mean, i_mean, OM_mean, om_mean, M_mean] # [km rad]

    ME_K = mean_elements
    ME_U = mean_elements_ustinov

    return ME_U, ME_K