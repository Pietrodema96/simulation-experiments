# -----------------------
# MORE FREQUENTLY USED SPACE CONSTANTS AND MATH
# -----------------------
# This is a brief collection of the most useful constants used in space application (Earth related)
#
# AUTHOR: AIKOSIM
# LAST UPDATE: 22/03/2022
#
#---------------------------------------------------
import math, time
import numpy as np
import pandas as pd


# --- MACROS :
D2R = math.pi/180
m2km = 1000

def find_nearest(array, value):
    """ Find the nearest value to a guess
        inside a 1D array """
    idx = np.nanargmin( (np.abs(array - value)) )
    # print(array[idx])
    
    return round(array[idx], 2), idx

def Frac(x):
    """ Gives the residual decimal between a number
        and its floor integer """
    res = x-math.floor(x)
    
    return res


# ------ CONSTANTS:


AU = 149597870.693  # astronomical unit in units of kilometers #


# Gravitational Constants mu = G*m
MU_SUN = 132712440023.310 # [km3/s2]
MU_MERCURY = 22032.080 # [km3/s2]
MU_VENUS = 324858.599 # [km3/s2]
MU_MOON = 4902.799 # [km3/s2]
MU_MARS = 42828.314 # [km3/s2]
MU_JUPITER = 126712767.881 # [km3/s2]
MU_SATURN = 37940626.068 # [km3/s2]
MU_URANUS = 5794559.128 # [km3/s2]
MU_NEPTUNE = 6836534.065 # [km3/s2]
MU_PLUTO = 983.055 # [km3/s2]
MU_E = 398600.4360 # Earth grav constant [km3/s2]

# Sun #
REQ_SUN = 695000.  # km #

# Earth #
R_E = 6378.1366  # km, from SPICE #
RP_EARTH = 6356.7519  # km, from SPICE #
J2_EARTH = 1082.616e-6
J3_EARTH = -2.53881e-6
J4_EARTH = -1.65597e-6
J5_EARTH = -0.15e-6
J6_EARTH = 0.57e-6
SMA_EARTH = 1.00000011 * AU
I_EARTH = 0.00005 * D2R
E_EARTH = 0.01671022
OMEGA_EARTH = 0.00007292115  # Earth's planetary rotation rate, rad/sec #

# Moon #
REQ_MOON = 1737.4
J2_MOON = 202.7e-6
SMA_MOON = 0.3844e6
E_MOON = 0.0549


# Computation utilities:
sec_day = 86400 # sec per day [sec]
sec_month = 2592000 # sec in one month
Earth_Rot = 7.29211510e-5 # Earth rotation mean value [rad/s]
HA = math.pi/12 # hour angle [rad]
days_tr_y = 365 #  days tropical year [days]


def reduced_distance(h):
    """ Reduced distance ni
        given an orbit altitude [km] at a certain time """

    ni = (R_E + h)/R_E
    return ni # [-]
