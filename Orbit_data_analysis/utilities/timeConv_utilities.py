import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import time, math, os, sys
from datetime import datetime, timedelta
import datetime
import utilities.leapseconds as lps
import utilities.AstroConstants as AC
from scipy.integrate import odeint
import utilities.plotting_utilities as PU
from astropy.time import Time
from astropy import coordinates




def LTAN_2_RAAN(launch_date, yy, LTAN):

    """    
    Computation of RAAN of SSO from the LTAN

    INPUT
        LTAN = Local time at the ascending node, [h]
        yy = year, if launch date is before spring equinox...it should be actual_year-1
        launch_date = %d/%m/%Y %H:%M:%S' [str]

    OUTPUT:
        RAAN = in [deg]
    """

    date_1 = '21/03/{} 00:00:00'.format(yy) # MARCH SPRING EQUINOX - STARTING DATE
    date_2 = launch_date # Launch date
    date_format_str = '%d/%m/%Y %H:%M:%S'
    d1 = datetime.datetime.strptime(date_1, date_format_str)
    d2 = datetime.datetime.strptime(date_2, date_format_str)
    diff = d2 - d1
    # print(diff.days)
    sun_RA_change = 24/365 # [h/day] RA changes linearly 24h in 365 days
    actual_change = diff.days * sun_RA_change # [h] actual change due to the difference in days
    # print(actual_change)
    earth_rot = ( AC.Earth_Rot / AC.D2R )*3600 # [deg/h] Earth rotation in h
    # print(earth_rot)
    combined_change = earth_rot * actual_change # [deg]
    LTAN_deg = LTAN * earth_rot # [deg]
    RAAN_fromLTAN = LTAN_deg + combined_change # [deg] RAAN from LTDN

    d = Time(d2)
    sc = coordinates.get_sun(d)
    ra = sc.ra.degree + 15*(LTAN-12) 

    # return RAAN_fromLTDN [deg]
    return ra


def monthText_2_num (date1):

    """    
    Transform the date mounth to num date mounth
    date1 is in UTC
    """

    date1 = date1.split()

    if date1[1]=='JAN':
        date1[1] = '01'
    elif date1[1]=='FEB':
        date1[1] = '02'
    elif date1[1]=='MAR':
        date1[1] = '03'
    elif date1[1]=='APR':
        date1[1] = '04'
    elif date1[1]=='MAY':
        date1[1] = '05'
    elif date1[1]=='JUN':
        date1[1] = '06'
    elif date1[1]=='JUL':
        date1[1] = '07'
    elif date1[1]=='AUG':
        date1[1] = '08'
    elif date1[1]=='SEP':
        date1[1] = '09'
    elif date1[1]=='OCT':
        date1[1] = '10'
    elif date1[1]=='NOV':
        date1[1] = '11'
    elif date1[1]=='DEC':
        date1[1] = '12'

    date1[1] = str(date1[1])
    date1[0] = "-".join(date1[0:3])
    date1 = " ".join([date1[0], date1[3]])


    #date1 = datetime.datetime.strptime(date1, '%Y-%m-%d %H:%M:%S.%f')

    # date_num = datetime.datetime.strftime(date1,'%Y-%m-%d %H:%M:%S.%f')
    #print(type(date1))
    return date1



def conv_UTC2GPSsec (date):

    """    
    FROM UTC TO GPS sec
    INPUT:
    date: date for which i want the GPS sec [string]
    OUTPUT:
    secGPS: GPS sec for the date [sec]
    """

    datetimeformat = "%Y-%m-%d %H:%M:%S"
    date1 = datetime.datetime.strptime(date,datetimeformat)
    GPS = lps.utc_to_gps(date1) # [GPS sec, format UTC] DATETIME
    GPS_date = datetime.datetime.strptime('1980-01-6 00:00:00','%Y-%m-%d %H:%M:%S') # starting date for GPS, DATETIME

    secGPS = sec_btw_dates(GPS, GPS_date) # [sec]

    return secGPS # SEC



def add_sec_dates (date, secs):

    """    
    Add sec to a date:
    
    INPUT:
    the input date is a 'str' type such as ''%Y-%m-%d %H:%M:%S.%f''
    OUTPUT:
    new: the nuw date with added seconds [str]
    """

    date_new = np.empty(6)
    date = date.split(' ')
    date_new[0:3] = date[0].split("-")
    date_new[3:6] = date[1].split(":")

    boss = datetime.datetime(int(date_new[0]), int(date_new[1]), int(date_new[2]), int(date_new[3]), int(date_new[4]), int(date_new[5]))
    new = boss + datetime.timedelta(seconds=secs)
    new = datetime.datetime.strftime(new,'%Y-%m-%d %H:%M:%S')

    return new #[date UTC format]


def add_sec_datesDATETIME (date, secs):

    """    
    Add sec to a date:
    
    INPUT:
    the input date is a 'str' type such as ''%Y-%m-%d %H:%M:%S.%f''
    OUTPUT:
    new: the nuw date with added seconds [datetime]
    """

    date_new = np.empty(6)
    date = date.split(' ')
    date_new[0:3] = date[0].split("-")
    date_new[3:6] = date[1].split(":")

    boss = datetime.datetime(int(date_new[0]), int(date_new[1]), int(date_new[2]), int(date_new[3]), int(date_new[4]), int(date_new[5]))
    new = boss + datetime.timedelta(seconds=secs)

    return new #[datetime format]


def sec_btw_dates(date1, date2):

    """    
    Seconds between two dates in UTC format
    input:
    date1/date2 is a 'str' type such as ''%Y-%m-%d %H:%M:%S.%f''
    In datetime format
    
    """

    totSec = (date2-date1).total_seconds() # [sec]

    return totSec


def gmst(Mjd_UT1):

    """    
    From MJD to Greenwich Mean Siderial Time
    """
    Secs = 86400                       # Seconds per day
    MJD_J2000 = 51544.5
    Mjd_0 = math.floor(Mjd_UT1)
    UT1   = Secs*(Mjd_UT1-Mjd_0)       # [s]
    T_0   = (Mjd_0  -MJD_J2000)/36525
    T     = (Mjd_UT1-MJD_J2000)/36525
    gmst  = 24110.54841 + 8640184.812866 * T_0 + 1.002737909350795 * UT1 + (0.093104-  6.2e-6 *T) * T * T  # [s]
    gmstime = 2*math.pi*AC.Frac(gmst/Secs)     # [rad], 0..2pi
    return gmstime


def timeLaw(initS, timeSpan, n_points):

    """    
    This function integrate the orbit with the time law
    timeSpan = time amount [s]
    n_points = discretization of simulation
    """
    
    # Initial Conditions
    X_0 = initS[0]  # [km]
    Y_0 = initS[1]  # [km]
    Z_0 = initS[2]  # [km]
    VX_0 = initS[3] # [km/s]
    VY_0 = initS[4]  # [km/s]
    VZ_0 = initS[5]  # [km/s]
    state_0 = [X_0, Y_0, Z_0, VX_0, VY_0, VZ_0]

    # Time Array
    t = np.linspace(0, timeSpan, n_points)  

    # Solving ODE
    sol = odeint(PU.model_2BP, state_0, t)
    X_Sat = sol[:, 0]  # X-coord [km] of satellite over time interval 
    Y_Sat = sol[:, 1]  # Y-coord [km] of satellite over time interval
    Z_Sat = sol[:, 2]  # Z-coord [km] of satellite over time interval

    return X_Sat, Y_Sat, Z_Sat