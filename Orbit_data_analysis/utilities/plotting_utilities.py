
from cmath import pi
import math, os, sys
from turtle import color, width
import matplotlib.pyplot as plt
import matplotlib 
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
import utilities.AstroConstants as AC
import utilities.CarKep as C2K
from scipy.integrate import odeint
#from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU
import utilities.styles as ST
m2km = 1000




max_radius=0


def plotEarth(ax):
    """
    Earth plot for subplots
    """

    img = plt.imread('utilities/EARTH.jpg')

    N = 50
    phi = np.linspace(0, 2 * np.pi, N)
    theta = np.linspace(0, np.pi, N)
    theta, phi = np.meshgrid(theta, phi)

    theta = np.linspace(0, np.pi, img.shape[0])
    phi = np.linspace(0, 2*np.pi, img.shape[1])
    count = 200 # keep 180 points along theta and phi
    theta_inds = np.linspace(0, img.shape[0] - 1, count).round().astype(int)
    phi_inds = np.linspace(0, img.shape[1] - 1, count).round().astype(int)
    theta = theta[theta_inds]
    phi = phi[phi_inds]
    img = img[np.ix_(theta_inds, phi_inds)]

    X_Earth = AC.R_E * np.cos(phi) * np.sin(theta)
    Y_Earth = AC.R_E * np.sin(phi) * np.sin(theta)
    Z_Earth = AC.R_E * np.cos(theta)

    ax.plot_surface(X_Earth, Y_Earth, Z_Earth, rstride=1, cstride=1, color='blue', alpha=0.7)



def plotOrbitElements(times, osc_elems):

    """    
    Plot of orbital elements:

    INPUT:
    osc_elems = orbital elements by time, [timesx6] -- [rad] [km]
    times = time vector of elapsed times
    
    """

    plt.figure(1, figsize=(6, 5))
    fig = plt.gcf()
    ax = fig.gca()
    fig, axs = plt.subplots(2, 3)
    axs[0, 0].plot(times ,osc_elems[:,0])
    axs[0, 0].set_title("SMA [sec] vs [km]")
    axs[0, 1].plot(times ,osc_elems[:,1])
    axs[0, 1].set_title("ECC [sec] vs [-]")
    axs[0, 2].plot(times ,osc_elems[:,2]/AC.D2R)
    axs[0, 2].set_title("INC [sec] vs [deg]")
    axs[1, 0].plot(times ,osc_elems[:,3]/AC.D2R)
    axs[1, 0].set_title("RAAN [sec] vs [deg]")
    axs[1, 1].plot(times ,osc_elems[:,4]/AC.D2R)
    axs[1, 1].set_title("om [sec] vs [deg]")
    axs[1, 2].plot(times ,osc_elems[:,5]/AC.D2R)
    axs[1, 2].set_title("f [sec] vs [deg]")
    plt.title('Distribution of OE by time')

    plt.show()


def plotOrbitElements_Equinotial(times, osc_elems):

    """    
    Plot of orbital Equinotial elements:

    INPUT:
    osc_elems = orbital elements by time, [timesx6] -- [rad] [km]
    times = time vector of elapsed times

    https://ai-solutions.com/_help_Files/orbit_element_types.htm#achr_equinoctial
    
    """

    plt.figure(1, figsize=(6, 5))
    fig = plt.gcf()
    ax = fig.gca()
    fig, axs = plt.subplots(2, 3)
    axs[0, 0].plot(times ,osc_elems[:,0])
    axs[0, 0].set_title("SMA [sec] vs [km]")
    axs[0, 1].plot(times ,osc_elems[:,1])
    axs[0, 1].set_title("h [sec] vs [-]")
    axs[0, 2].plot(times ,osc_elems[:,2]/AC.D2R)
    axs[0, 2].set_title("k [sec] vs [-]")
    axs[1, 0].plot(times ,osc_elems[:,3]/AC.D2R)
    axs[1, 0].set_title("p [sec] vs [-]")
    axs[1, 1].plot(times ,osc_elems[:,4]/AC.D2R)
    axs[1, 1].set_title("q [sec] vs [-]")
    axs[1, 2].plot(times ,osc_elems[:,5]/AC.D2R)
    axs[1, 2].set_title("L [sec] vs [-]")
    plt.suptitle('Distribution of EQ-OE by time')

    plt.show()




def plotOrbit_MEAN_Elements(times, osc_elems, mean_elements_K, mean_elements_EU, out_folder):

    """    
    Plot of orbital elements:

    INPUT:
    osc_elems = orbital elements by time, [timesx6] -- [rad] [km]
    times = time vector of elapsed times
    
    """
    d = AC.sec_day
    
    plt.figure()
    fig = plt.gcf()
    fig, axs = plt.subplots(2, 3, figsize=(23, 15))
    axs[0, 0].plot(times/d ,osc_elems[:,0], 'b')
    axs[0, 0].plot(times/d ,mean_elements_K[:,0], 'r')
    axs[0, 0].set_title("SMA ")
    axs[0, 0].set_xlabel(" [days] ")
    axs[0, 0].set_ylabel(" [km] ")

    axs[0, 1].plot(times/d ,osc_elems[:,1], 'b')
    axs[0, 1].plot(times/d ,mean_elements_K[:,1], 'r')
    axs[0, 1].set_title("ECC")
    axs[0, 1].set_xlabel(" [days] ")
    axs[0, 1].set_ylabel(" [-] ")
    
    axs[0, 2].plot(times/d ,osc_elems[:,2]/AC.D2R, 'b')
    axs[0, 2].plot(times/d ,mean_elements_K[:,2]/AC.D2R, 'r')
    axs[0, 2].set_title("INC")
    axs[0, 2].set_xlabel(" [days] ")
    axs[0, 2].set_ylabel(" [deg] ")
    
    axs[1, 0].plot(times/d ,osc_elems[:,3]/AC.D2R, 'b')
    axs[1, 0].plot(times/d ,mean_elements_K[:,3]/AC.D2R, 'r')
    axs[1, 0].set_title("RAAN")
    axs[1, 0].set_xlabel(" [days] ")
    axs[1, 0].set_ylabel(" [deg] ")
    
    axs[1, 1].plot(times/d ,osc_elems[:,4]/AC.D2R, 'b')
    axs[1, 1].plot(times/d ,mean_elements_K[:,4]/AC.D2R, 'r')
    axs[1, 1].set_title("om")
    axs[1, 1].set_xlabel(" [days] ")
    axs[1, 1].set_xlabel(" [deg] ")

    # axs[1, 2].plot(times/d ,osc_elems[:,5]/AC.D2R, 'b')
    axs[1, 2].plot(times/d ,mean_elements_K[:,5]/AC.D2R, 'r')
    # axs[1, 2].plot(times/d ,mean_elements_EU[:,5]/AC.D2R, 'r')

    axs[1, 2].set_title("AOL_EU")
    axs[1, 2].set_xlabel(" [days] ")
    axs[1, 2].set_xlabel(" [deg] ")
    
    plt.suptitle('Distribution of OE (blue) and ME (red) by time')

    fig.savefig(os.path.join(out_folder, "Mean_elem.png"), bbox_inches='tight')   
    return


def plotOrbit_MEAN_ElementsEU(times, osc_el, mean_elements, out_folder, h_osc, l_osc):

    """    
    Plot of orbital elements:

    INPUT:
    osc_elems = orbital elements by time, [timesx6] -- [rad] [km]
    times = time vector of elapsed times
    mean_elements = EU mean elements
    out_folder = out folder path
    h_osc, l_osc = EU parameters osculating
    
    """
    d = AC.sec_day
    
    plt.figure()
    fig = plt.gcf()
    fig, axs = plt.subplots(3, 1, figsize=(13, 13))

    axs[0].plot(mean_elements[:,1] , mean_elements[:,2], 'r')
    axs[0].set_title("h_mean [-] l_mean [-]")

    axs[1].plot(h_osc[:] , l_osc[:], 'b')
    axs[1].plot(mean_elements[:,1] , mean_elements[:,2], 'r')
    axs[1].set_title("h [-] vs l [-]")
    
    axs[2].plot(times/d , osc_el[:,5], 'b')
    axs[2].plot(times/d , mean_elements[:,5], 'r')
    axs[2].set_title("AOL_EU")
    axs[2].set_xlabel(" [days] ")
    axs[2].set_xlabel(" [deg] ")

    plt.suptitle('Distribution of OE (blue) and EU_ME (red) by time')

    fig.savefig(os.path.join(out_folder, "Mean_elem_EU.png"), dpi=100)   
    return


def model_2BP(state, t):
    """
    Earth model of plot ellipsoid
    """
    mu = AC.MU_E # Earth's gravitational parameter  
                          # [km^3/s^2]
    x = state[0]
    y = state[1]
    z = state[2]
    x_dot = state[3]
    y_dot = state[4]
    z_dot = state[5]
    x_ddot = -mu * x / (x ** 2 + y ** 2 + z ** 2) ** (3 / 2)
    y_ddot = -mu * y / (x ** 2 + y ** 2 + z ** 2) ** (3 / 2)
    z_ddot = -mu * z / (x ** 2 + y ** 2 + z ** 2) ** (3 / 2)
    dstate_dt = [x_dot, y_dot, z_dot, x_ddot, y_ddot, z_ddot]
    return dstate_dt



def plot_orbitTimeLaw(initS, n_points, timeSpan, mod, ra):

    """    Computing an orbit with its time law (NO PERTURBATION!!!)
    
    INPUTS: 
     - initS: initial state for the integration
     - n_points: number of points in the time span to obtain
     - timeSpan: temporal span for integration [s]
     - mod: mode for plotting (dynamic or static)
     - ra: apoapsis of the orbit [km], to set plot-limits

    """

    img = plt.imread('utilities/index.jpeg')

    # Sat positions integrated:
    X_Sat, Y_Sat, Z_Sat = TCU.timeLaw(initS, timeSpan, n_points)

    mean_alt = np.empty(len(X_Sat))
    for kk in range(0, len(X_Sat)):
        mean_alt[kk] = math.sqrt(X_Sat[kk]**2 + Y_Sat[kk]**2 + Z_Sat[kk]**2) - AC.R_E

    # Setting up Spherical Earth to Plot----------------------
    if mod == 'dyn':
        N = 50
        phi = np.linspace(0, 2 * np.pi, N)
        theta = np.linspace(0, np.pi, N)
    if mod == 'static':
        theta = np.linspace(0, np.pi, img.shape[0])
        phi = np.linspace(0, 2*np.pi, img.shape[1])
        count = 200 # keep 180 points along theta and phi
        theta_inds = np.linspace(0, img.shape[0] - 1, count).round().astype(int)
        phi_inds = np.linspace(0, img.shape[1] - 1, count).round().astype(int)
        theta = theta[theta_inds]
        phi = phi[phi_inds]
        img = img[np.ix_(theta_inds, phi_inds)]
    # ---------------------------------------------------------
    
    theta, phi = np.meshgrid(theta, phi)


    d = 1.5 * AC.R_E # quiver dimensions (function of Earth radium)
    X_Earth = AC.R_E * np.cos(phi) * np.sin(theta)
    Y_Earth = AC.R_E * np.sin(phi) * np.sin(theta)
    Z_Earth = AC.R_E * np.cos(theta)

    if mod == 'static':
        # Plotting Earth and Orbit
        fig = plt.figure(figsize=(15,7))
        # ax = fig.gca(projection='3d')
        ax  = fig.add_subplot(111, projection='3d', aspect='auto', computed_zorder=True)
        ax.plot_surface(X_Earth, Y_Earth, Z_Earth, color='blue', alpha=0.7)
        ax.plot_surface(X_Earth.T, Y_Earth.T, Z_Earth.T, facecolors=img/255, cstride=1, rstride=1) # we've already pruned ourselves
        
        
        #plotEarth(ax)
        ax.plot(X_Sat, Y_Sat, Z_Sat, 'r-', zorder =10)

        ax.quiver([0, 0, 0], [0, 0, 0], [0, 0, 0], [d, 0, 0], [0, d, 0], [0, 0, d], colors=['r', 'b', 'g'])
        ax.view_init(30, 145)  # Changing viewing angle (adjust as needed)
        plt.title('Two-Body Orbit')
        ax.set_xlabel('X [km]')
        ax.set_ylabel('Y [km]')
        ax.set_zlabel('Z [km]')
        txt = fig.suptitle('Duaratin of the simulation: %f [h]' %(timeSpan/3600))
        # Make axes limits
        XYZlim = ra
        # ax.set_xlim3d(-XYZlim, XYZlim)
        # ax.set_ylim3d(-XYZlim, XYZlim)
        # ax.set_zlim3d(-XYZlim, XYZlim)
        plt.show()
    
    if mod == 'dyn':

        fig = plt.figure(figsize=(15,7))
        ax = plt.axes(projection='3d')
        ax.plot_surface(X_Earth, Y_Earth, Z_Earth, rstride=1, cstride=1, color='blue', alpha=0.7, edgecolors='silver')
        ax.plot3D(X_Sat[:, 0], Y_Sat[:, 1], Z_Sat[:, 2],'r--', markersize=0.6)
        ax.quiver([0, 0, 0], [0, 0, 0], [0, 0, 0], [d, 0, 0], [0, d, 0], [0, 0, d], colors=['r', 'b', 'g'])
        SAT, = ax.plot(X_Sat, Y_Sat, Z_Sat,'b.', markersize=10)
        ax.view_init(30, 145)  # Changing viewing angle (adjust as needed)
        plt.title('Two-Body Orbit')
        ax.set_xlabel('X [km]')
        ax.set_ylabel('Y [km]')
        ax.set_zlabel('Z [km]')
        txt1 = fig.suptitle('')


        #---Make axes limits
        XYZlim = ra
        ax.set_xlim3d(-XYZlim, XYZlim)
        ax.set_ylim3d(-XYZlim, XYZlim)
        ax.set_zlim3d(-XYZlim, XYZlim)

        #---Dynamic plot:
        timeStep = timeSpan/n_points
        fps = 100 # frames per sec
        print('matplotlib: '+matplotlib.__version__)
        print('numpy: '+np.version.full_version)
        def animate(i, X_Sat, Y_Sat, Z_Sat, SAT):
            txt1.set_text('Elapsed Time ={:f} sec'.format(i*timeStep)) # for debug purposes
            # txt2.set_text('Mean alt from Earth ={:f} km'.format(mean_alt[i])) # for debug purposes
            x_Sat = X_Sat[i]  # X-coord [km] of satellite over time interval 
            y_Sat = Y_Sat[i]  # Y-coord [km] of satellite over time interval
            z_Sat = Z_Sat[i] # Z-coord [km] of satellite over time interval
            SAT.set_data([x_Sat], [y_Sat])
            SAT.set_3d_properties([z_Sat])
            return SAT

        numb=len(X_Sat)
        # ani = animation.FuncAnimation(fig, animate, len(X_Sat), fargs=(sol, SAT), interval=10)
        ani = animation.FuncAnimation(fig, animate, numb, fargs=(X_Sat, Y_Sat, Z_Sat, SAT), interval=1000/fps)
        plt.show()

    return X_Sat, Y_Sat, Z_Sat




def plot_Orbit(X_Sat, Y_Sat, Z_Sat, simTime, simTimeStep, mod, ra):

    """    
    Reproduce an orbit from ephemerides vectors in ECI/ECEF
    
    INPUTS: 
     - X, Y, Z _Sat: initial state for the integration
     - simTime: duration of the simulation
     - simTimeStep: time step of ephemeris [s]
     - mod: mode for plotting (dynamic or static)
     - ra: apoapsis of the orbit [km], to set plot-limits
    
    """
    

    
    img = plt.imread('utilities/world_map.png')
    # img = plt.imread('utilities/index.jpeg')

    n_steps = int(simTime / simTimeStep) # number of steps to plot
    # print(n_steps)
    mean_alt = np.empty(len(X_Sat))
    for kk in range(0, len(X_Sat)):
        mean_alt[kk] = math.sqrt(X_Sat[kk]**2 + Y_Sat[kk]**2 + Z_Sat[kk]**2) - AC.R_E

    # Setting up Spherical Earth to Plot----------------------
    if mod == 'dyn':
        N = 50
        phi = np.linspace(0, 2 * np.pi, N)
        theta = np.linspace(0, np.pi, N)
    if mod == 'static':
        theta = np.linspace(0, np.pi, img.shape[0])
        phi = np.linspace(0, 2*np.pi, img.shape[1])
        count = 550 # keep 180 points along theta and phi
        theta_inds = np.linspace(0, img.shape[0] - 1, count).round().astype(int)
        phi_inds = np.linspace(0, img.shape[1] - 1, count).round().astype(int)
        theta = theta[theta_inds]
        phi = phi[phi_inds]
        img = img[np.ix_(theta_inds, phi_inds)]
    # ---------------------------------------------------------
    
    theta, phi = np.meshgrid(theta, phi)


    d = 1.8 * AC.R_E # quiver dimensions (function of Earth radium)
    X_Earth = AC.R_E * np.cos(phi) * np.sin(theta)
    Y_Earth = AC.R_E * np.sin(phi) * np.sin(theta)
    Z_Earth = AC.R_E * np.cos(theta)

    if mod == 'static':
        # Plotting Earth and Orbit
        fig = plt.figure(figsize=(10,10))
        ax = plt.axes(projection='3d')
        # ax.plot_surface(X_Earth, Y_Earth, Z_Earth, color='blue', alpha=0.2)
        ax.plot_surface(X_Earth.T, Y_Earth.T, Z_Earth.T, facecolors=img/1,  cstride=1, rstride=1, alpha=0.4) # we've already pruned ourselves
        
        for ii in range(0, len(Y_Sat)):
            if X_Sat[ii] > 0:
                ax.plot3D(X_Sat[ii], Y_Sat[ii], Z_Sat[ii], 'k.', markersize=2)
            else:
                ax.plot3D(X_Sat[ii], Y_Sat[ii], Z_Sat[ii], 'k.', zorder=10, markersize=2)
        
        ax.plot3D(X_Sat[1], Y_Sat[1], Z_Sat[1], 'r--', label="SAT propagated")
        ax.plot3D(X_Sat[0], Y_Sat[0], Z_Sat[0], 'b.', markersize=10, label="SAT first state")
        
        
        ax.quiver([0, 0, 0], [0, 0, 0], [0, 0, 0], [d, 0, 0], [0, d, 0], [0, 0, 0], colors=['k'], linewidths=1, arrow_length_ratio=0.1)
        ax.view_init(15, -180)  # Changing viewing angle (adjust as needed)
        plt.title('Two-Body Orbit')
        ax.set_xlabel('X [km]')
        ax.set_ylabel('Y [km]')
        ax.set_zlabel('Z [km]')
        txt = fig.suptitle('')
        ax.grid(False)
        # Make axes limits
        XYZlim = ra *1.5
        # ax.set_xlim3d(-XYZlim, XYZlim)
        # ax.set_ylim3d(-XYZlim, XYZlim)
        # ax.set_zlim3d(-XYZlim, XYZlim)
        plt.legend(loc="upper left")
        plt.show()
    
    if mod == 'dyn':

        fig = plt.figure(figsize=(15,7))
        ax = plt.axes(projection='3d')
        ax.plot3D(0, 0, 0,'g.', markersize=50, label="Earth center")
        ax.plot3D(X_Sat[0:n_steps], Y_Sat[0:n_steps], Z_Sat[0:n_steps],'r--', markersize=0.6, label="SAT propagated")
        ax.quiver([0, 0, 0], [0, 0, 0], [0, 0, 0], [d, 0, 0], [0, d, 0], [0, 0, d], colors=['k'], linewidths=1)
        SAT, = ax.plot(X_Sat, Y_Sat, Z_Sat,'b.', markersize=10, label="SAT")
        ax.view_init(30, 145)  # Changing viewing angle (adjust as needed)
        plt.title('Two-Body Orbit')
        ax.set_xlabel('X [km]')
        ax.set_ylabel('Y [km]')
        ax.set_zlabel('Z [km]')
        txt1 = fig.suptitle('')


        #---Make axes limits
        XYZlim = ra
        ax.set_xlim3d(-XYZlim, XYZlim)
        ax.set_ylim3d(-XYZlim, XYZlim)
        ax.set_zlim3d(-XYZlim, XYZlim)
        plt.legend(loc="upper left")

        #---Dynamic plot:
        timeStep = simTimeStep
        fps = 100 # frames per sec
        print('matplotlib: '+matplotlib.__version__)
        print('numpy: '+np.version.full_version)
        def animate(i, X_Sat, Y_Sat, Z_Sat, SAT):
            txt1.set_text('Elapsed Time ={:f} sec'.format(i*timeStep)) # for debug purposes
            # txt2.set_text('Mean alt from Earth ={:f} km'.format(mean_alt[i])) # for debug purposes
            x_Sat = X_Sat[i]  # X-coord [km] of satellite over time interval 
            y_Sat = Y_Sat[i]  # Y-coord [km] of satellite over time interval
            z_Sat = Z_Sat[i] # Z-coord [km] of satellite over time interval
            SAT.set_data([x_Sat], [y_Sat])
            SAT.set_3d_properties([z_Sat])
            return SAT

        # ani = animation.FuncAnimation(fig, animate, len(X_Sat), fargs=(sol, SAT), interval=10)
        ani = animation.FuncAnimation(fig, animate, n_steps, fargs=(X_Sat, Y_Sat, Z_Sat, SAT), interval=1000/fps)
        plt.show()





def plot_MultiOrbit_R(RRx, RRy, RRz, sats, timestep, SMA, mission):

    """    
    Reproduce the constellation orbits from ephemeris file in ECI
    !!! THIS FUNCTION WORKS JUST FOR A 4 SATS CONSTELLATION !!!
    
    INPUTS: 
     RRx = x coordinates of satellites [instants x n_sats] [km]
     RRy = y coordinates of satellites [instants x n_sats] [km]
     RRz = z coordinates of satellites [instants x n_sats] [km]
     sats = n of satellites
     timestep: simulation time_step [sec]
     SMA:  of first orbit [km], to set plot-limits
     
    """


    # img = plt.imread('utilities/images_earth/world_map.png')
    img = plt.imread('utilities/images_earth/earth_bw.png')

    # Setting up Spherical Earth to Plot----------------------
    theta = np.linspace(0, np.pi, img.shape[0])
    phi = np.linspace(0, 2*np.pi, img.shape[1])
    count = 800 # keep 180 points along theta and phi
    theta_inds = np.linspace(0, img.shape[0] - 1, count).round().astype(int)
    phi_inds = np.linspace(0, img.shape[1] - 1, count).round().astype(int)
    theta = theta[theta_inds]
    phi = phi[phi_inds]
    img = img[np.ix_(theta_inds, phi_inds)]
    # ---------------------------------------------------------
    theta, phi = np.meshgrid(theta, phi)

    d = 1.5 * AC.R_E # quiver dimensions (function of Earth radium)
    X_Earth = AC.R_E * np.cos(phi) * np.sin(theta)
    Y_Earth = AC.R_E * np.sin(phi) * np.sin(theta)
    Z_Earth = AC.R_E * np.cos(theta)

    fig = plt.figure(figsize=(17,12))
    ax1= fig.add_subplot(1,2,1, projection='3d')

    # Plotting Earth and Orbit  SIDE VIEW
    ax1.plot_surface(X_Earth.T, Y_Earth.T, Z_Earth.T, facecolors=img/1,  cstride=1, rstride=1, alpha=0.8, zorder=5) # we've already pruned ourselves
    

    # Plot time:
    T = C2K.period(SMA) # [sec]
    steps = 1*int(round(T/timestep))
    
    # Plotting the orbit traces ( using just the first sat of each plane: the first satellites )
    # for jj in range(0, int(sats/2)): # over half sats
    for jj in range(0, sats): # over half sats
        for ii in range(0, steps): # over time
            if RRx[ii, jj] > 0:
                ax1.plot3D(RRx[ii, jj], RRy[ii, jj], RRz[ii, jj], 'k.', markersize=2, zorder=3)
            else:
                ax1.plot3D(RRx[ii, jj], RRy[ii, jj], RRz[ii, jj], 'k.', zorder=10, markersize=2)

    # Initial location of each satellite:
    #...first satellites are all on different planes (pl#)
    pl = 0 # plane number
    for jj in range(0, sats):
        pl += 1
        if pl > (sats/2):
            PL = int(pl-(sats/2))
            sc = 2
        else:
            PL = pl
            sc = 1
        if RRx[0, jj] > 0: # checking first x for each satellite, for the plot
            ax1.plot3D(RRx[0, jj], RRy[0, jj], RRz[0, jj], markersize=20, marker='.', color = ST.Colors[jj], label="S{}_P{}".format(sc, PL), zorder=3) 
        else:
            ax1.plot3D(RRx[0, jj], RRy[0, jj], RRz[0, jj], markersize=20, marker='.', color = ST.Colors[jj], label="S{}_P{}".format(sc, PL), zorder=10) 
     

    ax1.quiver([0, 0, 0], [0, 0, 0], [0, 0, 0], [d, 0, 0], [0, d, 0], [0, 0, 0], colors=['k'], linewidths=1, arrow_length_ratio=0.1)
    ax1.view_init(15, -180)  # Changing viewing angle (adjust as needed)
    plt.title('SIDE VIEW')
    ax1.set_xlabel('X [km]')
    ax1.set_ylabel('Y [km]')
    ax1.set_zlabel('Z [km]')
    ax1.grid(False)
    ax1.legend(loc="upper left")


    # Plotting Earth and Orbit  TOP VIEW
    ax2= fig.add_subplot(1,2,2, projection='3d')

    ax2.plot_surface(X_Earth.T, Y_Earth.T, Z_Earth.T, facecolors=img/1,  cstride=1, rstride=1, alpha=0.4, zorder=5) # we've already pruned ourselves
 
    # Initial location of each satellite:
    #...first satellites are all on different planes (pl#)
    pl = 0 # plane number
    for jj in range(0, sats):
        pl += 1
        if pl > (sats/2):
            PL = int(pl-(sats/2))
            sc = 2
        else:
            PL = pl
            sc = 1
        if RRz[0, jj] < 0: # checking z of each satellite, for the plot 
            ax2.plot3D(RRx[0, jj], RRy[0, jj], RRz[0, jj], marker='.', color = ST.Colors[jj], markersize=20, label="S{}_P{}".format(sc, PL), zorder=3) 
        else:
            ax2.plot3D(RRx[0, jj], RRy[0, jj], RRz[0, jj], marker='.', color = ST.Colors[jj], markersize=20, label="S{}_P{}".format(sc, PL), zorder=10) 
        

    # Plot time:
    T = C2K.period(SMA) # [sec]
    steps = 1*int(round(T/timestep))
    
    # Plotting the orbit traces ( using just the first sat of each plane: the first satellites )
    # for jj in range(0, int(sats/2)):
    for jj in range(0, sats): # over half sats

        for ii in range(0, steps): # over time
            if RRz[ii, jj] < 0: # checking z of each satellite, for the plot
                ax2.plot3D(RRx[ii, jj], RRy[ii, jj], RRz[ii, jj], 'k.', markersize=2, zorder=3)
            else:
                ax2.plot3D(RRx[ii, jj], RRy[ii, jj], RRz[ii, jj], 'k.', zorder=10, markersize=2)


    ax2.quiver([0, 0, 0], [0, 0, 0], [0, 0, 0], [d, 0, 0], [0, d, 0], [0, 0, 0], colors=['k'], linewidths=1, arrow_length_ratio=0.1)
    ax2.view_init(90, -180)  # Changing viewing angle (adjust as needed)
    plt.title('POLAR VIEW')
    ax2.set_xlabel('X [km]')
    ax2.set_ylabel('Y [km]')
    ax2.set_zlabel('Z [km]')
    txt = fig.suptitle('')
    ax2.grid(False)
    ax2.legend(loc="upper left")

    plt.suptitle('Constellation plot: %s' %mission)
    plt.show()





