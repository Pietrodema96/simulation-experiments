from mpl_toolkits.basemap import Basemap
import numpy as np
import math
import matplotlib.pyplot as plt
from numpy.random import uniform
from matplotlib.patches import Polygon
import utilities.spaceConv_utilities as SCU
import utilities.plotting_utilities_GT as PUGT
import utilities.AstroConstants as AC
import utilities.CarKep as CK




def node_precession(SMA, ECC, INC, orbit):
    # Computation of nodal precessino of the orbit
    # SMA = semi-major axis [km]
    # ECC = eccentricity [-]
    # INC = inclination [deg]
    # orbit = 'SSO' or 'gen'
    if orbit == 'SSO':
        w = 1
    else:
        rotVel = 2*math.pi / CK.period #  period in [sec] - velocity in [rad/s]
        w = - (3/2) * ( AC.R_E**2 (SMA*(1-ECC**2))**2 ) * AC.J2_EARTH * rotVel * math.cos(AC.D2R*INC)

    return w # [rad/s]

