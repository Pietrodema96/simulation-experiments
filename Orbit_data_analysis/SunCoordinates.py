import astropy
from astropy.time import Time
from astropy import coordinates
from astropy.time import Time
from astropy.coordinates import solar_system_ephemeris, EarthLocation

import os, csv
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt




# #--------------- PLEIADES -----------------

# sim_timeStep = 10 # [sec]
# sim_initEpoch1 = '02-12-2012 12:00:00'
# sim_initEpoch11 = '2012-12-02 12:00:00'

# date_format_str1 = '%d-%m-%Y %H:%M:%S'
# d1 = datetime.strptime(sim_initEpoch1, date_format_str1)
# date_format_str11 = '%Y-%m-%d %H:%M:%S'
# d11 = datetime.strptime(sim_initEpoch11, date_format_str11)
# # added_seconds = timedelta(seconds=25)
# # new_datetime = d1 + added_seconds

# orbit_period = 6179.918
# n_sats = 4 # numb satellites
# phasing = 1/n_sats 
# orbit_phasing = round(phasing * orbit_period) # [sec]

# secSat = [0, 0, 0, 0]
# for i in range(1, n_sats+1):
#     secSat[i-1] = int(round((orbit_phasing * i) / sim_timeStep)) # index when sats start
# print('Here the indexes from when sats start:')
# print(secSat)

# # data= pd.read_csv("DATA/Sim1/S1-P1_ephemeris.csv") # read the ephemeris file to use for analysis
# # data= pd.read_csv("DATA/Sim2/S1-P1_ephemeris.csv") # read the ephemeris file to use for analysis
# data= pd.read_csv("DATA/Sim3/S1-P1_ephemeris.csv") # read the ephemeris file to use for analysis

# # Recovering data from the original single satellite file:
# elapsed_times = data['T'][:] # elapsed seconds [sec]
# gps_sec = data['GPSS'][:] # gps seconds [sec]

# # Compute the Sun positions in ECI:
# # RA [deg]
# # DEC [deg]
# # Dist [AU]
# with solar_system_ephemeris.set('de432s'):
#     with open('DATA/Sim3/SUNPOS_PL.csv', 'w', encoding='UTF8', newline='') as f:
#         start1 = 0
#         end1 = len(elapsed_times)-secSat[3]
#         writer = csv.writer(f)
#         writer.writerow(['ElapsedTime', 'UTCtime', 'SUN_ra', 'SUN_dec', 'SUN_dist'])
#         timeD = timedelta(sim_timeStep)
#         for kk in range(start1, end1 ):
#             date1 = d1 + timedelta(seconds=elapsed_times[kk])
#             date11 = d11 + timedelta(seconds=elapsed_times[kk])
#             d = Time(date11)
#             sc = coordinates.get_sun(d)
#             ra = sc.ra.degree
#             dec = sc.dec.degree
#             dist = sc.distance.km
#             writer.writerow([elapsed_times[kk], date1, ra, dec, dist])

# with open('DATA/Sim3/TIMES_PL.csv', 'w', encoding='UTF8', newline='') as f:
#     writer = csv.writer(f)
#     writer.writerow(['ElapsedTime', 'GPSsec', 'UTCtime'])
#     timeD = timedelta(sim_timeStep)
#     for kk in range(start1, end1 ):
#         date = d1 + timedelta(seconds=elapsed_times[kk])
#         writer.writerow([elapsed_times[kk], gps_sec[kk], date])





#--------------- ENVISAT -----------------

# sim_timeStep = 10 # [sec]
# sim_initEpoch1 = '01-03-2002 02:53:40'
# sim_initEpoch11 = '2002-03-01 02:53:40'

# date_format_str1 = '%d-%m-%Y %H:%M:%S'
# d1 = datetime.strptime(sim_initEpoch1, date_format_str1)
# date_format_str11 = '%Y-%m-%d %H:%M:%S'
# d11 = datetime.strptime(sim_initEpoch11, date_format_str11)
# # added_seconds = timedelta(seconds=25)
# # new_datetime = d1 + added_seconds

# # data= pd.read_csv("DATA/Sim1/ES1_P1.csv") # read the ephemeris file to use for analysis
# # data= pd.read_csv("DATA/Sim2/ES1_P1.csv") # read the ephemeris file to use for analysis
# data= pd.read_csv("DATA/Sim3/ES1_P1.csv") # read the ephemeris file to use for analysis


# # Recovering data from the original single satellite file:
# elapsed_times = data['T'][:] # elapsed seconds [sec]
# gps_sec = data['GPSS'][:] # gps seconds [sec]

# # Compute the Sun positions in ECI:
# # RA [deg]
# # DEC [deg]
# # Dist [AU]
# start1 = 0
# end1 = len(gps_sec)
# with solar_system_ephemeris.set('de432s'):
#     with open('DATA/Sim3/SUNPOS_ENV.csv', 'w', encoding='UTF8', newline='') as f:

#         writer = csv.writer(f)
#         writer.writerow(['ElapsedTime', 'UTCtime', 'SUN_ra', 'SUN_dec', 'SUN_dist'])
#         timeD = timedelta(sim_timeStep)
#         for kk in range(start1, end1 ):
#             date1 = d1 + timedelta(seconds=elapsed_times[kk])
#             date11 = d11 + timedelta(seconds=elapsed_times[kk])
#             d = Time(date11)
#             sc = coordinates.get_sun(d)
#             ra = sc.ra.degree
#             dec = sc.dec.degree
#             dist = sc.distance.km
#             writer.writerow([elapsed_times[kk], date1, ra, dec, dist])

# with open('DATA/Sim3/TIMES_ENV.csv', 'w', encoding='UTF8', newline='') as f:
#     writer = csv.writer(f)
#     writer.writerow(['ElapsedTime', 'GPSsec', 'UTCtime'])
#     timeD = timedelta(sim_timeStep)
#     for kk in range(start1, end1):
#         date = d1 + timedelta(seconds=elapsed_times[kk])
#         writer.writerow([elapsed_times[kk], gps_sec[kk], date])



#--------------- CUSTOM -----------------
# Input the same date in the two formats:
sim_timeStep = 10 # [sec]
sim_initEpoch1 = '15-10-1994 00:00:00'
sim_initEpoch11 = '1994-10-15 00:00:00'
mission = 'SatRev'

date_format_str1 = '%d-%m-%Y %H:%M:%S'
d1 = datetime.strptime(sim_initEpoch1, date_format_str1)
date_format_str11 = '%Y-%m-%d %H:%M:%S'
d11 = datetime.strptime(sim_initEpoch11, date_format_str11)

data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1_P1.csv".format(mission))) # read the ephemeris file to use for analysis


# Recovering data from the original single satellite file:
elapsed_times = data['ElapsedTime'][:] # elapsed seconds [sec]
gps_sec = data['GPSsec'][:] # gps seconds [sec]

# Compute the Sun positions in ECI:
# RA [deg]
# DEC [deg]
# Dist [AU]
start1 = 0
end1 = len(gps_sec)
with solar_system_ephemeris.set('de432s'):
    with open(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/SUNPOS_{}.csv".format(mission,mission)), 'w', encoding='UTF8', newline='') as f:

        writer = csv.writer(f)
        writer.writerow(['ElapsedTime', 'UTCtime', 'SUN_ra', 'SUN_dec', 'SUN_dist'])
        timeD = timedelta(sim_timeStep)
        for kk in range(start1, end1):
            date1 = d1 + timedelta(seconds=elapsed_times[kk])
            date11 = d11 + timedelta(seconds=elapsed_times[kk])
            d = Time(date11)
            sc = coordinates.get_sun(d)
            ra = sc.ra.degree
            dec = sc.dec.degree
            dist = sc.distance.km
            writer.writerow([elapsed_times[kk], date1, ra, dec, dist])

with open(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TIMES_{}.csv".format(mission,mission)), 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['ElapsedTime', 'GPSsec', 'UTCtime'])
    timeD = timedelta(sim_timeStep)
    for kk in range(start1, end1):
        date = d1 + timedelta(seconds=elapsed_times[kk])
        writer.writerow([elapsed_times[kk], gps_sec[kk], date])