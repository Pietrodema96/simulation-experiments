
import math, time, sys, os
from numpy import diff
from os import times
import numpy as np
import pandas as pd
import math as m

import matplotlib.pyplot as plt
from numpy import linalg as la
from datetime import datetime, timedelta
import utilities.CarKep as CK
from astropy.time import Time
import utilities.AstroConstants as AC
import utilities.plotting_utilities_GT as PUGT
import utilities.plotting_utilities as PU
import utilities.osc2mean as OM


def mean_diff():

    path_ = os.path.abspath('Orbit_data_analysis/')
    sys.path.append('{}'.format(path_))

    data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/Test/Output/Ephemeris/S1_P1.csv")) 
    step = 10 # [sec]
    timeSpan = 3600*24*4  # 3 hours simulation [sec]
    n_points = int(timeSpan / step) # for the integration with Time Law and plot

    # Recovering data from the original single satellite file:
    elapsed_times = data['ElapsedTime'][:n_points] # elapsed seconds [sec]
    X_eci = data['Rx_eci'][:n_points] # ECI X positions [m]
    Y_eci = data['Ry_eci'][:n_points] # ECI Y positions [m]
    Z_eci = data['Rz_eci'][:n_points] # ECI Z positions [m]
    VX_eci = data['Vx_eci'][:n_points] # ECI VX velocities [m/s]
    VY_eci = data['Vy_eci'][:n_points] # ECI VZ velocities [m/s]
    VZ_eci = data['Vz_eci'][:n_points] # ECI VZ velocities [m/s]

    # Initializing arrays
    osc_elem = np.zeros((n_points, 6))
    mean_elem = np.zeros((n_points, 6))
    rr = np.zeros(3)
    vv = np.zeros(3)
    diff_mean_el = np.zeros((n_points-1, 6))

    # print(rr)
    for i in range(0, n_points):
        rr[0] = X_eci[i]
        rr[1] = Y_eci[i]
        rr[2] = Z_eci[i]
        vv[0] = VX_eci[i]
        vv[1] = VY_eci[i]
        vv[2] = VZ_eci[i]
        
        # Cartesia and Keplerian conversion:
        osc_elem[i, :] = CK.CtoK(AC.MU_E, rr/1000, vv/1000) # [km rad]                          [CURTIS VERSION]
        # osc_elem[i, :] = CK.keplerian_elems_from_cart(AC.MU_E, rr/1000, vv/1000) # [km rad]   [OREKIT VERSION]
        
        # Possible representations:
        _, mean_elem[i,:] = OM.osc2mean(rr/1000, vv/1000, 0)

    # Differentiating:
    diff_mean_el[:, 0] = diff(mean_elem[:,0])/AC.D2R/step # mean SMA differentiation
    diff_mean_el[:, 1] = diff(mean_elem[:,1])/AC.D2R/step # mean ECC differentiation
    diff_mean_el[:, 2] = diff(mean_elem[:,2])/AC.D2R/step # mean INC differentiation
    diff_mean_el[:, 3] = diff(mean_elem[:,3])/AC.D2R/step # mean RAAN differentiation
    diff_mean_el[:, 4] = diff(mean_elem[:,4])/AC.D2R/step # mean AOP differentiation
    diff_mean_el[:, 5] = diff(osc_elem[:,5])/AC.D2R/step # osc TA differentiation

    d = 60*60*24 # sec/day
    fig, axs = plt.subplots(2, 3, figsize=(23, 15))
    parax1 = axs[0, 0].twinx()
    axs[0, 0].plot(elapsed_times[0:n_points] ,mean_elem[:,0], '--bo')
    parax1.plot(elapsed_times[0:n_points-1] ,diff_mean_el[:,0], 'r')
    axs[0, 0].set_title("SMA ")
    axs[0, 0].set_xlabel(" [s] ")
    axs[0, 0].set_ylabel(" [km] ")

    parax2 = axs[0, 1].twinx()
    axs[0, 1].plot(elapsed_times[0:n_points] ,mean_elem[:,1], '--bo')
    parax2.plot(elapsed_times[0:n_points-1] ,diff_mean_el[:,1], 'r')
    axs[0, 1].set_title("ECC")
    axs[0, 1].set_xlabel(" [dasys] ")
    axs[0, 1].set_ylabel(" [-] ")
    
    parax3 = axs[0, 2].twinx()
    axs[0, 2].plot(elapsed_times[0:n_points] ,mean_elem[:,2]/AC.D2R, '--bo')
    parax3.plot(elapsed_times[0:n_points-1] ,diff_mean_el[:,2], 'r')
    axs[0, 2].set_title("INC")
    axs[0, 2].set_xlabel(" [s] ")
    axs[0, 2].set_ylabel(" [deg] ")
    
    parax4 = axs[1, 0].twinx()
    axs[1, 0].plot(elapsed_times[0:n_points] ,mean_elem[:,3]/AC.D2R, '--bo')
    parax4.plot(elapsed_times[0:n_points-1] ,diff_mean_el[:,3], 'r')
    axs[1, 0].set_title("RAAN")
    axs[1, 0].set_xlabel(" [s] ")
    axs[1, 0].set_ylabel(" [deg] ")
    
    parax5 = axs[1, 1].twinx()
    axs[1, 1].plot(elapsed_times[0:n_points] ,mean_elem[:,4]/AC.D2R, '--bo')
    parax5.plot(elapsed_times[0:n_points-1] ,diff_mean_el[:,4], 'r')
    axs[1, 1].set_title("om")
    axs[1, 1].set_xlabel(" [s] ")
    axs[1, 1].set_xlabel(" [deg] ")

    parax6 = axs[1, 2].twinx()
    axs[1, 2].plot(elapsed_times[0:n_points] ,osc_elem[:,5]/AC.D2R, '--bo')
    parax6.plot(elapsed_times[0:n_points-1] ,diff_mean_el[:,5], 'r')
    axs[1, 2].set_title("TA")
    axs[1, 2].set_xlabel(" [s] ")
    axs[1, 2].set_xlabel(" [deg] ")
    
    plt.suptitle('Derivatives of OE (red) and ME (blue) by time')
    plt.show()

    return

def mean_el():
    path_ = os.path.abspath('Orbit_data_analysis/')
    # print(path_)
    sys.path.append('{}'.format(path_))


    data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/Test/Output/Ephemeris/S1_P1.csv")) 
    step = 10 # [sec]
    timeSpan = 3600*24*4  # 3 hours simulation [sec]
    n_points = int(timeSpan / step) # for the integration with Time Law and plot

    # Recovering data from the original single satellite file:
    elapsed_times = data['ElapsedTime'][:n_points] # elapsed seconds [sec]
    X_eci = data['Rx_eci'][:n_points] # ECI X positions [m]
    Y_eci = data['Ry_eci'][:n_points] # ECI Y positions [m]
    Z_eci = data['Rz_eci'][:n_points] # ECI Z positions [m]
    VX_eci = data['Vx_eci'][:n_points] # ECI VX velocities [m/s]
    VY_eci = data['Vy_eci'][:n_points] # ECI VZ velocities [m/s]
    VZ_eci = data['Vz_eci'][:n_points] # ECI VZ velocities [m/s]

    osc_elem = np.zeros((n_points, 6))
    osc_elem_EQ = np.zeros((n_points, 6))
    mean_elem = np.zeros((n_points, 6))
    EU_mean_elem = np.zeros((n_points, 6))

    rr = np.zeros(3)
    vv = np.zeros(3)
    h_osc = np.zeros(n_points)
    l_osc = np.zeros(n_points)

    # print(rr)
    for i in range(0, n_points):
        rr[0] = X_eci[i]
        rr[1] = Y_eci[i]
        rr[2] = Z_eci[i]
        vv[0] = VX_eci[i]
        vv[1] = VY_eci[i]
        vv[2] = VZ_eci[i]
        
        # Cartesia and Keplerian conversion:
        osc_elem[i, :] = CK.CtoK(AC.MU_E, rr/1000, vv/1000) # [km rad]                          [CURTIS VERSION]
        # osc_elem[i, :] = CK.keplerian_elems_from_cart(AC.MU_E, rr/1000, vv/1000) # [km rad]   [OREKIT VERSION]
        osc_elem_EQ[i, :] = CK.equinoctial_elems_from_kep(osc_elem[i, :]) # [km -]    [EQUINOTIAL - Orekit version]

        
        h_osc[i] = osc_elem[i,1]*m.sin(osc_elem[i,4])
        l_osc[i] = osc_elem[i,1]*m.cos(osc_elem[i,4])
        pr_scal = np.dot(rr, vv)
        # Possible representations:
        M_osc = CK.meanAnomaly(AC.MU_E, rr, vv, osc_elem[i, 0], osc_elem[i, 1])
        osc_elem[i, 5] = M_osc + osc_elem[i, 4] # AOP + M = AOL_U [rad]
        EU_mean_elem[i,:], mean_elem[i,:] = OM.osc2mean(rr/1000, vv/1000, 0)

    # Saving the image file:
    # !!! Ustinov mean set !!!
    # mean_elements_ustinov = [a_mean, h_mean, l_mean, i_mean, OM_mean, lambda_mean]
    # !!! Keplerian mean set !!!
    # mean_elements=[a_mean, e_mean, i_mean, OM_mean, om_mean, M_mean]

    pth = os.path.abspath("./Orbit_data_analysis/Figures_of_merit/")
    PU.plotOrbit_MEAN_Elements(elapsed_times[0:n_points], osc_elem, mean_elem, EU_mean_elem, pth)
    PU.plotOrbit_MEAN_ElementsEU(elapsed_times[0:n_points], osc_elem, EU_mean_elem, pth, h_osc, l_osc)
    PU.plotOrbitElements_Equinotial(elapsed_times[0:n_points], osc_elem_EQ)

    return


if __name__ == '__main__':
    mean_diff()