from cmath import pi
import math, csv
from turtle import color, width
import matplotlib.pyplot as plt
import matplotlib 
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
import utilities.AstroConstants as AC
import utilities.CarKep as C2K
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.plotting_utilities as PU
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU
m2km = 1000

# TIME LAW ORBIT INTEGRATION
# This function simply integrate an orbit with the classicle time-law

# Time values
step = 60 # [sec]
timeSpan = 86400.0 # [sec]
n_points = int(timeSpan / step)

# Initial Conditions
X_0 = 6476081.66706421 /1000   # [km]
Y_0 = 2104206.4880419965 /1000   # [km]
Z_0 = -41.46811186889096 /1000  # [km]
VX_0 = 0.  # [km/s]
VY_0 = 127.62572521662743 /1000  # [km/s]
VZ_0 =  7687.947851134086 /1000 # [km/s]
state_0 = [X_0, Y_0, Z_0, VX_0, VY_0, VZ_0]


# Final Conditions
X_E = -2651373.818066721 /1000   # [km]
Y_E = -728790.4562371197 /1000   # [km]
Z_E = 6867802.980910127 /1000  # [km]
VX_E = -6480.213813899842 / 1000  # [km/s]
VY_E = -2139.6553932925804 /1000  # [km/s]
VZ_E =  -2674.906836349 /1000 # [km/s]
state_E = [X_E, Y_E, Z_E, VX_E, VY_E, VZ_E]

# Time Array
t = np.arange(0, timeSpan+60, 60)  
# print(t)

# Solving ODE
sol_0 = odeint(PU.model_2BP, state_0, t)
X_Sat_IN = sol_0[:, 0]  # X-coord [km] of satellite over time interval 
Y_Sat_IN = sol_0[:, 1]  # Y-coord [km] of satellite over time interval
Z_Sat_IN = sol_0[:, 2]  # Z-coord [km] of satellite over time interval

sol_E = odeint(PU.model_2BP, state_E, t)
X_Sat_E = sol_E[:, 0]  # X-coord [km] of satellite over time interval 
Y_Sat_E = sol_E[:, 1]  # Y-coord [km] of satellite over time interval
Z_Sat_E = sol_E[:, 2]  # Z-coord [km] of satellite over time interval


with open('DATA/AISAC/S1_P1_orbits.csv', 'w', encoding='UTF8', newline='') as f:
# with open('DATA/Sim2/S2-P1_ephemeris.csv', 'w', encoding='UTF8', newline='') as f:
# with open('DATA/Sim3/S2-P1_ephemeris.csv', 'w', encoding='UTF8', newline='') as f:


    writer = csv.writer(f)
    writer.writerow(['ElapsedTime', 'Rx_eciSTART', 'Ry_eciSTART', 'Rz_eciSTART', 'Rx_eciEND', 'Ry_eciEND', 'Rz_eciEND'])

    # Computing data for the 2nd satellite:
    for kk in range(0, len(t)):
        # Write down the position of initial and final orbit:
        writer.writerow([t[kk], X_Sat_IN[kk]*1000, Y_Sat_IN[kk]*1000, Z_Sat_IN[kk]*1000, X_Sat_E[kk]*1000, Y_Sat_E[kk]*1000, Z_Sat_E[kk]*1000]) # [m]

fig = plt.figure(figsize=(15,7))
ax = fig.gca(projection='3d')
#plotEarth(ax)
ax.plot(X_Sat_IN, Y_Sat_IN, Z_Sat_IN, 'r-', zorder =10)
ax.plot(X_Sat_E, Y_Sat_E, Z_Sat_E, 'r-', zorder =10)


ax.view_init(30, 145)  # Changing viewing angle (adjust as needed)
plt.title('Two-Body Orbit')
ax.set_xlabel('X [km]')
ax.set_ylabel('Y [km]')
ax.set_zlabel('Z [km]')
ax.set_box_aspect([1,1,1])
plt.show()