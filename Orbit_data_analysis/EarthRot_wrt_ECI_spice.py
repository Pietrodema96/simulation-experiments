# -----------------------
# EARTH ROTATION WRT ECI
# -----------------------
# This script inspects the Earth relative rotation in quaternions with respect to the ECI RF
# Reference:
# https://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 22/03/2022
#
#---------------------------------------------------

from astropy.time import Time
from astropy.time import Time
import os, csv, math, sys, time
import numpy as np
import pandas as pd
import utilities.spaceConv_utilities as MF
path_ = os.path.abspath('Orbit_data_analysis/utilities/')
print(path_)
sys.path.append('{}'.format(path_))

import spiceypy as spice
from datetime import datetime, timedelta
import AstroConstants as AC


def Earth_rots(mission):
    """
    This function provide the Earth rotation of IAU frame
    wrt the J2000 frame
    Data are computed wrt the SPICE kernels
    
    INPUT:
        mission = mission name [str]
    
    """
    
    print('\nStaring to write IAU Earth-orientations file...')
    t = time.time()
    data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TIMES_{}.csv".format(mission, mission)))


    # Recovering data from the original single satellite file:
    elapsed_times = data['ElapsedTime'][:] # elapsed seconds [sec]
    utc_times = data['UTCtime'][:] # elapsed seconds [sec]

    spice.furnsh(os.path.abspath('./SPICE_experiments/kernel/MetaK.txt'))


    with open(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/EQUAT_{}.csv".format(mission, mission)), 'w', encoding='UTF8', newline='') as f:

        writer = csv.writer(f)
        writer.writerow(['ElapsedTime', 'qw', 'qx', 'qy', 'qz'])
        for kk in range(0, len(elapsed_times)):
            matrix = spice.pxform("J2000", "IAU_EARTH", spice.str2et(str(utc_times[kk])))
            qw, qx, qy, qz = MF.Matrix2Quaternions(matrix[0,0], matrix[0,1], matrix[0,2], matrix[1,0], matrix[1,1], matrix[1,2], matrix[2,0], matrix[2,1], matrix[2,2])
            writer.writerow([elapsed_times[kk], qw, qx, qy, qz])
    
    # Printing when completed:
    elapsed = time.time() - t
    print('Earth-orientations file written!')
    print('[time taken: {} sec]'.format(round(elapsed, 4)))

    # clean up the kernels
    spice.kclear()

    return

# #--------------------------------------


# date = datetime(1994, 10, 15, 0, 0, 0)

# # get et values
# et = spice.str2et(str(date))

# matrix = spice.pxform("J2000", "IAU_EARTH", et)

# print(matrix)

if __name__ == "__main__":

    mission = 'Test'

    Earth_rots(mission)