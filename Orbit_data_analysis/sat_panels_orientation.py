# RELATIVE POSITION BETWEEN SOLAR PANELS NORMAL AND SUN
# Computation of relative angle between the sun vector and
# solar panels normal
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 17/05/2022
#
# YAW = z coordinate
# ROLL = x coordinate
# PITCH = y coordinate
#
#---------------------------------------------------

import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import utilities.CarKep as C2K
import utilities.AstroConstants as AC
import utilities.spaceConv_utilities as SCU


points = 1000
time_step = 10 # [s]

# Coordinate of the Sun in time:
# f = "DATA/Sim1/SUNPOS_PL_10S_1M_212201212.csv"
f = "DATA/TestPanels/SUNPOS_pan.csv"

data_Sun = pd.read_csv(f)
RA = data_Sun['SUN_ra'][0:points]
DEC = data_Sun['SUN_dec'][0:points]
dist = data_Sun['SUN_dist'][0:points]

# Coordinate of the Sat in time:
R = np.zeros((points, 3))
V = np.zeros((points, 3))

# f1 = "DATA/Sim1/SC_PL/S1-P1_ephemeris.csv"
f1 = "DATA/TestPanels/S1_P1_DD.csv"

data_Sat = pd.read_csv(f1)
# in [m] and [m/s] 
R[:, 0] = data_Sat['Rx_eci'][0:points]
R[:, 1] = data_Sat['Ry_eci'][0:points]
R[:, 2] = data_Sat['Rz_eci'][0:points]
V[:, 0] = data_Sat['Vx_eci'][0:points]
V[:, 1] = data_Sat['Vy_eci'][0:points]
V[:, 2] = data_Sat['Vz_eci'][0:points]


# Attitude in time
case=1
panel_location = 1 # it could be on different surfaces of the sat (1/6 faces)

#-----------------------------------
att = np.zeros((points, 3)) # attitude matrix in time (times x att)  att=[yaw roll pitch]
# THIS ATTITUDE IS ARTIFICIAL...CREATED USING TASKS

# CASE-1:  Always pointing to the Earth center
if case == 0:
    for j in range(0, points):
        att[j, 0] = 0 # [deg] yaw, z
        att[j, 1] = 0 # [deg] roll, x
        att[j, 2] = 0 # [deg] pitch, y

# CASE-2:  Always the main panel face (IT IS FACE 1) pointing to the Sun
if case == 2:
    # It is computed later...
    for j in range(0, points):
        att[j, 0] = np.NaN # [deg] yaw, z
        att[j, 1] = np.NaN # [deg] roll, x
        att[j, 2] = np.NaN # [deg] pitch, y
# ----------------------------------

r_Su = np.zeros((points, 3))
r_Sa = np.zeros((points, 3))
sun_angle = np.zeros(points)

plt.figure()

el = C2K.rv2elem(AC.MU_E, R[1, :]/1000, V[1, :]/1000) # elements of the second time iteration
T = C2K.period(el[0]) # Orbit period [s]

for j in range(0, points):
    # Sun vector in ECI
    r_Su[j, 0] = dist[j] * math.cos(RA[j]*AC.D2R) * math.cos(DEC[j]*AC.D2R) # x from RA,DEC [m]
    r_Su[j, 1] = dist[j] * math.sin(RA[j]*AC.D2R) * math.cos(DEC[j]*AC.D2R) # y from RA,DEC [m]
    r_Su[j, 2] = dist[j] * math.sin(DEC[j]*AC.D2R) # z from RA,DEC [m]

    # Sat vector in ECI
    r_Sa[j, 0] = R[j, 0] # [m]
    r_Sa[j, 1] = R[j, 1] # [m]
    r_Sa[j, 2] = R[j, 2] # [m]
    dist_ECI = np.linalg.norm(r_Sa[j, :])

    # SS versor ( pointing from the sat to the Sun ):
    SS = (r_Su[j, :] - r_Sa[j, :]) / np.linalg.norm(r_Su[j, :] - r_Sa[j, :])

    # Finding the versor normal to the orbital plane:
    elements = C2K.rv2elem(AC.MU_E, R[j, :]/1000, V[j, :]/1000)
    M = SCU.matrix_ECI2Perifocal(elements[3]/AC.D2R, elements[4]/AC.D2R, elements[2]/AC.D2R) # matrix for rotation from ECI to perifocal
    X_eci = np.zeros(3)
    X_eci[0] = 1
    X_perifocal = np.dot(M, X_eci) # the result is the x-versor in the perifocal frame
    Z = r_Sa[j, :]/dist_ECI # versor pointing to the sat from ECI ( or the normal to the  upper panel)
    Y = np.cross(X_perifocal, Z) # vector normal to the orbit plane
    X = np.cross(Y, Z) # versor X, orthogonal to the two others, right-handed frame

    if case == 2: # attitude CASE-2 ANGLES COMPUTATIONS
        # att[j, 0] = math.acos(np.dot(Z, SS)) # attitude angle for rotation matrix
        # att[j, 1] = math.acos(np.dot(Y, SS)) # attitude angle for rotation matrix
        # att[j, 2] = math.acos(np.dot(X, SS)) # attitude angle for rotation matrix
        Y = np.cross(Z, SS) # computed from the sat-sun vector
        Z = SS # imposing the Z+ face is in the sat-sun direction
        X = np.cross(Y, Z) # computing the third versor

    # if case != 1:
    #     # Rotation wrt the attitude case:
    #     # If a case is not specified is reffered to case-1
    #     Rz, Ry, Rx = SCU.yaw_pitch_roll_M(att[j, 0], att[j, 1], att[j, 2])
    #     Z = np.dot(Rx, Z) # rotated wrt the sat attitude
    #     Y = np.dot(Rx, Y) # rotated wrt the sat attitude

    # The Sun angle is the angle between the satellite solar-panel plane's nomral and the satellite-sun conjunction
    # THE MAIN FACE IS ALWAYS THE FACE 1
    if panel_location ==1:
        cc = np.dot(Z, SS)
        if cc > 1 and cc-1 < 1e-6:
            cc = 1
            sun_angle[j] = math.acos(cc)
        else:
            sun_angle[j] = math.acos(cc)

    elif panel_location == 2:
        sun_angle[j] = math.acos(np.dot(-Y, SS))

    elif panel_location ==3:
        cc = np.dot(-Z, SS)
        if cc < -1 and cc+1 < 1e-6:
            cc = -1
            sun_angle[j] = math.acos(cc)
        else:
            sun_angle[j] = math.acos(cc)
        #print(sun_angle[j])

    elif panel_location == 4:
        sun_angle[j] = math.acos(np.dot(Y, SS))

    elif panel_location ==5:
        sun_angle[j] = math.acos(np.dot(-X, SS))

    elif panel_location == 6:
        sun_angle[j] = math.acos(np.dot(X, SS))

plt.axvline(x = T/60, color='red')
plt.axhline(y = 0, color = 'grey', linestyle='--')
plt.axhline(y = 0, color = 'grey', linestyle='--')
plt.fill_between(np.arange(0, points)*time_step/60, 0, 90, color='red', alpha=0.4)
plt.plot(np.arange(0, points)*time_step/60, sun_angle/AC.D2R, label='sun incidence')
plt.xlabel('times [min]')
plt.ylabel('angles [deg]')
plt.title('Sun angles')

plt.savefig('utilities/images_plot/sun_angle.png')

fig = plt.figure()
ax = fig.add_subplot(121, projection = '3d')
for i in range(0, points):
    sun = r_Su[i, :]/np.linalg.norm(r_Su)
    # print(sun)
    sat = r_Sa[i, :]/np.linalg.norm(r_Sa)
    # print(sat)
    ax.quiver(0, 0, 0, sun[0], sun[1], sun[2], length = 2, color='red')
    ax.quiver(0, 0, 0, sat[0], sat[1], sat[2], length = 2, color='k')
    ax.view_init(0, 0)  # Changing viewing angle (adjust as needed)

ax.quiver(0, 0, 0, sun[0], sun[1], sun[2], length = 2, color='red', label='Sun')
ax.quiver(0, 0, 0, sat[0], sat[1], sat[2], length = 2, color='k', label = 'Sat normal')   
ax.set_title('View 0')
ax.legend()

ax1 = fig.add_subplot(122, projection = '3d')
for i in range(0, points):
    sun = r_Su[i, :]/np.linalg.norm(r_Su)
    # print(sun)
    sat = r_Sa[i, :]/np.linalg.norm(r_Sa)
    # print(sat)
    ax1.quiver(0, 0, 0, sun[0], sun[1], sun[2], length = 2, color='red')
    ax1.quiver(0, 0, 0, sat[0], sat[1], sat[2], length = 2, color='k')
    ax1.view_init(0, 90)  # Changing viewing angle (adjust as needed)

ax1.quiver(0, 0, 0, sun[0], sun[1], sun[2], length = 2, color='red', label='Sun')
ax1.quiver(0, 0, 0, sat[0], sat[1], sat[2], length = 2, color='k', label = 'Sat normal')   
ax1.set_title('View 90')
ax1.legend()
plt.suptitle('Vectors of Sun and satellite panel normal')
fig.savefig('utilities/images_plot/sun_vectors.png')


