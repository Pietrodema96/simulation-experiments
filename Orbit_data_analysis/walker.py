# WALKER COMPUTATION
# This functions want to inspect the analysis of the  ground track plot and figures
# of merits with grids
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 29/04/2022
#
#---------------------------------------------------


from ctypes.wintypes import PLARGE_INTEGER
from pickletools import read_string1, read_string4
from tkinter import N
from astropy.time import Time
from astropy import coordinates
from astropy.time import Time
from astropy.coordinates import solar_system_ephemeris, EarthLocation
import os, csv, math
import utilities.grid_analysis as GA
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import utilities.CarKep as C2K
import utilities.AstroConstants as AC
import utilities.spaceConv_utilities as MF
import utilities.timeConv_utilities as TCU
import utilities.plotting_utilities as PU
import utilities.plotting_utilities_GT as PUGT
import utilities.plotting_utilities_UmbraCont as PUUC
import utilities.plotting_utilities_Grid as PUG


# Set walker main parameters
pattern = 'delta'
N = 6 # number of satellites
P = 3 # number of planes
f = 2# [0, P-1]
inc = 60 # inclinations [deg]
start_RAAN = 0 # [deg]
# --------------------------

if pattern == 'star':
    # STAR PATTERN:
    discr_RAAN = 180/P
    RAAN = np.zeros(N)
    for i in range(0, P):
        if i == 0:
            RAAN[i] = start_RAAN+discr_RAAN*(0.5) # RAAN of planes
        else:
            RAAN[i] = RAAN[i-1]+discr_RAAN # RAAN of planes


elif pattern == 'delta':
    # DELTA PATTERN
    discr_RAAN = 360/P
    RAAN = np.zeros(P)
    for i in range(0, P):
        if i == 0:
            RAAN[i] = 0 # RAAN of planes
        else:
            RAAN[i] = RAAN[i-1]+discr_RAAN # RAAN of planes


phasing = 360 * f/N # phasing angle
AOL_sat = np.zeros(P)
for j in range(0, P):
    if j == 0:
        AOL_sat[j] = 0 # RAAN of planes
    else:
        AOL_sat[j] = AOL_sat[j-1]+phasing # RAAN of planes

#----------------------------------------



# Profile
print('----------------------------------')
print('PATTERN :  %s' %pattern)
print('Parameters:')
print('number of sats, N = %d' %N)
print('number of planes, P = %d' %P)
print('phasing number, f = %d' %f)
print('Inclinations: %f' %inc)
print('RAAN_1 = %f [deg]' %RAAN[0])
print('RAAN_2 = %f [deg]' %RAAN[1])
print('RAAN_3 = %f [deg] \n' %RAAN[2])
print('AOL_sat_1 = %f [deg]' %AOL_sat[0])
print('AOL_sat_2 = %f [deg]' %AOL_sat[1])
print('AOL_sat_3 = %f [deg]' %AOL_sat[2])
print('----------------------------------')

