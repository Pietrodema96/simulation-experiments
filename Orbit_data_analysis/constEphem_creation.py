# -----------------------
# CONSTELLATION ARTIFICIAL EPHEMERIDES CREATION
# -----------------------
# This is a simple algorithm generating ephemerides of a  constellation starting from a 
# single satellite propagation, based on uniform phasing on same plane
#
# MAIN PHYSICAL ASSUMPTIONS:
# 1) JUST THE GRAVITY MODEL considered for the simulation (J2, J70,...)
# 2) this is not the absolute truth (some physical inspections of the behaviour are produced) 
#
# INPUT:
# .csv file with the ephemerides of the single satellite
# - ephemerides are produced with a fixed time-step
#
# OUTPUT:
# multiple csv file with ephemerides of the constellation


# AUTHOR: Pietro De Marchi
# LAST UPDATE: 20/03/2022
#
#---------------------------------------------------

import os, csv, glob
from turtle import color

import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from astropy.time import Time
import utilities.AstroConstants as AC
import utilities.plotting_utilities as PU
import utilities.CarKep as C2K


#---------------------------------------------------------------------------

# SIMULATION GENERALITIES:
sim_timeStep = 10 # [sec] SIMULATION TIME STEP
# sim_timeStep = 5 # [sec] SIMULATION TIME STEP

sim_initEpoch = '2021-05-01 10:28:30'  # INITIAL EPOCH OF THE SIMULATION
date_format_str = '%Y-%m-%d %H:%M:%S'
d1 = datetime.strptime(sim_initEpoch, date_format_str)

orbit_period = 6000
n_sats = 2 # numb satellites in each plane
# phasing = 1/n_sats 
phasing = [32/360, 143/360, 116/360] # LEN(PHASING) == N^ PLANES

FF = 0 # tracking the numb of planes


path = "DATA/AISAC/Test3_fin/" # Path to the SpOCK results
# Identifing all the csv file names:
csv_files = glob.glob(os.path.join(path, "*.csv"))
csv_files.sort()
# Loop over the list of csv files
print('----------------------------')
print(' READING CSV EPHEMERIS FILE : ')
print('number of satellites: %d \n' %n_sats)

for f in csv_files: # running over satellites

    #----------------------- COMPUTING PHASING IN-PLANE:
    orbit_phasing = round(phasing[FF] * orbit_period) # [sec]
    secSat = np.zeros(n_sats)
    for i in range(1, n_sats+1):
        secSat[i-1] = int(round(orbit_phasing / sim_timeStep)*i) # index when sats start
    print('\n --------------------------------------------------  ')
    print('\n Here the indexes from when sats start in ephemeris:')
    print(secSat*sim_timeStep)
    print('\n')

    FF += 1 # increasing the plane numb
    #-------------------------------------------

    # read the csv file
    data = pd.read_csv(f)
    # print the location and filename
    print('\n Location:', f)
    print('File Name:', f.split("\\")[-1])


    # Recovering data from the original single satellite file:
    elapsed_times = data['T'][:] # elapsed seconds [sec]
    gps_sec = data['GPSsec'][:] # gps seconds [sec]
    X_eci = data['Rx_eci'][:] # ECI X positions [m]
    Y_eci = data['Ry_eci'][:] # ECI Y positions [m]
    Z_eci = data['Rz_eci'][:] # ECI Z positions [m]
    VX_eci = data['Vx_eci'][:] # ECI VX velocities [m/s]
    VY_eci = data['Vy_eci'][:] # ECI VZ velocities [m/s]
    VZ_eci = data['Vz_eci'][:] # ECI VZ velocities [m/s]
    X_ecef = data['Rx_ecef'][:] # ECEF X positions [m]
    Y_ecef = data['Ry_ecef'][:] # ECEF Y positions [m]
    Z_ecef = data['Rz_ecef'][:] # ECEF Z positions [m]
    VX_ecef = data['Vx_ecef'][:] # ECEF VX velocities [m/s]
    VY_ecef = data['Vy_ecef'][:] # ECEF VZ velocities [m/s]
    VZ_ecef = data['Vz_ecef'][:] # ECEF VZ velocities [m/s]



    # Writing the new n (number of sats in the plane) files of ephemerides: 
    rrB = np.empty(3)
    vvB = np.empty(3)
    RRx = np.empty((len(elapsed_times)-int(secSat[-1]), n_sats))
    RRy = np.empty((len(elapsed_times)-int(secSat[-1]), n_sats))
    RRz = np.empty((len(elapsed_times)-int(secSat[-1]), n_sats))
    rrE = np.empty(3)
    vvE = np.empty(3)


    AOL = np.zeros(n_sats)
    AOL_E = np.zeros(n_sats)

    # # Identifing all the csv file names:
    # csv_files = glob.glob(os.path.join(path, "*.csv"))

    # Loop over the list of csv files
    for SS in range(0, n_sats): # running over satellites

        print(SS)
        print(FF)

        #------- SATs:
        with open('DATA/AISAC/Test3E_fin/S{0}-P{1}_ephemeris.csv'.format((SS+1), FF), 'w', encoding='UTF8', newline='') as f:
        # with open('DATA/Sim1/S1-P1_ephemeris.csv', 'w', encoding='UTF8', newline='') as f:
        # with open('DATA/Sim2/S1-P1_ephemeris.csv', 'w', encoding='UTF8', newline='') as f:
        # with open('DATA/Sim3/S1-P1_ephemeris.csv', 'w', encoding='UTF8', newline='') as f:

            writer = csv.writer(f)
            writer.writerow(['T', 'GPSsec', 'Rx_eci', 'Ry_eci', 'Rz_eci', 'Vx_eci', 'Vy_eci', 'Vz_eci', 'Rx_ecef',\
                'Ry_ecef', 'Rz_ecef', 'Vx_ecef', 'Vy_ecef', 'Vz_ecef'])
            # Indexes when the sat start and end:
            start = int(secSat[0]*SS)
            end = len(elapsed_times)-int(secSat[-(SS+1)])
            print(start)
            print(end)
            # Computation of AOL for shifting validation at the beginning:
            rrB[0:3] = [X_eci[start], Y_eci[start], Z_eci[start]] # initial position ECI [m]
            vvB[0:3] = [VX_eci[start], VY_eci[start], VZ_eci[start]] # initial velocity ECI [m]
            oe = C2K.rv2elem(AC.MU_E, rrB/1000, vvB/1000) # input[km], output[km][rad]
            AOL[SS] = (abs(oe[5]) + abs(oe[4]))/AC.D2R # TA + AOP [deg]
            print('TA = %f' %(oe[5]/AC.D2R))
            print('AOP = %f' %(oe[4]/AC.D2R))
            #--------------------------------------------------
            # Computing data for the 1st satellite:
            for kk in range(start, end):
                jj = kk-start
                RRx[jj,SS] = X_eci[kk]
                RRy[jj,SS] = Y_eci[kk]
                RRz[jj,SS] = Z_eci[kk]
                writer.writerow([elapsed_times[kk]-start*sim_timeStep, gps_sec[kk]-start*sim_timeStep, RRx[jj,SS], RRy[jj,SS], RRz[jj,SS], VX_eci[kk], VY_eci[kk],\
                                VZ_eci[kk], X_ecef[kk], Y_ecef[kk], Z_ecef[kk], VX_ecef[kk], VY_ecef[kk], VZ_ecef[kk]])

            rrE[0:3] = [X_eci[end], Y_eci[end], Z_eci[end]] # initial position ECI [m]
            vvE[0:3] = [VX_eci[end], VY_eci[end], VZ_eci[end]] # initial position ECI [m]
            oe = C2K.rv2elem(AC.MU_E, rrE/1000, vvE/1000) # input[km], output[km][rad]
            # Computation of AOL for shifting validation at the end:
            AOL_E[SS] = (abs(oe[5]) + abs(oe[4]))/AC.D2R # TA + AOP [deg]



    # VERIFICATION OF CORRECT PHASING:
    print('\n\n --------------------------')
    print('Checking the phasing at the beginning:\n')
    for SS in range(0, n_sats):
        print('AOL {0}^ satellite: {1}'.format(SS+1, AOL[SS]) )
        if SS > 0:
            print('\nDifferences: ')
            print('AOL-{0}{1} difference : {2}'.format(SS+1, SS+2, AOL[SS-1]-AOL[SS]) )


    print('\n\n Checking the phasing at the end:')
    for SS in range(0, n_sats):
        print('AOL {}^ satellite: {}'.format(SS+1, AOL_E[SS]) )
        if SS > 0:
            print('\n Differences: ')
            print('AOL-{}{} difference : {}'.format(SS+1, SS+2, AOL_E[SS-1]-AOL_E[SS]) )
    print('--------------------------')

    # CONSTELLATION PLANES VISUALIZATION:
    # - Orbit plot

    # OE_0 = C2K.rv2elem(AC.MU_E, rrB/1000, vvB/1000)
    # ra = OE_0[0]*(1 + OE_0[1]) 
    # PU.plot_MultiOrbit_R(RRx/1000, RRy/1000, RRz/1000, n_sats, sim_timeStep, ra, 'Ephemeris creation for constellation')


