# PERIODICITY AND RECURRENCE FOR SSO LEO
# This functions wants to inspect the plot of recurrence for
# the SSO and not SSO, in order to provide an idea of when 
# missions' orbits repeat in time
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 02/05/2022
#
#---------------------------------------------------

from cmath import nan
from mpl_toolkits.basemap import Basemap
import numpy as np
import math, os, csv
import matplotlib.pyplot as plt
from numpy.random import uniform
from matplotlib.patches import Polygon
import utilities.spaceConv_utilities as SCU
import utilities.plotting_utilities_GT as PUGT
import utilities.AstroConstants as AC
import utilities.CarKep as CK
from mpl_toolkits.mplot3d.axes3d import Axes3D
from mpl_toolkits.mplot3d import proj3d


def periodicity_SSO(orbit_h):

    """
    This function tries to understand the 
    orbit periodicity of a SSO starting from altitude
    
    INPUT
        orbit_H = orbit altitude [km]
        
    OUTPUT
        Value = closest periodicity point altitude [km]
        RC = closest periodicity point repeat-cycle [days]"""

    # Boundaries definitions for SSOs:
    # orbit_h = 705 # altitude of the orbit [km]
    rev_by_day = np.arange(12, 17) # revolutions by day
    repeat_cycle = np.arange(0, 40) # days after which repet. occurs


    # Figures settings
    max_size = np.arange(repeat_cycle[-1]*rev_by_day[0], repeat_cycle[-1]*rev_by_day[-1]+1)
    h = np.zeros((len(repeat_cycle), len(max_size))) # max size for h (altitude) vector
    h[:, :] = np.nan

    fig = plt.figure(figsize=(15, 10))
    ax1 = fig.add_subplot(1,2,1, projection='3d')
    ax2 = fig.add_subplot(1,2,2)



    ax1.get_proj = lambda: np.dot(Axes3D.get_proj(ax1), np.diag([0.5, 1, 1, 1]))

    filt = 0

    NAME_FILE = "./Orbit_data_analysis/Figures_of_merit/RGT_SSO.csv"
    with open(os.path.abspath(NAME_FILE), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Repeat cycle (days)', 'Altitude [km]', 'Inclination [deg]', 'Revolutions/day'])
        
        for jj in range(0, len(repeat_cycle)):

            # integers between repeat cycle by the rev days:
            rev = np.arange(repeat_cycle[jj]*rev_by_day[0], repeat_cycle[jj]*rev_by_day[-1]+1) 

            for ii in range(0, len(rev)):
                div = rev[ii] / repeat_cycle[jj]

                if repeat_cycle[jj] == 1:
                    P = AC.sec_day / div # nodal period [sec]
                    SMA = np.cbrt( AC.MU_E * (P/(2*np.pi))**2 ) # [km]
                    h[jj, ii] = SMA - AC.R_E # altitude [km]

                elif rev[ii]%repeat_cycle[jj] == 0:
                    filt += 1
                    continue

                elif repeat_cycle[jj]%2 == 0 and int(repeat_cycle[jj]/2)>1 and rev[ii]%int(repeat_cycle[jj]/2) == 0:
                    filt += 1
                    continue

                elif repeat_cycle[jj]%2 == 0 and rev[ii]%2 == 0:
                    filt += 1
                    continue

                else:
                    P = AC.sec_day / div # nodal period [sec]
                    SMA = np.cbrt( AC.MU_E * (P/(2*np.pi))**2 ) # [km]
                    h[jj, ii] = SMA - AC.R_E # altitude [km]


                inc = CK.inc_SSO(h[jj, ii]) # inclination [deg]
                ax1.plot3D(h[jj, ii], repeat_cycle[jj], inc,'b.', markersize=10)

                # Writing data about periodicity:
                writer.writerow([repeat_cycle[jj], h[jj, ii], inc, rev[ii]/repeat_cycle[jj]])

                if repeat_cycle[jj] < 11:
                    ax2.plot(h[jj, ii], repeat_cycle[jj], 'b.', markersize=10)
                    if repeat_cycle[jj] < 4:
                        label = '(%d, %d)' %(rev[ii]/repeat_cycle[jj], repeat_cycle[jj])
                        ax2.text(h[jj, ii], repeat_cycle[jj], label)




    # Plot of the actual value of the orbit h:
    ax2.axvline(x=orbit_h, color='r', lw=1, label='Actual orbit')
    value = np.zeros(len(h[:,0]))
    # value[:] = np.NaN
    idx = np.zeros(len(h[:,0]))

    for jj in range(0, len(repeat_cycle)):
        if np.all(np.isnan(h[jj, :])):
            # print('YYY')
            value[jj] = np.NaN
            continue    
        else:
            value[jj], idx[jj] = AC.find_nearest(h[jj, :], orbit_h)
            # print(idx)

    Value, Idx = AC.find_nearest(value, orbit_h)
    RC = repeat_cycle[Idx]
    print('-----------------------------')
    print('Actual orbit h : %.3f [km]' %orbit_h)
    print('Closest point:')
    print('h_plot: %.3f [km]' %Value)
    print('repeat_cycle: %.3f [km]' %RC)
    print('-----------------------------')



    # Plot labels settings:
    ax1.set_xlabel('h [km]')
    ax1.set_ylabel('N_d [days]')
    ax1.set_zlabel('inc [deg]')
    ax1.view_init(15, 145)  # Changing viewing angle (adjust as needed)
    ax1.set_title('First 10-days repeat cycles, f(inc)')

    ax2.set_xlabel('h [km]')
    ax2.set_ylabel('N_d [days]')
    ax2.set_title('First 10-days repeat cycles')
    major_ticks1 = np.arange(200, 2000, 200)
    minor_ticks1 = np.arange(200, 1800, 100)
    major_ticks2 = np.arange(0, 12, 1)
    minor_ticks2 = np.arange(0, 11.5, 0.5)
    ax2.set_xticks(major_ticks1)
    ax2.set_xticks(minor_ticks1, minor=True)
    ax2.legend()
    ax2.set_facecolor('gainsboro')

    # And a corresponding grid AX2
    ax2.grid(which='both')
    # Or if you want different settings for the grids:
    ax2.grid(which='minor', alpha=0.4)
    ax2.grid(which='major', alpha=0.9)
    ax2.set_yticks(major_ticks2)
    ax2.set_yticks(minor_ticks2, minor=True)
    # And a corresponding grid
    ax2.grid(which='both')
    # Or if you want different settings for the grids:
    ax2.grid(which='minor', alpha=0.4)
    ax2.grid(which='major', alpha=0.9)
    # -----------------------------------------------
    plt.suptitle('SSO repeat GT analysis, LEO orbits')
    plt.show()

    return Value, RC


if __name__ == "__main__":
    
    altitude = 750 # [km]
    periodicity_SSO(altitude)