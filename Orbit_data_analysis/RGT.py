# RPEATED GROUND TRACK for orbits
# This functions wants to inspect the plot of recurrence for
# the  repeated ground track conditions
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 25/05/2022
#
#---------------------------------------------------

import numpy as np
import math
import matplotlib.pyplot as plt
import utilities.spaceConv_utilities as SCU
import utilities.plotting_utilities_GT as PUGT
import utilities.AstroConstants as AC
import utilities.CarKep as CK
from scipy.optimize import fsolve




def B1():
    b1 = TAU * 6*AC.R_E**2 * AC.J2_EARTH * math.cos(INC*AC.D2R) / (4*(1-ECC**2))
    return b1 

def B2():
    b2 = 3 * AC.R_E**2 * AC.J2_EARTH * (2-3*math.sin(INC*AC.D2R)**2) / (4*pow((1-ECC**2),3/2))
    return b2 

def B3():
    b3 = ( (4-5*math.sin(INC*AC.D2R)**2) * 3 * AC.R_E**2 * AC.J2_EARTH) / (4*pow((1-ECC**2), 2))
    return b3

def C1():
    c1 = 1-TAU * AC.Earth_Rot 
    return c1

def ff(SMA):
    return SMA**2 -pow(SMA, 7/2) * TAU*AC.Earth_Rot/np.sqrt(AC.MU_E) - B1() + B2() + B3()

#-------------------------------

global INC, TAU, ECC

ecc = np.arange(0.001, 0.1, 0.001) # eccentricities
inc = np.arange(30, 100, 1) # inclinations [deg]
tt = [14, 14.5, 15, 15.5, 16, 16.5] # cycles/day

SMA = np.zeros((len(ecc), len(inc), len(tt)))

fig, axs = plt.subplots(1, 2)

# for k in range(0, len(tt)):
#     for i in range(0, len(ecc)):
#         for j in range(0, len(inc)):
for k, TT in enumerate(tt):
    for i, ECC_i in enumerate(ecc):
        for j, INC_i in enumerate(inc):

            TAU = TT
            INC = INC_i
            ECC = ECC_i

            SMA[i, j, k] = fsolve(ff, 7000)

        if k < 2:
            plx = 0
            ply = k
        else:
            plx = 1
            ply = k-2

        axs[0].plot( inc, SMA[i, :, k]-AC.R_E )
        axs[1].plot( inc, SMA[i, :, k]-AC.R_E )


        # axs[plx, ply].set_title('RGT cycles: {}'.format(tt[k]))

axs[0].text(90, 825, 'tau = 14 rev', ha='right', wrap=True)
axs[0].text(90, 644, 'tau = 14.5 rev', ha='right', wrap=True)
axs[0].text(90, 490, 'tau = 15 rev', ha='right', wrap=True)
axs[0].text(90, 350, 'tau = 15.5 rev', ha='right', wrap=True)
axs[0].text(90, 200, 'tau = 16 rev', ha='right', wrap=True)
axs[0].text(90, 60, 'tau = 16.5 rev', ha='right', wrap=True)

axs[0].axvline(x=98, color='r', linestyle='--')
axs[1].axvline(x=98, color='r', linestyle='--')

for ax in axs.flat:
    ax.set(xlabel='INC [deg]', ylabel='h [km]')

axs[0].set_title('RGT for different tau')
axs[1].set_title('tau = 15 (different INCs)')

plt.suptitle('RGT solution - given INC and tau')

plt.show()

