# GROUND TRACK AND GRIDS ANALYSIS
# This functions want to inspect the analysis of the  ground track plot and figures
# of merits with grids
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 04/05/2022
#
#---------------------------------------------------


from astropy.time import Time
import os, glob, configparser
import utilities.grid_analysis as GA
import numpy as np
import pandas as pd
import utilities.CarKep as C2K
import utilities.AstroConstants as AC
import utilities.plotting_utilities_UmbraCont as PUUC
import utilities.plotting_utilities as PU
import utilities.plotting_utilities_GT as PUGT
import utilities.plotting_utilities_Grid as PUG


# ----------------- SIMULATIONS ROADMAP ----------------------

home_path = os.getcwd()


# -- Here the type of the mission considered:

sim_timeStep = 10 # [sec] SIMULATION TIME STEP
FOV = 12 #[deg] semi angle
grid_type = 'neareq'
duration_track = 5 # [h]
points = int(round(duration_track*3600 /sim_timeStep))
mission = 'Test'
test = mission
config = configparser.ConfigParser(inline_comment_prefixes="#")
config.read(os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input/input.cfg'.format(home_path, mission)))
sim_initEpoch = config["Simulation Parameters"]["launch_date"] # [str]
path = "Orbit_data_analysis/DATA/{}/Output/Ephemeris/".format(mission)
sats = 1 # !!! TO BE MODIFIED !!!  Number of satellites considered in the constellation



# Grid data:
if grid_type == 'equal':
    minLat = -90 # [deg]
    minLon = -180 # [deg]
    maxLat = 90 # [deg]
    maxLon = 180 # [deg]
    discrLat = 2 # [deg]
    discrLon = 2 # [deg]
    latGRID, lonGRID = GA.grid_implementation(grid_type, minLat, minLon, maxLat, maxLon, discrLat, discrLon)
    size = 0 # initialization

elif grid_type == 'grad':
    minLat = -90 # deg
    minLon = -180 # deg
    maxLat = 90 # deg
    maxLon = 180 # deg
    discrLat = 2 # deg 9/5
    discrLon = 5 # deg 12/10
    lonGRID, latGRID, size = GA.grid_implementation(grid_type, minLat, minLon, maxLat, maxLon, discrLat, discrLon)

elif grid_type == 'neareq':
    minLat = -90 # deg
    minLon = -180 # deg
    maxLat = 90 # deg
    maxLon = 180 # deg
    discrLat = 2 # deg 9/5   !!! probs with 4 !!!
    discrLon = 10000 # deg NOT USED HERE
    lonGRID, latGRID, size = GA.grid_implementation(grid_type, minLat, minLon, maxLat, maxLon, discrLat, discrLon)



# ---------------------------  Mission analysis  ------------------------------
# -- GS,lat,lon,alt,min_El


GS_E= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/GS_list.csv".format(mission))) # reading the GS csv file ENV
# TG_E= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TG_list.csv".format(mission))) # reading the TG csv file ENV

#-----------------------------


# Identifing all the csv file names:
csv_files = glob.glob(os.path.join(path, "*.csv"))
csv_files.sort()


RRx = np.zeros((points,sats)) # Initializing positions x ECI
RRy = np.zeros((points,sats)) # Initializing positions y ECI
RRz = np.zeros((points,sats)) # Initializing positions z ECI

SS = 0 # numb of sats

# Loop over the list of csv files
print('----------------------------')
print(' READING CSV EPHEMERIS FILE : ')
print('number of satellites: %d' %sats)
for f in csv_files: # running over satellites
    # read the csv file
    data = pd.read_csv(f)
    # print the location and filename
    print('\n Location:', f)
    print('File Name:', f.split("\\")[-1])

    for jj in range(0, points):
        #  RRx / RRy / RRz:
        #      sat1   sat2   sat3   ...
        # ist1
        # ist2
        # ist3
        # ....
        RRx[jj, SS] = data['Rx_eci'][jj] # [m]
        RRy[jj, SS] = data['Ry_eci'][jj]
        RRz[jj, SS] = data['Rz_eci'][jj]
    
    SS += 1
print('\n\n')
print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
print('!!! Starting MISSION ANALYSIS !!!')
print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n')
# Velocities of last satellite
vv1 = np.zeros(3)
vv1[0] = data['Vx_eci'][0] # [m/s]
vv1[1] = data['Vy_eci'][0] # [m/s]
vv1[2] = data['Vz_eci'][0] # [m/s]
# SMA computation for plot:
RR = np.zeros(3)
RR[0] = RRx[0, -1]/1000 # [km]
RR[1] = RRy[0, -1]/1000 # [km]
RR[2] = RRz[0, -1]/1000 # [km]
SMA = C2K.rv2elem(AC.MU_E, RR[:], vv1[:]/1000)
SMA = SMA[0] # SMA of first orbit [km]

# Times for GTr
t_first = Time(sim_initEpoch)
t_jd1 = t_first.jd # first date in Jdays

# Setting the zone of interest:
lat_int = np.zeros(4)
lat_int[:] = np.NaN
lon_int = np.zeros(4)
lon_int[:] = np.NaN
ROI_check = 0 # 0-no ROI selected; 1-ROI selected
# To initialize to highlight the zone of interest
lat_int = [ -30, 30, 30, -30 ]
lon_int = [ 90, 90, 140, 140 ]
# -----------------------------------------------------------------------


# Orbit plot
# PU.plot_MultiOrbit_R(RRx/1000, RRy/1000, RRz/1000, sats, sim_timeStep, SMA, test)

# # Ground Track plot
PUGT.GrTr_MultiOrbit_R(sim_timeStep, RRx/1000, RRy/1000, RRz/1000, t_jd1, duration_track, \
    test, lat_int, lon_int, ROI_check, sats, FOV)

# Computation of contacts and revisit times
# mean_rev, corr_mat, latSAT, lonSAT, lon_fin, lat_fin, pas_lat, pas_lon, Rev_Min, Rev_Max, \
#     Rev_Numb, mat_ID, mat_IDs = GA.grid_passesConst_R(RRx/1000, RRy/1000, RRz/1000, grid_type, sim_timeStep, t_jd1, latGRID, lonGRID, FOV, duration_track, size, sats, points)

# # Plot of coverage
# PUG.grid_passesConst_R(latGRID, lonGRID, latSAT, lonSAT, \
#     lon_fin, lat_fin, test, duration_track, grid_type, lat_int, lon_int, ROI_check, sats)


# Revisit time averaged plot
# PUG.grid_revisit_mean(mean_rev, corr_mat, lonGRID, latGRID, test, duration_track, grid_type, minLon, \
#     maxLon, FOV)

# Global case - revisit
# PUG.grid_revisit(mean_rev, corr_mat, grid_type, latGRID, lonGRID, test, Rev_Min, \
#         Rev_Max, Rev_Numb, [-1, -1], [-1, -1], ROI_check, FOV, size, duration_track)
# # Local case - revisit
# PUG.grid_revisit(mean_rev, corr_mat, grid_type, latGRID, lonGRID, test, Rev_Min, \
#         Rev_Max, Rev_Numb, lat_int, lon_int, 1, FOV)

# # Ground stations passes local:
# # for gc in range(0, len(GS_E['lat'][:])):
# #     azimuths_IN = PUUC.polar_plot_GS(GS_E['lat'][gc], GS_E['lon'][gc], latSAT, lonSAT, RRx/1000, RRy/1000, RRz/1000, sats, points, GS_E['min_El'][gc], GS_E['GS'][gc], mission, sim_initEpoch, duration_track)
# numb_GS = 0
# azimuths_IN = PUUC.polar_plot_GS(GS_E['lat'][numb_GS], GS_E['lon'][numb_GS], latSAT, lonSAT, RRx/1000, RRy/1000, RRz/1000, sats, points, GS_E['min_El'][numb_GS], GS_E['GS'][numb_GS], mission, sim_initEpoch, duration_track)


# # Latency analysis
# Mean_latency, Min_latency, Max_latency, Median_latency = GA.latency_R(mat_IDs, sim_timeStep, duration_track, azimuths_IN, mat_ID, sats)
# # Global case:
# PUG.grid_latency(Mean_latency, corr_mat, grid_type, latGRID, lonGRID, mission, Min_latency, Max_latency, Median_latency, [-1, -1], [-1, -1], ROI_check, GS_E['GS'][numb_GS], size, duration_track)
# # Local case:
# PUG.grid_latency(Mean_latency, corr_mat, grid_type, latGRID, lonGRID, mission, Min_latency, Max_latency, Median_latency, lat_int, lon_int, 1, GS_E['GS'][numb_GS], size)

# if sats == 1:
#     PUGT.land_sea(lonSAT, latSAT, GS_E, TG_E, mission, duration_track)
