import math, time, csv
from os import times
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from numpy import linalg as la
from datetime import datetime, timedelta
from tletools import TLE
import utilities.CarKep as CK
from astropy.time import Time
import utilities.AstroConstants as AC
import utilities.plotting_utilities_GT as PUGT
import utilities.plotting_utilities as PU
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU

# -----------------------
# ORBIT CHARACTERISTICS
# -----------------------
# This is a brief collection of the most useful algorithms needed to identify
# an Earth orbit and its main characteristics
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 20/03/2022
#
#---------------------------------------------------
sats = 2

kepEl1 = [6778, 0.01, 37*AC.D2R, 60*AC.D2R, 20*AC.D2R, 60*AC.D2R]
rr1,vv1 = CK.elem2rv(AC.MU_E, kepEl1)
kepEl2 = [7015, 0.01, 60*AC.D2R, 60*AC.D2R, 20*AC.D2R, 120]
rr2,vv2 = CK.elem2rv(AC.MU_E, kepEl2)

T1 = CK.period(6720)
T2 = CK.period(6836)
timeSpan = max(T1, T2)*2 # [sec]
timeStep = 10 # [sec]
points = int(round(timeSpan/timeStep))

RRx = np.zeros((points, sats))
RRy = np.zeros((points, sats))
RRz = np.zeros((points, sats))

RRx[:, 0], RRy[:,0], RRz[:,0] = TCU.timeLaw([rr1[0], rr1[1], rr1[2], vv1[0], vv1[1], vv1[2]], timeSpan, points)
RRx[:, 1], RRy[:,1], RRz[:,1] = TCU.timeLaw([rr2[0], rr2[1], rr2[2], vv2[0], vv2[1], vv2[2]], timeSpan, points)

PU.plot_MultiOrbit_R(RRx, RRy, RRz, sats, timeStep, 7015*1.5, 'RGT')


