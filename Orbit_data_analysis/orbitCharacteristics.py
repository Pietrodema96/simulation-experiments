
import math, time, csv
from os import times
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from numpy import linalg as la
from datetime import datetime, timedelta
from tletools import TLE
import utilities.CarKep as CK
from astropy.time import Time
import utilities.AstroConstants as AC
import utilities.plotting_utilities_GT as PUGT
import utilities.plotting_utilities as PU
from scipy.integrate import odeint
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
import utilities.spaceConv_utilities as SCU
import utilities.timeConv_utilities as TCU
import periodicity_SSO_LEO as PSSO

# -----------------------
# ORBIT CHARACTERISTICS
# -----------------------
# This is a brief collection of the most useful algorithms needed to identify
# an Earth orbit and its main characteristics
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 20/03/2022
#
#---------------------------------------------------

def orb_characteristics(INPUT):

    if INPUT == 'SSO':

        # Orbit analysis
        ########################################
        #            INPUTS
        kepEl = np.empty(6)
        # a = AC.R_E + 530 # [km]
        a = 7182. # [km]
        kepEl[0] = a
        e = 0.001  # [-]
        kepEl[1] = e
        i = 98.0 * AC.D2R# [deg]
        kepEl[2] = i
        Om =  169.684 * AC.D2R# [deg]
        kepEl[3] = Om
        om = 181. * AC.D2R# [deg]
        kepEl[4] = om
        f = 1. * AC.D2R# [deg]
        kepEl[5] = f

        # Tipe here the launch date
        Date = '01/05/2020 12:00:00' # Launch date
        yy = 2020 # year of the launch
        LTAN = 10.5 # LTAN of SSO in decimals
        duration = 7 * 3600 # GT plot duration [s]

        date_format_str = '%d/%m/%Y %H:%M:%S'
        date =  datetime.strptime(Date, date_format_str)
        ########################################

        # GEOMETRY
        ra = a*(1 + e) 
        rp = a*(1 - e)

        if rp <= AC.R_E:
            print('Error !!!!   \n Orbit too low!!!!!')

        # TIME
        n = np.sqrt(AC.MU_E / a / a / a)
        T = CK.period(a)/60 # [min]
        n_revDay = AC.sec_day / (T*60) # [-]
        mean_alt = (rp + ra)/2 - AC.R_E # [km]
        t_first = Time(date)
        t_jd1 = t_first.jd # first date in Jdays

        # Periodicty:
        alt_per, days_per = PSSO.periodicity_SSO(mean_alt)

        # Positions and velocities at t0:
        rr = np.empty(3)
        vv = np.empty(3)
        rr,vv = CK.elem2rv(AC.MU_E, kepEl)

        # SSO CHARACTERISTICS:
        RAAN = TCU.LTAN_2_RAAN(Date, yy, LTAN)

        print('---------------------------------')
        print('Initial positions & velocities in ECI: ')
        print('rx [km]: %f' %rr[0])
        print('ry [km]: %f' %rr[1])
        print('rz [km]: %f' %rr[2])
        print('vx [km/s]: %f' %vv[0])
        print('vy [km/s]: %f' %vv[1])
        print('vz [km/s]: %f \n' %vv[2])
        print('Orbit period: %f [min]' %T)
        print('rev/day: %f' %n_revDay)
        print('Perigee: %f [km]' %rp)
        print('Apogee: %f [km]' %ra)
        print('Mean altitude: %f [km]' %mean_alt)
        print('RAAN from LTAN {}: {} [deg]'.format(LTAN, RAAN))
        print('\nPeriodicity of SSO:')
        print('- found periodicity closest altitude: {} km'.format(alt_per))
        print('- found periodicity closest repeat-cycle: {} days'.format(days_per))
        print('---------------------------------')

        # Plot of the orbit with time law:
        sim_timeStep = 10
        n_points = int(duration / sim_timeStep) # for the integration with Time Law and plot

        # Orbit integration with timeLaw
        X_SAT = np.zeros((n_points,1)) # Initializing positions x ECI
        Y_SAT = np.zeros((n_points,1)) # Initializing positions y ECI
        Z_SAT = np.zeros((n_points,1)) # Initializing positions z ECI
        X_SAT[:,0], Y_SAT[:,0], Z_SAT[:,0] = TCU.timeLaw([rr[0], rr[1], rr[2], vv[0], vv[1], vv[2]], duration, n_points)   

        # 2) Ground Track plot:
        lat_int = np.zeros(4)
        lat_int[:] = np.NaN
        lon_int = np.zeros(4)
        lon_int[:] = np.NaN
        ROI_check = 0 # 0-no ROI selected; 1-ROI selected
        # To initialize to highlight the zone of interest
        lat_int = [ -30, 30, 30, -30 ]
        lon_int = [ 90, 90, 140, 140 ]
        PUGT.GrTr_MultiOrbit_R(sim_timeStep, X_SAT, Y_SAT, Z_SAT, t_jd1, duration/3600, \
            INPUT, lat_int, lon_int, ROI_check, 1, 10)



    


    elif INPUT == 'tle':

        # TLE conversion
        # thir number/line-1 gives the epoch: 02060.12060913 = 2002 + 60th day of the year + 0.12... fraction of the day from midnight
        # 0.12060913 * 24 = hours
        # (result -hours) *60 = min
        # (results -min)*60 = sec
        tle_string = """
    ISS (ZARYA)
    1 25544U 98067A   19249.04864348  .00001909  00000-0  40858-4 0  9990
    2 25544  51.6464 320.1755 0007999  10.9066  53.2893 15.50437522187805
    """

        tle_lines = tle_string.strip().splitlines()

        tle = TLE.from_lines(*tle_lines)
        print('--------------------------------------------------')
        print('Here the TLE converted: ')
        print(tle)
        print('--------------------------------------------------\n\n')

        




    elif INPUT == 'prop':


        print('ciao')


    return


if __name__ == '__main__':
    type = 'SSO'
    orb_characteristics(type)


