# -----------------------
# INSPECTION OF UMBRA DISTRIBUTION
# -----------------------
# A simple fuction allowing to visualize the time-distribution of the umbra along mission
# propagation
#
# INPUT:
# .csv file with the umbra distribution
#
# OUTPUT:
# plot of the figure off merits for umbra distribution


# AUTHOR: Pietro De Marchi
# LAST UPDATE: 04/04/2022
#
#---------------------------------------------------

import os, configparser
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import utilities.CarKep as C2K
from astropy.time import Time
import utilities.AstroConstants as AC
import utilities.plotting_utilities_UmbraCont as PUUC
import utilities.CarKep as CK
import utilities.timeConv_utilities as TCU


home_path = os.getcwd()


def umbra_plotMaster(mission):
    """
    This function gather all the plot routines for eclipses distribution
    analysis
    
    INPUT
        mission = name of the mission inspected [str]
    """

    # SIMULATION GENERALITIES:
    # --------------------------------
    config = configparser.ConfigParser(inline_comment_prefixes="#")
    config.read(os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input/input.cfg'.format(home_path, mission)))
    sim_initEpoch = config["Simulation Parameters"]["launch_date"] # [str]
    data1= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/AllEclipses_times_{}.csv".format(mission,mission))) # reading the data csv file PL


    date_format_str = '%Y-%m-%d %H:%M:%S'
    d1 = datetime.strptime(sim_initEpoch, date_format_str)
    # --------------------------------

    # Penumbra_start_UTC,Umbra_start_UTC,Umbra_end_UTC,Penumbra_end_UTC,Umbra_duration,Penumbra_duration

    # Recovering data from the original single satellite file:
    U_start = data1['Umbra_start_UTC'][:] # beginning of Umbra [UTC]
    P_start = data1['Penumbra_start_UTC'][:] # beginning of Penumbra [UTC]
    U_end = data1['Umbra_end_UTC'][:] # end of Umbra [UTC]
    P_end = data1['Penumbra_end_UTC'][:] # end of Penumbra [UTC]
    U_dur = data1['Umbra_duration'][:] # umbra duration [min]
    # P_dur = data1['Penumbra_duration'][:] # umbra duration [min]

    U_start1 = np.zeros(len(U_start))
    P_dur_1segm = np.zeros(len(U_start))
    P_dur_2segm = np.zeros(len(U_start))
    P_start_1segm = np.zeros(len(U_start))
    P_end_1segm = np.zeros(len(U_start))


    for ii in range(0, len(U_start)):
        
        U_start1[ii] = float(TCU.sec_btw_dates(d1, datetime.strptime(U_start[ii], date_format_str))) # computing sec from the beginning of simulation
        
        P_start_1segm[ii] = float(TCU.sec_btw_dates(d1, datetime.strptime(P_start[ii], date_format_str))) # computing sec from the beginning of simulation
        P_end_1segm[ii] = U_start1[ii]
        P_dur_1segm[ii] = P_end_1segm[ii] - P_start_1segm[ii] # sec from the beg of sim
        if P_dur_1segm[ii] > 15:
            P_dur_1segm[ii] = 10
        P_start_2segm = float(TCU.sec_btw_dates(d1, datetime.strptime(U_end[ii], date_format_str))) # computing sec from the beginning of simulation
        P_end_2segm = float(TCU.sec_btw_dates(d1, datetime.strptime(P_end[ii], date_format_str))) # computing sec from the beginning of simulation
        P_dur_2segm[ii] = P_end_2segm - P_start_2segm # sec from the beg of sim
        print(P_dur_2segm[ii])

        if P_dur_2segm[ii] > 15:
            P_dur_2segm[ii] = 10

    # In case the first value start in the middle of an umbra....
    if U_dur[0]< (U_dur[1]/2):
        PUUC.plot_umbra(U_start1[1:], U_dur[1:], P_dur_1segm[1:], P_dur_2segm[1:], mission)
    else:
        PUUC.plot_umbra(U_start1, U_dur, P_dur_1segm, P_dur_2segm, mission)
    
    return




if __name__ =="__main__":
    
    mission = 'SatRev'

    umbra_plotMaster(mission)