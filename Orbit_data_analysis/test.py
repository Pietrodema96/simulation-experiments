from astropy.time import Time
from datetime import datetime
from astropy import coordinates
from astropy.time import Time
import numpy as np
import pandas as pd
import math, os, glob, sys
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import utilities.AstroConstants as AC
import utilities.spaceConv_utilities as SCU
import utilities.CarKep as CK
import utilities.plotting_utilities as PU
import utilities.timeConv_utilities as TCU


# from orbdetpy import configure, Frame
# from orbdetpy.conversion import get_J2000_epoch_offset, get_UTC_string
# from orbdetpy.propagation import propagate_orbits
# from orbdetpy.utilities import interpolate_ephemeris

# # Propagate for 1 hour at 5 minute intervals with default settings
# cfg = configure(prop_start=get_J2000_epoch_offset("2020-03-09T22:59:59.678"),
#                 prop_initial_state=[-152408.166, -958234.785, 6908448.381, -7545.691, 285.553, -126.766],
#                 prop_end=get_J2000_epoch_offset("2020-03-09T23:59:59.678"), prop_step=300.0)

# times, states = [], []
# for o in propagate_orbits([cfg])[0].array:
#     times.append(o.time)
#     states.append(o.true_state)

# # Interpolate over the same period at 1 minute intervals
# for i in interpolate_ephemeris(Frame.GCRF, times, states, 5, Frame.GCRF, cfg.prop_start, cfg.prop_end, 60.0):
#     print(get_UTC_string(i.time), i.true_state)
date1 = '2020-12-01 00:00:20.120'
date1 = datetime.strptime(date1, '%Y-%m-%d %H:%M:%S.%f')
date2 = '2020-12-01 00:00:30.0'
date2 = datetime.strptime(date2, '%Y-%m-%d %H:%M:%S.%f')
diff = TCU.sec_btw_dates(date2, date1)
ff = np.linspace()
print(diff)