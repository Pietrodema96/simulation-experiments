import math
from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import uniform
from matplotlib.patches import Polygon



D2R = math.pi/180

# -----------------------
# Single satellite and constellation performances
# -----------------------
# This is a brief collection of the most useful grids developed along the AIKO projects
# These grids are useful for performance inspections related to Earth observation
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 20/03/2022
#
#---------------------------------------------------


# In order to plot rectangles:
def draw_screen_poly( lats, lons, m):
    x, y = m( lons, lats )
    xy = zip(x,y)
    poly = Polygon( list(xy), facecolor='None', edgecolor='lightgrey', alpha=0.7 )
    plt.gca().add_patch(poly)


# llcrnrlat,llcrnrlon,urcrnrlat,urcrnrlon
# are the lat/lon values of the lower left and upper right corners
# of the map.
# resolution = 'c' means use crude resolution coastlines.

# -- Types of projection: --
proj = 'cyl' 

plt.figure(figsize=(15,7))
# -------------------------------------------------------------------
if proj == 'cyl': # cylindrical projection
    m = Basemap(projection=proj,llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')
if proj == 'eck4': # Eckert projection
    m = Basemap(projection=proj,lon_0=0,resolution='c')
if proj == 'ortho': # orthonormal projection
    m = Basemap(projection='ortho',lon_0=80,lat_0=20,resolution='l')
if proj == 'eqdc':
    m = Basemap(width=12000000,height=9000000,
                resolution='l',projection='eqdc',\
                lat_1=45.,lat_2=55,lat_0=40,lon_0=-107.)
# --------------------------------------------------------------------

m.drawcoastlines()
# m.drawmapboundary(fill_color='aqua')
# m.fillcontinents(color='coral',lake_color='aqua') # orange and light-blue
# m.bluemarble() # real plot high definition




# DRAW THE GRID FOR THE ANALYSIS:
# ------------------------------------
# Types implemented:
# 1. 'equal' = equally spaced grid depending from degs in lat and lon
# 2. 'grad' = gradient increas spaced grid
# ------------------------------------

grid_type = 'neareq'

if grid_type == 'equal':
    # draw parallels and meridians.
    minLat = -90 # deg
    minLon = -180 # deg
    maxLat = 90 # deg
    maxLon = 180 # deg
    discrLat = 5 # deg
    discrLon = 10 # deg
    m.drawparallels(np.arange(minLat, maxLat, 5), linewidth=1, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(minLon, maxLon, 15), linewidth=1, labels=[1,0,0,1], color='r')

    # n_Lat_points = int((abs(minLat)+maxLat)/discrLat) +1
    # n_Lon_points = int((abs(minLon)+maxLon)/discrLon) +1


    # DRAW CENTRAL POINTS OF GRID:
    y = np.arange(minLat+discrLat, maxLat+discrLat, discrLat) - np.ones(len(np.arange(minLat+discrLat, maxLat+discrLat, discrLat)))*(discrLat)/2
    x = np.arange(minLon+discrLon, maxLon+discrLon, discrLon) - np.ones(len(np.arange(minLon+discrLon, maxLon+discrLon, discrLon)))*(discrLon)/2


    for lats in range(0, len(y)):
        for lons in range(0, len(x)):
            m.scatter(x[lons], y[lats], marker='.', color='b', s=5, latlon=True)
 
            
    plt.title("Mission: ENVISAT, [equally spaced - %s]" %proj)



if grid_type == 'grad':

    minLat = -90 # deg
    minLon = -180 # deg
    maxLat = 90 # deg
    maxLon = 180 # deg
    discrLat = 5 # deg
    discrLon = 20 # deg
    m.drawparallels(np.arange(minLat, maxLat, 5), linewidth=1, labels=[1,0,0,1], color='r')
    m.drawmeridians(np.arange(minLon, maxLon, 10), linewidth=1, labels=[1,0,0,1], color='r')
    # Gradient:
    grad_mult = 1.04

    # DRAW CENTRAL POINTS OF GRID:
    n_Lat_points = int((abs(minLat)+maxLat)/discrLat ) 
    n_Lon_points = int((abs(minLon)+maxLon)/discrLon ) 

    for kk in range(0, int(n_Lat_points/2)):

        lat1 = minLat + kk*discrLat
        lat2 = minLat + (kk+1)*discrLat
        lat3 = lat2
        lat4 = lat1

        for jj in range(0, n_Lon_points):

            lon1 = minLon + jj*discrLon
            lon2 = lon1
            lon3 = minLon + (jj+1)*discrLon
            lon4 = lon3
            # print(lon1)

            lats_down = [ lat1, lat2, lat3, lat4 ]
            lons_down = [ lon1, lon2, lon3, lon4 ]
            lats_up = [ -lat1, -lat2, -lat3, -lat4 ]
            lons_up = lons_down
            
            draw_screen_poly( lats_down, lons_down, m )
            draw_screen_poly( lats_up, lons_up, m )

        # DRAW CENTRAL POINTS OF GRID:
        #x = np.arange(minLon+discrLon, maxLon, discrLon)
        x = np.arange(minLon + discrLon, (maxLon + discrLon), discrLon) - np.ones(len(np.arange(minLon + discrLon, maxLon + discrLon, discrLon)))*(discrLon)/2
        y = np.ones(len(x)) * (lat1 + abs((lat1-lat2)/2))

        m.scatter(x[:], y, marker='.', color='b', s=10, latlon=True) # points boreal emisphere starting from the bottom
        m.scatter(x[:], -y, marker='.', color='b', s=10, latlon=True) # points austal emesphere starting from the top

        n_Lon_points = round(n_Lon_points * grad_mult) # updated number of points per row
        discrLon = ( maxLon + abs(minLon) )/n_Lon_points # updated range of lon per rectangle in the row

    plt.title("Mission: ENVISAT, (gradient (1.01) spaced - %s)" %proj)


if grid_type == 'neareq':

    # draw parallels and meridians.
    minLat = -90 # deg
    minLon = -180 # deg
    maxLat = 90 # deg
    maxLon = 180 # deg
    discrLat = 3 # deg


    lats = np.arange(minLat+discrLat, maxLat+discrLat, discrLat) # points' lat
    # print(len(lats))
    lats_parall = np.arange(minLat, maxLat+discrLat, discrLat) # parallels' lat

    lons = np.arange(minLon+discrLat, maxLon+discrLat, discrLat) # points' lon
    lons_eq_lat = np.arange(minLon, maxLon+discrLat, discrLat) # equatorial lon borders

    y = lats - np.ones(len(lats))*(discrLat)/2 # effective lattitudes [lats x 0]
    x = np.zeros([len(y), round((abs(minLon)+abs(maxLon))/discrLat)]) # initialization of the matrix, with the max numb of lons ( @ the equator )

    span=discrLat
    cells=np.zeros(int(len(y)/2))
    for ii in range(0, int(len(y)/2)-1):

        # Latitudes for the polygons plot:
        lat1 = minLat + ii*discrLat
        lat2 = minLat + (ii+1)*discrLat
        lat3 = lat2
        lat4 = lat1
        # ---------------------------------

        var = (discrLat) / (math.cos( (y[len(y/2)-(ii+1)]) * D2R))
        #print(var)
        cell = 360/var
        while (360%cell) != 0:
            var = round(var) +1
            cell = round(360/var)
        cells[ii] = round(cell) # number of cells per row (half latitudes)
        print(cell)
        discr = (abs(minLon)+abs(maxLon))/int(cells[ii]) # discretization per each row

        # Writing the longitudes per row:
        x[ii, 0:int(cells[ii])] = np.arange(minLon+discr, maxLon+discr, int(discr)) - np.ones(len(np.arange(minLon+discr, maxLon+discr, int(discr))))*(discr)/2
        x[(-1)-ii, 0:int(cells[ii])] = np.arange(minLon+discr, maxLon+discr, int(discr)) - np.ones(len(np.arange(minLon+discr, maxLon+discr, int(discr))))*(discr)/2

        for jj in range(0, int(cells[ii])):
            # Longituted for the plot:
            lon1 = minLon + jj*discr
            lon2 = lon1
            lon3 = minLon + (jj+1)*discr
            lon4 = lon3
            lats_down = [ lat1, lat2, lat3, lat4 ]
            lons_down = [ lon1, lon2, lon3, lon4 ]
            lats_up = [ -lat1, -lat2, -lat3, -lat4 ]
            lons_up = lons_down
            draw_screen_poly( lats_down, lons_down, m )
            draw_screen_poly( lats_up, lons_up, m )
            # ---------------------------------------

    # Equatorial cases:
    cells[-1] = len(lons)
    discr = (abs(minLon)+abs(maxLon))/int(cells[-1])
    x[int(len(y)/2)-1, 0:int(cells[-1])] = np.arange(minLon+discr, maxLon+discr, int(discr)) - np.ones(len(np.arange(minLon+discr, maxLon+discr, int(discr))))*(discr)/2

    # Latitudes for the polygons plot:
    lat1 = minLat + int(len(y)/2)*discrLat
    lat2 = minLat + (int(len(y)/2)+1)*discrLat
    lat3 = lat2
    lat4 = lat1
    # ---------------------------------
    for jj in range(0, int(cells[-1])):
        # Longituted for the plot:
        lon1 = minLon + jj*discr
        lon2 = lon1
        lon3 = minLon + (jj+1)*discr
        lon4 = lon3
        lats_down = [ lat1, lat2, lat3, lat4 ]
        lons_down = [ lon1, lon2, lon3, lon4 ]
        lats_up = [ -lat1, -lat2, -lat3, -lat4 ]
        lons_up = lons_down
        draw_screen_poly( lats_down, lons_down, m )
        draw_screen_poly( lats_up, lons_up, m )
    # ---------------------------------------

    #m.drawparallels(np.arange(minLat, maxLat, 3), linewidth=0.7, labels=[1,0,0,1], color='lightgrey')

    # Drawing the points of the grid:
    n_cells = len(y)*2*len(cells)
    for lat in range(0, int(len(y)/2)):
        for lon in range(0, len(x[lat, 0:int(cells[lat])]) ):
            m.scatter(x[lat, lon], y[lat], marker='.', color='darkgray', s=15, latlon=True)
            m.scatter(x[lat, lon], -y[lat], marker='.', color='darkgray', s=15, latlon=True)

    print(sum(cells)*2)
            
    # plt.title("Mission: ENVISAT, [inv-cosine spaced - %s]" %proj)


# SHOW
plt.show()
