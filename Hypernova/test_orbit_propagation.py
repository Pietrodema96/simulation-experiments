from hypernova.visualization import plot_orbit
from hypernova.propagation import propagate_orbit, initialize_spacecraft, initialize_problem 
from hypernova.solvers import YOSHIDA8, YOSHIDA4, VERLET, RK45, RK4
import numpy as np
import pandas as pd
import time, os, sys, csv

path_ = os.path.abspath('Orbit_data_analysis/')
# print(path_)
sys.path.append('{}'.format(path_))

import utilities.AstroConstants as AC
import utilities.CarKep as CK



def test_solver_consistency(KepEL, duration, time_step, mass):
    # TODO: THIS IS BROKEN RIGHT NOW

    spacecraft = initialize_spacecraft(mass)

    R, V = CK.elem2rv(AC.MU_E, KepEL)
    y0 = np.array([R[0]*1000, R[1]*1000, R[2]*1000, V[0]*1000, V[1]*1000, V[2]*1000]) # [m m/s]
    problem = initialize_problem(0, duration, y0, spacecraft) # JD

    start_time = time.perf_counter()
    solution = propagate_orbit(problem, YOSHIDA8, time_step, 1e-6)
    # Writing the solution on the csv file
    with open(os.path.abspath("./Hypernova/Ephem_{}.csv".format('Test')), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['ElapsedTime','GPSsec','Rx_eci','Ry_eci','Rz_eci','Vx_eci','Vy_eci','Vz_eci','Rx_ecef','Ry_ecef','Rz_ecef','Vx_ecef','Vy_ecef','Vz_ecef'])

        for kk in range(0, len(solution.t)):
            writer.writerow([solution.t[kk], solution.jd[kk], solution.y[kk,0], solution.y[kk,1], \
                solution.y[kk,2], solution.y[kk,3], solution.y[kk,4], solution.y[kk, 5] \
                    , solution.y[kk,0], solution.y[kk,1],\
                    solution.y[kk,2], solution.y[kk,3], solution.y[kk,4], solution.y[kk, 5]])
            
    end_time = time.perf_counter()
    
    print("Time to propagate orbit: {} ({} steps taken)".format(end_time - start_time, solution.n))
    print('theoretical steps: {}'.format(duration*3600*24/time_step))
    print('First state: R = {} m/s, V = {} m/s '.format(R*1000, V*1000))
    # print(solution.y[0,:])
    # print(solution.t)
    


    return solution



if __name__ == '__main__':
    KepEL = [7000.0, 0.001, 80*AC.D2R, 30*AC.D2R, 11*AC.D2R, 11*AC.D2R]
    duration = 30 # JD
    time_step = 10 # [s]
    mass = 10 # [kg]
    sol = test_solver_consistency(KepEL, duration, time_step, mass)
    plot_orbit(sol, title="Orbit", show=True)
    