from __future__ import annotations
from inspect import _void
import os, sys, time
import string
import datetime
from datetime import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt; plt.show()
from scipy import interpolate
from mpl_toolkits.mplot3d import Axes3D
import spiceypy.spiceypy as ss
import spiceypy as spice

base_path = os.getcwd()
path_ = os.path.abspath('Orbit_data_analysis/')
sys.path.append('{}'.format(path_))

import utilities.timeConv_utilities as TCU






def data_extr(mission, duration, time_step):
    """
    Extraxting ephemeris data from csv file propagated
    
    INPUT:
        mission = mission name [str]
        duration = duration of retrieved data [s]
        time_step = of the data propagated in csv file [s]
    OUTPUT:
        POS_VEL = [N x 6] positions and velocities from propagation [m m/s]
        
    """
    pts = round(duration/time_step) +1
    POS_VEL = np.zeros((pts,6)) # array of positions and speeds
    Times = np.zeros(pts) # elapsed times [sec]
    Dates = []
    data= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/Ephemeris/S1_P1.csv".format(mission))) # read the ephemeris file to use for analysis
    data1= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/TIMES_{}.csv".format(mission, mission))) # read the ephemeris file to use for analysis

    for i in range(0, pts):
        POS_VEL[i,0] = data['Rx_eci'][i] # Rx [m]
        POS_VEL[i,1] = data['Ry_eci'][i] # Ry [m]
        POS_VEL[i,2] = data['Rz_eci'][i] # Rz [m]
        POS_VEL[i,3] = data['Vx_eci'][i] # Vx [m/s]
        POS_VEL[i,4] = data['Vy_eci'][i] # Vy [m/s]
        POS_VEL[i,5] = data['Vz_eci'][i] # Vz [m/s] 
        Times[i] = data['ElapsedTime'][i] # Elapsed seconds [sec]     
        Dates.append(data1['UTCtime'][i]) # UTC format
         
    return POS_VEL, Times, Dates




def interp_pol(STATES_precise,sma, STATES_large, time_step_pr, jump):
    """
    Orbit interpolation using scipy
    https://spiceypy.readthedocs.io/en/main/documentation.html
    
    functions used: interpolate.splprep()
                    interpolate.splev()
                    
    INPUT:
        STATES_precise: precise positions of propagated S/C ECI [m m m]
        STATES_large : propagated points picked witha certain jump ECI [m m m]
        sma: SMA of the S/C orbit for plot
        time_step_pr = propagation time step
        jump = time spacing jump between data
    
    """
    
    # Zero vectors for the interpolation products
    STATES_interp = np.zeros((len(STATES_precise), 6))
    diff_interp = np.zeros((len(STATES_precise), 6))
    
    # Interpolation of the S/C positions each jump set
    mm = len(STATES_large[:,0])
    SS = round((mm-np.sqrt(2*mm) + mm+np.sqrt(2*mm))/2) # in oredr to compute smoothness
    print(mm-np.sqrt(2*mm))
    pos_indx1, _ = interpolate.splprep([STATES_large[:,0],STATES_large[:,1],STATES_large[:,2]], s=SS, k=4)
    pos_service1 = np.linspace(0,1,len(STATES_precise[:,0])) # service indexes
    STATES_interp[:,0], STATES_interp[:,1], STATES_interp[:,2] = interpolate.splev(pos_service1, pos_indx1) # interpolation

    # Interpolation of the S/C speeds each jump set
    pos_indx2, _ = interpolate.splprep([STATES_large[:,3],STATES_large[:,4],STATES_large[:,5]], s=2, k=4)
    pos_service2 = np.linspace(0,1,len(STATES_precise[:,0])) # service indexes
    STATES_interp[:,3], STATES_interp[:,4], STATES_interp[:,5] = interpolate.splev(pos_service2, pos_indx2) # interpolation


    times = np.arange(1, len(STATES_precise[:,0])+1)*time_step_pr/60 # [min]

    for i in range(0, len(STATES_precise[:,0])):
        diff_interp[i,0] = (STATES_interp[i,0] - STATES_precise[i,0]) # Rx [m]
        diff_interp[i,1] = (STATES_interp[i,1] - STATES_precise[i,1]) # Ry [m]
        diff_interp[i,2] = (STATES_interp[i,2] - STATES_precise[i,2]) # Rz [m]
        diff_interp[i,3] = (STATES_interp[i,3] - STATES_precise[i,3]) # Vx [m/s]
        diff_interp[i,4] = (STATES_interp[i,4] - STATES_precise[i,4]) # Vy [m/s]
        diff_interp[i,5] = (STATES_interp[i,5] - STATES_precise[i,5]) # Vz [m/s]

    # Plot of interpolation result
    fig1 = plt.figure(1, figsize=(23, 15))
    fig1.suptitle('Orbit positions interpolation ECI, duration:{} h'.format(times[-1]/60))
    ax3d = fig1.add_subplot(111, projection='3d')
    ax3d.plot(STATES_precise[:,0]/1000, STATES_precise[:,1]/1000, STATES_precise[:,2]/1000, 'b', label='propagated') # propagated 
    ax3d.plot(STATES_large[:,0]/1000, STATES_large[:,1]/1000, STATES_large[:,2]/1000, 'r*', label='samples') # propagated samples
    ax3d.plot(STATES_interp[:,0]/1000, STATES_interp[:,1]/1000, STATES_interp[:,2]/1000, 'g', label='interp') # product of the interpolation
    # ax3d.plot(x_knots/1000, y_knots/1000, z_knots/1000, 'g*', label='interp') # knots of interpolation
    # First/last point highlight
    # ax3d.plot(POS_precise[-1,0]/1000, POS_precise[-1,1]/1000, POS_precise[-1,2]/1000, 'b*', label='propagated')
    # ax3d.plot(x_fine[-1]/1000, y_fine[-1]/1000, z_fine[-1]/1000, 'g*', label='interp')
    ax3d.plot([0,-6900], [0,0], [0,0], 'k-', label='apse-line')
    ax3d.plot(0, 0, 0, 'go', linewidth=170, label='Earth')
    ax3d.set_xlim3d(-sma, sma)
    ax3d.set_ylim3d(-sma, sma)
    ax3d.set_zlim3d(-sma, sma)
    plt.legend()
    path = os.path.abspath('./SPICE_experiments/Ephemeris_interp/')
    fig1.savefig(os.path.join(path, "3D_num_interp.png"), bbox_inches='tight')
    # plt.show()

    # plot of speeds distribution
    fig2, axs = plt.subplots(3, 1, figsize=(23, 15))
    fig2.suptitle('Speeds distribution wrt the SPoCK propagation')
    axs[0].plot(times, STATES_interp[:,3], 'r', label='INTERP')
    axs[0].plot(times, STATES_precise[:,3], 'b', label='PROP')
    axs[0].set(xlabel='Time [min]',ylabel='Vx_ECI [m/s]')
    axs[0].legend(loc='upper right')
    
    axs[1].plot(times, STATES_interp[:,4],'r', label='INTERP')
    axs[1].plot(times, STATES_precise[:,4],'b', label='PROP')
    axs[1].set(xlabel='Time [min]',ylabel='Vy_ECI [m/s]')
    axs[1].legend(loc='upper right')

    axs[2].plot(times, STATES_interp[:,5],'r', label='INTERP')
    axs[2].plot(times, STATES_precise[:,5],'b', label='PROP')
    axs[2].set(xlabel='Time [min]',ylabel='Vz_ECI [m/s]')
    axs[2].legend(loc='upper right')
    plt.legend()
    fig2.savefig(os.path.join(path, "speeds.png"), bbox_inches='tight')
    # plt.show()


    # Plot of errors distribution
    fig3, axs = plt.subplots(2, 3, figsize=(23, 15))
    fig3.suptitle('Error from interpolation (WRT SPOCK prop.), duration:{} h'.format(times[-1]/60))
    axs[0,0].plot(times, diff_interp[:,0], 'r')
    axs[0,0].axhline(0.0, linestyle='--', color='b')
    axs[0,0].set(xlabel='Time [min]',ylabel='dx_ECI [m]')

    axs[0,1].plot(times, diff_interp[:,1],'r')
    axs[0,1].axhline(0.0, linestyle='--', color='b')
    axs[0,1].set(xlabel='Time [min]',ylabel='dy_ECI [m]')

    axs[0,2].plot(times, diff_interp[:,2],'r')
    axs[0,2].axhline(0.0, linestyle='--', color='b')
    axs[0,2].set(xlabel='Time [min]',ylabel='dz_ECI [m]')
    
    axs[1,0].plot(times, diff_interp[:,3], 'r')
    axs[1,0].axhline(0.0, linestyle='--', color='b')
    axs[1,0].set(xlabel='Time [min]',ylabel='dVx_ECI [m/s]')

    axs[1,1].plot(times, diff_interp[:,4],'r')
    axs[1,1].axhline(0.0, linestyle='--', color='b')
    axs[1,1].set(xlabel='Time [min]',ylabel='dVy_ECI [m/s]')

    axs[1,2].plot(times, diff_interp[:,5],'r')
    axs[1,2].axhline(0.0, linestyle='--', color='b')  
    axs[1,2].set(xlabel='Time [min]',ylabel='dVz_ECI [m/S]')

    fig3.savefig(os.path.join(path, "deltas.png"), bbox_inches='tight')
    # plt.show()

    return



def interp_request(STATES_large, date_read, date_start, date_end, jump):
    """
    Orbit interpolation using scipy at a precise time instant
    The function:
    - recieves the input date
    - chooses the interval where to interpolate
    - interpolate in order to find the state
    
    PRECISION: it is provided up to the milli-seconds
                    
    INPUT:
        POS_precise: precise positions of propagated S/C ECI [m m m]
        POS_large : propagated points picked witha certain jump ECI [m m m]
        date: date requested for analysis
    
    """

    # checking if the diff btw the date to read and the first date is less than half day
    date_read = datetime.strptime(date_read, '%Y-%m-%d %H:%M:%S')
    date_start = datetime.strptime(date_start, '%Y-%m-%d %H:%M:%S')
    date_end = datetime.strptime(date_end, '%Y-%m-%d %H:%M:%S')

    diff1 = TCU.sec_btw_dates(date_start, date_read)
    diff2 = TCU.sec_btw_dates(date_end, date_read)

    sec_part = 3600*10 # 5 h range
    if abs(diff1) < round(sec_part/2): # CASE 1: the point is close to the beginning of the dataset
        # Computing where the range of interpolation is located and 
        # the coeff of the point for interpolation
        STATES_interp = STATES_large[0:round(sec_part/jump), :]
        service = abs(diff1)/sec_part

    elif abs(diff2) < round(sec_part/2): # CASE 2: the point is close to the end of the dataset
        # Computing where the range of interpolation is located and 
        # the coeff of the point for interpolation
        STATES_interp = STATES_large[-round(sec_part/jump):-1, :]
        service = (sec_part-abs(diff2))/sec_part

    else: # CASE 3: th epoint in the 'middle' of the ephemeris segment
        start = round((abs(diff1)-sec_part/2)/jump) # index
        end = round((abs(diff1)+sec_part/2)/jump) # index
        STATES_interp = STATES_large[start:end, :]
        service = abs(start*jump-diff1)/abs(start*jump-end*jump) 

    
    # Zero vectors for the interpolation products
    STATE_date = np.zeros(6)

    # Interpolation of the S/C positions each jump set
    ttime = time.time()
    mm = len(STATES_interp[:,0])
    SS = round((mm-np.sqrt(2*mm) + mm+np.sqrt(2*mm))/2) # in oredr to compute smoothness
    pos_indx1, _ = interpolate.splprep([STATES_interp[:,0],STATES_interp[:,1],STATES_interp[:,2]], s=SS, k=4)
    STATE_date[0], STATE_date[1], STATE_date[2] = interpolate.splev(service, pos_indx1) # interpolation

    # Interpolation of the S/C speeds each jump set
    pos_indx2, _ = interpolate.splprep([STATES_interp[:,3],STATES_interp[:,4],STATES_interp[:,5]], s=SS, k=4)
    STATE_date[3], STATE_date[4], STATE_date[5] = interpolate.splev(service, pos_indx2) # interpolation

    elapsed = time.time()-ttime


    print('State vector interpolated SPICE')
    print('date of interpolation: {}'.format(date_read))
    print('Time taken for interpolation: {}'.format(elapsed))
    print('----------------------')
    print(' X        = {:20.6f}'.format(STATE_date[0]))
    print(' Y        = {:20.6f}'.format(STATE_date[1]))
    print(' Z        = {:20.6f}'.format(STATE_date[2]))
    print('VX        = {:20.6f}'.format(STATE_date[3]))
    print('VY        = {:20.6f}'.format(STATE_date[4]))
    print('VZ        = {:20.6f}'.format(STATE_date[5]))
    print('----------------------')

    return STATE_date

def interp_S_tradeoff(STATES_precise,sma, STATES_large, time_step_pr, jump):
    """
    Orbit interpolation using scipy
    https://spiceypy.readthedocs.io/en/main/documentation.html
    
    functions used: interpolate.splprep()
                    interpolate.splev()
                    
    INPUT:
        STATES_precise: precise positions of propagated S/C ECI [m m m]
        STATES_large : propagated points picked witha certain jump ECI [m m m]
        sma: SMA of the S/C orbit for plot
        time_step_pr = propagation time step
        jump = time spacing jump between data
    
    """
    
    # Zero vectors for the interpolation products
    STATES_interp = np.zeros((len(STATES_precise), 6))
    diff_interp = np.zeros((len(STATES_precise), 6))
    
    # Array of smoothness coeff.s:
    mm = len(STATES_large[:,0])
    S_arr = np.linspace(2, mm+np.sqrt(2*mm), 20)
    
    times = np.arange(1, len(STATES_precise[:,0])+1)*time_step_pr/60 # [min]

    # Plot of errors distribution
    fig3, axs = plt.subplots(2, 3, figsize=(23, 15))
    fig3.suptitle('Error from interpolation (S tradeoff), duration:{} h'.format(times[-1]/60))
    
    for jj in range(0, len(S_arr)):
    
        # Interpolation of the S/C positions each jump set
        pos_indx1, _ = interpolate.splprep([STATES_large[:,0],STATES_large[:,1],STATES_large[:,2]], s=S_arr[jj], k=4)
        pos_service1 = np.linspace(0,1,len(STATES_precise[:,0])) # service indexes
        STATES_interp[:,0], STATES_interp[:,1], STATES_interp[:,2] = interpolate.splev(pos_service1, pos_indx1) # interpolation

        # Interpolation of the S/C speeds each jump set
        pos_indx2, _ = interpolate.splprep([STATES_large[:,3],STATES_large[:,4],STATES_large[:,5]], s=S_arr[jj], k=4)
        pos_service2 = np.linspace(0,1,len(STATES_precise[:,0])) # service indexes
        STATES_interp[:,3], STATES_interp[:,4], STATES_interp[:,5] = interpolate.splev(pos_service2, pos_indx2) # interpolation


        times = np.arange(1, len(STATES_precise[:,0])+1)*time_step_pr/60 # [min]

        for i in range(0, len(STATES_precise[:,0])):
            diff_interp[i,0] = (STATES_interp[i,0] - STATES_precise[i,0]) # Rx [m]
            diff_interp[i,1] = (STATES_interp[i,1] - STATES_precise[i,1]) # Ry [m]
            diff_interp[i,2] = (STATES_interp[i,2] - STATES_precise[i,2]) # Rz [m]
            diff_interp[i,3] = (STATES_interp[i,3] - STATES_precise[i,3]) # Vx [m/s]
            diff_interp[i,4] = (STATES_interp[i,4] - STATES_precise[i,4]) # Vy [m/s]
            diff_interp[i,5] = (STATES_interp[i,5] - STATES_precise[i,5]) # Vz [m/s]

        axs[0,0].plot(times, diff_interp[:,0])
        axs[0,1].plot(times, diff_interp[:,1])
        axs[0,2].plot(times, diff_interp[:,2])
        axs[1,0].plot(times, diff_interp[:,3])
        axs[1,1].plot(times, diff_interp[:,4])
        axs[1,2].plot(times, diff_interp[:,5])
        
        
    axs[0,0].axhline(0.0, linestyle='--', color='b')
    axs[0,0].set(xlabel='Time [min]',ylabel='dx_ECI [m]')        
    axs[0,1].axhline(0.0, linestyle='--', color='b')
    axs[0,1].set(xlabel='Time [min]',ylabel='dy_ECI [m]')       
    axs[0,2].axhline(0.0, linestyle='--', color='b')
    axs[0,2].set(xlabel='Time [min]',ylabel='dz_ECI [m]')        
    axs[1,0].axhline(0.0, linestyle='--', color='b')
    axs[1,0].set(xlabel='Time [min]',ylabel='dVx_ECI [m/s]')        
    axs[1,1].axhline(0.0, linestyle='--', color='b')
    axs[1,1].set(xlabel='Time [min]',ylabel='dVy_ECI [m/s]')        
    axs[1,2].axhline(0.0, linestyle='--', color='b')  
    axs[1,2].set(xlabel='Time [min]',ylabel='dVz_ECI [m/S]')
    
    path = os.path.abspath('./SPICE_experiments/Ephemeris_interp/')
    # fig3.savefig(os.path.join(path, "trade_off_S.png"), bbox_inches='tight')
    plt.show()

    return



def interp_tradeoff(durations, time_step, jump):
    """
    functions used: interpolate.splprep()
                    interpolate.splev()
    
    Trade-off about the impact of the duration of propagation
    considered
    INPUT:
        durations = durations of the interpolation periods considered
        time_step = precise time step [sec]
        jump = large points step [sec]
    
    """
    max_diff_int = np.zeros((len(durations), 6))


    for kk, value in enumerate(durations):

        # This loop computes max differences between interpolation and propagation
        duration = durations[kk]*3600 # [sec]

        STATES_precise,_ = data_extr('Test', duration, time_step) # pos & vel [m m m] [m/s m/s m/s]
        indx = np.arange(0,int(round(duration/10))+1, round(jump/time_step))
        STATES_large = STATES_precise[indx,:] # pos & vel selected [m m m] [m/s m/s m/s]

        # Zero vectors for the interpolation products
        STATES_interp = np.zeros((len(STATES_precise), 6))
        diff_interp = np.zeros((len(STATES_precise), 6))
        
        # Interpolation of the S/C positions each jump set
        mm = len(STATES_large[:,0])
        SS = round((mm-np.sqrt(2*mm) + mm+np.sqrt(2*mm))/2) # in oredr to compute smoothness
        pos_indx1, _ = interpolate.splprep([STATES_large[:,0],STATES_large[:,1],STATES_large[:,2]], s=SS, k=4)
        pos_service1 = np.linspace(0,1,len(STATES_precise[:,0])) # service indexes
        STATES_interp[:,0], STATES_interp[:,1], STATES_interp[:,2] = interpolate.splev(pos_service1, pos_indx1) # interpolation

        # Interpolation of the S/C speeds each jump set
        pos_indx2, _ = interpolate.splprep([STATES_large[:,3],STATES_large[:,4],STATES_large[:,5]], s=SS, k=4)
        pos_service2 = np.linspace(0,1,len(STATES_precise[:,0])) # service indexes
        STATES_interp[:,3], STATES_interp[:,4], STATES_interp[:,5] = interpolate.splev(pos_service2, pos_indx2) # interpolation


        for i in range(0, len(STATES_precise[:,0])):
            diff_interp[i,0] = (STATES_interp[i,0] - STATES_precise[i,0]) # Rx [m]
            diff_interp[i,1] = (STATES_interp[i,1] - STATES_precise[i,1]) # Ry [m]
            diff_interp[i,2] = (STATES_interp[i,2] - STATES_precise[i,2]) # Rz [m]
            diff_interp[i,3] = (STATES_interp[i,3] - STATES_precise[i,3]) # Vx [m/s]
            diff_interp[i,4] = (STATES_interp[i,4] - STATES_precise[i,4]) # Vy [m/s]
            diff_interp[i,5] = (STATES_interp[i,5] - STATES_precise[i,5]) # Vz [m/s]

        # Max of the differences for components
        max_diff_int[kk, 0] = max(diff_interp[:,0])    
        max_diff_int[kk, 1] = max(diff_interp[:,1])    
        max_diff_int[kk, 2] = max(diff_interp[:,2])  
        # if max_diff_int[kk,0] > 1e10 and max_diff_int[kk,1] > 1e10 and max_diff_int[kk,2] > 1e10:
        #     max_diff_int[kk,0] = max_diff_int[kk-1,0]
        #     max_diff_int[kk,1] = max_diff_int[kk-1,1]
        #     max_diff_int[kk,2] = max_diff_int[kk-1,2]
        max_diff_int[kk, 3] = max(diff_interp[:,3])    
        max_diff_int[kk, 4] = max(diff_interp[:,4])    
        max_diff_int[kk, 5] = max(diff_interp[:,5])    
        print('{} completed...'.format(kk))

    # Plot of errors distribution
    fig1, axs = plt.subplots(2, 3, figsize=(23, 15))
    fig1.suptitle('Max error of interpolation (WRT prop.) - max duration:{} h'.format(durations[-1]))
    axs[0,0].plot(durations, max_diff_int[:,0], '--ro')
    for jj in range(0, len(durations)):
        axs[0,0].text(durations[jj], max_diff_int[[jj],0], str(durations[jj]))
    axs[0,0].set(xlabel='durations [h]',ylabel='diff_Rx [m]')

    axs[0,1].plot(durations, max_diff_int[:,1],'--ro')
    for jj in range(0, len(durations)):
        axs[0,1].text(durations[jj], max_diff_int[[jj],1], str(durations[jj]))
    axs[0,1].set(xlabel='durations [h]',ylabel='diff_Ry [m]')

    axs[0,2].plot(durations, max_diff_int[:,2],'--ro')
    for jj in range(0, len(durations)):
        axs[0,2].text(durations[jj], max_diff_int[[jj],2], str(durations[jj]))
    axs[0,2].set(xlabel='durations [h]',ylabel='diff_Rz [m]')
    
    axs[1,0].plot(durations, max_diff_int[:,3], '--ro')
    for jj in range(0, len(durations)):
        axs[1,0].text(durations[jj], max_diff_int[[jj],3], str(durations[jj]))
    axs[1,0].set(xlabel='durations [h]',ylabel='diff_Vx [m/s]')

    axs[1,1].plot(durations, max_diff_int[:,4],'--ro')
    for jj in range(0, len(durations)):
        axs[1,1].text(durations[jj], max_diff_int[[jj],4], str(durations[jj]))
    axs[1,1].set(xlabel='durations [h]',ylabel='diff_Vy [m/s]')

    axs[1,2].plot(durations, max_diff_int[:,5],'--ro')
    for jj in range(0, len(durations)):
        axs[1,2].text(durations[jj], max_diff_int[[jj],5], str(durations[jj]))
    axs[1,2].set(xlabel='durations [h]',ylabel='diff_Vz [m/S]')
    
    path = os.path.abspath('./SPICE_experiments/Ephemeris_interp/')
    fig1.savefig(os.path.join(path, "numTDO.png"), bbox_inches='tight')
    # plt.show()

    return



def kernel_creation_spice(states, duration, jump, time_step, EL_Times):
    """
    Kernel definition through the propagated states
    Propagates states used to create Kernel are selected through a 
    time step define as 'jump'

    """

    secs = EL_Times
    sec_J2 = np.zeros(len(secs))

    spice.furnsh(os.path.abspath('./SPICE_experiments/kernel/MetaK.txt'))

    
    for i in range(0, len(secs)):
        dt = TCU.add_sec_datesDATETIME("2020-05-01 00:00:00", int(secs[i]))
        sec_J2[i] = ss.datetime2et(dt)

    print(secs[:10])
    
    # Creating a new SPK file of type 5:
    ttime = time.time()
    path = os.path.abspath('./SPICE_experiments/kernel/')
    handle = ss.spkopn(os.path.join(path, "test.bsp"), 'test', 0)
    # print(type(handle))
    
    # Writing data (segment) to the SPK file opened:
    first = sec_J2[0]
    last = sec_J2[-1]
    print(secs[-1])
    print(states[-1,:])
    ss.spkw08( handle, -100001,  399, 'J2000',  first, last, \
                    'test 1',  20, len(secs), states[0:len(secs), :], first, jump )

    # Closing spk file
    ss.spkcls(handle)
    elapsed = time.time()-ttime # time taken in order to create the kernel
    print('Time taken in order to create the SPICE SPK Kernel: {} sec'.format(elapsed))

    return handle
    

def reading_SPK_state(utc):
    spice.furnsh(os.path.abspath('./SPICE_experiments/kernel/MetaK.txt'))
    target = '-100001'
    frame  = 'J2000'
    corrtn = 'NONE'
    observ = 'EARTH'
    # utc =  '2020-05-01T00:02:05'
    # utc = TCU.add_sec_datesDATETIME (utc, 0)
    
    ttime = time.time()
    et = ss.str2et(utc)
    ttime = time.time()
    state, ltime = spice.spkezr(target, et, frame,
                                   corrtn, observ)
    elapsed = time.time()-ttime

    print('State vector interpolated SPICE')
    print('date of interpolation: {}'.format(utc))
    print('Time taken for interpolation: {}'.format(elapsed))
    print('----------------------')
    print(' X        = {:20.6f}'.format(state[0]))
    print(' Y        = {:20.6f}'.format(state[1]))
    print(' Z        = {:20.6f}'.format(state[2]))
    print('VX        = {:20.6f}'.format(state[3]))
    print('VY        = {:20.6f}'.format(state[4]))
    print('VZ        = {:20.6f}'.format(state[5]))
    print('----------------------')

    elapsed = time.time() -ttime # time taken in order to compute interpolation
    print(' Time taken to interpolate {} '.format(elapsed))
    
    return state


def eval_SPICE_interp(POS_precise, time_step):
    """
    Evaluation of the quality of SPICE interpolation
    as compared with the propagated ephemeris
    """

    spice.furnsh(os.path.abspath('./SPICE_experiments/kernel/MetaK.txt'))
    target = '-100001'
    frame  = 'J2000'
    corrtn = 'NONE'
    observ = 'EARTH'
    utc =  "2020-05-01 00:00:10" # start date

    tt = 0
    utcc = []
    durations = np.arange(1,len(POS_precise)+1)*time_step / 3600 # [min]
    states = np.zeros((len(POS_precise),6))
    diff = np.zeros((len(POS_precise),6))
    print('Starting interpolation...')
    ttime = time.time()
    for i in range(0,len(POS_precise)-7):
        
        utc1 = TCU.add_sec_datesDATETIME (utc, tt)
        et = ss.datetime2et(utc1)
        # utcc.append(datetime.strftime(utc1,'%Y-%m-%d %H:%M:%S'))
        states[i, :], _ = spice.spkezr(target, et, frame,
                                        corrtn, observ)
        tt += time_step # time increment for dates
        
        diff[i, 0] = states[i,0]-POS_precise[i,0]
        diff[i, 1] = states[i,1]-POS_precise[i,1]
        diff[i, 2] = states[i,2]-POS_precise[i,2]
        diff[i, 3] = states[i,3]-POS_precise[i,3]
        diff[i, 4] = states[i,4]-POS_precise[i,4]
        diff[i, 5] = states[i,5]-POS_precise[i,5]

    # print(states[:10,:])
    # print(POS_precise[:10,:])
    
    elapsed = time.time() -ttime # time taken in order to compute interpolation
    print(' Time taken to interpolate {} points, with spice: {} sec'.format(len(POS_precise), elapsed))
    print('Interpolation completed!')
    
    fig, axs = plt.subplots(2, 3, figsize=(23, 15))
    fig.suptitle('Max error of interpolation (WRT prop.)')
    axs[0,0].plot(durations[:], diff[:,0]*1000, 'r')
    # axs[0,0].plot(durations, POS_precise[:,0], 'b')
    axs[0,0].set(xlabel='durations [h]',ylabel='Rx [m]')

    axs[0,1].plot(durations[:], diff[:,1]*1000,'r')
    # axs[0,1].plot(durations, POS_precise[:,1],'b')
    axs[0,1].set(xlabel='durations [h]',ylabel='Ry [m]')

    axs[0,2].plot(durations[:], diff[:,2]*1000,'r')
    # axs[0,2].plot(durations, POS_precise[:,2],'b')
    axs[0,2].set(xlabel='durations [h]',ylabel='Rz [m]')
    
    axs[1,0].plot(durations[:], diff[:,3]*1000, 'r')
    # axs[1,0].plot(durations, POS_precise[:,3], 'b')
    axs[1,0].set(xlabel='durations [h]',ylabel='Vx [m/s]')

    axs[1,1].plot(durations[:], diff[:,4]*1000,'r')
    # axs[1,1].plot(durations, POS_precise[:,4],'b')
    axs[1,1].set(xlabel='durations [h]',ylabel='Vy [m/s]')

    axs[1,2].plot(durations[:], diff[:,5]*1000,'r')
    # axs[1,2].plot(durations, POS_precise[:,5],'b')
    axs[1,2].set(xlabel='durations [h]',ylabel='Vz [m/s]')
    path = os.path.abspath('./SPICE_experiments/Ephemeris_interp/')
    fig.savefig(os.path.join(path, "SPICE_Spock.png"), bbox_inches='tight')
    # plt.show()


    return 









if __name__ == "__main__":

    # TESTING THE MAIN CAPABILITIES OF AN INTERPOLATION
    # OF EPHEMERIS COMBINED TO A PROPAGATION OF THE SATELLITE
    
    # General data:
    sma = 7000 # for plotting reasons
    mission = 'Test'
    duration = 3600*30 # small [sec]
    duration1 = 3600*24*28 # huge [sec]
    # Arrey of simulation duration for trade-off analysis
    durations = np.arange(2, 200, 6)

    # Temporal data:
    time_step = 10 # [sec]
    jump = 60 # jump to consider just a part of the ephemeris [sec]
    reading_utc = '2020-05-28T23:55:20' # data for the SPICE interpolation
    
    STATES_precise, _, _ = data_extr(mission, duration, time_step) # pos & vel [m m m] [m/s m/s m/s]
    indx = np.arange(0,int(round(duration/10))+1, round(jump/time_step))
    STATES_large = STATES_precise[indx,:] # pos & vel selected [m m m] [m/s m/s m/s]
    
    STATES_precise1, EL_Times1, Dates = data_extr(mission, duration1, time_step) # pos & vel [m m m] [m/s m/s m/s]
    indx1 = np.arange(0,int(round(duration1/10))+1, round(jump/time_step))
    STATES_large1 = STATES_precise1[indx1,:] # pos & vel selected [m m m] [m/s m/s m/s]
    TIMES1 = EL_Times1[indx1]
    DATES = [Dates[ind] for ind in indx1]
    print(STATES_precise[-1,:])
    print(STATES_large[-1,:])

    #---------------------------------------------
    
    # interp_pol(STATES_precise, sma, STATES_large, time_step, jump)
    # interp_tradeoff(durations, time_step, jump)
    # interp_S_tradeoff(STATES_precise,sma, STATES_large, time_step, jump)
    # interp_request(STATES_large1, '2020-05-28 23:55:20', DATES[0], DATES[-1], jump)
    # kernel_creation_spice(STATES_large1/1000, duration1, jump, time_step, TIMES1) # input [km] [km/s]
    # reading_SPK_state(reading_utc)
    eval_SPICE_interp(STATES_precise1/1000, time_step) # input [km] [km/s]