
import os, sys, csv, configparser
import matplotlib.pyplot as plt

path_ = os.path.abspath('Orbit_data_analysis/utilities/')
print(path_)
sys.path.append('{}'.format(path_))

import numpy as np
import pandas as pd
import CarKep as CK
import AstroConstants as AC





def reading_txt(content):

    """
    This function is capable to read the content of 
    the .txt file used as input for the SpOCK scenario.
    
    INPUT
     content = is a vector containing all the strings as read from the input
                .txt file
    """
    OP = np.zeros(6) # orbital parameters [km, deg]
    OP[0] = float((content[5].split())[1]) # SMA
    OP[1] = float((content[6].split())[1]) # ECC
    OP[2] = float((content[7].split())[1]) # INC
    OP[3] = float((content[8].split())[1]) # RAAN
    OP[4] = float((content[9].split())[1]) # AOP
    OP[5] = float((content[10].split())[1]) # TA

    time_step = float((content[13].split())[1]) # time step [sec]
    duration = float((content[14].split())[1]) # time step [days]
    date = ' '.join([(content[15].split())[1], (content[15].split())[2]]) # launch date [yyyy-mm-ddThh-mm-ss]
    mission_name = (content[16].split())[1] # mission name [str]
    propagation_type = (content[17].split())[1] # keyword for propagation characteristics

    FOV = float((content[22].split())[1]) # Field-of-View [deg]
    eclipses = (content[23].split())[1] # eclipses detection [true/false]
    TGs_grid = (content[24].split())[1] # TG grid definition in SpOCK
    TGs_passes = (content[26].split())[1] # TGs analysis is desired [true/false]
    GSs_contacts = (content[27].split())[1] # GSs analysis is deisred [true/false]
    TGs_random = (content[28].split())[1] # creation of random TGs based on capitles
    TGs_number = int((content[29].split())[1]) # numb of capitles selected for TGs
    GSs_random = (content[30].split())[1] # creation of GSs based on capitles
    GSs_number = int((content[31].split())[1]) #numb of capitles for GSs
    TGs_custom = (content[32].split())[1] # if a custom list of TGs is desired
    TGs_c_num = (content[33].split())[1] # number of custom targets
    if TGs_c_num == '1':
        TGs_names = (content[34].split())[1] # TGs names LIST OF STRINGS
        TGs_longitude = (content[35].split())[1] # longitudes [deg] LIST OF STRINGS
        TGs_latitude = (content[36].split())[1] # latitudes [deg] LIST OF STRINGS
        TGs_altitude = (content[37].split())[1] # altitudes [m] LIST OF STRINGS
    else:
        TGs_names = ((content[34].split())[1]).split(",") # TGs names LIST OF STRINGS
        TGs_longitude = ((content[35].split())[1]).split(",") # longitudes [deg] LIST OF STRINGS
        TGs_latitude = ((content[36].split())[1]).split(",") # latitudes [deg] LIST OF STRINGS
        TGs_altitude = ((content[37].split())[1]).split(",") # altitudes [m] LIST OF STRINGS
    GSs_custom = (content[38].split())[1] # if a custom list of GSs is desired
    GSs_c_num = (content[39].split())[1] # number of custom GS
    if GSs_c_num == '1':
        GSs_names = str((content[40].split())[1]) # GSs names LIST OF STRINGS
        GSs_longitude = (content[41].split())[1] # longitudes [deg] LIST OF STRINGS
        GSs_latitude = (content[42].split())[1] # latitudes [deg] LIST OF STRINGS
        GSs_altitude = (content[43].split())[1] # altitudes [m] LIST OF STRINGS
        GSs_minelevation = (content[44].split())[1] # minimum elevatins [deg] LIST OF STRINGS
    else:
        GSs_names =((content[40].split())[1]).split(",")
        GSs_longitude = ((content[41].split())[1]).split(",") # longitudes [deg] LIST OF STRINGS
        GSs_latitude = ((content[42].split())[1]).split(",") # latitudes [deg] LIST OF STRINGS
        GSs_altitude = ((content[43].split())[1]).split(",") # altitudes [m] LIST OF STRINGS
        GSs_minelevation = ((content[44].split())[1]).split(",") # minimum elevatins [deg] LIST OF STRINGS
    SS = float((content[47].split())[1]) # spacecraft number
    PP = float((content[48].split())[1]) # spacecraft plane


    return OP, time_step, duration, date, mission_name, propagation_type, FOV, eclipses, TGs_grid, TGs_random, \
        TGs_number, GSs_random, GSs_number, TGs_custom, TGs_names, TGs_longitude, TGs_latitude, TGs_altitude, \
            GSs_custom, GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, SS, PP, TGs_passes, GSs_contacts, GSs_c_num, TGs_c_num





def reading_cfg(file):

    """
    This function is capable to read the content of 
    the .cfg file used as input for the SpOCK scenario.
    
    INPUT
     file = the path to the cfg file used as input
    """
    config = configparser.ConfigParser(inline_comment_prefixes="#")
    config.read(file)

    # Orbit characteristics:
    OP = np.zeros(6) # orbital parameters [km, deg]
    if config["Orbit Characteristics"]["OP"] == 'K':
        OP[0] =  float(config["Orbit Characteristics"]["SMA"]) # SMA
        OP[1] =  float(config["Orbit Characteristics"]["ECC"]) # ECC
        OP[2] =  float(config["Orbit Characteristics"]["INC"]) # INC
        OP[3] =  float(config["Orbit Characteristics"]["RAAN"]) # RAAN
        OP[4] =  float(config["Orbit Characteristics"]["AOP"]) # AOP
        OP[5] =  float(config["Orbit Characteristics"]["TA"]) # TA
    else:
        rr = [float(config["Orbit Characteristics"]["rrx"]), float(config["Orbit Characteristics"]["rry"]), float(config["Orbit Characteristics"]["rrz"])]
        vv = [float(config["Orbit Characteristics"]["vvx"]), float(config["Orbit Characteristics"]["vvy"]), float(config["Orbit Characteristics"]["vvz"])]
        OP[:] = CK.rv2elem(AC.MU_E ,rr, vv) # [km, rad]
        OP[2:] = OP[2:]/AC.D2R # [km, deg]

    # Simulation parameters.:
    time_step = float(config["Simulation Parameters"]["time_step"]) # [sec]
    duration = float(config["Simulation Parameters"]["duration"]) # [days]
    date = config["Simulation Parameters"]["launch_date"] # [str]
    mission_name = config["Simulation Parameters"]["mission_name"] # [str]
    propagation_type = config["Simulation Parameters"]["propagation_type"] # description of propagation type [str]
    
    # Mission analysis parameters:
    FOV = float(config["Mission analysis paramters"]["FOV"])
    eclipses = config["Mission analysis paramters"]["eclipses"]
    TGs_grid = config["Mission analysis paramters"]["TGs_grid"]
    TGs_passes = config["Mission analysis paramters"]["TGs_passes"]
    GSs_contacts = config["Mission analysis paramters"]["GSs_contacts"] # [true/false]
    TGs_random = config["Mission analysis paramters"]["TGs_random"] # [true/false]
    TGs_number = int(config["Mission analysis paramters"]["TGs_number"]) # [if-random-is-on]
    GSs_random = config["Mission analysis paramters"]["GSs_random"] # [true/false]
    GSs_number = int(config["Mission analysis paramters"]["GSs_number"]) # [if-random-is-on]
    TGs_custom = config["Mission analysis paramters"]["TGs_custom"] # [true/false]
    TGs_c_num = config["Mission analysis paramters"]["TGs_numb"] # [numb-of-custom-targets]
    if TGs_c_num == '1':
        TGs_names = config["Mission analysis paramters"]["TGS_names"]  # [names-of-TGSs]
        TGs_longitude = config["Mission analysis paramters"]["TGs_longitude"] # [deg]
        TGs_latitude = config["Mission analysis paramters"]["TGs_latitude"] # [deg]
        TGs_altitude = config["Mission analysis paramters"]["TGs_altitude"] # [m]
    else: 
        TGs_names = config["Mission analysis paramters"]["TGS_names"].split(",")  # [names-of-TGSs]
        TGs_longitude = config["Mission analysis paramters"]["TGs_longitude"].split(",") # [deg]
        TGs_latitude = config["Mission analysis paramters"]["TGs_latitude"].split(",") # [deg]
        TGs_altitude = config["Mission analysis paramters"]["TGs_altitude"].split(",") # [m]
    GSs_custom = config["Mission analysis paramters"]["GSs_custom"] # [true/false]
    GSs_c_num = config["Mission analysis paramters"]["GSs_numb"] # [numb-of-custom-stations]
    if GSs_c_num == '1':
        GSs_names = config["Mission analysis paramters"]["GSs_names"] # [names-of-GS]
        GSs_longitude = config["Mission analysis paramters"]["GSs_longitude"] # [deg]
        GSs_latitude = config["Mission analysis paramters"]["GSs_latitude"] # [deg]
        GSs_altitude = config["Mission analysis paramters"]["GSs_altitude"] # [m]
        GSs_minelevation = config["Mission analysis paramters"]["GS_minelevation"] # [deg]
    else:
        GSs_names = config["Mission analysis paramters"]["GSs_names"].split(",") # [names-of-GS]
        GSs_longitude = config["Mission analysis paramters"]["GSs_longitude"].split(",") # [deg]
        GSs_latitude = config["Mission analysis paramters"]["GSs_latitude"].split(",") # [deg]
        GSs_altitude = config["Mission analysis paramters"]["GSs_altitude"].split(",") # [m]
        GSs_minelevation = config["Mission analysis paramters"]["GS_minelevation"].split(",") # [deg]

    # Spacecraft ID:
    SS = config["Spacecraft ID"]["S"] # [s/c number]
    PP = config["Spacecraft ID"]["P"] # [plane number]
    
    # Specacreft hardware characteristics.
    SRP_area = config["Hardware and SC characteristics"]["SRP_area"] # SRP area [m2]
    Drag_area = config["Hardware and SC characteristics"]["Drag_area"] # Drag area [m2]
    Cd = config["Hardware and SC characteristics"]["Cd"] # drag coeffic
    Cr = config["Hardware and SC characteristics"]["Cr"] # SRP coefficient
    d_mass = config["Hardware and SC characteristics"]["Dry_mass"] # SC dry mass [kg]

    
    return OP, time_step, duration, date, mission_name, propagation_type, FOV, eclipses, TGs_grid, TGs_random, \
        TGs_number, GSs_random, GSs_number, TGs_custom, TGs_names, TGs_longitude, TGs_latitude, TGs_altitude, \
            GSs_custom, GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, SS, PP, TGs_passes, GSs_contacts, GSs_c_num, \
                TGs_c_num, SRP_area, Drag_area, Cr, Cd, d_mass




def header(mission, TGs_passes, GSs_contacts, eclipses):
    """
    This function adds or modifies the headers to the 
    file coming from SpOCK
    
    INPUT:
        mission = mission name
        TGs_passes = if passes are computed [true/false]
        GSs_contacts = contacts with GSs [true/false]
        eclipses = if eclipses are computed [true/false]
        
    """
    # Ephemeris file:
    ff = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/Ephemeris/S1_P1.csv".format(mission))
    file = pd.read_csv(ff)
    headerList = ['ElapsedTime','GPSsec','Rx_eci','Ry_eci','Rz_eci','Vx_eci','Vy_eci','Vz_eci','Rx_ecef','Ry_ecef','Rz_ecef','Vx_ecef','Vy_ecef','Vz_ecef']
    file.to_csv(ff, header=headerList, index=False)

    # All Eclipses file:
    if eclipses == 'true':
        ff1 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/AllEclipses_times_{}.csv".format(mission, mission))
        file = pd.read_csv(ff1)
        headerList = ['Penumbra_start_UTC','Umbra_start_UTC','Umbra_end_UTC','Penumbra_end_UTC','Umbra_duration','Penumbra_duration','P','S',]
        file.to_csv(ff1, header=headerList, index=False)

        # S1_P1 Eclipses file:
        ff2 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_Eclipse_times.csv".format(mission))
        file = pd.read_csv(ff2)
        headerList = ['Penumbra_start_UTC','Umbra_start_UTC','Umbra_end_UTC','Penumbra_end_UTC','Umbra_duration','Penumbra_duration']
        file.to_csv(ff2, header=headerList, index=False)

    if GSs_contacts == 'true':
        # All GS contacts file:
        ff3 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/All_GS_contacts_{}.csv".format(mission, mission))
        file = pd.read_csv(ff3)
        headerList = ['GS','AOS_UTC','LOS_UTC','AOS','Duration','Epoch_UTC_Max_El','Max_Elevation','Az_AOS','Az_LOS' ,'Az_Max_El','lon','lat','P','S',]
        file.to_csv(ff3, header=headerList, index=False)

        # S1_P1 GS contacts by time file:
        ff4 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_GS_contacts_bytime.csv".format(mission))
        file = pd.read_csv(ff4)
        headerList = ['GS','AOS_UTC','LOS_UTC','AOS','Duration','Epoch_UTC_Max_El','Max_Elevation','Az_AOS','Az_LOS' ,'Az_Max_El','lon','lat',]
        file.to_csv(ff4, header=headerList, index=False)

        # S1_P1 GS contacts file:
        ff5 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_GS_contacts.csv".format(mission))
        file = pd.read_csv(ff5)
        headerList = ['GS','AOS_UTC','LOS_UTC','AOS','Duration','Epoch_UTC_Max_El','Max_Elevation','Az_AOS','Az_LOS' ,'Az_Max_El','lon','lat',]
        file.to_csv(ff5, header=headerList, index=False)

    if TGs_passes == 'true':
        # All TG contacts file:
        ff6 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/All_TG_contacts_{}.csv".format(mission, mission))
        file = pd.read_csv(ff6)
        headerList = ['TG','AOS_UTC','LOS_UTC','GPS_time_in','Duration','El_in','El_out','Epoch_UTC_Max_El','Max_El','Az_in','Az_out','Az_Max_El','lon','lat','P','S']
        file.to_csv(ff6, header=headerList, index=False)

        # S1_P1 TG contacts by time file:
        ff7 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_TG_contacts_bytime.csv".format(mission))
        file = pd.read_csv(ff7)
        headerList = ['TG','AOS_UTC','LOS_UTC','GPS_time_in','Duration','El_in','El_out','Epoch_UTC_Max_El','Max_El','Az_in','Az_out','Az_Max_El','lon','lat',]
        file.to_csv(ff7, header=headerList, index=False)

        # S1_P1 TG contacts file:
        ff8 = os.path.abspath("./Orbit_data_analysis/DATA/{}/Output/S1-P1_TG_contacts.csv".format(mission))
        file = pd.read_csv(ff8)
        headerList = ['TG','AOS_UTC','LOS_UTC','GPS_time_in','Duration','El_in','El_out','Epoch_UTC_Max_El','Max_El','Az_in','Az_out','Az_Max_El','lon','lat']
        file.to_csv(ff8, header=headerList, index=False)

    return


if __name__ == "__main__":
    mission = 'SatRev'
    file = os.path.abspath('./Orbit_data_analysis/DATA/{}/Input/input.cfg'.format(mission))
    OP, time_step, duration, date, mission_name, propagation_type, FOV, eclipses, TGs_grid, TGs_random, \
        TGs_number, GSs_random, GSs_number, TGs_custom, TGs_names, TGs_longitude, TGs_latitude, TGs_altitude, \
            GSs_custom, GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, SS, PP, TGs_passes, GSs_contacts, GSs_c_num, TGs_c_num = reading_cfg(file)


    print(OP)
    print(time_step)
    print(duration)
    print(date)
    print(mission_name)
    print(propagation_type)
    print(FOV)
    print(eclipses)
    print(TGs_grid)
    print(TGs_random)
    print(TGs_number)
    print(GSs_random)
    print(GSs_number)
    print(TGs_custom)
    print(TGs_names)
    print(TGs_longitude)
    print(TGs_latitude)
    print(TGs_altitude)
    print(GSs_custom)
    print(GSs_names)
    print(GSs_longitude)
    print(GSs_latitude)
    print(GSs_altitude)
    print(GSs_minelevation)
    print(SS)
    print(PP)
    print(TGs_passes)
    print(GSs_contacts)
    print(GSs_c_num)
    print(TGs_c_num)