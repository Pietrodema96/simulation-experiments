
######################
COMPUTATION PARAMETERS
######################

simstep: 60
duration: 259200

FOV_cross: 30
FOV_along: 30

SC_start: 1
SC_end: 1
PL_start: 1
PL_end: 1

TGs_on: 0
GSs_on: 0
TGs_grid_on: 0
Eclipse_on: 1


######################
TARGETS
######################

TG_grid_limits(0): -90
TG_grid_limits(1): 90
TG_grid_limits(2): -90
TG_grid_limits(3): 90
gridstep: 10

TG name: DL-Salt-Desert
lon: 59.067
lat: 30.605
alt: 10


######################
GROUND STATIONS
######################

GS name: ENV-FOS-DA
lon: 8.6469
lat: 49.8787
alt: 144
minelev: 2

######################
INPUT FILES PATHS
######################

Orbit Orbit_ephemeris_path: /home/pietro/Documenti/SpOCK/output/OrbitEphemerides/AIKOSim/AISAC
Orbit_ephemeris_rootname: AISACS1_P1.csv
Data path: /home/pietro/Documenti/SpOCK/data
Planet ephemeris: de440.bsp
EOP: earth_000101_220328_220103.bpc
PCK: pck00010.tpc
Leap second: naif0012.tls

######################
OUTPUT FILES PATHS
######################

TG_filename: /home/pietro/Documenti/SpOCK/output/TG_Contacts/AIKOSim/All_Contacts_ENV.csv
GS_filename: /home/pietro/Documenti/SpOCK/output/GS_Contacts/AIKOSim/All_GScontacts_ENV.csv
Eclipse_filename: /home/pietro/Documenti/SpOCK/output/Eclipses/AIKOSim/AISAC/AllEclipse_times.csv

