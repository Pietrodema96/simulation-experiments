
from textwrap import indent
import xml.etree.ElementTree as ET
import os, sys, shutil, subprocess
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt

path_ = os.path.abspath('Orbit_data_analysis/utilities/')
print(path_)
sys.path.append('{}'.format(path_))

import numpy as np
import pandas as pd
import AstroConstants as AC
import CarKep as CK
import GS_custom as GC




def parsing_simparam(path_file_SIM, leap_second, planet_ephemeris, EOP_params, pck, gravity_model, atmo_model, magn_field, path_folder,\
                        RR, VV, realtime, realtime_wait, ggrad_on, mag_on, srp_on, drag_on, nMAX, sunmoon_on, Drag_Model,\
                            SRP_Model, AttitudeType, step_STR, duration_STR, dymh, ephem_out_file, ephem_in, att_in, Data_path, \
                                att_out, sens_out, torq_out, acc_out, sunmoon_model, TLE, SRP_area, Drag_area, Cr, Cd, d_mass):
    """
    Parsing the simulation parameters XML file
    of SpOCK
    """
    # Checking the root:
    tree1 = ET.parse(path_file_SIM)
    root1 = tree1.getroot()

    # 0. Input/output Files:
    # Planet Ephemeris
    ss0 = root1.find("./InputFiles/Leap_second")
    ss0.text = ss0.text.replace('{}'.format(ss0.text), leap_second)
    # Planet Ephemeris
    ss1 = root1.find("./InputFiles/Planet_ephemeris")
    ss1.text = ss1.text.replace('{}'.format(ss1.text), planet_ephemeris)
    # EOP
    ss2 = root1.find("./InputFiles/EOP_parameters")
    ss2.text = ss2.text.replace('{}'.format(ss2.text), EOP_params)
    # PCK
    ss3 = root1.find("./InputFiles/PCK_data")
    ss3.text = ss3.text.replace('{}'.format(ss3.text), pck)
    # gravity model
    ss4 = root1.find("./InputFiles/Gravity_model")
    ss4.text = ss4.text.replace('{}'.format(ss4.text), gravity_model)
    # atmo model
    ss5 = root1.find("./InputFiles/Atmospheric_model")
    ss5.text = ss5.text.replace('{}'.format(ss5.text), atmo_model)
    # magnetic field
    ss6 = root1.find("./InputFiles/Magnetic_model")
    ss6.text = ss6.text.replace('{}'.format(ss6.text), magn_field)
    # Data path for 
    # ss8 = root.find("./OutputFiles/Orbit_ephemeris")
    # ss8.text = ss8.text.replace('{}'.format(ss8.text), path_folder)

    # INPUT FILES:
    ss12 = root1.find("./InputFiles/Orbit_ephemeris")
    ss12.text = ss12.text.replace('{}'.format(ss12.text), ephem_in)
    ss13 = root1.find("./InputFiles/Attitude_ephemeris")
    ss13.text = ss13.text.replace('{}'.format(ss13.text), att_in)
    ss14 = root1.find("./InputFiles/Data_path")
    ss14.text = ss14.text.replace('{}'.format(ss14.text), Data_path)
    ss15 = root1.find("./InputFiles/SunMoon_model")
    ss15.text = ss15.text.replace('{}'.format(ss15.text), sunmoon_model)
    ss16 = root1.find("./InputFiles/TLE")
    ss16.text = ss16.text.replace('{}'.format(ss16.text), TLE)

    # OUTPUT FILES:
    ss7 = root1.find("./OutputFiles/Orbit_ephemeris")
    ss7.text = ss7.text.replace('{}'.format(ss7.text), ephem_out_file)
    ss8 = root1.find("./OutputFiles/Attitude_ephemeris")
    ss8.text = ss8.text.replace('{}'.format(ss8.text), att_out)
    ss9 = root1.find("./OutputFiles/Sensor_output")
    ss9.text = ss9.text.replace('{}'.format(ss9.text), sens_out)
    ss10 = root1.find("./OutputFiles/Torques")
    ss10.text = ss10.text.replace('{}'.format(ss10.text), torq_out)
    ss11 = root1.find("./OutputFiles/Accelerations")
    ss11.text = ss11.text.replace('{}'.format(ss11.text), acc_out)


    # 1. Propagation:
    # Time Step
    mm = root1.find("./SimParameters/durstep/simstep")
    mm.text = mm.text.replace('{}'.format(mm.text), step_STR)
    # Duration
    mm = root1.find("./SimParameters/durstep/simduration")
    mm.text = mm.text.replace('{}'.format(mm.text), duration_STR)
    # Initial date
    mm = root1.find("./SimParameters/ORB_initstate/Initime")
    dymh = 'T'.join(dymh.split(" "))
    mm.text = mm.text.replace('{}'.format(mm.text), dymh)
    # X ECI - NEEDED IN M
    mm1X = root1.find("./SimParameters/ORB_initstate/Position/x")
    mm1X.text = mm1X.text.replace('{}'.format(mm1X.text), str(RR[0]*1000))
    # Y ECI - NEEDED IN M
    mm1Y = root1.find("./SimParameters/ORB_initstate/Position/y")
    mm1Y.text = mm1Y.text.replace('{}'.format(mm1Y.text), str(RR[1]*1000))
    # Z ECI - NEEDED IN M
    mm1Z = root1.find("./SimParameters/ORB_initstate/Position/z")
    mm1Z.text = mm1Z.text.replace('{}'.format(mm1Z.text), str(RR[2]*1000))
    # VX ECI
    mm1VX = root1.find("./SimParameters/ORB_initstate/Velocity/vx")
    mm1VX.text = mm1VX.text.replace('{}'.format(mm1VX.text), str(VV[0]*1000))
    # VY ECI
    mm1VY = root1.find("./SimParameters/ORB_initstate/Velocity/vy")
    mm1VY.text = mm1VY.text.replace('{}'.format(mm1VY.text), str(VV[1]*1000))
    # VZ ECI
    mm1VZ = root1.find("./SimParameters/ORB_initstate/Velocity/vz")
    mm1VZ.text = mm1VZ.text.replace('{}'.format(mm1VZ.text), str(VV[2]*1000))

    mm2 = root1.find("./SimParameters/simoptions/realtime")
    mm2.text = mm2.text.replace('{}'.format(mm2.text), realtime)

    mm3 = root1.find("./SimParameters/simoptions/realtime_wait")
    mm3.text = mm3.text.replace('{}'.format(mm3.text), str(realtime_wait))

    mm4 = root1.find("./SimParameters/simoptions/ggrad_on")
    mm4.text = mm4.text.replace('{}'.format(mm4.text), ggrad_on)

    mm5 = root1.find("./SimParameters/simoptions/mag_on")
    mm5.text = mm5.text.replace('{}'.format(mm5.text), mag_on)

    mm6 = root1.find("./SimParameters/simoptions/srp_on")
    mm6.text = mm6.text.replace('{}'.format(mm6.text), srp_on)

    mm7 = root1.find("./SimParameters/simoptions/drag_on")
    mm7.text = mm7.text.replace('{}'.format(mm7.text), drag_on)

    mm8 = root1.find("./SimParameters/simoptions/nMAX")
    mm8.text = mm8.text.replace('{}'.format(mm8.text), str(nMAX))

    mm9 = root1.find("./SimParameters/simoptions/sunmoon_on")
    mm9.text = mm9.text.replace('{}'.format(mm9.text), sunmoon_on)

    mm10 = root1.find("./SimParameters/simoptions/Drag_Model")
    mm10.text = mm10.text.replace('{}'.format(mm10.text), Drag_Model)

    mm11 = root1.find("./SimParameters/simoptions/SRP_Model")
    mm11.text = mm11.text.replace('{}'.format(mm11.text), SRP_Model)

    mm12 = root1.find("./SimParameters/simoptions/AttitudeType")
    mm12.text = mm12.text.replace('{}'.format(mm12.text), AttitudeType)

    mm17 = root1.find("./SC_properties/Coefficients/Cd")
    mm17.text = mm17.text.replace('{}'.format(mm17.text), Cd)

    mm18 = root1.find("./SC_properties/Coefficients/Cr")
    mm18.text = mm18.text.replace('{}'.format(mm18.text), Cr)

    mm19 = root1.find("./SC_properties/Areas/Area_D")
    mm19.text = mm19.text.replace('{}'.format(mm19.text), Drag_area)

    mm20 = root1.find("./SC_properties/Areas/Area_R")
    mm20.text = mm20.text.replace('{}'.format(mm20.text), SRP_area)

    mm21 = root1.find("./SC_properties/CoG/SC_mass")
    mm21.text = mm21.text.replace('{}'.format(mm21.text), d_mass)  
    
    # Write on the xml the modified values:
    tree1.write('./automatic_Prop/XML_f/simparam.xml')

    return



def parsing_eventsparam(path_file_EV, FOV_cross, FOV_along, SC_start, SC_end, PL_start, PL_end, TGs_on, GSs_on, \
                        TGs_grid_on, Eclipse_on, TGS, GSs, tg_names, gs_names, step_STR, duration_STR, TG_out, GS_out, Eclipse_out,\
                             Data_path, ephem_in_EV, ephem_rtn_EV, TGs_passes, GSs_contacts, leap_second, planet_ephemeris, EOP_params, pck, num_t, num_g):
    """
    Parsing the events parameters XML file
    of SpOCK

    INPUTS:
    TGS:   TG_ID || TG_lat || TG_lon || TG_alt
    GS:    GS_ID || GS_lat || GS_lon || GS_alt || GS_minel

    """
    # Checking the root:
    tree1 = ET.parse(path_file_EV)
    root1 = tree1.getroot()

    # 0. Events file parsing:
    # Duration and step:
    ss1 = root1.find("./CompParameters/durstep/simstep")
    ss1.text = ss1.text.replace('{}'.format(ss1.text), step_STR)
    ss2 = root1.find("./CompParameters/durstep/simduration")
    ss2.text = ss2.text.replace('{}'.format(ss2.text), duration_STR)

    # 1. Payload FOV:
    ss2 = root1.find("./CompParameters/Payload/FOV_cross")
    ss2.text = ss2.text.replace('{}'.format(ss2.text), str(FOV_cross))   
    ss3 = root1.find("./CompParameters/Payload/FOV_along")
    ss3.text = ss3.text.replace('{}'.format(ss3.text), str(FOV_along))

    # 2. Spacecraft and analysis parameters:
    ss4 = root1.find("./CompParameters/Spacecraft/SC_start")
    ss4.text = ss4.text.replace('{}'.format(ss4.text), str(SC_start))   
    ss5 = root1.find("./CompParameters/Spacecraft/SC_end")
    ss5.text = ss5.text.replace('{}'.format(ss5.text), str(SC_end))
    ss6 = root1.find("./CompParameters/Spacecraft/PL_start")
    ss6.text = ss6.text.replace('{}'.format(ss6.text), str(PL_start))
    ss7 = root1.find("./CompParameters/Spacecraft/PL_end")
    ss7.text = ss7.text.replace('{}'.format(ss7.text), str(PL_end))

    # 3. TG & GS:
    ss8 = root1.find("./CompParameters/Compoptions/TGs_on")
    ss8.text = ss8.text.replace('{}'.format(ss8.text), TGs_on)   
    ss9 = root1.find("./CompParameters/Compoptions/GSs_on")
    ss9.text = ss9.text.replace('{}'.format(ss9.text), GSs_on)    
    ss10 = root1.find("./CompParameters/Compoptions/TGs_grid_on")
    ss10.text = ss10.text.replace('{}'.format(ss10.text), TGs_grid_on)   
    ss11 = root1.find("./CompParameters/Compoptions/Eclipse_on")
    ss11.text = ss11.text.replace('{}'.format(ss11.text), Eclipse_on)  

    rr1 = root1.find("GSs")
    # print(ss8)


    # Adding GS:
    if GSs_contacts == 'true':
        for i in range(0, len(GSs[:, 0])): # running over all the GS

            gs = ET.Element("GS")
            gs.tail = "\n\t"            
            rr1.insert(3, gs)
            if num_g == '1':
                gs.set('name', '{}'.format(gs_names))
            else:
                gs.set('name', '{}'.format(gs_names[i]))
            b1 = ET.SubElement(gs, "lon")
            b1.set('unit', 'deg')
            b1.text = "{}".format(GSs[i, 0])
            b1.tail = "\n\t" 
            b2 = ET.SubElement(gs, "lat")
            b2.set('unit', 'deg')
            b2.text = "{}".format(GSs[i, 1])
            b2.tail = "\n\t"            
            b3 = ET.SubElement(gs, "alt")
            b3.set('unit', 'm')
            b3.text = "{}".format(GSs[i, 2])
            b3.tail = "\n\t" 
            b4 = ET.SubElement(gs, "minelev")
            b4.text = "{}".format(GSs[i, 3])
            b4.set('unit', 'deg')
            b4.tail = "\n\t" 
    else:
        gs = ET.Element("GS")
        gs.tail = "\n\t"            
        rr1.insert(3, gs)
        gs.set('name', '{}'.format('FAKE'))
        b1 = ET.SubElement(gs, "lon")
        b1.set('unit', 'deg')
        b1.text = "{}".format('0.0')
        b1.tail = "\n\t" 
        b2 = ET.SubElement(gs, "lat")
        b2.set('unit', 'deg')
        b2.text = "{}".format('0.0')
        b2.tail = "\n\t"            
        b3 = ET.SubElement(gs, "alt")
        b3.set('unit', 'm')
        b3.text = "{}".format('0.0')
        b3.tail = "\n\t" 
        b4 = ET.SubElement(gs, "minelev")
        b4.text = "{}".format('0.0')
        b4.set('unit', 'deg')
        b4.tail = "\n\t" 
    

    # Adding TG:
    rr2 = root1.find("TGs/TGs_list")
    if TGs_passes == 'true':
        for i in range(0, len(TGS[:, 0])): # running over all the GS
            # print('1')
            tg = ET.Element("TG")
            tg.tail = "\n\t"            
            rr2.insert(2, tg)
            if num_t == '1':
                tg.set('name', '{}'.format(tg_names))
            else:
                tg.set('name', '{}'.format(tg_names[i]))
            c1 = ET.SubElement(tg, "lon")
            c1.set('unit', 'deg')
            c1.text = "{}".format(TGS[i, 0])
            c1.tail = "\n\t" 
            c2 = ET.SubElement(tg, "lat")
            c2.set('unit', 'deg')
            c2.text = "{}".format(TGS[i, 1])
            c2.tail = "\n\t"            
            c3 = ET.SubElement(tg, "alt")
            c3.set('unit', 'm')
            c3.text = "{}".format(TGS[i, 2])
            c3.tail = "\n\t" 
    else:
        tg = ET.Element("TG")
        tg.tail = "\n\t"            
        rr2.insert(2, tg)
        tg.set('name', '{}'.format('FAKE'))
        c1 = ET.SubElement(tg, "lon")
        c1.set('unit', 'deg')
        c1.text = "{}".format('0.0')
        c1.tail = "\n\t" 
        c2 = ET.SubElement(tg, "lat")
        c2.set('unit', 'deg')
        c2.text = "{}".format('0.0')
        c2.tail = "\n\t"            
        c3 = ET.SubElement(tg, "alt")
        c3.set('unit', 'm')
        c3.text = "{}".format('0.0')
        c3.tail = "\n\t" 

    # OUTPUT EPHEMERIS FILE:
    # Input:
    ss15 = root1.find("./EventsInputFiles/Data_path")
    ss15.text = ss15.text.replace('{}'.format(ss15.text), Data_path)
    ss16 = root1.find("./EventsInputFiles/Orbit_ephemeris_path")
    ss16.text = ss16.text.replace('{}'.format(ss16.text), ephem_in_EV)
    ss17 = root1.find("./EventsInputFiles/Orbit_ephemeris_rootname")
    ss17.text = ss17.text.replace('{}'.format(ss17.text), ephem_rtn_EV)  

    # Output:
    ss12 = root1.find("./EventsOutputFiles/TG_contacts")
    ss12.text = ss12.text.replace('{}'.format(ss12.text), TG_out)
    ss13 = root1.find("./EventsOutputFiles/GS_contacts")
    ss13.text = ss13.text.replace('{}'.format(ss13.text), GS_out)
    ss14 = root1.find("./EventsOutputFiles/Eclipse_times")
    ss14.text = ss14.text.replace('{}'.format(ss14.text), Eclipse_out)   
    
    # SPICE files:
    ss15 = root1.find("./EventsInputFiles/Leap_second")
    ss15.text = ss15.text.replace('{}'.format(ss15.text), leap_second)
    # Planet Ephemeris
    ss16 = root1.find("./EventsInputFiles/Planet_ephemeris")
    ss16.text = ss16.text.replace('{}'.format(ss16.text), planet_ephemeris)
    # EOP
    ss17 = root1.find("./EventsInputFiles/EOP_parameters")
    ss17.text = ss17.text.replace('{}'.format(ss17.text), EOP_params)
    # PCK
    ss18 = root1.find("./EventsInputFiles/PCK_data")
    ss18.text = ss18.text.replace('{}'.format(ss18.text), pck)


    tree1 = ET.ElementTree(root1)
    # Write on the xml the modified values:
    tree1.write(path_file_EV, encoding="utf-8", xml_declaration=True)
    
    return




def plot_GSTG(mission, output_path, GSs_contacts, TGs_passes):
    """
    Plot of GSs and TGs into a Basemap Background
    for visualization
    
    INPUT:
        mission = mission name [str]
        output_path = path to the output folder [str]
        GSs_contacts = str defining if GS contacts are computed by simulator [true/false]
        TGs_passes= str defining if TG contacts are computed by simulator [true/false]
    
    """
    # Checking if TG and GS contacts are computed:
    if TGs_passes == 'true':
        TGS = pd.read_csv("./Orbit_data_analysis/DATA/{}/Output/TG_list.csv".format(mission)) 
    if GSs_contacts == 'true':
        GSS = pd.read_csv("./Orbit_data_analysis/DATA/{}/Output/GS_list.csv".format(mission)) 


    fig = plt.figure(figsize=(15,10))

    # First Basemap for TG:
    ax1 = plt.subplot(211)
    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')
    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 20), linewidth=0.4, labels=[1,0,0,1], color='grey')
    m.drawmeridians(np.arange(-180, 180, 30), linewidth=0.4, labels=[1,0,0,1], color='grey')

    if TGs_passes == 'true':
        for kk in range(0, len(TGS["TG"][:])):
            x,y = m(TGS["lon"][kk], TGS["lat"][kk])
            m.scatter(x, y, marker='*',color='red', s=17)
            c = plt.Circle((x,y), 5, color='red', fill=False)
            ax1.add_patch(c)
    ax1.set_title("Targets distribution")


    # Second Basemap for GS:
    ax2 = plt.subplot(212)

    m = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
                llcrnrlon=-180,urcrnrlon=180,resolution='c')

    m.drawmapboundary(fill_color='w')
    m.fillcontinents(color='w')
    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 20), linewidth=0.4, labels=[1,0,0,1], color='grey')
    m.drawmeridians(np.arange(-180, 180, 30), linewidth=0.4, labels=[1,0,0,1], color='grey')

    if GSs_contacts == 'true':
        for kk in range(0, len(GSS["GS"][:])):
            x,y = m(GSS["lon"][kk], GSS["lat"][kk])
            m.scatter(x, y, marker='*',color='b', s=17)
            c = plt.Circle((x,y), 5, color='b', fill = False)
            ax2.add_patch(c)
        ax2.set_title("Ground Stations distribution")

    plt.suptitle('TGs and GSs, mission:{}'.format(mission))
    # ax2.legend(bbox_to_anchor=(1.04,1), loc="upper left")

    fig.savefig(os.path.join(output_path, "TG_GS.png"), bbox_inches='tight')


    return




def scenarios(name, year):
    """
    Allows to select the proper scenario for
    the propagation with SpOCK. It allows to
    slect between different combinations of parameters.
    
    INPUT:
        name = name of the simulation type
        year = year of the simulation [str]
    
    EOP files available:
    earth_720101_070426.bpc
    earth_000101_200719_200427.bpc
    earth_000101_220328_220103.bpc
    earth_000101_220814_220522.bpc
    earth_000101_220831_220607.bpc

    Gravity models:
    GGM02C, GGM03C, GGM03S, EIGEN-6S
    GGM03C 30x30 Hardcoded

    Atmo Models:
    JB2008, NRLMSISE-00, Harris-Priester

    Magnetic Field
    IGRF13, WMM2020

    """
    print(year)
    if year < 2000:
        EOP_params = 'earth_720101_070426.bpc'
    elif year >= 2000 and year < 2007:
        EOP_params = 'earth_000101_200719_200427.bpc'
    elif year >=2007:
        EOP_params = 'earth_000101_220814_220522.bpc'

    realtime = 'false'
    realtime_wait = 1.0
    ggrad_on = 'false'
    planet_ephemeris = 'de440.bsp'
    pck = 'pck00010.tpc'
    gravity_model = 'EIGEN-6S'
    atmo_model = 'NRLMSISE-00'
    magn_field = 'IGRF13'
    leap_second = 'naif0012.tls'
    sunmoon_model = 'DE' # JPL ephemeris

    if name == 'simple_constellation':
        mag_on = 'false'
        srp_on = 'false'
        drag_on = 'false'
        nMAX = 70
        sunmoon_on = 'false'
        Drag_Model = 'RefArea'
        SRP_Model = 'RefArea'
        AttitudeType = 'RTN_fixed'


    elif name == 'basic_propagation':
        mag_on = 'false'
        srp_on = 'true'
        drag_on = 'true'
        nMAX = 10
        sunmoon_on = 'false'
        Drag_Model = 'RefArea'
        SRP_Model = 'RefArea'
        AttitudeType = 'RTN_fixed'

    elif name == 'J2':
        mag_on = 'false'
        srp_on = 'false'
        drag_on = 'false'
        nMAX = 2
        sunmoon_on = 'false'
        Drag_Model = 'RefArea'
        SRP_Model = 'RefArea'
        AttitudeType = 'RTN_fixed'      

    elif name == 'J2_drag':
        mag_on = 'false'
        srp_on = 'false'
        drag_on = 'true'
        nMAX = 2
        sunmoon_on = 'false'
        Drag_Model = 'RefArea'
        SRP_Model = 'RefArea'
        AttitudeType = 'RTN_fixed' 

    elif name == 'J70_drag':
        mag_on = 'false'
        srp_on = 'false'
        drag_on = 'true'
        nMAX = 70
        sunmoon_on = 'false'
        Drag_Model = 'RefArea'
        SRP_Model = 'RefArea'
        AttitudeType = 'RTN_fixed'

    elif name == 'full_models':
        mag_on = 'false'
        srp_on = 'true'
        drag_on = 'true'
        nMAX = 70
        sunmoon_on = 'true'
        Drag_Model = 'RefArea'
        SRP_Model = 'RefArea'
        AttitudeType = 'RTN_fixed'


    return realtime, realtime_wait, ggrad_on, mag_on, srp_on, drag_on, nMAX, sunmoon_on, Drag_Model, \
        SRP_Model, AttitudeType, planet_ephemeris, EOP_params, pck, gravity_model, atmo_model, magn_field, leap_second, sunmoon_model




def prepare_folder(work_folder, orig_folder):
    """
    Rearrange working folders for the XML files
    
    INPUT:
    work_folder = is the folder which is completely restored at each iteration
    orig_folder = where original 'simparam.xml' file and 'eventsparam.xml' file
            are copied from
    
    """
    
    # Deleting files in the working folder:
    print('Deleting working-directory files: ...')
    ind = 0
    for filename in os.listdir(work_folder):
        file_path = os.path.join(work_folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
            ind += 1
            print('{} deleted'.format(ind))
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))
    
    # Preparing the working folder for the new simulation:
    print('\n\nCopying in working-directory original files: ...')
    for file_name in os.listdir(orig_folder):
        # construct full file path
        # source = orig_folder + file_name
        # destination = work_folder + file_name
        source = os.path.join(orig_folder, file_name)
        destination = os.path.join(work_folder, file_name)

        # copy only files
        if os.path.isfile(source):
            shutil.copy(source, destination)
            print('copied', file_name)

    print('\n\n')
    return






def print_scenario(OE_init, step, duration, dymh, srp_on, drag_on, sunmoon_on, FOV_cross, FOV_along, TGs_on, GSs_on, TGs_grid_on, \
                Eclipse_on, TGS, tg_names, GSs, gs_names, num_t, num_g, mission):

    print('\n-----------------------------')
    print('-- SpOCK simulation inputs --')
    print('-----------------------------\n')
    print('#### MISSION NAME: {}'.format(mission))
    print('Orbital parameters:')
    print('[SMA={}[km] , ECC={}[-] , INC={}[deg] , RAAN={}[deg] , AOP={}[deg] , TA={}[deg]] \n'.format(OE_init[0], OE_init[1], \
                            OE_init[2], OE_init[3], OE_init[4], OE_init[5]))
    print('Simulation parameters:')
    print('Start date: {}'.format(dymh))
    print('step={}sec , duration={}days \n'.format(step, duration))
    print('SRP={} , Drag={} , S&M={} \n'.format(srp_on, drag_on, sunmoon_on))
    print('FOV_cross={}[deg] , FOV_along={}[deg] \n'.format(FOV_cross, FOV_along))
    print('TGs={} , GSs={} \n'.format(TGs_on, GSs_on))
    print('TGs_grid={} , Eclipse={} \n'.format(TGs_grid_on, Eclipse_on))

    # print(gs_names)

    if TGs_on == 'true':
        print('\nTGs considered: ')
        if num_t == '1':
            print('TGs_{}: lon={}[deg], lat={}[deg], alt={}[m] '.format(tg_names, TGS[0,0], TGS[0,1], TGS[0,2]))
        for j in range(0, int(num_t)):
            print('TGs_{}: lon={}[deg], lat={}[deg], alt={}[m] '.format(tg_names[j], TGS[j,0], TGS[j,1], TGS[j,2]))
    if GSs_on == 'true': 
        print('\nGSs considered: ') 
        # print(num)
        if num_g == '1':
            print('GSs_{}:  lon={}[deg], lat={}[deg], alt={}[m], min_el={}[deg] '.format(gs_names, GSs[0,0], GSs[0,1], GSs[0,2],GSs[0,3]))
        else:
            for k in range(0, int(num_g)):  
                print('GSs_{}:  lon={}[deg], lat={}[deg], alt={}[m], min_el={}[deg] '.format(gs_names[k], GSs[k,0], GSs[k,1] ,GSs[k,2],GSs[k,3]))

    print('\n\n')

    return







def input_collection(home_path, OP, step, duration, date, mission_name, propagation_type, FOV, eclipses, TGs_grid, TGs_random, \
        TGs_number, GSs_random, GSs_number, TGs_custom, tg_names, TGs_longitude, TGs_latitude, TGs_altitude, \
            GSs_custom, gs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, SS, PP, TGs_passes, GSs_contacts, \
                GSs_c_num, TGs_c_num , SRP_area, Drag_area, Cr, Cd, d_mass):
    """
    Collects all the inputs from input .txt file and constant data
    and pass informations to the parsing functions
    """
    

    # Deleting files in the output folder:
    print('Cleaning output folder files: ...')
    ind = 0
    for filename in os.listdir('{}/Orbit_data_analysis/DATA/{}/Output'.format(home_path, mission_name)):
        file_path = os.path.join('{}/Orbit_data_analysis/DATA/{}/Output'.format(home_path, mission_name), filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
            ind += 1
            print('{} deleted'.format(ind))
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

    # We need to recreate the folder Ephemeris in Output/:
    directory = "Ephemeris"
    parent_dir = os.path.abspath("{}/Orbit_data_analysis/DATA/{}/Output/".format(home_path, mission_name))
    path = os.path.join(parent_dir, directory)
    os.mkdir(path)
    directory = "MA_plots"
    parent_dir = os.path.abspath("{}/Orbit_data_analysis/DATA/{}/Output/".format(home_path, mission_name))
    path = os.path.join(parent_dir, directory)
    os.mkdir(path)
    print('\n\n')


    # INPUTS:
    OE_init = [OP[0], OP[1], OP[2]*AC.D2R, OP[3]*AC.D2R, OP[4]*AC.D2R, OP[5]*AC.D2R]
    dymh = date
    ephem_file_name = 'S{}_P{}'.format(int(SS), int(PP))
    # Definiton of MA output files
    GS_contacts_file_name = 'All_GS_contacts_{}'.format(mission_name)
    TG_contacts_file_name = 'All_TG_contacts_{}'.format(mission_name)
    eclipses_file_name = 'AllEclipses_times_{}'.format(mission_name)

    name = propagation_type
    # Possibilities:
    #     1. simple_scenario
    #     2. all_perturbations
    #     3. DJ2: drag J2
    #     5. basic_propagation: J70, drag, SRP
    
    # Set the scenario model files:
    year = int(((dymh.split(" ")[0]).split("-"))[0]) # year as an int in order to set SPK file
    realtime, realtime_wait, ggrad_on, mag_on, srp_on, drag_on, nMAX, sunmoon_on, Drag_Model, \
        SRP_Model, AttitudeType, planet_ephemeris, EOP_params, pck, gravity_model, atmo_model, magn_field, leap_second, sunmoon_model = scenarios(name, year)

    # Events computations data:
    FOV_cross = FOV # [deg]
    FOV_along = FOV # [deg]
    SC_start = 1 # [deg]
    SC_end = 1 # [deg]
    PL_start = 1 # [deg]
    PL_end = 1 # [deg]
    TGs_on = TGs_passes
    GSs_on = GSs_contacts
    TGs_grid_on = TGs_grid
    Eclipse_on = eclipses
    
    # Generation of GS and TG list:
    # TG
    if TGs_passes == 'true':
        if TGs_random == 'true':
            num_t = TGs_number
            TGS, tg_names = GC.sorting_custom_GS(2, num_t, [np.NaN], 'TG', mission_name)
        elif TGs_custom == 'true':
            num_t = TGs_c_num
            TGS = GC.input_TG(tg_names, TGs_longitude, TGs_latitude, TGs_altitude, num_t, mission_name)
    else:
        TGS = 0
        tg_names = 0
        num_t = 0

    # GS
    if GSs_contacts == 'true':
        if GSs_random == 'true':
            num_g = GSs_number
            GSs, gs_names = GC.sorting_custom_GS(2, num_g, [np.NaN], 'GS', mission_name)
        elif GSs_custom == 'true':
            num_g = GSs_c_num
            GSs = GC.input_GS(gs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, num_g, mission_name)
    else:
        GSs = 0
        gs_names = 0
        num_g = 0

    # Plotting the TG and GS distribution:
    if GSs_contacts == 'true' or TGs_passes == 'true':
        plot_GSTG(mission_name, os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/'.format(mission_name)), GSs_contacts, TGs_passes)


    ephem_in = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input'.format(home_path, mission_name))
    ephem_in_EV = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output/Ephemeris/'.format(home_path, mission_name))
    ephem_rtn_EV = '{}.csv'.format(ephem_file_name)
    TLE = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input'.format(home_path, mission_name))
    att_in = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input'.format(home_path, mission_name))
    Data_path = '/SpOCK-master/data'
    ephem_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output/Ephemeris/{}.csv'.format(home_path, mission_name, ephem_file_name))
    TG_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output/{}.csv'.format(home_path, mission_name, TG_contacts_file_name))
    GS_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output/{}.csv'.format(home_path, mission_name, GS_contacts_file_name))
    Eclipse_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output/{}.csv'.format(home_path, mission_name, eclipses_file_name))
    att_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output'.format(home_path, mission_name))
    sens_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output'.format(home_path, mission_name))
    torq_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output'.format(home_path, mission_name))
    acc_out = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Output'.format(home_path, mission_name))

    # Manipulation for parsing:
    RR = [0, 0, 0]
    VV = [0, 0, 0]
    RR[:], VV[:] = CK.elem2rv(AC.MU_E, OE_init) # [km km/s]
    step_STR = 'PT{}S'.format(int(step))
    duration_STR = 'P{}D'.format(int(duration))
    duration_STR_EV = 'P{}D'.format(int(duration)) # !!! because of an error in eventsparam !!!

    # SPOCK SIMULATION PRINT
    OE_pr = [OP[0], OP[1], OP[2], OP[3], OP[4], OP[5]]
    print_scenario(OE_pr, step, duration, dymh, srp_on, drag_on, sunmoon_on, FOV_cross, FOV_along, TGs_on, GSs_on, TGs_grid_on, \
                Eclipse_on, TGS, tg_names, GSs, gs_names, num_t, num_g, mission_name)

    # -- Recovering path to 'eventsparam.xml' file:
    path_folder_orig = os.path.abspath('{}/automatic_Prop/XML_or/'.format(home_path))
    path_folder_f = os.path.abspath('{}/automatic_Prop/XML_f/'.format(home_path))

    path_file_SIM = os.path.abspath('{}/automatic_Prop/XML_f/simparam.xml'.format(home_path))
    path_folder = os.path.dirname(path_file_SIM)
    path_file_EV = os.path.abspath('{}/automatic_Prop/XML_f/eventsparam.xml'.format(home_path))

    # Preparing folders for inputs:
    prepare_folder(path_folder_f, path_folder_orig)


    # PARSING THE SIMPARAM FILE:
    print('\nParsing _simparam file...')
    parsing_simparam(path_file_SIM, leap_second, planet_ephemeris, EOP_params, pck, gravity_model, atmo_model, magn_field, path_folder,\
                        RR, VV, realtime, realtime_wait, ggrad_on, mag_on, srp_on, drag_on, nMAX, sunmoon_on, Drag_Model,\
                            SRP_Model, AttitudeType, step_STR, duration_STR, dymh, ephem_out, ephem_in, att_in, Data_path, \
                                att_out, sens_out, torq_out, acc_out, sunmoon_model, TLE, SRP_area, Drag_area, Cr, Cd, d_mass)
    print('Completed parsing of _simparam file...\n\n')
    print('Parsing of _eventsparam file...')
    # PARSING THE EVENTSPARAM FILE
    parsing_eventsparam(path_file_EV, FOV_cross, FOV_along, SC_start, SC_end, PL_start, PL_end, TGs_on, GSs_on, \
                        TGs_grid_on, Eclipse_on, TGS, GSs, tg_names, gs_names, step_STR, duration_STR_EV, TG_out, GS_out, Eclipse_out, \
                            Data_path, ephem_in_EV, ephem_rtn_EV, TGs_passes, GSs_contacts, leap_second, planet_ephemeris, EOP_params, pck, num_t, num_g)
    print('Completed parsing of _eventsparam file...\n\n')



    return


if __name__ == '__main__':
    # possible tests abou the function
    mission = 'SatRev'
    # plot_GSTG(mission, os.path.abspath('./Orbit_data_analysis/DATA/{}/Output/'.format(mission)))