# CUSTOMIZATION OF GS/TG STARTING FROM ALL CAPITALS
# This function allows 2 modes of sorting for the whole list of world 
# states capitles. It can be used as for targets or ground station analysis.
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 01/06/2022
#
#---------------------------------------------------

from curses.ascii import alt
import os, random, csv
from tkinter import ROUND
import numpy as np
import pandas as pd



def sorting_custom_GS(case, numb, names, id, mission):
    """
    This function sorts from the file 'country_capitals.csv'
    the GS/TG desired between all the capitles stored (from all around the world)

    INPUT:
        case = allows to select between the two cases of sorting*
        numb = amount of capitles selected
        names = names of capitle to select (case1)
    
    OUTPUT:
        GS =   matrix of (capitlesx4)
             lon [deg] || lat [deg] || alt [m] || min_el [deg]

        cap_names = name of capitles/cities (1xn_cities)

    * case1 = precise selection of some capitles deciding the name
      case2 = random choice of capitals given the amount desired

    """
    path_file_SIM=os.path.abspath(__file__)
    path_folder=os.path.dirname(path_file_SIM)

    GS = pd.read_csv("{}/capitles/country_capitals.csv".format(path_folder)) # reading the GS csv file ENV
    capt_names = np.empty(len(GS['CapitalName'][:]), dtype=object)
    capt_names[:] = GS['CapitalName'][:]
    # print(type(capt_names))
    Cap_names = np.empty(numb, dtype=object)

    if case == 1: # 1 is custom choice
        GSS = np.zeros((len(names), 4)) # initialization of the matrix for output

        for i in range(0, len(names)):
            if names[i] in capt_names[:]:
                ind = np.where(capt_names == names[i])
                # print(ind)
                Cap_names[i] = names[i]
                GSS[i, 0] = round(GS['CapitalLongitude'][ind[0]],3)
                GSS[i, 1] = round(GS['CapitalLatitude'][ind[0]],3)
                GSS[i, 2] = 100. # [m]
                GSS[i, 3] = 2. # [deg] minimum elevation

    if case == 2: # 1 is custom choice

        GSS = np.zeros((numb, 4)) # initialization of the matrix for output
        numbs = random.sample(range(0, len(capt_names)), numb) # finding random indexes for the list

        for i in range(0, len(numbs)):
            ind = numbs[i] # indexes
            Cap_names[i] = capt_names[ind]
            GSS[i, 0] = round(GS['CapitalLongitude'][ind],3)
            GSS[i, 1] = round(GS['CapitalLatitude'][ind],3)
            GSS[i, 2] = 100. # [m]
            GSS[i, 3] = 2. # [deg]

    TGGS_csv('{}_list'.format(id), GSS, Cap_names, id, mission, numb)

    return GSS, Cap_names


def input_GS(GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, numb, mission_name):
    """
    This function just allows to retrieve from inputs the GSs characteristics
    
    INPUT:
        GSs_names (1xGSs)
        GSs_longitude (1xGSs)
        GSs_latitude (1xGSs)
        GSs_altitude (1xGSs)
        GSs_minelevation (1xGSs)

    OUTPUT:
        GS =   matrix of (capitlesx4)
             lon [deg] || lat [deg] || alt [m] || min_el [deg]

        cap_names = name of capitles/cities (1xn_cities)

        """
    # print(len(GSs_names))
    GSS = np.zeros((int(numb), 4)) # initialization of the matrix for output

    if numb == '1':
        GSS[0,0] = float(GSs_longitude)
        GSS[0,1] = float(GSs_latitude)
        GSS[0,2] = float(GSs_altitude)
        GSS[0,3] = float(GSs_minelevation)
    else:
        numb = int(numb)
        if len(GSs_longitude)!=numb or len(GSs_latitude)!=numb or len(GSs_altitude)!=numb or len(GSs_minelevation)!=numb:
            raise ValueError('Values in GSs properties are not enouh for all GSs !')
        for i in range(0, numb):
            # print(i)
            GSS[i,0] = float(GSs_longitude[i])
            GSS[i,1] = float(GSs_latitude[i])
            GSS[i,2] = float(GSs_altitude[i])
            GSS[i,3] = float(GSs_minelevation[i])

    TGGS_csv('GS_list', GSS, GSs_names, 'GS', mission_name, numb)

    return GSS


def input_TG(GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, numb, mission_name):
    """
    This function just allows to retrieve from inputs the TGs characteristics
    
    INPUT:
        TGs_names (1xGSs)
        TGs_longitude (1xGSs)
        TGs_latitude (1xGSs)
        TGs_altitude (1xGSs)
        TGs_minelevation (1xGSs)

    OUTPUT:
        TG =   matrix of (capitlesx4)
             lon [deg] || lat [deg] || alt [m] || min_el [deg]

        cap_names = name of capitles/cities (1xn_cities)

        """

    TGS = np.zeros((int(numb), 3)) # initialization of the matrix for output
    for i in range(0, int(numb)):
        TGS[i,0] = float(GSs_longitude[i])
        TGS[i,1] = float(GSs_latitude[i])
        TGS[i,2] = float(GSs_altitude[i])
    
    TGGS_csv('TG_list', TGS, GSs_names, 'TG', mission_name, numb)

    return TGS


def TGGS_csv(name_file, TGGS, names, id, mission, num):
    """
    Writing file with tabulated GS or TG names
    and data as computed in previous parts

    INPUT:
        name_file = the name of the file to write
        TGGS = GS or TG matrixes ; lon || lat || alt || min_el
        names = GS or TG list of names
        id = TG or GS, in order to ce [str]
        mission = mission name [str]
    """
    if id == 'GS':
        with open('./Orbit_data_analysis/DATA/{}/Output/{}.csv'.format(mission, name_file), 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow([id, 'lon', 'lat', 'alt', 'min_El'])
            if num == '1':
                writer.writerow([names, TGGS[0,0], TGGS[0,1], TGGS[0,2], TGGS[0,3]])
            else:
                for i in range(0, len(TGGS[:,0])):
                    writer.writerow([names[i], TGGS[i,0], TGGS[i,1], TGGS[i,2], TGGS[i,3]])
    if id == 'TG':
        with open('./Orbit_data_analysis/DATA/{}/Output/{}.csv'.format(mission, name_file), 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow([id, 'lon', 'lat', 'alt'])
            if num == '1':
                writer.writerow([names, TGGS[0,0], TGGS[0,1], TGGS[0,2]])
            else:
                for i in range(0, len(TGGS[:,0])):
                    writer.writerow([names[i], TGGS[i,0], TGGS[i,1], TGGS[i,2]])

    return


if __name__ == "__main__":

    case = 2
    numb = 2
    names = ['London', 'Paris']
    sorting_custom_GS(case, numb, names)

