from textwrap import indent
import xml.etree.ElementTree as ET
import os, sys, shutil, subprocess

path_ = os.path.abspath('Orbit_data_analysis/')
# print(path_)
sys.path.append('{}'.format(path_))

import GS_custom as GC
import fill_XML as FXML

import SunCoordinates_spice as TCS
import EarthRot_wrt_ECI_spice as ERS
import header_modif_XML as HMX



# Insert the mission name to switch directly to the correct folder:
# SatRev, Cicero,
mission_name = 'Test'
base_path = os.getcwd()

####################################################################
file = os.path.abspath('{}/Orbit_data_analysis/DATA/{}/Input/input.cfg'.format(base_path, mission_name))

# file = open("{}".format(input_fold), "r")
# content=file.readlines()

OP, time_step, duration, date, mission_name, propagation_type, FOV, eclipses, TGs_grid, TGs_random, \
        TGs_number, GSs_random, GSs_number, TGs_custom, TGs_names, TGs_longitude, TGs_latitude, TGs_altitude, \
            GSs_custom, GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, SS, PP, TGs_passes, GSs_contacts, GSs_c_num, TGs_c_num\
                , SRP_area, Drag_area, Cr, Cd, d_mass = HMX.reading_cfg(file)

FXML.input_collection(base_path, OP, time_step, duration, date, mission_name, propagation_type, FOV, eclipses, TGs_grid, TGs_random, \
        TGs_number, GSs_random, GSs_number, TGs_custom, TGs_names, TGs_longitude, TGs_latitude, TGs_altitude, \
            GSs_custom, GSs_names, GSs_longitude, GSs_latitude, GSs_altitude, GSs_minelevation, SS, PP, TGs_passes, GSs_contacts, GSs_c_num, TGs_c_num\
                , SRP_area, Drag_area, Cr, Cd, d_mass)

# RUNNING SPOCK SIMULATION:
sss = '/workspace/automatic_Prop/XML_f/simparam.xml'
subprocess.run('cd /SpOCK-master; ./bin/OrbitPropagator {}'.format(sss), shell=True)

print('\n\n')


if TGs_passes == 'true' or GSs_contacts == 'true' or eclipses == 'true': # onli if some events are needed
    # RUNNING SPOCK EVENTS ANALYSIS:
    sss = '/workspace/automatic_Prop/XML_f/eventsparam.xml'
    subprocess.run('cd /SpOCK-master; ./bin/EventsComputation {}'.format(sss), shell=True)

# MODIFYING HEADERS OF CSV FILES:
HMX.header(mission_name, TGs_passes, GSs_contacts, eclipses)

# COMPUTING TIMES-FILE:
TCS.time_convs(date, mission_name)

# COMPUTING SUN POSITIONS:
TCS.suncoordinates(mission_name)

# COMPUTING EARTH IAU ORIENTATIONS:
ERS.Earth_rots(mission_name)


print('------!!! MISSION FILES WRITTEN !!!-----')
