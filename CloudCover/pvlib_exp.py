from asyncio.base_tasks import _task_print_stack
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import os, cfgrib, pygrib, math
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.basemap import shiftgrid
import matplotlib as mpl
import matplotlib.cm as cm
from numpy import arange, floor, ceil
from matplotlib.cm import ScalarMappable

# Dataset model: https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=overview
# https://spire.com/tutorial/spire-weather-tutorial-intro-to-processing-grib2-data-with-python/


base_path = os.getcwd()

def clippedcolorbar(CS, **kwargs):
    
    fig = CS.ax.get_figure()
    vmin = CS.get_clim()[0]
    vmax = CS.get_clim()[1]
    m = ScalarMappable(cmap=CS.get_cmap())
    m.set_array(CS.get_array())
    m.set_clim(CS.get_clim())
    step = CS.levels[1] - CS.levels[0]
    cliplower = CS.zmin<vmin
    clipupper = CS.zmax>vmax
    noextend = 'extend' in kwargs.keys() and kwargs['extend']=='neither'
    # set the colorbar boundaries
    boundaries = arange((floor(vmin/step)-1+1*(cliplower and noextend))*step, (ceil(vmax/step)+1-1*(clipupper and noextend))*step, step)
    kwargs['boundaries'] = boundaries
    # if the z-values are outside the colorbar range, add extend marker(s)
    # This behavior can be disabled by providing extend='neither' to the function call
    if not('extend' in kwargs.keys()) or kwargs['extend'] in ['min','max']:
        extend_min = cliplower or ( 'extend' in kwargs.keys() and kwargs['extend']=='min' )
        extend_max = clipupper or ( 'extend' in kwargs.keys() and kwargs['extend']=='max' )
        if extend_min and extend_max:
            kwargs['extend'] = 'both'
        elif extend_min:
            kwargs['extend'] = 'min'
        elif extend_max:
            kwargs['extend'] = 'max'
    return fig.colorbar(m, **kwargs)


def linear_map_Plots(time, ds, name):
    """
    Test plots for the Copernicus data
    """
    
    DD = ds.tcc[time]
    plt.figure(figsize=(15,7))
    ds_area  = ds.isel(latitude=42,longitude=12)
    ds_area.tcc[:].plot()
    path = os.path.abspath('./CloudCover/images_gif/linear/')
    plt.savefig(os.path.join(path, "Rome_linear_{}.png".format(name)), bbox_inches='tight')
    
    # for kk in range(0, 36): #3 days plot
    #     print(kk)
    #     DD = ds.tcc[kk]
    #     plt.figure(figsize=(15,7))
    #     m= Basemap(projection='cyl',lat_0=0,lon_0=0)
    #     m.drawcoastlines()
    #     # meridians = np.arange(10.,351.,20.)
    #     # m.drawmeridians(meridians,labels=[True,False,False,True])
    #     DD.plot(cmap=plt.cm.Blues)
    #     path = os.path.abspath('./CloudCover/images_gif/map/')
    #     plt.savefig(os.path.join(path, "map_{}_{}.png".format(name,kk)), bbox_inches='tight')




def season_map(name, ds, LONS, LATS, Ds_tass_mean, Ds_tass_median, Ds_tass_dev, lt):
    """
    Basemap of the statistical properties of the TCC
    from copernicusa data
    """

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(17,9)) # Setting basemap
    m1 = Basemap(projection='cyl',lat_0=0,lon_0=0, ax=ax1)        
    m2 = Basemap(projection='cyl',lat_0=0,lon_0=0, ax=ax2)  
    m3 = Basemap(projection='cyl',lat_0=0,lon_0=0, ax=ax3)      
    
    m1.drawcoastlines()
    m2.drawcoastlines()
    m3.drawcoastlines()

    Z_1 = 2
    Z_2 = 5
    Z_3 = 10
    Z_4 = 20
    Z_5 = 30
    Z_6 = 50
    Z_7 = 100
    bounds =  [0, Z_1, Z_2, Z_3, Z_4, Z_5, Z_6, Z_7]    
    cmap = colors.ListedColormap(["green", "orange", 
         "gold", "skyblue", "k", 
        "#550011", "purple",
         "red"]).with_extremes(over='blue')
    norm = colors.BoundaryNorm(bounds, cmap.N)
    R1 = m1.scatter(LONS, LATS, marker='o', c=Ds_tass_mean, s=15, latlon=True,  cmap=cmap, norm=norm)
    R2 = m2.scatter(LONS, LATS, marker='o', c=Ds_tass_median, s=15, latlon=True,  cmap=cmap, norm=norm)
    R3 = m3.scatter(LONS, LATS, marker='o', c=Ds_tass_dev, s=15, latlon=True,  cmap=cmap, norm=norm)

    clb1 = plt.colorbar(R1, ax=[ax1, ax2, ax3, ax4],boundaries = bounds ,extend = 'max', extendfrac='auto' ,ticks=bounds)
    clb1.ax.set_title('[h]')
    ax1.set_title("Mean")
    ax1.legend(loc="upper left")

    ax2.set_title("Median ")
    ax2.legend(loc="upper left")

    ax3.set_title("Deviation standard ")
    ax3.legend(loc="upper left")

    LT = lt*100/lt[0] # convert into percentage

    txt = ['>2', '>4', '>6', '>10', '>15', '>20']
    ax4.bar(txt, LT)
    #ax4.text(r[i], MEAN, round(MEAN, 3) )
    ax4.set_xlabel('[h]')
    ax4.set_ylabel('% of cloud events')
    ax4.set_title('Amount of events through duration threshold')

    plt.suptitle('TCC residence analysis - 2004 - {}'.format(name))

    # plt.show()
    path = os.path.abspath('./CloudCover/weather_plot/')
    fig.savefig(os.path.join(path, "Residence_analysis_tcc_{}.png".format(name)), bbox_inches='tight')
    
    return






if __name__ == "__main__":

    # Grib data
    # YEAR: 2004
    # Frequence: couples of months, every 2 hours
    #---------------------------
    nn = 'NovDec'
    fname='{}.grib'.format(nn)

    #---------------------------

    # Reading the grib file
    #----
    path=os.path.abspath('{}/CloudCover/weather_data/{}'.format(base_path, fname))
    ds=xr.load_dataset(path,engine='cfgrib',backend_kwargs={'indexpath': ''})


    # In order to read keys and units:
    #----
    # print(ds)
    # for v in ds:
    #     print("{}, {}, {}".format(v, ds[v].attrs["long_name"], ds[v].attrs["units"]))
    
    # Computing a grid for variation inspection:
    # Mean, median and std_dev are inspected
    lats = np.arange(-80, 90, 10) # latitudes for the analysis grid [deg]
    lons = np.arange(-170, 180, 20) # longitudes [deg]
    ds_area  = ds.isel(latitude=10,longitude=20)
    time = len(ds_area.tcc[:]) # extracting total CC
    TT = 0.5 # threshold for the cloud coverage values
    fl = 0
    #trk_time = np.NaN
    Trk_time = 0
    residence = np.zeros(time)
    residence[:] = np.NaN

    Ds_tass_mean = np.zeros((len(lats), len(lons)))
    Ds_tass_median = np.zeros((len(lats), len(lons)))
    Ds_tass_dev = np.zeros((len(lats), len(lons)))
    LONS = np.zeros((len(lats), len(lons))) # Grid longitudes
    LATS = np.zeros((len(lats), len(lons))) # Grid latitudes
    temp = np.zeros(time) # temporary vector for definition of cloud presence

    lt_tot = 0
    lt = np.zeros(6) # less tha: 2,4,6,10,15,20 h

    print('Extracting data!')
    for i in range(0, len(lats)):
        for j in range(0, len(lons)):
            for k in range(0, time):
                DD = ds.tcc[k]
                temp[k] = DD.isel(latitude=lats[i], longitude=lons[j])

                if temp[k] > TT:
                    fl = 1
                    Trk_time += 2 # [h]
                    
                else:
                    if fl == 1:
                        # if Trk_time>20:
                        #     Trk_time = np.NaN
                        residence[k] = Trk_time # [h]
                        Trk_time = 0
                        fl = 0
                    else:
                        residence[k] = np.NaN # [h]
                # print(residence[k])
                # print(Trk_time)
            
              

            # Chenge 'temp' or 'trk_time'
            Ds_tass_mean[i,j] = np.nanmean(residence)
            Ds_tass_median[i,j] = np.nanmedian(residence)
            Ds_tass_dev[i,j] = np.nanstd(residence)
            # print(residence)
            
            lt[0] += np.count_nonzero(residence >= 2)
            lt[1] += np.count_nonzero(residence >= 4)
            lt[2] += np.count_nonzero(residence >= 6)
            lt[3] += np.count_nonzero(residence >= 10)
            lt[4] += np.count_nonzero(residence >= 15)
            lt[5] += np.count_nonzero(residence >= 20)
                   
            LONS[i,j] = lons[j]
            LATS[i,j] = lats[i]

            #print(Ds_tass_mean[i,j])  
            residence[:] = np.NaN
            Trk_time = 0
            fl = 0
    print('Computed!')
    
    
    
    # Analysis
    linear_map_Plots(100, ds, nn)
    season_map(nn, ds, LONS, LATS, Ds_tass_mean, Ds_tass_median, Ds_tass_dev, lt)