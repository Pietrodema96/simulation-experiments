
import math
import numpy as np
import pandas as pd
import CAR2KEP as C2K
import matplotlib.pyplot as plt

# COMPARISON BETWEEN 3 TOOLS INSPECTED:
#    Name        Author        Version        notes:
#    GMAT         NASA         -R2020a        considered as reference in following comparisons
#  Basilisk     Hans. S.        2.1.1         - under verification
#    SpOCK      Sergio DF       V 2022        - matter of comparison
# 
#
# AUTHOR: Pietro De Marchi
# LAST UPDATE: 20/03/2022
#---------------------------------------------------------------


# # ---------------------------
# # --Perturbation comparisons:
# # ---------------------------

# # Orbit: Iridium
# # 	INIT EPOCH: 01 FEB 2019 12:00:00.00
# # 	SMA:    7161.241 km
# # 	ECC:    0.0012
# # 	INC     86.4 deg
# # 	RAAN:   50   deg
# # 	AOP:    90   deg
# # 	TA:     0    deg

# # 	mean altitude = 780 km
	
# # 	- drag_area = 10 m2
# # 	  drag_coeff = 2.2
# # 	- SRP_area = 10 m2
# # 	  SRP_coeff = 1.2

# # GS:
# #  ID: TURIN-aikoHQ
# #  lat = 45 deg, lon = 7 deg, alt = 15m


# # TOOLS MODELS:

# # GMAT: integrator=rk45
# # SpOCK: lib int
# # Basilisk: integrator=rk45, drag=MSIS




# # -- CASE 1:
# # 	DURATION: 3 MONTHS (7 776 000 sec)
# # 	TIME STEP: 10 sec

# # 	PERTURBATIONS: J2 & 70
# # 	MONITORING: position_ECI, velocity_ECI, eclipses, contacts


# # -- CASE 2:
# # 	DURATION: 3 MONTHS (7 776 000 sec)
# # 	TIME STEP: 10 sec

# # 	PERTURBATIONS: J2, drag
# # 	MONITORING: position_ECI, velocity_ECI


# # -- CASE 3:
# # 	DURATION: 3 MONTHS (7 776 000 sec)
# # 	TIME STEP: 10 sec

# # 	PERTURBATIONS: J2, SRP
# # 	MONITORING: position_ECI, velocity_ECI


# # -- CASE 4:
# # 	DURATION: 3 MONTHS (7 776 000 sec)
# # 	TIME STEP: 10 sec

# # 	PERTURBATIONS: J2, drag, SRP, Moon+Sun
# # 	MONITORING: position_ECI, velocity_ECI

# Insert the number of data = (sim_duration / time_step)
n_data = 777601
# n_data = 300000

#de2ibfi2bf
# ----------------------------------------------------------------------------------





def comparison():

    times = np.empty(n_data)


    case = 70
    

    if case == 1:
        data_SPOCK= pd.read_csv("SPOCK-simulations/SPOCK-C1-POSVEL-J2.csv") # SCENARIO 1
        data_BAS= pd.read_csv("BASILISK-simulations/BAS-C1-POSVEL-J2RK45.csv") # SCENARIO 1
        data_GMAT= pd.read_csv("GMAT-simulations/GMAT-C1-POSVEL-J2.csv") # SCENARIO 1

    if case == 2:
        data_SPOCK= pd.read_csv("SPOCK-simulations/SPOCK-C2-POSVEL-J2.csv") # SCENARIO 2
        data_BAS= pd.read_csv("BASILISK-simulations/BAS-C2-POSVEL-J2.csv") # SCENARIO 2
        data_GMAT= pd.read_csv("GMAT-simulations/GMAT-C2-POSVEL-J2.csv") # SCENARIO 2

    if case == 3:
        data_SPOCK= pd.read_csv("SPOCK-simulations/SPOCK-C3-POSVEL-J2.csv") # SCENARIO 3
        data_BAS= pd.read_csv("BASILISK-simulations/BAS-C3-POSVEL-J2.csv") # SCENARIO 3
        data_GMAT= pd.read_csv("GMAT-simulations/GMAT-C3-POSVEL-J2.csv") # SCENARIO 3

    if case == 4:
        data_SPOCK= pd.read_csv("SPOCK-simulations/SPOCK-C4-POSVEL-J2SD.csv") # SCENARIO 4
        data_BAS= pd.read_csv("BASILISK-simulations/BAS-C4-POSVEL-J2SD.csv") # SCENARIO 4
        data_GMAT= pd.read_csv("GMAT-simulations/GMAT-C4-POSVEL-J2SD.csv") # SCENARIO 4

    if case == 70:
        data_SPOCK= pd.read_csv("SPOCK-simulations/SPOCK-C4-POSVEL-J70.csv") # SCENARIO 70
        data_BAS= pd.read_csv("BASILISK-simulations/BAS-C4-POSVEL-J70.csv") # SCENARIO J70
        data_GMAT= pd.read_csv("GMAT-simulations/GMAT-C4-POSVEL-J70.csv") # SCENARIO J70

    
    
    

    X_SPOCK = np.empty(n_data)
    Y_SPOCK = np.empty(n_data)
    Z_SPOCK = np.empty(n_data)
    VX_SPOCK = np.empty(n_data)
    VY_SPOCK = np.empty(n_data)
    VZ_SPOCK = np.empty(n_data)
    dX_SPOCK = np.empty(n_data)
    dY_SPOCK = np.empty(n_data)
    dZ_SPOCK = np.empty(n_data)
    r_SPOCK = np.empty(n_data)
    v_SPOCK = np.empty(n_data)
    rr_SPOCK = np.empty(3)
    vv_SPOCK = np.empty(3)
    delta_a_SPOCK = np.empty(n_data)
    delta_e_SPOCK = np.empty(n_data)
    delta_i_SPOCK = np.empty(n_data)
    delta_Om_SPOCK = np.empty(n_data)
    delta_f_SPOCK = np.empty(n_data)

    elem_SPOCK = np.empty([n_data,6])


    X_BAS = np.empty(n_data)
    Y_BAS = np.empty(n_data)
    Z_BAS = np.empty(n_data)
    VX_BAS = np.empty(n_data)
    VY_BAS = np.empty(n_data)
    VZ_BAS = np.empty(n_data)
    dX_BAS = np.empty(n_data)
    dY_BAS = np.empty(n_data)
    dZ_BAS = np.empty(n_data)
    r_BAS = np.empty(n_data)
    v_BAS = np.empty(n_data)
    rr_BAS = np.empty(3)
    vv_BAS = np.empty(3)
    elem_BAS = np.empty([n_data,6])
    delta_a_BAS = np.empty(n_data)
    delta_e_BAS = np.empty(n_data)
    delta_i_BAS = np.empty(n_data)
    delta_Om_BAS = np.empty(n_data)
    delta_f_BAS = np.empty(n_data)

    r_GMAT = np.empty(n_data)
    v_GMAT = np.empty(n_data)
    rr_GMAT = np.empty(3)
    vv_GMAT = np.empty(3)
    X_GMAT = np.empty(n_data)
    Y_GMAT = np.empty(n_data)
    Z_GMAT = np.empty(n_data)
    VX_GMAT = np.empty(n_data)
    VY_GMAT = np.empty(n_data)
    VZ_GMAT = np.empty(n_data)
    elem_GMAT = np.empty([n_data,6])


    

    for i in range(0, n_data):
        
        times[i] = float(data_SPOCK['T'][i]) # sim times [sec]
    
        X_GMAT[i] = float(data_GMAT['X'][i])*1000 #[m]
        Y_GMAT[i] = float(data_GMAT['Y'][i])*1000 #[m]
        Z_GMAT[i] = float(data_GMAT['Z'][i])*1000 #[m]
        if abs(X_GMAT[i]) > 9000000:
            X_GMAT[i] = X_GMAT[i]/1000
        if abs(Y_GMAT[i]) > 9000000:
            Y_GMAT[i] = Y_GMAT[i]/1000
        if abs(Z_GMAT[i]) > 9000000:
            Z_GMAT[i] = Z_GMAT[i]/1000

        VX_GMAT[i] = float(data_GMAT['VX'][i])*1000 #[m/s]
        VY_GMAT[i] = float(data_GMAT['VY'][i])*1000 #[m/s]
        VZ_GMAT[i] = float(data_GMAT['VZ'][i])*1000 #[m/s]
        r_GMAT[i] = math.sqrt(X_GMAT[i]**2 + Y_GMAT[i]**2 + Z_GMAT[i]**2)
        v_GMAT[i] = math.sqrt(VX_GMAT[i]**2 + VY_GMAT[i]**2 + VZ_GMAT[i]**2)


        X_SPOCK[i] = float(data_SPOCK['X'][i]) #[m]
        Y_SPOCK[i] = float(data_SPOCK['Y'][i]) #[m]70x70
        Z_SPOCK[i] = float(data_SPOCK['Z'][i]) #[m]70x70
        VX_SPOCK[i] = float(data_SPOCK['VX'][i]) #[m/s]
        VY_SPOCK[i] = float(data_SPOCK['VY'][i]) #[m/s]
        VZ_SPOCK[i] = float(data_SPOCK['VZ'][i]) #[m/s]
        dX_SPOCK[i] = X_SPOCK[i] - X_GMAT[i] #[m]
        dY_SPOCK[i] = Y_SPOCK[i] - Y_GMAT[i] #[m]
        dZ_SPOCK[i] = Z_SPOCK[i] - Z_GMAT[i] #[m]
        r_SPOCK[i] = math.sqrt(X_SPOCK[i]**2 + Y_SPOCK[i]**2 + Z_SPOCK[i]**2) - r_GMAT[i]
        v_SPOCK[i] = math.sqrt(VX_SPOCK[i]**2 + VY_SPOCK[i]**2 + VZ_SPOCK[i]**2) - v_GMAT[i]


        X_BAS[i] = float(data_BAS['X'][i])*1000 #[m]
        Y_BAS[i] = float(data_BAS['Y'][i])*1000 #[m]
        Z_BAS[i] = float(data_BAS['Z'][i])*1000 #[m]
        VX_BAS[i] = float(data_BAS['VX'][i])*1000 #[m/s]
        VY_BAS[i] = float(data_BAS['VY'][i])*1000 #[m/s]
        VZ_BAS[i] = float(data_BAS['VZ'][i])*1000 #[m/s]
        dX_BAS[i] = X_BAS[i] -X_GMAT[i] #[m]
        dY_BAS[i] = Y_BAS[i] -Y_GMAT[i] #[m]
        dZ_BAS[i] = Z_BAS[i] -Z_GMAT[i] #[m]
        r_BAS[i] = math.sqrt(X_BAS[i]**2 + Y_BAS[i]**2 + Z_BAS[i]**2) - r_GMAT[i]
        v_BAS[i] = math.sqrt(VX_BAS[i]**2 + VY_BAS[i]**2 + VZ_BAS[i]**2) - v_GMAT[i]


        # rr_BAS[0] = X_BAS[i] 
        # rr_BAS[1] = Y_BAS[i]
        # rr_BAS[2] = Z_BAS[i]
        # vv_BAS[0] = VX_BAS[i] 
        # vv_BAS[1] = VY_BAS[i]
        # vv_BAS[2] = VZ_BAS[i]

        # rr_SPOCK[0] = X_SPOCK[i] 
        # rr_SPOCK[1] = Y_SPOCK[i]
        # rr_SPOCK[2] = Z_SPOCK[i]
        # vv_SPOCK[0] = VX_SPOCK[i] 
        # vv_SPOCK[1] = VY_SPOCK[i]
        # vv_SPOCK[2] = VZ_SPOCK[i]

        # rr_GMAT[0] = X_GMAT[i] 
        # rr_GMAT[1] = Y_GMAT[i]
        # rr_GMAT[2] =  Z_GMAT[i]
        # vv_GMAT[0] = VX_GMAT[i] 
        # vv_GMAT[1] = VY_GMAT[i]
        # vv_GMAT[2] = VZ_GMAT[i]

        # elem_BAS[i, :] = C2K.rv2elem(398600.436, rr_BAS/1000, vv_BAS/1000)
        # elem_SPOCK[i, :] = C2K.rv2elem(398600.436, rr_SPOCK/1000, vv_SPOCK/1000)
        # elem_GMAT[i, :] = C2K.rv2elem(398600.436, rr_GMAT/1000, vv_GMAT/1000)

        # delta_a_BAS[i] = elem_BAS[i, 0] - elem_GMAT[i, 0] # [m]
        # delta_e_BAS[i] = elem_BAS[i, 1] - elem_GMAT[i, 1] 
        # delta_i_BAS[i] = elem_BAS[i, 2] - elem_GMAT[i, 2] # [RAD]
        # delta_Om_BAS[i] = elem_BAS[i, 3] - elem_GMAT[i, 3] # [RAD]
        # delta_f_BAS[i] = elem_BAS[i, 5] - elem_GMAT[i, 5] # [RAD]

        # delta_a_SPOCK[i] = elem_SPOCK[i, 0] - elem_GMAT[i, 0]
        # delta_e_SPOCK[i] = elem_SPOCK[i, 1] - elem_GMAT[i, 1]
        # delta_i_SPOCK[i] = elem_SPOCK[i, 2] - elem_GMAT[i, 2]
        # delta_Om_SPOCK[i] = elem_SPOCK[i, 3] - elem_GMAT[i, 3]
        # delta_f_SPOCK[i] = elem_SPOCK[i, 5] - elem_GMAT[i, 5]


    print(elem_GMAT[1,:])

    times = times/(60*60*24)

    # PLOT SECTION: variations positions components
    fig, axs = plt.subplots(3, 1)
    fig.suptitle('Basilisk(r) positions components with GMAT - Sim4')
    axs[0].plot(times, dX_BAS, 'r')
    # axs[0].plot(times, dX_SPOCK,'b')
    #axs[0].plot(times, X_GMAT,'k')
    axs[0].set(ylabel='dx_ECI [m]')


    axs[1].plot(times, dY_BAS,'r')
    # axs[1].plot(times, dY_SPOCK,'b')
    #axs[1].plot(times, Y_GMAT,'k')
    axs[1].set(ylabel='dy_ECI [m]')


    axs[2].plot(times, dZ_BAS,'r')
    # axs[2].plot(times, dZ_SPOCK,'b')
    #axs[2].plot(times, Z_GMAT,'k')
    axs[2].set(xlabel='Time [d]', ylabel='dz_ECI [m]')






    #PLOT SECTION: variations positions
    fig, axs = plt.subplots(2, 1)
    fig.suptitle('Comparison Spock(b), Basilisk(r)  d-positions with GMAT - CASE-3')
    axs[0].plot(times, r_SPOCK,'b')
    axs[0].set(ylabel='d|r_ECI| [m]')

    axs[1].plot(times, r_BAS,'r')
    axs[1].set(xlabel='Time [d]', ylabel='d|r_ECI| [m]')

    #PLOT SECTION: variations velocities
    fig, axs = plt.subplots(2, 1)
    fig.suptitle('Comparison Spock(b), Basilisk(r) d-velocities with GMAT - CASE-3')
    axs[0].plot(times, v_SPOCK,'b')
    axs[0].set(ylabel='d|v_ECI| [m/s]')

    axs[1].plot(times, v_BAS,'r')
    axs[1].set(xlabel='Time [d]', ylabel='d|v_ECI| [m/s]')








    # PLOTTING THE OEs
    # fig, axs = plt.subplots(4, 1)
    # fig.suptitle('Comparison Spock(b), Basilisk(r) positions components with GMAT - CASE-2')
    # axs[0].plot(times, elem_GMAT[:, 0], 'r')
    # axs[0].set(ylabel='SMA [km]')

    # axs[1].plot(times, elem_GMAT[:, 1],'r')
    # axs[1].set(ylabel='ECC [-]')

    # axs[2].plot(times, elem_GMAT[:, 2],'r')
    # axs[2].set(ylabel='INC [rad]')

    # axs[3].plot(times, elem_GMAT[:, 3],'r')
    # axs[3].set(xlabel='Time [d]', ylabel='Om [rad]')

    # fig, axs = plt.subplots(5, 1)
    # fig.suptitle('Comparison Spock(b), Basilisk(r) positions components with GMAT - CASE-2')
    # axs[0].plot(times, delta_a_BAS[:], 'r')
    # axs[0].plot(times, delta_a_SPOCK[:], 'b')
    # axs[0].set(ylabel='SMA [km]')

    # axs[1].plot(times, delta_e_BAS[:], 'r')
    # axs[1].plot(times, delta_e_SPOCK[:], 'b')
    # axs[1].set(ylabel='ECC [-]')

    # axs[2].plot(times, delta_i_BAS[:], 'r')
    # axs[2].plot(times, delta_i_SPOCK[:], 'b')
    # axs[2].set(ylabel='INC [rad]')

    # axs[3].plot(times, delta_Om_BAS[:], 'r')
    # axs[3].plot(times, delta_Om_SPOCK[:], 'b')
    # axs[3].set(xlabel='Time [d]', ylabel='Om [rad]')
    
    # axs[4].plot(times, delta_f_BAS[:], 'r')
    # axs[4].plot(times, delta_f_SPOCK[:], 'b')
    # axs[4].set(xlabel='Time [d]', ylabel='Om [rad]')
    

    fig = plt.figure( figsize=(6, 5))
    ax = plt.axes(projection='3d')
    ax.plot(X_GMAT[:]/1000, Y_GMAT[:]/1000, Z_GMAT[:]/1000,
            label="Spacecraft")
    ax.scatter(0, 0, 0, 'r')
    ax.set_xlim3d(-10000, 10000)
    ax.set_ylim3d(-10000, 10000)
    ax.set_zlim3d(-10000, 10000)
    ax.set_title('Spacecraft ECI Orbit')
    ax.set_xlabel('x [km]')
    ax.set_ylabel('y [km]')
    ax.set_zlabel('z [km]')
    ax.legend(loc=2)




    plt.show()






if __name__ == "__main__":

    comparison()
