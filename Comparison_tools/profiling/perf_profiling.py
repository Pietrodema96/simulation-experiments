from tqdm import tqdm
from time import sleep
import psutil
import signal
import os

stop = False

def exit_(*args):
    """Handle exit signal.
    """
    global stop
    print("Interrupt catched, exiting...")
    stop = True

signal.signal(signal.SIGINT, exit_)
signal.signal(signal.SIGTERM, exit_)


def start_perf_logging(filepath=None, period=0.5):
    if filepath is not None:
        filepath = os.path.abspath(filepath)
        if os.path.exists(filepath):
            os.remove(filepath)
        log_file = open(filepath, 'w')
        file_save = True
        header_str = "RAM"
    else:
        file_save = False
    print("=================================")
    print("      Performance Profiling      ")
    print("=================================")
    total_mem = int(psutil.virtual_memory().total / 1000000)
    rambar = tqdm(total=total_mem, desc='RAM [MB]', position=0)
    n_cpus = psutil.cpu_count(logical=False)
    cpubars = []
    for i in range(n_cpus):
        cpubars.append(tqdm(total=100, desc="CPU" + str(i) + " [%]", position=i+1))
        if file_save:
            header_str += " CPU" + str(i)
    if file_save:
        log_file.write(header_str+"\n")
    while not stop:
        used_ram =psutil.virtual_memory().used / 1000000
        rambar.n = int(used_ram)
        rambar.refresh()

        cpubars_val = psutil.cpu_percent(percpu=True)
        for i in range(n_cpus):
            cpubars[i].n = cpubars_val[i]
            cpubars[i].refresh()
        if file_save:
            log_file.write(f"{int(used_ram)} ")
            for i in range(n_cpus):
                log_file.write(f"{cpubars_val[i]} ")
            log_file.write("\n")

        sleep(period)

    if file_save:
        print("Saving perfomance log to " + filepath)
        log_file.close()
    print("Logging completed")

if __name__ == "__main__":
    start_perf_logging("perf_log.txt", 1)
