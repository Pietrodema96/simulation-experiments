import os
import matplotlib
import sys
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

if len(sys.argv) < 2:
    print('Usage: %s <perf_log_file> [--divide_cpus]' % sys.argv[0])
    sys.exit(-1)

log_file_path = sys.argv[1]
div_cpu = False
if len(sys.argv) == 3:
    if(sys.argv[2] == "--divide_cpus"):
        div_cpu = True

log_file = open(log_file_path, 'r')

values = log_file.readlines()[0].split()
num_proc = len(values) - 1
log_file.close()

# ram_values = []
# cpus = []
# for _ in range(num_proc):
#     cpu_vals = []
#     cpus.append(cpu_vals)
#
# for line in log_file.readlines():
#     values = line.split()
#     ram_values.append(int(values[0]))
#     for i in range(1, len(values) - 1):
#         cpus[i-1].append(float(values[i]))
#

perfs = pd.read_csv(log_file_path, sep=' ')
x = list(range(0, len(perfs["RAM"])))
if div_cpu:
    num_plots = num_proc + 1
else:
    num_plots = 2
ram_mean = np.mean(perfs["RAM"])
cpus_mean = []
fig, axs = plt.subplots(num_plots)
fig.suptitle('Performance Profiling')
axs[0].plot(x, perfs["RAM"])
axs[0].set_title("RAM (Avg: " + "{:.2f}".format(ram_mean) + " MB)", loc="left")
axs[0].set_ylabel("MB")
axs[0].set_xlabel("Seconds")
for i in range(num_proc):
    cpu_mean = np.mean(perfs["CPU"+ str(i)])
    cpus_mean.append(cpu_mean)
    if div_cpu:
        plot_idx = i+1
        plot_name = "CPU"+ str(i) + " (Avg: " + "{:.2f}".format(cpu_mean) + "%)"
    else:
        all_cpus_mean = np.mean(cpus_mean)
        plot_idx = 1
        plot_name = "CPU (Avg: " + "{:.2f}".format(all_cpus_mean) + "%)"
    axs[plot_idx].plot(x, perfs["CPU"+ str(i)])
    axs[plot_idx].set_title(plot_name, loc="left")
    axs[plot_idx].set_ylabel("%")
    axs[plot_idx].set_xlabel("Seconds")

plt.subplots_adjust(left=0.1,
                    bottom=0.1,
                    right=0.9,
                    top=0.85,
                    wspace=0.4,
                    hspace=0.9)
plt.show()
