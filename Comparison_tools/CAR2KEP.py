

import math
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from numpy import linalg as la

def rv2elem(mu, rVec, vVec):
    """
    Translates the orbit elements inertial Cartesian position
    vector rVec and velocity vector vVec into the corresponding
    classical orbit elements where

    === ========================= =======
    a   semi-major axis           km
    e   eccentricity
    i   inclination               rad
    AN  ascending node            rad
    AP  argument of periapses     rad
    f   true anomaly angle        rad
    === ========================= =======

    If the orbit is rectilinear, then this will be the eccentric or hyperbolic anomaly

    The attracting body is specified through the supplied
    gravitational constant mu (units of km^3/s^2).

    :param mu:  gravitational parameter
    :param rVec: position vector
    :param vVec: velocity vector
    :return:  orbital elements
    """
    hVec = [0.0] * 3
    v3 = [0.0] * 3
    nVec = [0.0] * 3
    eVec = [0.0] * 3
    eVec2 = [0.0] * 3

    eps = 1e-13

    if (np.isnan(np.sum(rVec)) or np.isnan(np.sum(vVec))):
        print("ERROR: received NAN rVec or vVec values.")
        a = np.NaN
        alpha = np.NaN
        e = np.NaN
        i = np.NaN
        AN = np.NaN
        AP = np.NaN
        f = np.NaN
        rmag = np.NaN
        return


    # Calculate the specific angular momentum and its magnitude #
    hVec = np.cross(rVec, vVec)
    h = la.norm(hVec)
    p = h * h / mu

    # Calculate the line of nodes #
    v3 = np.array([0.0, 0.0, 1.0])
    nVec = np.cross(v3, hVec)
    n = la.norm(nVec)

    # Orbit eccentricity and energy #
    r = la.norm(rVec)
    v = la.norm(vVec)
    eVec = (v * v / mu - 1.0 / r) * rVec
    v3 = (np.dot(rVec, vVec) / mu) * vVec
    eVec = eVec - v3
    e = la.norm(eVec)
    rmag = r
    rPeriap = p / (1.0 + e)

    # compute semi-major axis #
    alpha = 2.0 / r - v * v / mu
    if math.fabs(alpha) > eps:
        # elliptic or hyperbolic case #
        a = 1.0 / alpha
        rApoap = p / (1.0 - e)
    else:
        rp = p / 2.
        a = -rp
        rApoap = -1.0

    # Calculate the inclination #
    i = math.acos(hVec[2] / h)

    if e >= 1e-11 and i >= 1e-11:
        # Case 1: Non-circular, inclined orbit #
        Omega = math.acos(nVec[0] / n)
        if nVec[1] < 0.0:
            Omega = 2.0 * np.pi - Omega
        omega = math.acos(np.clip(np.dot(nVec, eVec) / n / e, a_min=-1.0, a_max=1.0))
        if eVec[2] < 0.0:
            eomega = 2.0 * np.pi - omega
        f = math.acos(np.clip(np.dot(eVec, rVec) / e / r, a_min=-1.0, a_max=1.0))
        if np.dot(rVec, vVec) < 0.0:
            f = 2.0 * np.pi - f
    elif e >= 1e-11 and i < 1e-11:
        # Case 2: Non-circular, equatorial orbit #
        # Equatorial orbit has no ascending node #
        Omega = 0.0
        # True longitude of periapsis, omegatilde_true #
        omega = math.acos(eVec[0] / e)
        if eVec[1] < 0.0:
            omega = 2.0 * np.pi - omega
        f = math.acos(np.clip(np.dot(eVec, rVec) / e / r, a_min=-1.0, a_max=1.0))
        if np.dot(rVec, vVec) < 0.0:
            f = 2.0 * np.pi - f
    elif e < 1e-11 and i >= 1e-11:
        # Case 3: Circular, inclined orbit #
        Omega = math.acos(nVec[0] / n)
        if nVec[1] < 0.0:
            Omega = 2.0 * np.pi - Omega
        omega = 0.0
        # Argument of latitude, u = omega + f #
        f = math.acos(np.clip(np.dot(nVec, rVec) / n / r, a_min=-1.0, a_max=1.0))
        if rVec[2] < 0.0:
            f = 2.0 * np.pi - f
    elif e < 1e-11 and i < 1e-11:
        # Case 4: Circular, equatorial orbit #
        Omega = 0.0
        omega = 0.0
        # True longitude, lambda_true #
        f = math.acos(rVec[0] / r)
        if rVec[1] < 0:
            f = 2.0 * np.pi - f
    else:
        print("Error: rv2elem couldn't identify orbit type.")
    if e > 1.0 and math.fabs(f) > np.pi:
        twopiSigned = math.copysign(2.0 * np.pi, f)
        f -= twopiSigned

    elements = np.empty(6)
    elements[0] = a
    elements[1] = e
    elements[2] = i
    elements[3] = Omega
    elements[4] = omega
    elements[5] = f

    return elements