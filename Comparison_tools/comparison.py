import math, os
import numpy as np
import pandas as pd
import CAR2KEP as C2K
import matplotlib.pyplot as plt



# Comparison between peopagations in ECI
# 'ElapsedTime','GPSsec'
# 'Rx_eci','Ry_eci','Rz_eci','Vx_eci','Vy_eci','Vz_eci'
# 'Rx_ecef','Ry_ecef','Rz_ecef','Vx_ecef','Vy_ecef','Vz_ecef'




def comparison():

    data_ref= pd.read_csv(os.path.abspath("./Orbit_data_analysis/DATA/Test/Output/Ephemeris/S1_P1.csv")) # SCENARIO ref
    data_comp1= pd.read_csv(os.path.abspath("./Hypernova/Ephem_Test.csv")) # SCENARIO 1
    # data_comp2= pd.read_csv(os.abs.path("./Hypernova/Ephem_Test.csv")) # SCENARIO 2

    n_data = len(data_ref['ElapsedTime'][:])-10
    dX1 = dY1 = dZ1 = dVX1 = dVY1 = dVZ1 = np.empty(n_data)
    # dX2 = dY2 = dZ2 = dVX2 = dVY2 = dVZ2 = np.empty(n_data)
    offset = 1
    for i in range(0, n_data):
        k = i+1
        dX1[i] = data_comp1['Rx_eci'][k] - data_ref['Rx_eci'][i]
        dY1[i] = data_comp1['Ry_eci'][k] - data_ref['Ry_eci'][i]
        dZ1[i] = data_comp1['Rz_eci'][k] - data_ref['Rz_eci'][i]
        
        dVX1[i] = data_comp1['Vx_eci'][k] - data_ref['Vx_eci'][i]
        dVY1[i] = data_comp1['Vy_eci'][k] - data_ref['Vy_eci'][i]
        dVZ1[i] = data_comp1['Vz_eci'][k] - data_ref['Vz_eci'][i]
        
        # dX2[i] = data_comp2['Rx_eci'][i] - data_ref['Rx_eci'][i]
        # dY2[i] = data_comp2['Ry_eci'][i] - data_ref['Ry_eci'][i]
        # dZ2[i] = data_comp2['Rz_eci'][i] - data_ref['Rz_eci'][i]
        
        # dVX2[i] = data_comp2['Vx_eci'][i] - data_ref['Vx_eci'][i]
        # dVY2[i] = data_comp2['Vy_eci'][i] - data_ref['Vy_eci'][i]
        # dVZ2[i] = data_comp2['Vz_eci'][i] - data_ref['Vz_eci'][i]
        
    times = data_ref['ElapsedTime'][:-10]/(60*60*24)

    # PLOT SECTION: variations positions components
    fig, axs = plt.subplots(3, 1)
    fig.suptitle('Reference SpOCK: Hyper(r)')
    axs[0].plot(times, dX1, 'r')
    # axs[0].plot(times, dX2, 'b')
    axs[0].set(ylabel='dx_ECI [m]')


    axs[1].plot(times, dY1,'r')
    #axs[1].plot(times, dY2,'b')
    axs[1].set(ylabel='dy_ECI [m]')


    axs[2].plot(times, dZ1,'r')
    #axs[2].plot(times, dZ2,'r')
    axs[2].set(xlabel='Time [d]', ylabel='dz_ECI [m]')


    path = os.path.abspath('./Comparison_tools/')
    fig.savefig(os.path.join(path, "Hyper_test.png"), bbox_inches='tight')
    return






if __name__ == "__main__":

    comparison()
