# syntax = edrevo/dockerfile-plus

# orbital_OLIVER x86_64 Compilation Environment

# Base Image
INCLUDE+ ../../base_images/python3_8_x86_64.Dockerfile

# Set Environment variables for the platform
ARG ARCH=x86_64-linux
ENV PY_PLAT_NAME=linux-amd64
ENV AS=gcc-as \
    AR=gcc-ar \
    CC=gcc \
    CXX=g++ \
    LD=gcc-ld \
    READELF=gcc-readelf \
    FC=gfortran


# Install PROJ-4
RUN apt-get update && apt-get install -y proj-bin 

# Install GEO:
RUN curl -L \
    http://download.osgeo.org/geos/geos-3.11.0beta2.tar.bz2 \
    --output geos-3.11.0beta2.tar.bz2 
    # geos package geos-3.11.0beta2

RUN tar -xf geos-3.11.0beta2.tar.bz2 
RUN ls -a

RUN mkdir GEOS
RUN mv geos-3.11.0beta2 GEOS
# RUN ls GEOS

WORKDIR /GEOS/geos-3.11.0beta2
RUN echo "$GEOS_DIR"
#RUN export GEOS_DIR = "/usr/local"
RUN ./configure
RUN make
RUN make install
WORKDIR /

# Install BASEMAP:
# RUN curl -L \
#     https://github.com/matplotlib/basemap/archive/refs/tags/v1.3.3.tar.gz \
#     --output v1.3.3.tar.gz 


# RUN tar -xf v1.3.3.tar.gz
# RUN ls -a

# RUN mkdir BASEMAP
# RUN mv basemap-1.3.3 BASEMAP
# WORKDIR /BASEMAP/basemap-1.3.3/packages/basemap
# RUN ls -a

# RUN python setup.py install

