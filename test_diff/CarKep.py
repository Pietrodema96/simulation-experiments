

import math
from tkinter import E
import numpy as np
import pandas as pd
import os, sys

from numpy import einsum, linalg as la
import AstroConstants as AC

# -----------------------
# Car2Kep and Kep2Car
# -----------------------
# Transformations between Cartesian and Keplerian elements for an Earth orbit
# These functions structure is copied from the tool Basilisk (by Dr. Hanspeter Schaub)
#
# AUTHOR: Pietro De Marchi
# FROM: Basilisk, by H. Schaub
# LAST UPDATE: 20/03/2022
#
#---------------------------------------------------

def meanAnomaly(mu, rr, vv, SMA, ECC):
    """"
    This function compute the mean anomaly
    from the satellite instantaneous ECI position and speed
    
    INPUT
        mu = Earth grav parameter
        rr = ECI position of the satellite [km]
        vv = ECI speed of the satellite [km/s]
        SMA = osculating  SMA [km]
        ECC = osculating ECC [-]
        
    OUTPUT
        M = mean anomaly [rad]
        """
        
    pr_scal = np.dot(rr, vv)
    NNorm = np.linalg.norm(rr)
    E = math.atan2(pr_scal/(math.sqrt(mu*SMA)), (1-NNorm/SMA))
    M = E - ECC*math.sin(E)
    
    return M



def meanMotion(SMA):

    """    
    Compute orbit mean motion
    SMA = semi-major axis [km]
    """

    n = np.sqrt(AC.MU_E / (SMA**3))

    return n # [rad/s]

def period(SMA):

    """    
    Approximation of orbit period
    SMA = semi major axis at the instant [km]
    """

    n = meanMotion(SMA)
    T = 2. * np.pi / n # orbit period [sec]

    return T # [s]


def period_inv(T):

    """    
    Approximation of orbit period
    SMA = semi major axis at the instant [km]
    """

    SMA = (( T/(2*np.pi) )**2 *AC.MU_E)**(1/3)

    return SMA # [km]




def inc_SSO(h):
    """
    Compute the inclination for a SSO from h [km]
    """
    SMA = h + AC.R_E # [km]
    T = period(SMA)/3600 # [h]
    # print(np.float_power(T/(3.795), (7/3)))
    inc = math.acos( -np.float_power(T/(3.759), (7/3)) ) # [rad]

    return inc/AC.D2R # [deg]


def rv2elem(mu, rVec, vVec):
    
    """
    Translates the orbit elements inertial Cartesian position
    vector rVec and velocity vector vVec into the corresponding
    classical orbit elements where

    === ========================= =======
    a   semi-major axis           km
    e   eccentricity
    i   inclination               rad
    AN  ascending node            rad
    AP  argument of periapses     rad
    f   true anomaly angle        rad
    === ========================= =======

    If the orbit is rectilinear, then this will be the eccentric or hyperbolic anomaly

    The attracting body is specified through the supplied
    gravitational constant mu (units of km^3/s^2).

    :param mu:  gravitational parameter
    :param rVec: position vector
    :param vVec: velocity vector
    :return:  orbital elements
    """

    hVec = [0.0] * 3
    v3 = [0.0] * 3
    nVec = [0.0] * 3
    eVec = [0.0] * 3
    eVec2 = [0.0] * 3

    eps = 1e-13

    if (np.isnan(np.sum(rVec)) or np.isnan(np.sum(vVec))):
        print("ERROR: received NAN rVec or vVec values.")
        a = np.NaN
        alpha = np.NaN
        e = np.NaN
        i = np.NaN
        AN = np.NaN
        AP = np.NaN
        f = np.NaN
        rmag = np.NaN
        return


    # Calculate the specific angular momentum and its magnitude #
    hVec = np.cross(rVec, vVec)
    h = la.norm(hVec)
    p = h * h / mu

    # Calculate the line of nodes #
    v3 = np.array([0.0, 0.0, 1.0])
    nVec = np.cross(v3, hVec)
    n = la.norm(nVec)

    # Orbit eccentricity and energy #
    r = la.norm(rVec)
    v = la.norm(vVec)
    eVec = (v * v / mu - 1.0 / r) * rVec
    v3 = (np.dot(rVec, vVec) / mu) * vVec
    eVec = eVec - v3
    e = la.norm(eVec)
    rmag = r
    rPeriap = p / (1.0 + e)

    # compute semi-major axis #
    alpha = 2.0 / r - v * v / mu
    if math.fabs(alpha) > eps:
        # elliptic or hyperbolic case #
        a = 1.0 / alpha
        rApoap = p / (1.0 - e)
    else:
        rp = p / 2.
        a = -rp
        rApoap = -1.0

    # Calculate the inclination #
    i = math.acos(hVec[2] / h)

    if e >= 1e-11 and i >= 1e-11:
        # Case 1: Non-circular, inclined orbit #
        Omega = math.acos(nVec[0] / n)
        if nVec[1] < 0.0:
            Omega = 2.0 * np.pi - Omega
        omega = math.acos(np.clip(np.dot(nVec, eVec) / n / e, a_min=-1.0, a_max=1.0))
        if eVec[2] < 0.0:
            eomega = 2.0 * np.pi - omega
        f = math.acos(np.clip(np.dot(eVec, rVec) / e / r, a_min=-1.0, a_max=1.0))
        if np.dot(rVec, vVec) < 0.0:
            f = 2.0 * np.pi - f
    elif e >= 1e-11 and i < 1e-11:
        # Case 2: Non-circular, equatorial orbit #
        # Equatorial orbit has no ascending node #
        Omega = 0.0
        # True longitude of periapsis, omegatilde_true #
        omega = math.acos(eVec[0] / e)
        if eVec[1] < 0.0:
            omega = 2.0 * np.pi - omega
        f = math.acos(np.clip(np.dot(eVec, rVec) / e / r, a_min=-1.0, a_max=1.0))
        if np.dot(rVec, vVec) < 0.0:
            f = 2.0 * np.pi - f
    elif e < 1e-11 and i >= 1e-11:
        # Case 3: Circular, inclined orbit #
        Omega = math.acos(nVec[0] / n)
        if nVec[1] < 0.0:
            Omega = 2.0 * np.pi - Omega
        omega = 0.0
        # Argument of latitude, u = omega + f #
        f = math.acos(np.clip(np.dot(nVec, rVec) / n / r, a_min=-1.0, a_max=1.0))
        if rVec[2] < 0.0:
            f = 2.0 * np.pi - f
    elif e < 1e-11 and i < 1e-11:
        # Case 4: Circular, equatorial orbit #
        Omega = 0.0
        omega = 0.0
        # True longitude, lambda_true #
        f = math.acos(rVec[0] / r)
        if rVec[1] < 0:
            f = 2.0 * np.pi - f
    else:
        print("Error: rv2elem couldn't identify orbit type.")
    if e > 1.0 and math.fabs(f) > np.pi:
        twopiSigned = math.copysign(2.0 * np.pi, f)
        f -= twopiSigned

    elements = np.empty(6)
    elements[0] = a
    elements[1] = e
    elements[2] = i
    elements[3] = Omega
    elements[4] = omega
    elements[5] = f

    return elements





def elem2rv(mu, elements):
    """
    Translates the orbit elements:

    === ========================= =======
    a   semi-major axis           km
    e   eccentricity
    i   inclination               rad
    AN  ascending node            rad
    AP  argument of periapses     rad
    f   true anomaly angle        rad
    === ========================= =======

    to the inertial Cartesian position and velocity vectors.
    The attracting body is specified through the supplied
    gravitational constant mu (units of km^3/s^2).

    :param mu: gravitational parameter
    :param elements: orbital elements
    :return:   rVec, position vector
    :return:   vVec, velocity vector
    """
    rVec = np.zeros(3)
    vVec = np.zeros(3)

    tolerance = 1e-15

    a = elements[0]
    e = elements[1]
    i = elements[2]
    Omega = elements[3]
    omega = elements[4]
    f = elements[5]


    if 1.0 + e * math.cos(f) < tolerance:
        print('WARNING: Radius is near infinite in elem2rv conversion.')

    # Calculate the semilatus rectum and the radius #
    p = a * (1.0 - e * e)
    r = p / (1.0 + e * math.cos(f))
    theta = omega + f
    rVec[0] = r * (
        math.cos(theta) * math.cos(Omega) - math.cos(i) * math.sin(theta) * math.sin(
            Omega))
    rVec[1] = r * (
        math.cos(theta) * math.sin(Omega) + math.cos(i) * math.sin(theta) * math.cos(
            Omega))
    rVec[2] = r * (math.sin(theta) * math.sin(i))

    if math.fabs(p) < tolerance:
        if math.fabs(1.0 - e) < tolerance:
            # Rectilinear orbit #
            raise ValueError('elem2rv does not support rectilinear orbits')
        # Parabola #
        rp = -a
        p = 2.0 * rp

    h = math.sqrt(mu * p)
    vVec[0] = -mu / h * (math.cos(Omega) * (e * math.sin(omega) + math.sin(theta)) +
                         math.cos(i) * (
                             e * math.cos(omega) + math.cos(theta)) * math.sin(Omega))
    vVec[1] = -mu / h * (math.sin(Omega) * (e * math.sin(omega) + math.sin(theta)) -
                         math.cos(i) * (e * math.cos(omega) + math.cos(theta)) *
                         math.cos(Omega))
    vVec[2] = mu / h * (e * math.cos(omega) + math.cos(theta)) * math.sin(i)

    return rVec, vVec


def CtoK(mu, rr, vv):
    """
    Second version of the conversion between keplerian and cartesian elements
    rr and vv are positions and velocoties [km] [km/s]
    """

    r=np.linalg.norm(rr)
    v=np.linalg.norm(vv)
    ii = [1, 0, 0]
    kk = [0, 0, 1]
    E=0.5*v**2-mu/r
    a=-mu/(2*E)
    hh=np.cross(rr, vv)
    ee=np.cross(vv, hh)/mu-rr/r
    e=np.linalg.norm(ee)
    i=math.acos(np.dot(hh, kk)/np.linalg.norm(hh))
    if i==0:
        N=[1, 0, 0]
        Om=0
    
    N=np.cross(kk, hh)/np.linalg.norm(np.cross(kk, hh))
    if N[1]>0:
        Om=math.acos(np.dot(ii, N))
    else:
        Om=2*np.pi-math.acos(np.dot(ii, N))
    
    if e==0:
        w=0
        ee=N
        e=np.linalg.norm(N)
    
    if np.dot(ee,kk) > 0:
        w=math.acos(np.dot(N, ee)/np.linalg.norm(ee))
    else:
        w=2*np.pi-math.acos(np.dot(N, ee)/np.linalg.norm(ee))
    
    if np.dot(vv, rr)>0:
        teta=math.acos(np.dot(rr, ee)/(np.linalg.norm(rr)*np.linalg.norm(ee)))
    else:
        teta=2*np.pi-math.acos(np.dot(rr, ee)/(np.linalg.norm(rr)*np.linalg.norm(ee)))
    

    theta=teta
    om=w
    OM=Om
    kepEl=[a,e,i,OM,om,theta]

    return kepEl


def keplerian_elems_from_cart(mu, rVec, vVec):
    """
    Returns the keplerian elements of the input orbit given through its cartesian parameters.
    The implementation is taken from Orekit (https://gitlab.orekit.org/orekit/orekit/-/blob/develop/src/main/java/org/orekit/orbits/KeplerianOrbit.java).
    """

    if (np.isnan(np.sum(rVec)) or np.isnan(np.sum(vVec))):
        raise ValueError("Received NAN rVec or vVec values in Cartesian to Keplerian conversion.")

    # Useful quantities
    r       = la.norm(rVec)
    r2      = r**2
    V2      = la.norm(vVec)**2
    rV2OnMu = r * V2 / mu

    if (rV2OnMu > 2):
        raise ValueError('Hyperbolic orbits not handled. Cartesian to Equinoctial conversion unfeasible.')

    # Calculate the specific angular momentum and its magnitude
    hVec = np.cross(rVec, vVec)
    h    = la.norm(hVec)

    # Compute inclination
    wVec = hVec / h
    v3   = np.array([0.0, 0.0, 1.0])
    inc  = np.arccos(np.dot(wVec, v3))

    # Compute raan
    crosshv3 = np.cross(v3, hVec)
    raan     = math.atan2(crosshv3[1], crosshv3[0])

    # Compute semi-major axis
    sma   = r / (2 - rV2OnMu)
    smaMu = sma * mu

    # Compute true anomaly
    eSE  = np.dot(rVec, vVec) / math.sqrt(smaMu)
    eCE  = rV2OnMu - 1
    ecc  = math.sqrt(eSE * eSE + eCE * eCE)
    anom = ellipticEccentricToTrue(math.atan2(eSE, eCE), ecc)

    # Compute argument of perigee
    nodeVec = np.array([math.cos(raan), math.sin(raan), 0.0])
    dotX    = np.dot(rVec, nodeVec)
    dotY    = np.dot(rVec, np.cross(hVec, nodeVec)) / h
    argp    = math.atan2(dotY, dotX) - anom

    # Build list of Keplerian elems
    keplerianElems = [sma, ecc, inc, raan, argp, anom]

    return keplerianElems




def ellipticEccentricToTrue(E_anom, ecc):
    """
    Returns the true anomaly from the elliptic eccentric anomaly.

    Args:
        E_anom (float): eccentric anomaly [rad]
        ecc    (float): eccentricity
    """

    beta = ecc / (1 + math.sqrt((1 - ecc) * (1 + ecc)))
    sinE = math.sin(E_anom)
    cosE = math.cos(E_anom)
    return E_anom + 2 * math.atan(beta * sinE / (1 - beta * cosE))



def equinoctial_elems_from_kep(kepElems):
    """
    Returns the equinoctial elements of the input orbit given through its keplerian parameters.
    """

    # Retrieve keplerian elems
    smaKep  = kepElems[0] 
    eccKep  = kepElems[1]
    incKep  = kepElems[2]
    raanKep = kepElems[3]
    argpKep = kepElems[4]
    anomKep = kepElems[5]

    # Compute equinoctial elems from keplerian
    sma = smaKep
    ex  = eccKep * math.cos(raanKep + argpKep)
    ey  = eccKep * math.sin(raanKep + argpKep)
    hx  = math.tan(incKep / 2.) * math.cos(raanKep)
    hy  = math.tan(incKep / 2.) * math.sin(raanKep)
    lv  = anomKep + argpKep + raanKep

    # Build list of Equinoctial elems
    equinoctialElems = [sma, ex, ey, hx, hy, lv]

    return equinoctialElems