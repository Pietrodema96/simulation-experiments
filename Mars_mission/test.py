# -*- coding: utf-8 -*-
"""
This project considers a small satellite orbiting Mars at a low altitude
gathering science data. However, this small satellite needs to transfer this
data to a larger mother satellite at a higher altitude. Further, to keep the
batteries from draining all the way, periodically the satellite must point its
solar panels at the sun to recharge. Thus, three mission goals must be
considered by the satellite:
    
    1) point sensor platformstraight down at Mars,
    2) point communication platform at the mother satellite,
    3) point the solar arrays at the sun.
    
In all scenarios the small spacecraft and mother craft are on simple circular
orbits whose motion is completely known. The high-level goal of this project
is to design a thruster-based attitude control to achieve these attitude
control scenarios. In order to satisfy the mission requirements, the space-
craft’s body-frame B must be driven towards various reference body-frames
R that corresponds to the desired attitude.

The reference attitude is computed from the knowledge of the spacecraft
position and velocity about Mars, as well as the knowledge of the mother
satellite motion for the communication scenario and the knowledge of the sun
heading for the power generation scenario. Once this reference is derived,
you will implement the torque control law u that drives the current attitude
MRP σ_B/N and the angular velocity ω_B/N towards their reference values σ_R/N,
and ω_R/N ). The scope of this project encompasses reference frame generation,
attitude characterization and feedback control. By the end of this project,
you will have gained valuable practical experience in the aforementioned areas
thanks to analytic derivations and software implementation of the different
project milestones.


Created on Sat Jul 11 19:38:34 2020, by Samuel Low. DSO National Laboratories.
Credits to: Professor Hanspeter Schaub and his team for the work on Basilisk,
and the Autonomous Vehicle Systems Lab, University of Colorado at Boulder.
"""

# We need NumPy and the local file attitude_mrp.py
import numpy as np
import attitude_mrp as mrp
import attitude_dcm as dcm
import matplotlib.pyplot as plt

plt.close("all")
    

# To understand the logic of the numerical integrator, let us work backwards.
# The final deliverable is to propagate MRPs and plot σ-123 components.
# In general, for both tracking and regulation, we want to plot MRPs relative
# to the reference attitude and not the inertial zero attitude. Thus, in the
# integrator, we must continuously update MRP_BR components. This means we
# need MRP_BR_DOT as a differential update. We can relate this derivative to
# the MRP_BR and angular velocity OHM_BR at that instant; keeping in mind to
# switch MRP_BR to the appropriate shadow and non-shadow set values by
# checking if the MRP norm exceeds 1. Since angular velocities are simply
# vectors, we can find OHM_BR by the vector addition of OHM_BN and -OHM_RN,
# but this means that both OHM_BN and OHM_RN must be continuously updated in
# the integrator loop too. Thus, before doing any of that, we must first find
# the first order derivatives (dots) of OHM_BN and OHM_RN. We can solve for
# the derivative OHM_RN_DOT by simply taking the numerical first order
# gradient. For OHM_BN_DOT, we can substitute the control vector "u" into
# Euler's equations of rotational motion. We can solve for the derivative of
# OHM_RN_DOT by differentiating the time-varying function of MRP_RN by hand,
# solve for the current MRP_RN, and use both MRP_RN and MRP_RN_DOT to find
# the angular velocity OHM_RN using the "attitude_mrp.py" library. Take
# careful note that OHM_RN and OHM_RN_DOTs must be seen in the B-frame. Now,
# we have had all the ingredients we needed to move onto the next loop of
# the integrator, and then we solve for everything all over again.

''' User defined constants, control function, and target reference below. '''

# Constants used throughout the integration:
# Set gains below to zero to remove their control effect.
I  = np.diag([10.0,5.0,7.5]) # Principal inertia tensor
L  = np.array([0.0,0.0,0.0]) # External environmental torques

# Set the desired decay time constants.
Idecay = np.array([10.0,5.0,7.5]) # Principal inertias
Tdecay = 120 # seconds
Pdecay = 2*Idecay / Tdecay
Kdecay = (Pdecay**2) / Idecay

# print("\n Below are the possible set of P constants, please select one.")
# print(Pdecay)
# print("\n Below are the possible set of K constants, please select one.")
# print(Kdecay)
# print("")

# Set the control gains based on the printed output message above?
P  = 0.2 # Kd
# Position control gain, in Nms
K  = 0.01 # Kp

print("Chosen P and K gains are: " + str(P) + " and " + str(K) + "\n")
Xi_Value = np.array([P,P,P]) / np.sqrt(K*Idecay)
print("Damping indicator Xi = " + str(Xi_Value) + "\n")

print("Below are the resultant decay time constants for each MRP.")
print(2*Idecay / P)
print("\n ******************************************** \n")

# Initialise the time axis
ti = 0.0  # Start, in seconds
tf = 500.0 # Final, in seconds

# this is for the dynamics control loop step
t_div = 100 

tx = np.linspace(ti, tf, (t_div * int(tf-ti)) + 1) # Time axis in ms-steps.
ts = tx[1] - tx[0] # Time step

# Initialised parameters to be iterated throughout the integration:
OHM_BN = np.deg2rad([1.0, 1.75, -2.2]) # Initial inertial omega (rad/s)
MRP_BN = np.array([-0.3,-0.4,0.5]) # Initial MRP, body-to-inertial

''' Setup the orbit parameters of the drone (d) and mothership (m) below. '''

mu = 42828300000000.0 # GM constant for Mars

# For the drone spacecraft
oe_d_sma = 3396190 + 400000 # m
oe_d_ran = float(np.deg2rad(20.0)) # Right-angle ascending node(rad)
oe_d_inc = float(np.deg2rad(30.0)) # Inclination (rad)
oe_d_lat = float(np.deg2rad(60.0)) # True anomaly + argument of perigee (rad)
oe_d_vel = (mu/oe_d_sma) ** 0.5 # Velocity magnitude
p_d = ((4*(np.pi**2)) * oe_d_sma**3 / mu)**0.5
n_d = 2*np.pi / p_d

# For the mothership
oe_m_sma = 20424200 # m
oe_m_ran = float(np.deg2rad(0.0)) # Right-angle ascending node(rad)
oe_m_inc = float(np.deg2rad(0.0)) # Inclination (rad)
oe_m_lat = float(np.deg2rad(250.0)) # True anomaly + argument of perigee (rad)
oe_m_vel = (mu/oe_m_sma) ** 0.5
p_m = ((4*(np.pi**2)) * oe_m_sma**3 / mu)**0.5
n_m = 2*np.pi / p_m

# Get the initial hill frame DCM for the drone
DCM_HN = np.matmul(dcm.Mz(oe_d_lat),
                    np.matmul(dcm.Mx(oe_d_inc),
                              dcm.Mz(oe_d_ran)))
DCM_HN_STEP = dcm.Mz( ts * n_d )

# Get the initial hill frame DCM for the mothership
DCM_MN = np.matmul(dcm.Mz(oe_m_lat),
                    np.matmul(dcm.Mx(oe_m_inc),
                              dcm.Mz(oe_m_ran)))
DCM_MN_STEP = dcm.Mz( ts * n_m )

# Now, let us build the position and velocity vectors of the spacecraft.
r_di, r_mi = [oe_d_sma, 0.0, 0.0], [oe_m_sma, 0.0, 0.0]
v_di, v_mi = [0.0, oe_d_vel, 0.0], [0.0, oe_m_vel, 0.0]
r_d, r_m, v_d, v_m = [], [], [], []

# Initialise DCMs and angular velocities for LMO hill-frame, mothership
# hill-frame, LMO nadir-pointing frame, and the LMO sun-pointing frame, and
# the intersat comms frame, all with respect to the inertial frame with n2^
# pointing to the sun, and n3^ being the northern pole axis of Mars.
DCM_HN_ARR, DCM_MN_ARR, DCM_RN_ARR, DCM_SN_ARR, DCM_CN_ARR = [], [], [], [], []
OHM_HN_ARR, OHM_RN_ARR, OHM_MN_ARR, OHM_SN_ARR, OHM_CN_ARR = [], [], [], [], []
DCM_CN_PREV = np.zeros((3,3))

# Initialise some DCMs that don't change over time.
DCM_SN = np.array([[-1, 0, 0], [0, 0, 1], [0, 1, 0]]) # Sun-pointing from N
DCM_RH = np.array([[-1, 0, 0], [0, 1, 0], [0, 0, -1]]) # Nadir-pointing from H

# Finally, initialise the DCMs and angular velocities for relative pointing.
DCM_MH_ARR = []
OHM_MH_ARR = []

print("Orbit propagation for simple two-body circular orbit...")
print("Plus the generation of all the DCMs at every time step. \n")
for t in range(0,len(tx)):
    
    # Get the orbit position and velocity vectors.
    r_d.append(np.matmul(np.transpose(DCM_HN),r_di))
    r_m.append(np.matmul(np.transpose(DCM_MN),r_mi))
    v_d.append(np.matmul(np.transpose(DCM_HN),v_di))
    v_m.append(np.matmul(np.transpose(DCM_MN),v_mi))
    
    # Get the relative positions and ISC pointing frame.
    dr = r_m[-1] - r_d[-1]
    ds = np.cross( dr, [0,0,1] )
    c1 = -1* dr / np.linalg.norm(dr) # first axis in comms pointing frame wrt N
    c2 = ds / np.linalg.norm(ds) # second axis in comms pointing frame wrt N
    c3 = np.cross(c1, c2) # third axis in comms pointing frame wrt N
    
    # Update the relative pointing DCM
    DCM_CN = np.array([c1,c2,c3])
    if t == 0:
        DCM_CN_PREV = DCM_CN
    DCM_CN_DOT = (DCM_CN - DCM_CN_PREV) / ts
    
    # Get Omega_CN in Tilde form.
    # I found out that the correct equation was Wtilde = -RN(t).T * d(RN)/dt
    # and not -d(RN)/dt * RN(t).T, but I don't understand why. 
    OHM_CN_TILDE = np.matmul(-1*np.transpose(DCM_CN), DCM_CN_DOT)
    
    # Record the current CN frame DCM for the next loop to refer to.
    #DCM_CN_PREV = np.matmul(DCM_CH, DCM_HN)
    DCM_CN_PREV = np.array([c1,c2,c3])
    
    # Update the DCMs
    DCM_CN_ARR.append(DCM_CN)
    DCM_HN_ARR.append(DCM_HN)
    DCM_MN_ARR.append(DCM_MN)
    DCM_SN_ARR.append(DCM_SN)
    DCM_RN_ARR.append(np.matmul(DCM_RH,DCM_HN))
    DCM_MH_ARR.append(np.matmul(DCM_MN,np.transpose(DCM_HN)))
        
    # Now settle all angular velocities
    OHM_HN = np.matmul(np.transpose(DCM_HN_ARR[t]),[0.0, 0.0, n_d]) # N-frame
    OHM_MN = np.matmul(np.transpose(DCM_MN_ARR[t]),[0.0, 0.0, n_m]) # N-frame
    OHM_HN_ARR.append(OHM_HN)
    OHM_MN_ARR.append(OHM_MN)
    OHM_SN_ARR.append(np.array([0.0, 0.0, 0.0]))
    
    OHM_RN_ARR.append(OHM_HN)
    
    OHM_MH_ARR.append(OHM_MN - OHM_HN)
    OHM_CN_ARR.append(np.array([OHM_CN_TILDE[2][1],
                                OHM_CN_TILDE[0][2],
                                OHM_CN_TILDE[1][0]]))
    
    
    # Note, the angular velocity OHM_RN = OHM_HN, because the nadir pointing
    # frame is actually a fixed rotation from the standard hill frame. Also,
    # if you want to prove this by DCM multiplication, DCM_NH * [0.0,0.0,n_d]
    # = DCM_NR * [0.0,0.0,-n_d]. The same answer will be obtained.

    # Update the DCM steps
    DCM_HN = np.matmul(DCM_HN_STEP,DCM_HN)
    DCM_MN = np.matmul(DCM_MN_STEP,DCM_MN)
    
    # Uncomment below to debug some things...
    # if tx[t] == 330.0:
    #     # Below printing to check pos-vel vectors match the hill-frame.
    #     h1 = r_d[-1]/np.linalg.norm(r_d[-1])
    #     h3 = np.cross(r_d[-1],v_d[-1]) / np.linalg.norm(np.cross(r_d[-1],v_d[-1]))
    #     h2 = np.cross(h3,h1)
    #     print(DCM_HN)
    #     print(np.array([h1,h2,h3]))
    #     # print('')
    #     # print(r_d[t])
    #     # print(v_d[t])
    #     # print(r_m[t])
    #     # print(v_m[t])

# Generate the reference angular velocity vectors.
# OHM_HN = dcm.get_313w([oe_d_ran, oe_d_inc, oe_d_lat], [0.0, 0.0, n_d])
# OHM_nN = OHM_HN
# OHM_SN = [0.0, 0.0, 0.0]
# print(OHM_HN)

# Update first entry of OHM_CN_ARR
OHM_CN_ARR[0] = OHM_CN_ARR[1]

""" .........................................................................
# Setup the moving reference attitude coordinates functions.
# The reference attitudes and omegas are 2D arrays as we will compute how
# they move as a function of time first, before we enter the numerical
# integrator loop, whereby we will then reference them later on.
.......................................................................... """

# User-defined control law function here:
def control(K, P, MRP_BR, OHM_BR):
    
    # Return a simplified PD control law with only velocity and position gain.
    return (-K*MRP_BR) - ( P * OHM_BR)
    
''' End of user defined inputs. '''

##############################################################################
## SETTING UP OF NUMERICAL INTEGRATOR IS WRITTEN HERE ..................... ##
##############################################################################

# Let us now initialise the MRPs of the body-to-reference for plotting.
MRP_BR_1_PLT, MRP_BR_2_PLT, MRP_BR_3_PLT, MRP_BR_N_PLT = [], [], [], []

# Similarly, arrays for plotting the omegas.
OHM_BR_1_PLT, OHM_BR_2_PLT, OHM_BR_3_PLT = [], [], []

# Similarly, arrays for plotting control torques
U_1_PLT, U_2_PLT, U_3_PLT, U_N_PLT = [], [], [], []

# Finally, we initialise an Omega-BR value.
OHM_BR = np.zeros(3)

##############################################################################
## MAIN LOOP FOR NUMERICAL INTEGRATOR IS WRITTEN HERE ..................... ##
## This loop, unlike the first control numerical integrator with MRPs,      ##
## updates both the MRP BN and MRP BR components.                           ##
##############################################################################

print("Now running attitude control simulation.")

attitude_tasks = ["sun", "nadir", "isc"]
# time_prints = [300.0, 2100.0, 3400.0, 4400.0, 5600.0]
time_prints = [15, 100, 200, 400]

for t in range(0,len(tx)):
    
    # Assign the current attitude task in this leg of the loop.
    # First, check if the satellite is facing the sun by checking the sign
    # of the n2^ direction. 
    pd, pm = r_d[t], r_m[t]
    pdn, pmn = np.linalg.norm(pd), np.linalg.norm(pm)
    isc_angle = np.rad2deg(np.arccos(np.dot(pd,pm)/(pdn*pmn)))
    if pd[1] >= 0.0:
        task = attitude_tasks[0]
    elif isc_angle <= 35.0:
        task = attitude_tasks[2]
    else:
        task = attitude_tasks[1]
    task = attitude_tasks[2]
    
    # MRP_BN and OHM_BN should have been updated at the end of the last loop.
    # We now update the following reference attitudes and rates. But first,
    # we need to checkout what is the current reference it should point to.
    if task == "sun":
        MRP_RN = mrp.dcm2mrp(DCM_SN_ARR[t])
        DCM_RN = DCM_SN_ARR[t]
        OHM_RN = OHM_SN_ARR[t]
    elif task == "nadir":
        MRP_RN = mrp.dcm2mrp(DCM_RN_ARR[t])
        DCM_RN = DCM_RN_ARR[t]
        OHM_RN = OHM_RN_ARR[t]
    elif task == "isc":
        MRP_RN = mrp.dcm2mrp(DCM_CN_ARR[t])
        DCM_RN = DCM_CN_ARR[t]
        OHM_RN = OHM_CN_ARR[t]
    
    # Note that currently at this line, OHM_RN is seen in the R-frame.
    # We must convert it to be in the perspective of the B-frame later on.
    
    # Switch MRP_RN to the shadow set if necessary.
    if np.linalg.norm( MRP_RN ) >= 1:
        MRP_RN = mrp.mrp_s( MRP_RN ) # Switch to shadow set
    
    # Now, we need to initialise find the MRP of body-to-reference.
    DCM_BN = mrp.mrp2dcm(MRP_BN) # Get DCM equivalent of MRP_BN
    DCM_BR = np.matmul( DCM_BN, np.transpose(DCM_RN) ) # DCM of MRP_BR.
    MRP_BR = np.array(mrp.dcm2mrp( DCM_BR )) # Convert the DCM to MRP_BR.
    
    # Next, for OHM_RN, we will apply it in the control vector,
    # but it must be seen from the body from (since we use Euler's
    # rotational equations of motion which is a body-frame derivative).
    # The original OHM_RN is written in N frame coordinates.
    OHM_RN = np.matmul( DCM_BN, OHM_RN )
    OHM_BR = OHM_BN - OHM_RN # Update omega of body-to-reference (B-frame)
    
    # For debugging, print line below to check attitude and omega error.
    if tx[t] in time_prints:
        print('\n Printing attitude errors at time ' + str(tx[t]))
        print(MRP_BN)
    
    # Switch MRP_BR and its derivative to the shadow sets if necessary.
    if np.linalg.norm( MRP_BR ) >= 1:
        MRP_BR = mrp.mrp_s( MRP_BR ) # Switch to shadow set
    
    # Now that MRPs are updated, we can update them below:
    MRP_BR_1_PLT.append(MRP_BR[0]) # MRP 1st coordinate
    MRP_BR_2_PLT.append(MRP_BR[1]) # MRP 2nd coordinate
    MRP_BR_3_PLT.append(MRP_BR[2]) # MRP 3rd coordinate
    MRP_BR_N_PLT.append(np.linalg.norm(MRP_BR)) # MRP Error Norm
    
    # Now that OHMs are updated, we can also append them for plotting.
    OHM_BR_1_PLT.append(OHM_BR[0]) # Angular Velocity 1st-Axis
    OHM_BR_2_PLT.append(OHM_BR[1]) # Angular Velocity 2nd-Axis
    OHM_BR_3_PLT.append(OHM_BR[2]) # Angular Velocity 3rd-Axis
    
    # To find how OHM_BN changes, we substitute the control into Euler's EOM.
    # Note that Euler's EOM are valid only when the angular momentum vector
    # H and torque vector L are taken about the body center of mass.
    # Thus, for any components in the control law, such as angular velocities,
    # we must convert them into the body frame perspective using the DCMs.
    
    # Now, the control loop in most cases is usually only updates at a 1 Hz
    # rate even though in reality, the dynamics is updated an an infinitesimal
    # small rate. Here, we arbitrarily implement a 1 Hz control.
    if t % t_div == 0:
        U = control(K, P, MRP_BR, OHM_BR)
    #U = np.zeros(3)
    #U = np.array([0.01,-0.01,0.02])
    
    # Now that control torques are updated, we can update them below:
    U_1_PLT.append(U[0]) # MRP 1st coordinate
    U_2_PLT.append(U[1]) # MRP 2nd coordinate
    U_3_PLT.append(U[2]) # MRP 3rd coordinate
    U_N_PLT.append(np.linalg.norm(U)) # MRP Error Norm
    
    # Finally, we solve for OHM_BN_DOT using Euler's EOM.
    I_OHM      = np.matmul( I, OHM_BN ) # I multiplied by OHM
    I_inverse  = np.linalg.inv(I) # Inverse inertia tensor.
    OHM_BN_DOT = np.matmul( I_inverse, U - np.cross( OHM_BN, I_OHM ) )
    
    # We also need to figure out how MRP_BN changes.
    if np.linalg.norm( MRP_BN ) >= 1:
        MRP_BN = mrp.mrp_s( MRP_BN ) # MRP shadow set
        MRP_BN_DOT = mrp.mrp_diff_s( MRP_BN, OHM_BN ) # MRP shadow kinematics.
    else:
        MRP_BN_DOT = mrp.mrp_diff( MRP_BN, OHM_BN ) # MRP kinematic equation.
        
    # # We also need to figure out how MRP_BR changes.
    # if np.linalg.norm( MRP_BR ) >= 1:
    #     MRP_BR = mrp.mrp_s( MRP_BR ) # MRP shadow set
    #     MRP_BR_DOT = mrp.mrp_diff_s( MRP_BR, OHM_BR ) # MRP shadow kinematics.
    # else:
    #     MRP_BR_DOT = mrp.mrp_diff( MRP_BR, OHM_BR ) # MRP kinematic equation.
    
    # Now comes the actual "numerical integration". We simply multiple the
    # dots of OHM and MRP in the BN sense, by the time step.
    OHM_BN += ts * OHM_BN_DOT
    MRP_BN += ts * MRP_BN_DOT
    # MRP_BR += ts * MRP_BR_DOT
    
    # Then, the loop repeats itself with these updated values.
    
# Prepare the subplots object.
fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

# Plot the MRPs.
ax1.set_title('Plot of Modified Rodriques Parameters (Sigma-B/R)')
plt_m1 = ax1.plot(tx, MRP_BR_1_PLT, label = 'MRP-1')
plt_m2 = ax1.plot(tx, MRP_BR_2_PLT, label = 'MRP-2')
plt_m3 = ax1.plot(tx, MRP_BR_3_PLT, label = 'MRP-3')
plt_mn = ax1.plot(tx, MRP_BR_N_PLT, label = 'MRP-Norm')
legend_MRP = ax1.legend(loc = 'upper right')
ax1.grid()

# Plot the angular velocities.
ax2.set_title('Plot of Angular Velocities (B/R)')
plt_v1 = ax2.plot(tx,np.rad2deg(OHM_BR_1_PLT), label = 'Omega-1')
plt_v2 = ax2.plot(tx,np.rad2deg(OHM_BR_2_PLT), label = 'Omega-2')
plt_v3 = ax2.plot(tx,np.rad2deg(OHM_BR_3_PLT), label = 'Omega-3')
legend_VEL = ax2.legend(loc = 'upper right')
ax2.grid()

# Plot the control torque vectors
ax3.set_title('Plot of Control Torque Vectors (B/N)')
plt_u1 = ax3.plot(tx,U_1_PLT, label = 'Torque-1')
plt_u2 = ax3.plot(tx,U_2_PLT, label = 'Torque-2')
plt_u3 = ax3.plot(tx,U_3_PLT, label = 'Torque-3')
plt_un = ax3.plot(tx,U_N_PLT, label = 'Torque-Norm')
legend_VEL = ax3.legend(loc = 'upper right')
ax3.grid()