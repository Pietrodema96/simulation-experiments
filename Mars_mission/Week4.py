from tkinter import W
import numpy as np
import math as m
import numpy as np
from scipy.integrate import solve_ivp
from scipy.integrate import odeint


# DATA:
### ESSENTIAL INFO
R = 3396.19 # km
h = 400 # km
r_LMO = R + h # km
r_GMO = 20424.2 # km
mu = 42828.3 # km^3 /s^2
D2R = np.pi/180
RAAN_LMO = 20 # [deg]
RAAN_GMO = 0 # [deg]
INC_LMO = 30 # [deg]
INC_GMO = 0 # [deg]
theta0_LMO = 60 # [deg]
theta0_GMO = 250 # [deg]
rate_GMO = 0.0000709003 # np.sqrt(mu/(Br_GMO**3))  # rad/s
rate_LMO = 0.000884797 # np.sqrt(mu/(Br_LMO**3))    # rad/s
EA_LMO_0 = np.array([np.deg2rad(i) for i in [20, 30, 60]]).T

# LMO
w_BN_0 = np.array([1.0*D2R, 1.75*D2R, -2.2*D2R])
sigma_BN_0 = np.array( [0.3, -0.4, 0.5] )



def EAtoDCM(EA):
    s1 = np.sin(EA[0])
    c1 = np.cos(EA[0])
    s2 = np.sin(EA[1])
    c2 = np.cos(EA[1])
    s3 = np.sin(EA[2])
    c3 = np.cos(EA[2])
    
    DCM = np.array([[c3*c1-s3*c2*s1, c3*s1+s3*c2*c1, s3*s2], 
                    [-s3*c1-c3*c2*s1, -s3*s1+c3*c2*c1, c3*s2], 
                    [s2*s1, -s2*c1, c2]])
    return DCM

M1 = lambda th: [[1,0,0],
                 [0,m.cos(th),m.sin(th)],
                 [0,-m.sin(th),m.cos(th)]]
M3 = lambda th: [[m.cos(th),m.sin(th),0],
                 [-m.sin(th),m.cos(th),0],
                 [0,0,1]]

def MM1(th):
    MM = np.zeros((3,3))
    MM[0,0] = 1 ; MM[0,1] = 0 ; MM[0,2] = 0
    MM[1,0] = 0 ; MM[1,1] = m.cos(th) ; MM[1,2] = m.sin(th)
    MM[2,0] = 0 ; MM[2,1] = -m.sin(th) ; MM[2,2] = m.cos(th)
    return MM

def MM2(th):
    MM = np.zeros((3,3))
    MM[0,0] = m.cos(th) ; MM[0,1] = 0 ; MM[0,2] = -m.sin(th)
    MM[1,0] =0 ; MM[1,1] = 1 ; MM[1,2] = 0
    MM[2,0] = m.sin(th) ; MM[2,1] = 0 ; MM[2,2] = m.cos(th)
    return MM

def MM3(th):
    MM = np.zeros((3,3))
    MM[0,0] = m.cos(th) ; MM[0,1] = m.sin(th) ; MM[0,2] = 0
    MM[1,0] = -m.sin(th) ; MM[1,1] = m.cos(th) ; MM[1,2] = 0
    MM[2,0] = 0 ; MM[2,1] = 0 ; MM[2,2] = 1
    return MM


def mrp_to_rotM(s):
    M = np.zeros((3,3))
    s_norm = np.linalg.norm(s)
    DD = 1/(1+s_norm**2)**2
    M[0,0] = (4*(s[0]**2 - s[1]**2 -s[2]**2)+(1-s_norm**2)**2 )  *DD
    M[1,0] = (8*s[1]*s[0]-4*s[2]*(1-s_norm**2)) *DD
    M[2,0] = (8*s[2]*s[0] + 4*s[1]*(1-s_norm**2)) * DD
    
    M[0,1] = (8*s[0]*s[1] + 4*s[2]*(1-s_norm**2)) * DD
    M[1,1] = (4*(-s[0]**2 + s[1]**2 -s[2]**2)+(1-s_norm**2)**2 )  *DD
    M[2,1] = (8*s[2]*s[1]-4*s[0]*(1-s_norm**2)) *DD
    
    M[0,2] = (8*s[0]*s[2]-4*s[1]*(1-s_norm**2)) *DD
    M[1,2] = (8*s[1]*s[2]+4*s[0]*(1-s_norm**2)) *DD
    M[2,2] = (4*(-s[0]**2 - s[1]**2 +s[2]**2)+(1-s_norm**2)**2 )  *DD

    return M


def rot_to_mrp(matrix):
    zeta = np.sqrt(np.trace(matrix) + 1)
    constant = 1 / (zeta**2 + 2 * zeta)
    s1 = constant * (matrix[1, 2] - matrix[2, 1])
    s2 = constant * (matrix[2, 0] - matrix[0, 2])
    s3 = constant * (matrix[0, 1] - matrix[1, 0])
    return np.array([s1, s2, s3])    

#################################
# TASK-6: sigma and omega 
#################################
# This is for LEO orbit

# SUN POINTING:
RsN = np.matmul(MM1(90*D2R), MM3(180*D2R))
Nw_RN = np.dot(RsN.T, np.array([0,0,0]))
BN = mrp_to_rotM(sigma_BN_0)
BR = np.dot(BN, RsN.T)
sigma_br = rot_to_mrp(BR)
Bw_rn = np.dot(BN, Nw_RN)
Bw_br = w_BN_0 - Bw_rn

# print(sigma_br)
# print(Bw_br)

# NADIR POINTING
EA_LMO_2 = np.array([np.deg2rad(i) for i in [20, 30, 60]]).T
DC = EAtoDCM(EA_LMO_2)
RnN = np.matmul(MM2(180*D2R),DC)
w_HN = np.array([0, 0, -rate_LMO])
Nw_RN = np.dot(RnN.T, w_HN)
BN = mrp_to_rotM(sigma_BN_0)
BR = np.dot(BN, RnN.T)
sigma_br = rot_to_mrp(BR)
Bw_rn = np.dot(BN, Nw_RN)
Bw_br = w_BN_0 - Bw_rn

# print(sigma_br)
# print(Bw_br)


# GEO POINTING
def RcNmat(r_LMO_nframe, r_GMO_nframe):
    del_r = r_GMO_nframe - r_LMO_nframe
    
    rc_1 = -del_r / np.linalg.norm(del_r)
    del_rXn3 = np.cross(del_r,np.array([0, 0, 1]).T)
    rc_2 = del_rXn3 / np.linalg.norm(del_rXn3)
    rc_3 = np.cross(rc_1,rc_2)
    
    return np.array([rc_1.T, rc_2.T, rc_3.T])
    
# Calculate dRcN 
tt = 0
## LMO
EA_LMO_0 = np.array([np.deg2rad(i) for i in [20, 30, 60+rate_LMO/D2R*tt]]).T
Br_LMO_0 = np.array([r_LMO, 0, 0]).T
Nr_LMO_0 = np.dot(EAtoDCM(EA_LMO_0).T, Br_LMO_0)

## GMO
EA_GMO_0 = np.array([np.deg2rad(i) for i in [0, 0, 250+rate_GMO/D2R*tt]]).T
Br_GMO_0 = np.array([r_GMO, 0, 0]).T
Nr_GMO_0 = np.dot(EAtoDCM(EA_GMO_0).T, Br_GMO_0)

dRcN_0 = RcNmat(Nr_LMO_0, Nr_GMO_0)
 
tt1 = 0.0001
## LMO
EA_LMO = np.array([np.deg2rad(i) for i in [20, 30, 60+rate_LMO/D2R*tt1]]).T
Br_LMO = np.array([r_LMO, 0, 0]).T
Nr_LMO = np.dot(EAtoDCM(EA_LMO).T, Br_LMO)
## GMO
EA_GMO = np.array([np.deg2rad(i) for i in [0, 0, 250+rate_GMO/D2R*tt1]]).T
Br_GMO = np.array([r_GMO, 0, 0]).T
Nr_GMO = np.dot(EAtoDCM(EA_GMO).T, Br_GMO)
dRcN = RcNmat(Nr_LMO, Nr_GMO)

# DCM_CN_dot
t_diff = tt1-tt
DCM_CN_DOT = (dRcN-dRcN_0)/t_diff
OHM_CN_TILDE = np.matmul(-1*np.transpose(dRcN), DCM_CN_DOT)
omega = [OHM_CN_TILDE[2,1], OHM_CN_TILDE[0,2], OHM_CN_TILDE[1,0]]

Nw_RN = np.dot(dRcN.T, omega)
BN = mrp_to_rotM(sigma_BN_0)
BR = np.dot(BN, dRcN.T)
sigma_br = rot_to_mrp(BR)
Bw_rn = np.dot(BN, Nw_RN)
Bw_br = w_BN_0 - Bw_rn

# print(sigma_br)
# print(Bw_br)


#################################
# TASK-7: Numerical attitude simulation
#################################
w_BN_0 = np.array([1.0*D2R, 1.75*D2R, -2.2*D2R])
sigma_BN_0 = np.array( [0.3, -0.4, 0.5] )

def tilde(x):
    x = np.squeeze(x)
    return np.array([[0, -x[2], x[1]],
                     [x[2], 0, -x[0]],
                     [-x[1], x[0], 0]
                     ])
    
I = [ [10.0,0.0,0.0], 
      [0.0,5.0,0.0], 
      [0.0,0.0,7.5] ]

# u = 0
u = [0,0,0]
t_span = np.arange(0, 501, 1)

# def OEM(w, t):
#     I = [ [10.0,0.0,0.0], 
#       [0.0,5.0,0.0], 
#       [0.0,0.0,7.5] ]
#     u = 0
#     w1 = w[0]
#     w2 = w[1]
#     w3 = w[2]
#     w_tilde = tilde(w)
#     I_inv = np.linalg.inv(I)
#     [dw1, dw2, dw3] =  np.dot(I_inv,np.dot(np.dot(-w_tilde, I), [w1, w2, w3]) + u)
    
#     return [dw1, dw2, dw3]

init_state = np.array([w_BN_0[0], w_BN_0[1], w_BN_0[2]])
init_state1 = np.array([sigma_BN_0[0], sigma_BN_0[1], sigma_BN_0[2]])

def sig_tilde(s):
    C = mrp_to_rotM(s)
    eps = np.sqrt(np.trace(C)+1)
    s_tilde = (C.T - C) / (eps*(eps+2))
    
    return s_tilde
    
    
def OEM1(t, w):
    I = np.array([ [10.0,0.0,0.0], 
        [0.0,5.0,0.0], 
        [0.0,0.0,7.5] ])
    u = np.array([0.01, -0.01, 0.02])
    w1 = w[0]
    w2 = w[1]
    w3 = w[2]
    w_tilde = tilde([w1, w2, w3])
    I_inv = np.linalg.inv(I)
    
    [dy1, dy2, dy3] =  np.dot(I_inv,np.dot(np.dot(-w_tilde, I), [w1, w2, w3]) + u)
    
    return [dy1, dy2, dy3]



# Integrating with py.odeint
# sol = odeint(OEM, init_state, t_span, mxstep=500)
sol = solve_ivp(OEM1, [0, 730], y0=init_state, method='RK45', t_eval=np.arange(0, 501, 1))

# times of integration
# print(np.arange(0, 501, 1))

w_BN_500 = sol.y[0:3, -1]
### provide H = I w_BN (@ 500s):
H = np.dot(I, w_BN_500)
print('H @500s : {}'.format(H))

### provide T @500s:
T = 0.5* np.dot(w_BN_500.T, np.dot(I, w_BN_500))
print('T @500s : {}'.format(T))



### provide sigma_BN @500s:
def w_t(t, soll):
    
    ind = round(t)
    return np.array(soll[:, ind])
    

def OEM2(t, w, f, soll):
    I = np.array([ [10.0,0.0,0.0], 
        [0.0,5.0,0.0], 
        [0.0,0.0,7.5] ])
    sigma = w
    s_tilde = sig_tilde(sigma)
        
    [dy4, dy5, dy6] = 0.25*np.dot( ( I*( 1-np.linalg.norm(sigma)**2 ) + 2*s_tilde +2*np.outer(sigma, sigma.T)), f(t, soll))
    
    return [dy4, dy5, dy6]

sol2 = solve_ivp(lambda t,w: OEM2(t, w, w_t, sol.y), [0, 500], y0=init_state1, method='RK45', t_eval=np.arange(0, 101, 1))
print(sol2.y[:,-1])




