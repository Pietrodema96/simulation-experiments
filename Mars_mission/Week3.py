import numpy as np
import math as m


# DATA:
### ESSENTIAL INFO
R = 3396.19 # km
h = 400 # km
r_LMO = R + h # km
r_GMO = 20424.2 # km
mu = 42828.3 # km^3 /s^2
D2R = np.pi/180
RAAN_LMO = 20 # [deg]
RAAN_GMO = 0 # [deg]
INC_LMO = 30 # [deg]
INC_GMO = 0 # [deg]
theta0_LMO = 60 # [deg]
theta0_GMO = 250 # [deg]
rate_GMO = 0.0000709003 # np.sqrt(mu/(Br_GMO**3))  # rad/s
rate_LMO = 0.000884797 # np.sqrt(mu/(Br_LMO**3))    # rad/s
EA_LMO_0 = np.array([np.deg2rad(i) for i in [20, 30, 60]]).T

# LMO
w_0 = np.array([1.0*D2R, 1.75*D2R, -2.2*D2R])


#################################
# TASK-3: ORBIT SUN POINTING
#################################
# This is for LEO orbit


def EAtoDCM(EA):
    s1 = np.sin(EA[0])
    c1 = np.cos(EA[0])
    s2 = np.sin(EA[1])
    c2 = np.cos(EA[1])
    s3 = np.sin(EA[2])
    c3 = np.cos(EA[2])
    
    DCM = np.array([[c3*c1-s3*c2*s1, c3*s1+s3*c2*c1, s3*s2], 
                    [-s3*c1-c3*c2*s1, -s3*s1+c3*c2*c1, c3*s2], 
                    [s2*s1, -s2*c1, c2]])
    return DCM

M1 = lambda th: [[1,0,0],
                 [0,m.cos(th),m.sin(th)],
                 [0,-m.sin(th),m.cos(th)]]
M3 = lambda th: [[m.cos(th),m.sin(th),0],
                 [-m.sin(th),m.cos(th),0],
                 [0,0,1]]

def MM1(th):
    MM = np.zeros((3,3))
    MM[0,0] = 1 ; MM[0,1] = 0 ; MM[0,2] = 0
    MM[1,0] = 0 ; MM[1,1] = m.cos(th) ; MM[1,2] = m.sin(th)
    MM[2,0] = 0 ; MM[2,1] = -m.sin(th) ; MM[2,2] = m.cos(th)
    return MM

def MM2(th):
    MM = np.zeros((3,3))
    MM[0,0] = m.cos(th) ; MM[0,1] = 0 ; MM[0,2] = -m.sin(th)
    MM[1,0] =0 ; MM[1,1] = 1 ; MM[1,2] = 0
    MM[2,0] = m.sin(th) ; MM[2,1] = 0 ; MM[2,2] = m.cos(th)
    return MM

def MM3(th):
    MM = np.zeros((3,3))
    MM[0,0] = m.cos(th) ; MM[0,1] = m.sin(th) ; MM[0,2] = 0
    MM[1,0] = -m.sin(th) ; MM[1,1] = m.cos(th) ; MM[1,2] = 0
    MM[2,0] = 0 ; MM[2,1] = 0 ; MM[2,2] = 1
    return MM
   
# RsN = np.matmul(MM1(90*D2R), MM3(180*D2R))
# print(RsN)
# w_RN = [ 0, 0, 0]
# print(w_RN)




#################################
# TASK-4: NADIR POINTING
#################################
# This is for LEO orbit

EA_LMO_2 = np.array([np.deg2rad(i) for i in [20, 30, 60+rate_LMO/D2R*330]]).T

DC = EAtoDCM(EA_LMO_2)

theta = rate_LMO*330
RnN = np.matmul(MM2(180*D2R),DC)

# print(RnN)

w_HN = np.array([0, 0, -rate_LMO])
w_RN = np.dot(RnN.T, w_HN)

# print(w_RN)


#################################
# TASK-5: GMO POINTING
#################################
# This is for LEO orbit


def RcNmat(r_LMO_nframe, r_GMO_nframe):
    del_r = r_GMO_nframe - r_LMO_nframe
    
    rc_1 = -del_r / np.linalg.norm(del_r)
    del_rXn3 = np.cross(del_r,np.array([0, 0, 1]).T)
    rc_2 = del_rXn3 / np.linalg.norm(del_rXn3)
    rc_3 = np.cross(rc_1,rc_2)
    
    return np.array([rc_1.T, rc_2.T, rc_3.T])
    
# Calculate dRcN 
tt = 329.99
## LMO
EA_LMO_0 = np.array([np.deg2rad(i) for i in [20, 30, 60+rate_LMO/D2R*tt]]).T
Br_LMO_0 = np.array([r_LMO, 0, 0]).T
Nr_LMO_0 = np.dot(EAtoDCM(EA_LMO_0).T, Br_LMO_0)

## GMO
EA_GMO_0 = np.array([np.deg2rad(i) for i in [0, 0, 250+rate_GMO/D2R*tt]]).T
Br_GMO_0 = np.array([r_GMO, 0, 0]).T
Nr_GMO_0 = np.dot(EAtoDCM(EA_GMO_0).T, Br_GMO_0)

dRcN_0 = RcNmat(Nr_LMO_0, Nr_GMO_0)
 
tt1 = 330
## LMO
EA_LMO = np.array([np.deg2rad(i) for i in [20, 30, 60+rate_LMO/D2R*tt1]]).T
Br_LMO = np.array([r_LMO, 0, 0]).T
Nr_LMO = np.dot(EAtoDCM(EA_LMO).T, Br_LMO)

## GMO
EA_GMO = np.array([np.deg2rad(i) for i in [0, 0, 250+rate_GMO/D2R*tt1]]).T
Br_GMO = np.array([r_GMO, 0, 0]).T
Nr_GMO = np.dot(EAtoDCM(EA_GMO).T, Br_GMO)

dRcN = RcNmat(Nr_LMO, Nr_GMO)
print(dRcN)

# DCM_CN_dot
t_diff = tt1-tt
DCM_CN_DOT = (dRcN-dRcN_0)/t_diff
OHM_CN_TILDE = np.matmul(-1*np.transpose(dRcN), DCM_CN_DOT)

omega = [OHM_CN_TILDE[2,1], OHM_CN_TILDE[0,2], OHM_CN_TILDE[1,0]]
print(OHM_CN_TILDE)
print(omega)



