import numpy as np
import math as m


# DATA:
### ESSENTIAL INFO
R = 3396.19 # km
h = 400 # km
r_LMO = R + h # km
r_GMO = 20424.2 # km
mu = 42828.3 # km^3 /s^2
D2R = np.pi/180
RAAN_LMO = 20 # [deg]
RAAN_GMO = 0 # [deg]
INC_LMO = 30 # [deg]
INC_GMO = 0 # [deg]
theta0_LMO = 60 # [deg]
theta0_GMO = 250 # [deg]
rate_GMO = 0.0000709003 # np.sqrt(mu/(Br_GMO**3))  # rad/s
rate_LMO = 0.000884797 # np.sqrt(mu/(Br_LMO**3))    # rad/s



#########################
# TASK-1: ORBIT SIMULATION
#########################

## pv = r * ir = r * DCM * ir
## dpv = wbn X r = DCM * dtheta X r * ir

def EAtoDCM(EA):
    s1 = np.sin(EA[0])
    c1 = np.cos(EA[0])
    s2 = np.sin(EA[1])
    c2 = np.cos(EA[1])
    s3 = np.sin(EA[2])
    c3 = np.cos(EA[2])
    
    DCM = np.array([[c3*c1-s3*c2*s1, c3*s1+s3*c2*c1, s3*s2], 
                    [-s3*c1-c3*c2*s1, -s3*s1+c3*c2*c1, c3*s2], 
                    [s2*s1, -s2*c1, c2]])
    return DCM

## LMO
EA_LMO_0 = np.array([np.deg2rad(i) for i in [20, 30, 60]]).T
dEA_LMO = np.array([0, 0, rate_LMO]).T
Br_LMO_0 = np.array([r_LMO, 0, 0]).T
Nr_LMO_0 = np.dot(EAtoDCM(EA_LMO_0).T, Br_LMO_0)

## GMO
EA_GMO_0 = np.array([np.deg2rad(i) for i in [0, 0, 250]]).T
dEA_GMO = np.array([0, 0, rate_GMO]).T
Br_GMO_0 = np.array([r_GMO, 0, 0]).T
Nr_GMO_0 = np.dot(EAtoDCM(EA_GMO_0).T, Br_GMO_0)

# dtt = 0.001
# time = 1200
# tvec = np.linspace(0, time, int(time/dtt + 1))
# prev_t = 0
# for ti in tvec[0:]:
#     dt = ti - prev_t
#     prev_t = ti
#     EA_LMO_1 = EA_LMO_0 + dEA_LMO * dt
#     EA_GMO_1 = EA_GMO_0 + dEA_GMO * dt
    
#     Nr_LMO = np.dot(EAtoDCM(EA_LMO_1).T, Br_LMO_0)
#     Nr_GMO = np.dot(EAtoDCM(EA_GMO_1).T, Br_GMO_0)
#     rdot_LMO = np.dot(EAtoDCM(EA_LMO_1).T, [0, np.linalg.norm(Br_LMO_0)*rate_LMO, 0])
#     rdot_GMO = np.dot(EAtoDCM(EA_GMO_1).T, [0, np.linalg.norm(Br_GMO_0)*rate_GMO, 0])



#     if ti%25 == 0:
#         print("Simulated {} seconds".format(ti))

#     ### TASK 2
#     if ti == 300:
#         print(EAtoDCM(EA_LMO_1))
        
#     ### TASK 1
#     if ti == 450:
#         print("pv LMO")
#         print(Nr_LMO)
#         print("dpv LMO")
#         print(rdot_LMO)
#     if ti == 1150:
#         print("pv GMO")
#         print(Nr_GMO)
#         print("dpv GMO")
#         print(rdot_GMO)
        
#     EA_LMO_0 = EA_LMO_1
#     EA_GMO_0 = EA_GMO_1

    
    
#################################
# TASK-2: ORBIT FRAME ORIENTATION
#################################
# This is for LEO orbit

tt = 300 # [s]

EA_LMO_2 = np.array([np.deg2rad(i) for i in [20, 30, 60+rate_LMO/D2R*tt]]).T

DC = EAtoDCM(EA_LMO_2)
    
print(DC)
